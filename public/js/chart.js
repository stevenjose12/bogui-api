var labels = [
	{
      name: 'Lun',
      ganancias: {
        credito: 0,
        efectivo: 0,
        codigo: 0
      },
    },
    {
      name: 'Mar',
      ganancias: {
        credito: 0,
        efectivo: 0,
        codigo: 0
      },
    },
    {
      name: 'Mié',
      ganancias: {
        credito: 0,
        efectivo: 0,
        codigo: 0
      },
    },
    {
      name: 'Jue',
      ganancias: {
        credito: 0,
        efectivo: 0,
        codigo: 0
      },
    },
    {
      name: 'Vie',
      ganancias: {
        credito: 0,
        efectivo: 0,
        codigo: 0
      },
    },
    {
      name: 'Sáb',
      ganancias: {
        credito: 0,
        efectivo: 0,
        codigo: 0
      },
    },
    {
      name: 'Dom',
      ganancias: {
      credito: 0,
      efectivo: 0,
      codigo: 0
    },
  }
]

function load() {
	new Chart(document.getElementById("chart"), {
	    type: 'bar',
	    data: {
	      labels: labels.map(i => i.name),
	      datasets: [{
				backgroundColor: '#F70000',
				borderColor: '#F70000',
				borderWidth: 1,
				data: [
					labels[0].ganancias.credito,
					labels[1].ganancias.credito,
					labels[2].ganancias.credito,
					labels[3].ganancias.credito,
					labels[4].ganancias.credito,
					labels[5].ganancias.credito,
					labels[6].ganancias.credito,
				]
			}, {
				backgroundColor: '#00135B',
				borderColor: '#00135B',
				borderWidth: 1,
				data: [
					labels[0].ganancias.efectivo,
					labels[1].ganancias.efectivo,
					labels[2].ganancias.efectivo,
					labels[3].ganancias.efectivo,
					labels[4].ganancias.efectivo,
					labels[5].ganancias.efectivo,
					labels[6].ganancias.efectivo,
				]
			},{
	            backgroundColor: '#2c72b6',
	            borderColor: '#2c72b6',
	            borderWidth: 1,
	            data: [
	              labels[0].ganancias.codigo,
	              labels[1].ganancias.codigo,
	              labels[2].ganancias.codigo,
	              labels[3].ganancias.codigo,
	              labels[4].ganancias.codigo,
	              labels[5].ganancias.codigo,
	              labels[6].ganancias.codigo,
	            ]
	          }]
	    },
	    options: {
	         legend: {
	            display: false
	         },
	         tooltips: {
	            enabled: false
	         },
	         scales: {
	              xAxes: [{gridLines: { color: "#fff" }}],
	              yAxes: [{gridLines: { color: "#fff" }, ticks: {
		                display: false
		          }}]
	         }
	    }
	});
}