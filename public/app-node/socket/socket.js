'use strict'

const express = require('express')
const siofu = require("socketio-file-upload")
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server,{
    pingInterval: 2500,
    pingTimeout: 8000
})
const path = require('path');
const userCtrl = require('../controllers/UserController')
const msgCtrl = require('../controllers/MessagesController')
const raceCtrl = require('../controllers/RacesController')
const notificationCtrl = require('../controllers/NotificationController')
const RefreshTokenController = require('../controllers/RefreshTokenController');
const Route = require("../models/Route");
// const blackListCtrl = require('../controllers/BlackListController')
const moment = require('moment')

const httpsapp = express()
const fs = require('fs')
const sslPath = path.join(__dirname)

const options = {
    key: fs.readFileSync(sslPath+'/certs/bogui.key').toString(),
    cert: fs.readFileSync(sslPath+'/certs/bogui.crt').toString(),
    ca: fs.readFileSync(sslPath+'/certs/ca-bundle-bogui.crt').toString(),
    requestCert: false,
    rejectUnauthorized: false,
    ejectUnauthorized : true
}

const https = require('https').createServer(options, httpsapp)

io.attach(server);
io.attach(https);

siofu.listen(app);

var users_connected = []
var array_carreras = new Array()

// const app = firebaseAdmin.initializeApp();

var interval = null;

io.on('connection', (socket) => {
    
    users_connected = userCtrl.addUserSocket(socket.id, users_connected)
    var uploader = new siofu()
    uploader.dir = path.join(__dirname, '../../chats')
    uploader.mode = "0777"
    uploader.listen(socket)

    // refresh token (start)

    socket.on('refresh-token', RefreshTokenController.refreshToken);

    socket.on('firebase', data => RefreshTokenController.sendMessage('firebase', data) );

    // refresh token (end)

    // Actualizar la posicion de los usuarios
    socket.on('position', (_data) => {

        let data = _data;
        try {
            data = JSON.parse(_data);
        } catch (e) {

        }

        if (typeof data.user != 'undefined' && data.user != null) {
            users_connected = userCtrl.setLocationSocket(socket.id, data, users_connected)
            io.emit('driver-position', data);
        }
    })

    socket.on('position_emergency', data => {
        console.log(">>: position_emergency: ", data);
        io.emit('position_emergency', data);
    });

    socket.on('driver-panic', data => console.log('Socket: on driver-panic:', data) || io.emit('driver-panic', data));

    socket.on('actualizar-id', data => {
        userCtrl.setIdSocket(socket.id, data, users_connected);
        userCtrl.updateAlive(data.id, 1);
    });

    socket.on('usuario-desactivado', data => {
        io.emit('usuario-desactivado', data);
    });

    socket.on('usuario-activado', data => {
        io.emit('usuario-activado', data);
    });

    socket.on('usuario-validated', data => {
        // console.log('>>: usuario-validated: ', data);
        io.emit('usuario-validated', data);
    });

    socket.on('location',data => {
        io.emit('location',data)
    })

    socket.on('test', d => {
        // console.log(">>: tset > d: ", d);
    })

    socket.on('update-driver', data => {
        io.emit('update-driver', data);
    });

    socket.on('pago-ruta', data => {
        io.emit('pago-ruta', data);
    })

    socket.on('webpay', data => {
        io.emit('webpay', data);
    })

    socket.on('webpay-inscripcion', data => {
        // console.log('webpay-inscripcion',data);
        io.emit('webpay-inscripcion', data);
    })

    socket.on('carrera-aceptada', data => {
        // console.log(">>: carrera-aceptada: ", data);
        raceCtrl.raceAccepted(data.id, array_carreras);
        io.emit('carrera-aceptada', data);
        io.emit('new-notif', data);
    })

    socket.on('cancelada-conductor', data => {
        io.emit('cancelada-conductor', data)
    })

    socket.on('llamada-no-disponible', data => {
        io.emit('llamada-no-disponible', data)
    })

    socket.on('pasajero-abordo', data => {
        // console.log(">>: sin ssl pasajero-abordo: ", data);
        io.emit('pasajero-abordo', data)
    })

    socket.on('carrera-iniciada', data => {
        // console.log(">>: sin ssl carrera-iniciada: ", data);
        io.emit('carrera-iniciada', data)
    })

    socket.on('finalizar-carrera', data => {
        // console.log(">>: sin ssl finalizar-carrera: ", data);
        io.emit('finalizar-carrera', data)
    })

    socket.on('cancelada-cliente', data => {
        // console.log(">>: cancelada-cliente: ", data);
        io.emit('cancelada-cliente', data)
    })

    socket.on('refresh-page', data => {
        io.emit('refresh-page', data)
    })

    socket.on('iniciar-contador', data => {
        io.emit('iniciar-contador', data)
    })

    socket.on('detener-contador', data => {
        io.emit('detener-contador', data)
    })

    socket.on('ubicacion-pasajero', data => {
        io.emit('ubicacion-pasajero', data)
    })

    socket.on('update-floatwidget', data => {
        io.emit('update-floatwidget', data)
    })

    socket.on('notification', notification => {
        if (notification.type == 11) {
            io.emit('notification', notification);
            io.emit('notification-web', {type: 1, own: false});
            console.log('Emite', notification.id)
            return 
        }
        if (notification.type<9) RefreshTokenController.send('notification', notification, notification.user_id);

        let own = true;
        if (notification.type == '9') {
            own = false;
            notificationCtrl.createForAdmin(notification)
                .then( resp => {
                    io.emit('notification', resp);
                    io.emit('notification-web', {type: 1, own: own});
                })
                .catch( e => console.log(e)  )
        } else {
            notificationCtrl.create(notification)
                .then( resp => {
                    io.emit('notification', resp)
                    if (notification.type == '10') {
                        io.emit('notification-web', {type: 1, own: own});
                    }
                })
                .catch( e => console.log(e) )
        }
    });

    socket.on("carrera-finalizada", data => {
        // console.log(">>: sin ssl carrera-finalizada: ", data);
        io.emit("carrera-finalizada", data)
    })

    socket.on("login", data => {
        io.emit("login", data)
    })

    // Nueva Solicitudd de carrera
    socket.on('nueva-carrera', carrera => {
        raceCtrl.findById(carrera, array_carreras)
        io.emit('nueva-carrera', carrera)
    })

    socket.on('current-route-change', async object => {

        try {
            const route = await Route.findById( object.id );
            RefreshTokenController.send("current-route-change", object, route.driver_id);
        } catch (e) {
            console.log(">>: current-route-change > error: ", e);
        }
        io.emit('current-route-change',object);
    })

    socket.on('driver-deleted',(object) => {
        io.emit('driver-deleted',object)
    })

    socket.on('event-reject',(object) => {
        io.emit('event-reject', object)
    })

    socket.on('new-vehicle',(object) => {
        io.emit('new-vehicle',object)
    })

    socket.on("java", data => {
        console.log(">>: Hola java: ", data);
    })

    socket.on('llamada',data => {
        // console.log(">>: llamada: ", data);
        io.emit('llamada',data)
    })

    socket.on('llamada-colgada',data => {
        // console.log(">>: llamada-colgada: ", data);
        io.emit('llamada-colgada',data)
    })

    socket.on('carrera-rechazada', data => {
        raceCtrl.findRejects(data, (resp) => {
            if (resp >= 4) {
                io.emit('event-reject', {route: data.id});
            } else {
                io.emit('event-solicitude', data);
                const params = data;
                // try {
                //     params = {
                //         id: data.id,
                //         description: data.description,
                //         cost: data.cost,
                //         latitud_origin: data.latitud_origin,
                //         longitud_origin: data.longitud_origin,
                //         carrera: {
                //             id: data.carrera.id
                //         },
                //         to_user: {
                //             average: data.to_user.average,
                //             person: {
                //                 name: data.to_user.person.name,
                //                 lastname: data.to_user.person.lastname,
                //                 photo: data.to_user.person.photo,
                //                 phone: data.to_user.person.phone
                //             }
                //         },
                //         to_destinies: [],
                //         vehicle_type_id: data.vehicle_type_id
                //     };
                //
                //     for (let i=0; i<data.to_destinies.length; i++) {
                //         const item = data.to_destinies[i];
                //         params.to_destinies.push( {
                //             latitud: item.latitud,
                //             longitud: item.longitud,
                //             description: item.description
                //         } );
                //     }
                //
                // } catch (e) {
                //     console.log(">>: event-solicitude > error: ", e);
                //     params = data;
                // }
                // RefreshTokenController.send("event-solicitude", params, data.carrera.id);
            }
        })
        io.emit('carrera-rechazada', data)
    })

    socket.on('solicitude', data => io.emit('event-solicitude', data) );

    //Activacion de Estado del Conductor
    socket.on('activate-driver', data => io.emit('activate-driver', data) )

    // Envio de Mensajes
    socket.on('send-message', data => {
        // console.log(">>: send-message: ", data);
        msgCtrl.sendMessage( data, resp => {
            if (data.receiver_id) RefreshTokenController.send("send-message", data, data.receiver_id);
            io.emit("send-message", resp);
            if (resp.chat.support == '1') io.emit('notification-web', {type: 2, own: false} );
        });
    })

    // socket.on('admin-notification', data => {
    //     if (data.type != "") {
    //         if (data.type == '1') {
    //             userCtrl.getAll().then( resp => io.emit('admin-notification', {users: resp}) )
    //         } else {
    //             userCtrl.getAllByType(data.type).then( resp => io.emit('admin-notification', {users: resp}))
    //         }
    //     } else {
    //         io.emit('admin-notification', { users: data.users} )
    //     }
    // })

    socket.on('admin-notification', async data => {

        // console.log(">>: admin-notification: ", data);

        try {

            const payload = {
                users: data.users,
                type: data.type || 0
            };

            if (payload.type===0) {
                RefreshTokenController.sendMulticast('admin-notification', payload, payload.users);
            } else {
                // nota se pudiera mejorar esta logica. si se envia las notificaciones a los niveles de usuarios en
                // especifico.
                RefreshTokenController.senBroadcast('admin-notification', { type: data.type });
            }

            if (data.type != "") {
                if (data.type == '1') {
                    payload.users = await userCtrl.getAll();
                } else {
                    payload.users = await userCtrl.getAllByType(data.type);
                }
            }

            io.emit('admin-notification', { users: payload.users });

        } catch (e) {
            console.log(">>: admin-notification > error: ", e);
        }

    })

    uploader.on("saved", function (event) {
        // // console.log('Carga exitosa');
        // io.emit('upload-success')
    });

    uploader.on("error", function (event) {
        // // console.log("Error from uploader", event);
    });

    socket.on('getPendingTypeAct', (data) => {
        notificationCtrl.count(data).then(resp => {
            io.emit('getPendingTypeAct', resp)
        }).catch(err => {
            // console.log(err)
        })
    })

    // El usuariio cerro sesion
    socket.on('disconnecting', data => {
        if (data.user) {
            users_connected = userCtrl.deleteBySocket(socket.id, users_connected)
            io.emit('delete-marker', { user: data.user })
        }
    })

    // La socket se desconecto
    socket.on('disconnect', () => {
        try {
            const user = users_connected.find( item => item.socketId === socket.id );
            if (user) {
                userCtrl.updateAlive(user.userId, 0);
                io.emit('delete-marker', {user: user.userId});
                // io.emit('driver-deleted', {id: user.userId});
            }
        } catch (e) {
            console.log(">>: disconnect > error: ", e);
        }
    });

    socket.on('foreground', data => {
        io.emit('foreground', data);
    })

    // test
    // if (interval==null) interval = setInterval( () => {
    //     console.log(">>: emit test");
    //     try {
    //         io.emit('background', {test: 'test'});
    //     } catch (e) {
    //         console.log(">>: emit test error: ", e);
    //     }
    // }, 10000);
    // test

    socket.on(
      'driver-panic',
      data => console.log(data) || io.emit('driver-panic', data)
    );

    socket.on('driver-coming', data => {
        io.emit('driver-coming', data);
    });
})

app.get('/check-expired-documents',(req,res)=>{
    // console.log('revisa expirados')
    userCtrl.getAllDrivers().then(resp => {
        resp.forEach(res => {
            let today = moment();
            let exp_doc = null
            let exp_per = null
            let exp_cri = null
            let exp_lic = null
            let text = '';
            if (res.person.expiration_document != null) {
                exp_doc = moment(res.person.expiration_document,'YYYY-MM-DD');
            }
            if (res.person.expiration_permit != null) {
                exp_per = moment(res.person.expiration_permit,'YYYY-MM-DD');
            }
            if (res.person.expiration_criminal_records != null) {
                exp_cri = moment(res.person.expiration_criminal_records,'YYYY-MM-DD');
            }
            if (res.person.expiration_license != null) {
                exp_lic = moment(res.person.expiration_license,'YYYY-MM-DD');
            }
            if (exp_doc != null) {
                if (exp_doc.diff(today,'days') >= 0 && exp_doc.diff(today,'days') <= 3) {
                    text += 'El Documento (RUT o Pasaporte) esta por expirar, '
                }
            }
            if (exp_per != null) {
                if (exp_per.diff(today,'days') >= 0 && exp_per.diff(today,'days') <= 3) {
                    text += 'El Permiso de Circulación esta por expirar, '
                }
            }
            if (exp_cri != null) {
                if (exp_cri.diff(today,'days') >= 0 && exp_cri.diff(today,'days') <= 3) {
                    text += 'El Certificado de Antecedentes esta por expirar, '
                }
            }
            if (exp_lic != null) {
                if (exp_lic.diff(today,'days') >= 0 && exp_lic.diff(today,'days') <= 3) {
                    text += 'La Licencia de Conducir esta por expirar, '
                }
            }
            if (text != '') {
                let notification = {
                    type: 10,
                    description: text,
                    user_id: res.id
                }
                notificationCtrl.createForExpired(notification).then(resp => {
                    io.emit('notification',resp)
                    io.emit('notification-web', {type: 1, own: false})
                }).catch(err => {
                    // console.log(err)
                })
            }
        })
    })
        .catch(err => {
            // console.log(err)
        })
    res.send("Revisando documentos expirados")
})

httpsapp.get('*',(req, res) => {
    // console.log(req.url, 'http')
    res.redirect('https://bogui.cl:11021'+req.url)
})

app.get('/',(req,res) => {
    // console.log(req.url, 'https')
    res.redirect('https://bogui.cl')
})

module.exports = {
    server,
    https,
    io,
    httpsapp,
    app
}
