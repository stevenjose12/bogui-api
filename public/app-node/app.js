'use strict'

const express = require('express')
const bodyParser = require('body-parser')
// const socket = require('./socket/socket')
const socket = require('./socket/socket-sin-ssl')
// const app = socket.httpsapp
const server = socket.server
// const https = socket.https
const api = require('./routes/web')
const io = socket.io

// app.use(bodyParser.urlencoded({ extended: false }))
// app.use(bodyParser.json())
// app.use('/', api)

module.exports = {
  // app,
  server,
  // https
}
