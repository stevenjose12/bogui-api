'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')

const Person = sequelize.connections().define('persons', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: Sequelize.STRING,
        lastname: Sequelize.STRING,
        phone: Sequelize.STRING,
        document_type: Sequelize.STRING,
        document: Sequelize.STRING,
        document_photo: Sequelize.STRING,
        expiration_document: Sequelize.DATE,
        photo: Sequelize.STRING,
        permit: Sequelize.STRING,
        expiration_permit: Sequelize.DATE,
        criminal_records: Sequelize.STRING,
        expiration_criminal_records: Sequelize.DATE,
        license: Sequelize.STRING,
        expiration_license: Sequelize.DATE,
        user_id: Sequelize.INTEGER,
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'persons'
    }
)

module.exports = Person