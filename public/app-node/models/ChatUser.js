'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const User = require('./User')

const ChatUser = sequelize.connections().define('chat_user', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        chat_id: Sequelize.INTEGER,
        user_id: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'chat_user'
    }
)

ChatUser.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id'
});

module.exports = ChatUser