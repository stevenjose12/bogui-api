'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const VehiclePhoto = require('./VehiclePhoto')

const Vehicle = sequelize.connections().define('vehicles', {
		id: {
	        type: Sequelize.INTEGER,
	        primaryKey: true,
	        autoIncrement: true
	    },    
	    brand: Sequelize.STRING,
	    model: Sequelize.STRING,
	    plate: Sequelize.STRING,
	    year: Sequelize.INTEGER,
	    vehicle_type_id: Sequelize.INTEGER,
	    user_id: Sequelize.INTEGER,
	    status: { type: Sequelize.INTEGER, defaultValue: 1 }
	},{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'vehicles'
    }
)

Vehicle.hasMany(VehiclePhoto, {
    as: 'photos',
    foreignKey: 'vehicle_id'
});

module.exports = Vehicle