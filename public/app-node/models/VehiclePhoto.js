'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const Vehicle = require('./Vehicle')

const VehiclePhoto = sequelize.connections().define('vehicles_photos', {
		id: {
	        type: Sequelize.INTEGER,
	        primaryKey: true,
	        autoIncrement: true
	    },    
	    vehicle_id: Sequelize.INTEGER,
	    file: Sequelize.STRING
	},{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'vehicles_photos'
    }
)

module.exports = VehiclePhoto