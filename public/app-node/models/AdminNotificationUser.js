'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const User = require('./User')

const AdminNotificationUser = sequelize.connections().define('admin_notifications_users', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        notification_id: Sequelize.INTEGER,
        user_id: Sequelize.INTEGER,
        viewed: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'admin_notifications_users'
    }
)

AdminNotificationUser.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id',
    targetKey: 'id'
});

module.exports = AdminNotificationUser