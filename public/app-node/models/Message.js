'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const Chat = require('./Chat')
const User = require('./User')

const Message = sequelize.connections().define('messages', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    chat_id: Sequelize.INTEGER,
    user_id: Sequelize.INTEGER,
    text: Sequelize.TEXT,
    file: Sequelize.STRING,
    seen: Sequelize.ENUM('0', '1')
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    tableName: 'messages'
})

Message.belongsTo(Chat, {
    as: 'chat',
    foreignKey: 'chat_id'
});

Message.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id'
});

module.exports = Message