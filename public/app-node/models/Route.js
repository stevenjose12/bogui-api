'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')

const Route = sequelize.connections().define('routes', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: Sequelize.INTEGER,
        driver_id: Sequelize.INTEGER,
        latitud_origin: Sequelize.STRING,
        longitud_origin: Sequelize.STRING,
        status: Sequelize.INTEGER,
        status_route: Sequelize.INTEGER,
        date_driver: Sequelize.DATE,
        time: Sequelize.INTEGER,
        cost: Sequelize.DOUBLE,
        payment_id: Sequelize.INTEGER,
        payment: Sequelize.INTEGER,
        vehicle_type_id: Sequelize.INTEGER,
        tariff_id: Sequelize.INTEGER,
        special_tariff: Sequelize.INTEGER,
        canceled_by: Sequelize.INTEGER,
        time_stop: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'routes'
    }
)

module.exports = Route