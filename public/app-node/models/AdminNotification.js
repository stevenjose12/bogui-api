'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')

const AdminNotification = sequelize.connections().define('admin_notifications', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: Sequelize.STRING,
        description: Sequelize.STRING,
        file: Sequelize.STRING
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'admin_notifications'
    }
)

module.exports = AdminNotification