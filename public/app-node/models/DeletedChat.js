'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')

const DeletedChat = sequelize.connections().define('deleted_chats', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: Sequelize.INTEGER,
        chat_id: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        tableName: 'deleted_chats'
    }
)

module.exports = DeletedChat