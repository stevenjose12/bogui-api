'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')

const PendingDocs = sequelize.connections().define('pending_documents', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: Sequelize.INTEGER,
        vehicle_id: Sequelize.INTEGER,
        type: { type: Sequelize.INTEGER, defaultValue: 0 },
        status: Sequelize.INTEGER,
        comment: Sequelize.STRING,
        upload: { type: Sequelize.INTEGER, defaultValue: 0 }
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        tableName: 'pending_documents'
    }
)

module.exports = PendingDocs