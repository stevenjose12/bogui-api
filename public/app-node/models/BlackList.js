'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')

const BlackList = sequelize.connections().define('blacklist', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        route_id: Sequelize.INTEGER,
        driver_id: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'blacklist'
    }
)

module.exports = BlackList