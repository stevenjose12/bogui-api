'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const User = require('./User')

const Notification = sequelize.connections().define('notifications', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        type: Sequelize.INTEGER,
        description: Sequelize.STRING,
        user_id: Sequelize.INTEGER,
        viewed: Sequelize.INTEGER,
        route_id: Sequelize.INTEGER,
        driver_id: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'notifications'
    }
)

Notification.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id',
    targetKey: 'id'
});

Notification.belongsTo(User, {
    as: 'driver',
    foreignKey: 'driver_id',
    targetKey: 'id'
});

module.exports = Notification