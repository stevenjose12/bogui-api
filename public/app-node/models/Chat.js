'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const ChatUser = require('./ChatUser')

const Chat = sequelize.connections().define('chats', {
    	id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        route_id: Sequelize.INTEGER,
        support: Sequelize.INTEGER,
        motive_id: Sequelize.INTEGER
    },{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'chats'
    }
)

Chat.hasMany(ChatUser, {
    as: 'users',
    foreignKey: 'chat_id'
});

module.exports = Chat