const Sequelize = require('sequelize');
const sequelize = require('../core/conexion')

const RefreshToken = sequelize.connections().define('refresh_tokens',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        token: Sequelize.STRING,
        user_id: Sequelize.INTEGER,
        device_id: Sequelize.STRING,
    },
    {
        tableName: 'refresh_tokens',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    }
);

module.exports = RefreshToken;