'use strict'

const Sequelize = require('sequelize')
const sequelize = require('../core/conexion')
const Person = require('./Person')
const Vehicle = require('./Vehicle')
const PendingDocs = require('./PendingDocs')

const User = sequelize.connections().define('users', {
		id: {
	        type: Sequelize.INTEGER,
	        primaryKey: true,
	        autoIncrement: true
	    },    
	    level: Sequelize.INTEGER,
	    email: Sequelize.STRING,
	    password: Sequelize.STRING,
	    latitud: Sequelize.STRING,
	    longitud: Sequelize.STRING,
	    status: Sequelize.INTEGER,
	    status_driver: Sequelize.ENUM('1','0'),
	    default_app: Sequelize.INTEGER,
	    default_payment: Sequelize.INTEGER,
	    remember_token: Sequelize.STRING,
	    validate: Sequelize.TINYINT,
	    last_status: Sequelize.INTEGER,
		alive: Sequelize.INTEGER
	},{
    	createdAt: 'created_at',
    	updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
        tableName: 'users'
    }
)

User.hasOne(Person, {
    as: 'person',
    foreignKey: 'user_id'
});

User.hasOne(Vehicle, {
    as: 'vehicle',
    foreignKey: 'user_id'
});

User.hasMany(PendingDocs, {
    as: 'pending_documents',
    foreignKey: 'user_id'
});

module.exports = User