'use strict'

require('dotenv').config()
const app = require('./app')
const config = require('./config')
const server = app.server
// const https = app.https
// const https_port = config.port + 1
server.listen(config.port)
// https.listen(https_port)
