'use strict'

const express = require('express')
const config = require('../config')
const api = express.Router()
const GoogleController = require('../controllers/GoogleController');

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

api.use(allowCrossDomain);

api.get('/', (req, res) => {
	res.status(403).send("403 Acceso Denegado")
})

api.post('/google/directions',(req, res) => {
	GoogleController.Directions(req.body.origin,req.body.destination,req.body.waypoints).then(resp => {
		res.status(200).json(resp);
	}).catch(err => {
		console.log(err);
		res.status(500).send("Error");
	});
})

api.post('/google/autocomplete',(req, res) => {
	GoogleController.AutoComplete(req.body.input).then(resp => {
		res.status(200).json(resp);
	}).catch(err => {
		console.log(err);
		res.status(500).send("Error");
	});
})

api.post('/google/geocode',(req, res) => {
	GoogleController.Geocode(req.body.latitude,req.body.longitude).then(resp => {
		res.status(200).json(resp);
	}).catch(err => {
		console.log(err);
		res.status(500).send("Error");
	});
})

api.post('/google/place-id',(req, res) => {
	GoogleController.PlaceID(req.body.id).then(resp => {
		res.status(200).json(resp);
	}).catch(err => {
		console.log(err);
		res.status(500).send("Error");
	});
})

module.exports = api