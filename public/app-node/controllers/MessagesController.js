'use strict'

const Message = require('../models/Message')
const Chat = require('../models/Chat')
const User = require('../models/User')
const ChatUser = require('../models/ChatUser')
const Person = require('../models/Person')
const moment = require('moment-timezone')
const DeletedChat = require('../models/DeletedChat')

function sendMessage(obj, callback) {
	// console.log(moment().tz('America/Caracas').format('YYYY-MM-DD HH:mm:ss').toString());
	
	const newMessage = Message.create({
		chat_id: obj.chat_id,
		user_id: obj.user_id,
		text: obj.text,
		file: obj.file,
		seen: '0',
		created_at: moment().tz('America/Caracas').format('YYYY-MM-DD HH:mm:ss').toString(),
		updated_at: moment().tz('America/Caracas').format('YYYY-MM-DD HH:mm:ss').toString()
	}).then(resp =>{
		Message.findById(resp.id,{
			include: [
				{
					model: User,
					as: 'user',
					include: [
						{
							model: Person,
							as: 'person'
						}
					]
				},
				{
					model: Chat,
					as: 'chat',
					include: [
						{
							model: ChatUser,
							as: 'users'
						}
					]
				}
			]
		}).then(resp => {
			Chat.update(
				{
					update_at: moment().tz('America/Caracas').format('YYYY-MM-DD HH:mm')
				},
				{
					where: {
						id: resp.chat_id
					}
				}
			).then(() => {
				DeletedChat.destroy({
				  where: {
				    chat_id: resp.chat_id
				  }
				}).then(() => {
					callback(resp)
				}).catch(err => {
					console.log(err)
				})
			}).catch(err => {
				console.log(err)
			})			
		}).catch(err => {
			console.log(err)
		})		
	})
	.catch(err =>{
		console.log(err, 'err')
		return false
	})
}

module.exports = {
    sendMessage
}