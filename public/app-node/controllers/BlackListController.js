'use strict'

const BlackList = require('../models/BlackList')
const Route = require('../models/Route')

function check(data) {
	return new Promise((resolve,reject) => {
		Route.findById(data.carrera_id).then(carrera => {
			BlackList.create({
				driver_id: carrera.driver_id,
				route_id: carrera.id
			}).then(res => {
				BlackList.findAndCountAll({
					where: {
						route_id: carrera.id
					}
				}).then(blacklist => {
					if (blacklist < 4) {
						resolve({
							result: true,
							driver_id: carrera.driver_id
						})
					}
					else {
						resolve({
							result: false
						})
					}
				}).catch(reject)
			}).catch(reject)
		}).catch(reject)	
	})	
}

module.exports = {
    check
}