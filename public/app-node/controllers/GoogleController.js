var google = require('@google/maps').createClient({
  // key: 'AIzaSyCmfVIcUq5udO2xAwa4lwItPvdPJcFxdz4'
  key: 'AIzaSyB2AYKCUTc1BJJaL_SVBT7TwTPI85lCW5A' // LLAVE DE GOOGLE MAPS NO MODIFICAR
});

const Directions = (origin = {},destination = {},waypoints = []) => {
	console.log(origin,destination,'origin destination')
	return new Promise((resolve,reject) => {
		var request = {
	      origin: {
	        lat: origin.latitude,
	        lng: origin.longitude
	      },
	      destination: {
	        lat: destination.latitude,
	        lng: destination.longitude
	      },
	      mode: 'driving',
	      waypoints: waypoints
	    };
	    google.directions(request,(err,result) => {
	    	if (err) {
				console.log(err,'err')
	    		reject(err);
			}
			console.log(result.json, 'json')
			resolve(result.json);
	    }); 
	});	
}

const AutoComplete = (input) => {
	return new Promise((resolve,reject) => {
		var request = {
	      input: input
	    };
	    google.placesQueryAutoComplete(request,(err,result) => {
	    	if (err) {
	    		reject(err);
	    	}
			resolve(result.json.predictions);
	    }); 
	});	
}

const Geocode = (latitude,longitude) => {
	return new Promise((resolve,reject) => {
	  var latlng = { 
          lat: latitude,
          lng: longitude
      }
      google.reverseGeocode({ latlng: latlng }, (err, response) => {
        if (err) {
          reject(err);
        }
        resolve(response.json.results.length > 0 ? response.json.results[0].formatted_address : '');
      });
	});	
}


const PlaceID = (id) => {
	return new Promise((resolve,reject) => {
      google.reverseGeocode({ place_id: id }, (err, response) => {
        if (err) {
          reject(err);
        }
        resolve(response.json);
      });
	});	
}

module.exports = {
	Directions,
	Geocode,
	AutoComplete,
	PlaceID
}