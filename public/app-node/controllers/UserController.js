'use strict'

const User = require('../models/User')
const Person = require('../models/Person')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

function addUserSocket(socketId, users_online) {
    if (users_online.length > 0) {
        let find = users_online.find((el) => {
            return (el.socketId == socketId)
        })
        if (find == undefined || find == null) {
            users_online.push({
                socketId: socketId
            })
        }
    } else {
        users_online.push({
            socketId: socketId
        })
    }

    return users_online
}

function setLocationSocket(socketId, data, users_online) {
    if (users_online.length > 0) {
        let find = users_online.find((el) => {
            return (el.socketId == socketId)
        })
        if (find == undefined || find == null) {
            users_online.push({
                socketId: socketId
            })
        }
        for (let index = 0; index < users_online.length; index++) {
            if (users_online[index].socketId === socketId) {
                users_online[index].userId = data.user.id
                users_online[index].lat = data.lat
                users_online[index].lng = data.lng
            }
        }
    }
    return users_online
}

function setIdSocket(socketId, data, users_online) {
    if (users_online.length > 0) {
        let find = users_online.find((el) => {
            return (el.socketId == socketId)
        })
        if (find == undefined || find == null) {
            users_online.push({
                socketId: socketId
            })
        }
        for (let index = 0; index < users_online.length; index++) {
            if (users_online[index].socketId === socketId) {
                users_online[index].userId = data.id
            }
        }
    }
    return users_online
}

function findBySocket(socketId, users_online, callback) {
    let field = null
    if (users_online.length > 0) {
        field = users_online.find((el) => {
            return (el.socketId == socketId)
        })
    }
    callback(field)
}

function findById(userId, users_online, callback) {
    let field = null
    if (users_online.length > 0) {
        field = users_online.find((el) => {
            return (el.userId == userId)
        })
    }
    callback(field)
}

function deleteBySocket(socketId, users_online) {
    let online = new Array()
    if (users_online.length > 0) {
        online = users_online.filter((el) => {
            return (el.socketId != socketId)
        })
    }
    return online
}

function updateStatusDriver(id) {
    User.findById(id).then(user => {
        if (user != null || user != undefined) {
            User.update(
                {
                    last_status: parseInt(user.status_driver),
                    status_driver: '0',                
                },
                {
                    where: {
                        id: id
                    }
                }
            ).then(() => {
                // console.log("Last Status Update: " + id)
            }).catch(err => {
                console.log(err)
            })
        }
    }).catch(err => {
        console.log(err)
    })    
}

async function updateAlive(id, alive) {
    try {
        if (id) await User.update({ alive }, {where: { id } });
        // const user = await User.findById(id);
        // if (id===474) console.log(">>: UserController > user:  { id: ", user.id, ", alive: ", user.alive, " }");
    } catch (e) {
        console.log(">>: UserController > updateAlive > inputs : { id: ",id, ", alive: ",alive, "} > error: ", e);
    }
}

function getAllDrivers() {
    return new Promise((resolve,reject) => {
        User.findAll({
            where: {
                level: '2',
                status: '1',
                validate: '1'
            },
            include: [
                {
                    model: Person,
                    as: 'person',
                    where: {
                        expiration_document: {
                            [Op.ne]: null
                        },
                        expiration_permit: {
                            [Op.ne]: null
                        },
                        expiration_criminal_records: {
                            [Op.ne]: null
                        },
                        expiration_license: {
                            [Op.ne]: null
                        },
                    }
                }
            ]
        }).then(result =>{
            resolve(result)
        }).catch(err => {
            reject(err)
        })  
    })
}

function getAllByType(type) {
    return new Promise((resolve,reject) => {
        User.findAll({
            where: {
                level: type
            },
            raw: true,
        }).then(results =>{
            let values = results.map(result => result.id)
            resolve(values)
        }).catch(err => {
            reject(err)
        })  
    })
}

function getAll() {
    return new Promise((resolve,reject) => {
        User.findAll({
            raw: true,
        }).then(results =>{
            let values = results.map(result => result.id)
            resolve(values)
        }).catch(err => {
            reject(err)
        })  
    })
}

module.exports = {
    addUserSocket,
    setLocationSocket,
    findBySocket,
    deleteBySocket,
    findById,
    updateStatusDriver,
    setIdSocket,
    getAllDrivers,
    getAllByType,
    getAll,
    updateAlive
}
