const RefreshToken = require("../models/RefreshToken");
const admin = require('firebase-admin');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const serviceAccount = require("../boguitest-firebase-adminsdk-97eeo-52bbe6f907.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://boguitest.firebaseio.com"
});

/**
 * Actualiza el token de firebase en la tabla refresh_tokens
 */
const refreshToken = async data => {
    try {
        const refreshToken = await RefreshToken.findOne({where: { user_id: data.user_id } });
        if (refreshToken) {
            const update = await RefreshToken.update({token: data.token, user_id: data.user_id}, { where: { user_id: data.user_id } });
        } else {
            const create = await RefreshToken.create(data);
        }
    } catch (e) {
        console.log(">>: RefreshTokenController > error: ", e);
    }
};

/**
 * Retorna un array de todos los tokens registrado en la tabla refresh_tokens
 */
const getTokens = async () => {
    try {
        const refreshTokens = await RefreshToken.findAll({attributes: ['token']});
        return refreshTokens.map( refreshToken => refreshToken.token);
    } catch (e) {
        console.log(">>: RefreshTokenController > getTokens > error: ", e);
        return [];
    }
};

/**
 * Retorna el token de un usuario en especifico.
 */
const getTokenByUser = async user_id => {
    try {
        const refreshToken = await RefreshToken.findOne({where: {user_id}});
        return refreshToken.token;
    } catch (e) {
        console.log(">>: RefreshTokenController > getTokenByUser > error: ", e);
        return '';
    }
};

/**
 * Retorna un array de tokens de un array de id de usuarios en especificos.
 */
const getTokenByUsers = async users => {
    try {
        const refreshTokens = await RefreshToken.findAll({ where: { user_id: users } });
        return refreshTokens.map( refreshToken => refreshToken.token );
    } catch (e) {
        console.log(">>: RefreshTokenController > getTokenByUsers > error: ", e);
        return [];
    }
};

/**
 * Envia un mensaje de firebase a todos los dispositivos con tokens registrado
 */
const senBroadcast = async (eventName, data) => {

    try {
        data.eventName = eventName;
        const tokens = await getTokens();
        const message = {
            data: {
                payload: JSON.stringify( data )
            },
            tokens
        };
        const r = await admin.messaging().sendMulticast( message );
    } catch (e) {
        console.log(">>: RefreshTokenController > sendMessage > e: ", e);
    }

};

/**
 * Envia un mensaje de firebase a todos los id de usuarios contenido en el array users.
 */
const sendMulticast = async (eventName, data, users) => {

    try {

        data.eventName = eventName;

        const tokens = await getTokenByUsers(users);

        const message = {
            data: {
                payload: JSON.stringify( data )
            },
            tokens
        };

        const r = await admin.messaging().sendMulticast( message );

    } catch (e) {
        console.log(">>: RefreshTokenController > sendMessage > e: ", e);
    }

};

/**
 * Envia un mensaje de firebase a un usuario en especifico
 */
const send = async (eventName, data, user_id) => {

    try {

        data.eventName = eventName;
        const token = await getTokenByUser(user_id);

        const message = {
            data: {
                payload: JSON.stringify( data )
            },
            token
        };
        const r = await admin.messaging().send(message);

    } catch (e) {
        console.log(">>: RefreshTokenController > send > error: ", e);
    }

};

module.exports = {
    refreshToken,
    getTokens,
    getTokenByUser,
    getTokenByUsers,
    senBroadcast,
    sendMulticast,
    send
}