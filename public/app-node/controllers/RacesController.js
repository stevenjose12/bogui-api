'use strict'
const BlackList = require('../models/BlackList')
const moment = require('moment')

function findById(data, list) {
    let field = list.find((el) => {
        return (el.route_id == data.id)
    })
    if (field == null || field == undefined) {
        list.push({
            route_id: data.id,
            user_id: data.user_id,
            created_at: moment().format('YYYY-MM-DD HH:mm:ss')
        })
    }
    return list
}

function raceAccepted(id, list) {
    let index = list.findIndex((el) => {
        return (el.route_id == id)
    })
    if (index > -1) {
        list.splice(index, 1)
    }
    return list
}

function findRejects(race, cb) {
    BlackList.findAndCountAll({
        where: {
            route_id: race.id
        }
    }).then(res => {
        cb(res.count)
    })
    .catch(err => {
        console.log(err)        
    })
}

module.exports = {
    findById,
    raceAccepted,
    findRejects
}