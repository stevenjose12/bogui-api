'use strict'

const Notification = require('../models/Notification')
const User = require('../models/User')
const Person = require('../models/Person')
const PendingDocs = require('../models/PendingDocs')
const Vehicle = require('../models/Vehicle')
const VehiclePhoto = require('../models/VehiclePhoto')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

function create(data) {
	return new Promise((resolve,reject) => {
		Notification.create(data).then(resp => {
			resp = resp.get({ plain: true });
			Notification.findAndCountAll({
				where: {
					user_id: data.user_id,
					viewed: '0'
				}
			}).then(result => {
				resp.count = result.count
				resolve(resp)
			}).catch(err => {
				reject(err)
			})			
		}).catch(err => {
			reject(err)
		})
	})
}

function createForAdmin(data) {
	return new Promise((resolve,reject) => {
		Notification.create(data).then(resp => {
			Notification.find({
				where: {
					id: resp.id
				},
				include: [
					{
						model: User,
						as: 'driver',
						include: [
							{
								model: Person,
								as: 'person'
							},
							{
								model: PendingDocs,
								as: 'pending_documents',
								where: {
									status: {
										[Op.ne]: '1'
									}
								}
							},
							{
								model: Vehicle,
								as: 'vehicle',
								include: [
									{
										model: VehiclePhoto,
										as: 'photos'
									}
								]
							}
						]
					},
				]
			}).then(result => {
				// resp.count = result.count
				resolve(result)
			}).catch(err => {
				reject(err)
			})			
		}).catch(err => {
			reject(err)
		})
	})
}

function createForExpired(data) {
	return new Promise((resolve,reject) => {
		Notification.create(data).then(resp => {
			Notification.find({
				where: {
					id: resp.id
				},
				include: [
					{
						model: User,
						as: 'user',
						include: [
							{
								model: Person,
								as: 'person'
							},
							{
								model: PendingDocs,
								as: 'pending_documents'
							},
							{
								model: Vehicle,
								as: 'vehicle',
								include: [
									{
										model: VehiclePhoto,
										as: 'photos'
									}
								]
							}
						]
					},
				]
			}).then(result => {
				// resp.count = result.count
				resolve(result)
			}).catch(err => {
				reject(err)
			})			
		}).catch(err => {
			reject(err)
		})
	})
}

function count(type) {
	return new Promise((resolve,reject) => {
		Notification.findAndCountAll({
			where: {
				type: type.type,
				viewed: '0'
			}
		}).then(result => {
			resolve(result.count)
		}).catch(err => {
			reject(err)
		})
	})
}

module.exports = {
    create,
    createForAdmin,
    createForExpired,
    count
}