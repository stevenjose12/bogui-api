<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'drivers' => [
            'driver' => 'local',
            'root' => public_path('drivers'),
            'url' => env('APP_URL'),
            'visibility' => 'public'
        ],

        'vehicles' => [
            'driver' => 'local',
            'root' => public_path('vehicles'),
            'url' => env('APP_URL'),
            'visibility' => 'public'
        ],

        'passengers' => [
            'driver' => 'local',
            'root' => public_path('passengers'),
            'url' => env('APP_URL'),
            'visibility' => 'public'
        ],

        'moderators' => [
            'driver' => 'local',
            'root' => public_path('moderators'),
            'url' => env('APP_URL'),
            'visibility' => 'public'
        ],

        'chats' => [
            'driver' => 'local',
            'root' => public_path('chats'),
            'url' => env('APP_URL'),
            'visibility' => 'public'
        ],

        'files' => [
            'driver' => 'local',
            'root' => storage_path('app/files'),
            'url' => env('APP_URL').'/files',
            'visibility' => 'public',
        ],

        'notifications' => [
            'driver' => 'local',
            'root' => public_path('notifications'),
            'url' => env('APP_URL'),
            'visibility' => 'public'
        ],

        'information' => [
            'driver' => 'local',
            'root' => public_path('information'),
            'url' => env('APP_URL'),
            'visibility' => 'public',
        ],

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
