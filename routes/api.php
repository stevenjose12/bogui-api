<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function() {

    // TEST
    Route::post("test", "ApiDriver\RouteController@finalizar");
    
    // COUNTRIES
    Route::post("countries/get-phone-code", "CountryController@getPhoneCode");
    Route::post('register/verificar', 'api\RegisterController@uploadAvatar');
    Route::post('register/document', 'api\RegisterController@uploadDocument');
    Route::post("register/form", "api\RegisterController@register");
    Route::post("login", "api\RegisterController@login");
    Route::post("validate", "api\RegisterController@validar");
    Route::post('recuperar','api\RegisterController@recuperar');
    Route::post('validar','api\RegisterController@validar');
    Route::post('reset','api\RegisterController@reset');
    Route::post("regiseter/resendmail", "api\RegisterController@resendMail");
    Route::post("register/validarMail", "api\RegisterController@validarEmail");

    //Vehicles
    Route::get('vehicles/type','api\VehicleController@vehicleTypes');
    Route::post('vehicles/unoccupied','api\VehicleController@unoccupied');

    //User
    Route::post('accounts/position/store','api\UserController@storePosition');
    Route::post("position/storage", "api\UserController@setPosition");
    Route::post("profile/get", "api\UserController@getUser");
    Route::post('profile/password','api\UserController@password');
    Route::post("profile/post", "api\UserController@editUser");
    Route::post("user/up-avatar", "api\UserController@upAvatar");
    Route::post("user/get-driver", "api\UserController@getDriver");
    Route::post("user/getCercano", "api\RouteController@boguiCercano");
    Route::post("user/validar-pasajero/{id}", "api\UserController@validarPasajero");
    Route::post("user/get-user/{id}", "api\UserController@getUserById");

    //TarifasboguiCercano
    Route::post("tarifas/all",'api\TariffController@all');

    /* ROUTES */
    Route::resource('routes','api\RouteController');
    Route::post('routes/pagar', 'api\RouteController@pagar');
    Route::post('routes/accepted', 'api\RouteController@getRoutes');
    Route::post('routes/get', 'api\RouteController@get');
    Route::post('route/valid', "api\RouteController@valid");
    Route::post("route/cancelar", "api\RouteController@cancel");
    Route::post('store/get','ApiDriver\StoreController@get');
    Route::post('route/get-route', 'api\RouteController@getRoute');
    Route::post('route/get-route-specific', 'api\RouteController@getRouteSpecific');
    Route::post("route/get-current", "api\RouteController@getCurrentRoute");
    Route::post("route/get-current/edit", "api\RouteController@getCurrentRouteEdit");
    Route::post("route/is-airport", "api\RouteController@isAirportRoute");
    Route::post("route/is-airport-test", "api\RouteController@getCurrentRouteEdit_");
    Route::post("route/is-valid/{id}", "api\RouteController@isValid");

    Route::post('routes/clone', 'api\RouteController@CloneRoute');
    Route::post('routes/reenviar', 'api\RouteController@reenviar');
    Route::post('routes/get-radius', "api\RouteController@getRadius");
    Route::post("route/get-driver", "api\RouteController@getDriverRoute");
    Route::post('routes/calificar','api\RouteController@calificar');
    Route::post("route/por-pagar", "api\RouteController@rutasPorPagar");
    Route::post("route/cancel/reject","api\RouteController@routeCancelReject");
    Route::post("route/get-cost", "api\RouteController@getCost");
    Route::post("payment/update", "api\RouteController@paymentUpdate");

    /* NOTIFICATION */
    Route::post("notification/get", "api\NotificationController@get");

    /* CHAT */
    Route::post("chat/get", "api\ChatController@get");
    Route::post("chat/get-messages", "api\ChatController@getMessages");
    Route::post("chat/create", "api\ChatController@createChat");
    Route::post("chat/create-chat", "api\ChatController@create");
    Route::post('chat/delete','api\ChatController@delete');

    ///
    Route::post('chat/view','api\ChatController@view');
    Route::post('chat/update','api\ChatController@update');
    Route::post('chat/count','api\ChatController@count');
    Route::post('chat/load-one','api\ChatController@loadOne');
    Route::post('chat/seen','api\ChatController@seen');
    Route::post('chat/image','api\ChatController@image');
    Route::post("chat/exist", 'api\ChatController@exist');
    Route::post("chat/asuntos", 'api\ChatController@asuntos');
    Route::post("support/create", "api\ChatController@createSupport");
    // Route::post('support/get','SupportController@get');
    Route::post('support/get','api\ChatController@supportGet');

    // PROMOCION
    Route::post("promocion/get", "api\PromocionController@get");
    Route::post("promocion/accepted", "api\PromocionController@accepted");
    Route::post("promocion/get-by-route", "api\PromocionController@getPromotionRoute");
    
    ///
    Route::post('default-payment','api\UserController@defaultPayment');

    Route::get('webpayinit', 'api\TransbankController@index');

    // Webpay
    Route::get('webpay','api\WebpayController@get');
    Route::post('webpay/continue','api\WebpayController@continuar');
    Route::get('webpay/response','api\WebpayController@response');
    Route::post('webpay/check','api\WebpayController@check');
    Route::get('webpay/inscripcion','api\WebpayController@inscripcion');
    Route::post('webpay/inscripcion/response','api\WebpayController@inscripcionResponse');
    Route::post('webpay/delete','api\WebpayController@delete');

    // Documentos
    Route::post('documents/get','api\DocumentController@get');
    Route::post('documents/upload','api\DocumentController@upload');
    // Modal 
    Route::post('information', 'api\InformationController@index');
    
    /**
     * Contacts
     */
    Route::post('contacts/create', 'api\UserContactController@createContact');
    Route::post('contacts/edit', 'api\UserContactController@editContact');
    Route::post('contacts/delete', 'api\UserContactController@deleteContact');
    Route::post('contacts/get', 'api\UserContactController@getContact');
    Route::post('contacts/get-all', 'api\UserContactController@getAllContact');
    /**
     * Create Panic of passengers
     */
    Route::post('panic/create', 'FollowPassengerController@create');
});

include 'driver.php';

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});