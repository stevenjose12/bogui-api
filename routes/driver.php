<?php

    Route::group(['middleware' => 'api','namespace' => 'ApiDriver', 'prefix' => 'api-driver'],function() {

        // test
        Route::post('test/sendemail','RouteController@testEmailFinish');
         

	 	// Auth
	 	Route::post('auth/login','AuthController@login');
	 	Route::post('auth/register','AuthController@register');
	 	Route::post('auth/register/validar','AuthController@validarRegister');
	 	Route::post('auth/recuperar','AuthController@recuperar');
	 	Route::post('auth/validar','AuthController@validar');
	 	Route::post('auth/reset','AuthController@reset');
	 	Route::post('auth/countries','AuthController@countries');
	 	Route::post('auth/banks','AuthController@banks');
	 	Route::post('auth/upload','AuthController@upload');
	 	Route::post('auth/upload/photo-only','AuthController@uploadPhotoOnly');
	 	Route::post('auth/upload-file','AuthController@uploadFile');
	 	Route::post('auth/upload/permit','AuthController@uploadPermit');
	 	Route::post('auth/check-user','AuthController@checkUser');
	 	Route::post('auth/reenviar','AuthController@reenviar');
	 	Route::post('auth/verificar','AuthController@validarCodigo');
	 	Route::post('auth/upload/photo-only','AuthController@uploadPhotoOnly');

	 	// Perfil
	 	Route::post('profile/get','ProfileController@get');
	 	Route::post('profile/post','ProfileController@post');
	 	Route::post('profile/upload','ProfileController@upload');
	 	Route::post('profile/password','ProfileController@password');
	 	Route::post('profile/vehicle','ProfileController@postVehicle');
	 	Route::post('profile/vehicle/add','ProfileController@addVehicle');
	 	Route::post('profile/vehicle/toggle','ProfileController@toggleVehicle');
	 	Route::post('profile/vehicle/photo','ProfileController@vehiclePhoto');
	 	Route::post('profile/default-app','ProfileController@defaultApp');
	 	Route::post('profile/update-status','ProfileController@updateStatus');
	 	Route::post('profile/update-location','ProfileController@updateLocation');
	 	Route::post('profile/alive','ProfileController@updateAlive');

	 	// Rutas
	 	Route::post('routes/get','RouteController@get');
	 	Route::post('routes/aceptar','RouteController@aceptar');
	 	Route::post('routes/cancelar','RouteController@cancelar');
	 	Route::post('routes/status','RouteController@status');
	 	Route::post('routes/finalizar','RouteController@finalizar');
	 	Route::post('routes/calificar','RouteController@calificar');
	 	Route::post('routes/calificar/bogui','RouteController@calificarBogui');
	 	Route::post('routes/tiempo-adicional','RouteController@tiempoAdicional');
	 	Route::post('routes/new-driver','RouteController@getNewDriver');
	 	Route::post('routes/date-driver','RouteController@dateDriver');
	 	Route::post('routes/save-photo','RouteController@savePhoto');
	 	Route::post('routes/tarifas','RouteController@getMontos');
	 	Route::post('routes/recalcular','RouteController@recalcular');
	 	Route::post('routes/get/factura','RouteController@getFactura');

	 	// Datos Bancarios
	 	Route::post('bank/get','BankController@get');
	 	Route::post('bank/post','BankController@post');
	 	Route::post('bank/types','BankController@types');

	 	// Store
	 	Route::post('store/get','StoreController@get');

	 	// Support
	 	Route::post('support/get','SupportController@get');
	 	Route::post('support/create','SupportController@create');

	 	// Ganancias
	 	Route::post('ganancias/get','GananciaController@get');

	 	// Notificaciones
	 	Route::post('notifications/get','NotificationController@get');

	 	// Chat
	 	Route::post('chat/get','ChatController@get');
	 	Route::post('chat/delete','ChatController@delete');
	 	Route::post('chat/view','ChatController@view');
	 	Route::post('chat/update','ChatController@update');
	 	Route::post('chat/count','ChatController@count');
	 	Route::post('chat/load-one','ChatController@loadOne');
	 	Route::post('chat/seen','ChatController@seen');
	 	Route::post('chat/create','ChatController@create');
	 	Route::post('chat/image','ChatController@image');

	 	// Documentos
	 	Route::post('documents/get','DocumentController@get');
	 	Route::post('documents/upload','DocumentController@upload');
	 	Route::post('documents/upload-file','DocumentController@uploadFile');
	 	Route::post('documents/check','DocumentController@check');

	 	// Admin Notificaciones
	 	Route::post('admin-notifications/get','AdminNotificationController@get');
	 	Route::post('admin-notifications/check','AdminNotificationController@check');

	 	// Night Mode
	 	Route::post('night/get','NightModeController@get');

	 	// OpenTok
	 	Route::post('opentok/get','OpenTokController@get');

	 	// Modal 
	 	Route::post('information', 'InformationController@index');

		//Panic
	 	Route::post('panic/options','PanicController@getOptions');
	 	Route::post('panic/drivers','PanicController@getDrivers');
	 	Route::post('panic/finish','PanicController@finish');
	 	Route::post('panic/get','PanicController@get');
	 	Route::post('panic/set-option','PanicController@setOption');
	 	Route::post('panic/response','PanicController@driverResponse');

	 });

	 
