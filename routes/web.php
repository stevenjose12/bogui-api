<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include 'page.php';

// Index
Route::get('/admin', 'AuthController@index');
// Do login
Route::post('/login', 'AuthController@login');
// Do logout
Route::get('/logout', 'AuthController@logout');

//* Reporte de Ganancias */
Route::get('finances/weekly-report-bogui-excel', 'Admin\FinanceController@getWeeklyReportBoguiExcel');

//* Reporte de Ganancias */
Route::get('finances/weekly-report-bogui-excel-week', 'Admin\FinanceController@getWeeklyReportBoguiExcelLastWeek');

// Cronjob
Route::get('cronjob','CronjobController@get');

// Admin
Route::group(['prefix' => 'admin', 'namespace' => "Admin", 'middleware' => ['Auth']], function() {

    //* Conductores */
    Route::resource('/users', 'UsersController');
    Route::post('/users-activate/{id}', 'UsersController@activate');
    Route::post('/users-validate/{id}', 'UsersController@driverValidate');
    Route::post('/users-block/{id}', 'UsersController@block');
    Route::get('/users/{id}/routes', 'UsersController@travels');
    Route::get('/users-banks', 'UsersController@usersBanks');
    Route::get('/users-ratings', 'UsersController@usersRatings');
    Route::post('/users/users-check-documents/{id}', 'UsersController@checkDocuments');
    Route::post('/users/upload-files/{id}', 'UsersController@uploadFiles');

    //* Pasajeros */
    Route::resource('/passengers', 'PassengersController');
    Route::post('/passengers-activate/{id}', 'PassengersController@activate');
    Route::post('/passengers-validate/{id}', 'PassengersController@passengerValidate');
    Route::post('/passengers-block/{id}', 'PassengersController@block');
    Route::get('/passengers/{id}/routes', 'PassengersController@travels');
    Route::get('/passengers-ratings', 'PassengersController@passengersRatings');
    Route::post('/passengers/upload-files/{id}', 'PassengersController@uploadFiles');

    //* Pasajeros */
    Route::resource('/moderators', 'ModeratorsController');
    Route::post('/moderators-activate/{id}', 'ModeratorsController@activate');

    //* Tarifas */
    Route::resource('/tariffs', 'TariffsController');

    //* Comisiones */
    Route::resource('/commissions', 'CommissionsController');

    //* Tipos Tarifas */
    Route::resource('/types-tariff', 'TypesController');

    //* Vehiculos */
    Route::resource('/users/vehicles', 'VehiclesController');
    Route::post('/vehicles-activate/{id}', 'VehiclesController@activate');
    Route::post('/vehicles/upload-files/{id}', 'VehiclesController@uploadFiles');

    //* Promociones */
    Route::resource('/promotions', 'PromotionsController');
    Route::post('/promotions-activate/{id}', 'PromotionsController@activate');

    //* Soporte */
    Route::resource('/support', 'SupportController');
    Route::post('/support-activate/{id}', 'SupportController@activate');

    //* Calificaciones */
    Route::resource('/ratings', 'RatingsController');
    Route::post('/ratings/get-users', 'RatingsController@getUsers');
    Route::get('/ratings-app', 'RatingsController@ratingsApp');

    //* Rutas */
    Route::resource('/routes', 'RoutesController');
    Route::get('/users-routes', 'RoutesController@routesUsers');
    Route::get('/passengers-routes', 'RoutesController@routesPassengers');

    //* Perfil */
    Route::get('profile', 'ProfileController@index');
    Route::post('profile', 'ProfileController@store');

    //* Chats */
    Route::get('chats', 'ChatsController@index');
    Route::get('/chats/{id}/messages', 'ChatsController@getMessages');
    Route::post('/chats/new-chat/{id}', 'ChatsController@getNewChat');

    //* Finanzas */
    Route::get('earnings-drivers', 'FinanceController@earningsDrivers');
    Route::get('earnings-passengers', 'FinanceController@earningsPassengers');
    Route::get('payments-drivers', 'FinanceController@paymentsDrivers');
    Route::get('finances/{id}/details', 'FinanceController@details');
    Route::post('finances/weekly-report', 'FinanceController@getWeeklyReport');
    Route::post('finances-driver-pay', 'FinanceController@payDriver');
    Route::get('finances/get-payments', 'FinanceController@getPayments');
    Route::get('finances/{id}/details-passenger', 'FinanceController@detailsPassenger');
    Route::get('finances/weekly-report-bogui-excel-generate', 'FinanceController@getWeeklyReportBoguiExcelGenerate');
    Route::post('finances/weekly-report-passenger', 'FinanceController@getWeeklyReportPassenger');
    Route::post('finances/weekly-report-bogui', 'FinanceController@getWeeklyReportBogui');

    //* Busqueda */
    Route::resource('/search', 'SearchController');

    //* Modo Nocturno */
    Route::resource('/nightmode', 'NightModeController');

    //* Notificaciones */
    Route::resource('/notifications', 'NotificationsController');

    //* Admin Notificaciones */
    Route::resource('/send-notifications', 'AdminNotificationsController');
    Route::post('/send-notifications/users-type/{type}', 'AdminNotificationsController@getUsers');

    //* Webpay */
    Route::resource('/webpay', 'WebpayController');
    Route::post('/webpay-reverse/{id}', 'WebpayController@reverse');

    //* Information */
    Route::resource('/information', 'InformationController');

    Route::resource('/panics', 'PanicController');
});