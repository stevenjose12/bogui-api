<?php

	Route::group(['middleware' => ['store','lang']],function() {

		Route::get('/','HomeController@get');
		Route::get('nosotros','HomeController@nosotros');
		Route::get('informacion','HomeController@informacion');
		Route::get('solicitar','HomeController@solicitar');
		Route::get('beneficios/pasajero','HomeController@beneficiosPasajero');
		Route::get('beneficios/conductor','HomeController@beneficiosConductor');
		Route::get('consigue-empleo','HomeController@consigueEmpleo');

		Route::get('routes/{id}', 'RouteController@index');
		/**
		 * Seguimiento de pasajeros en panico
		 */
		Route::get('followPassenger/{id}', 'FollowPassengerController@index');

		// Idioma
		Route::get('lang/{lang}','LangController@change');

		Route::get('home/logout','AuthHomeController@logout');

		Route::group(['middleware' => 'login'],function() {

			// Auth
			Route::get('login','AuthHomeController@getLogin');
			Route::post('home/login','AuthHomeController@postLogin');
			Route::get('register','AuthHomeController@getRegister');
			Route::post('register','AuthHomeController@postRegister');
			Route::post('register/conductor','AuthHomeController@postRegisterConductor');
			Route::get('reset','AuthHomeController@getReset');
			Route::post('reset/code','AuthHomeController@sendCode');
			Route::post('reset/code/check','AuthHomeController@checkCode');
			Route::post('reset','AuthHomeController@postReset');
			
		});

		Route::group(['middleware' => 'AuthHome'],function() {

			// Mi Cuenta
			Route::get('account','AccountController@get')->middleware('code');

			Route::group(['middleware' => 'no-code'],function() {

				// Código de Verificación
				Route::get('codigo','AuthHomeController@getCodigo');
				Route::post('codigo','AuthHomeController@postCodigo');
				Route::post('codigo/reenviar','AuthHomeController@postReenviar');
			});			
		});
		
	});