let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js/script.js')
    .sass('resources/assets/sass/app.scss', 'public/css/style.css')
    .js('resources/assets/js/ruta.js', 'public/js/ruta.js')
    .sass('resources/assets/sass/ruta.scss', 'public/css/ruta.css')
    .sass('resources/assets/sass/no-disponible.scss', 'public/css/no-disponible.css')
    .options({
        processCssUrls: false
    });

// mix.js('resources/assets/js/ruta.js','public/js/ruta.js');