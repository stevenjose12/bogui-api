<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PanicContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('panic_contacts')->truncate();
        DB::table('panic_contacts')->insert([
            ['name'=> 'Administrador', 'phoneCode' => '56', 'phone'=> '954089330', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
