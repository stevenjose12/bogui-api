<?php

use Illuminate\Database\Seeder;

class InformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\Information::create([
            'title' => 'Titulo para la informacion del conductor',
            'description' => 'Descripcion de la informacion para el conductor',
            'type' => '1',
            'status' => '1',
            'created_by' => '1',
            'updated_by' => '1'
        ]);

        \App\Models\Information::create([
            'title' => 'Titulo para la informacion del pasajero',
            'description' => 'Descripcion de la informacion para el pasajero',
            'type' => '2',
            'status' => '1',
            'created_by' => '1',
            'updated_by' => '1'
        ]);

    }
}
