<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTariffSpecialTableRoutes extends Migration {

    public function up() {
        Schema::table('routes', function (Blueprint $table) {
            $table->integer('special_tariff')->nulable()->after('tariff_id');
        });
    }

    public function down() {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn("special_tariff");
        });
    }

}