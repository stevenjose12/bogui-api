<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpentok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('opentok', function (Blueprint $table) {
            $table->increments('id');
                $table->integer('route_id')->unsigned();
                $table->foreign('route_id')->references('id')->on('routes');
                $table->text('sessionId');
                $table->text('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opentok');
    }
}
