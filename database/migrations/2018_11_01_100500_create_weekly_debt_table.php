<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyDebtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_debt', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id');
            $table->double('debt_weekly', 15, 2);
            $table->double('debt_carried', 15, 2);
            $table->double('debt_total', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_debt');
    }
}
