<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAirports extends Migration {

    public function up() {
        Schema::create('airports', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("lat");
            $table->string("lng");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        Schema::dropIfExists('airports');
    }

}