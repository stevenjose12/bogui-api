<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldStatusTableUser extends Migration {

    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('status')->default(1)->change();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('status')->change();
        });
    }
}
