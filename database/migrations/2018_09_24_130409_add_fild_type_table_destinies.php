<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFildTypeTableDestinies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destinies', function (Blueprint $table) {
            $table->tinyInteger('type')->comment('1 = parada, 2 = destino');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('destinies', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
