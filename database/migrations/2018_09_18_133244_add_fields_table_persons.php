<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsTablePersons extends Migration
{

    public function up() {
        Schema::table('persons', function (Blueprint $table) {
            $table->string('phone')->after('lastname')->nullable();
            $table->string('document_photo')->after('document')->nullable();
            $table->string("document_type")->nullable()->change();
            $table->string("document")->nullable()->change();
            $table->string("photo")->nullable()->change();
        });
    }


    public function down() {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('document_photo');
            $table->string("document_type")->change();
            $table->string("document")->change();
            $table->string("photo")->change();
        });
    }
}
