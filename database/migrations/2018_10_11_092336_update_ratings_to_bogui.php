<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRatingsToBogui extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getDoctrineSchemaManager()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('ratings', function (Blueprint $table) {
            $table->integer("reviewed_id")->nullable()->unsigned()->change();
            $table->integer("bogui")->comment('Es calificación a Bogui')->default(0)->after('route_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            $table->integer("reviewed_id")->nullable()->change();
            $table->dropColumn('bogui');
        });
    }
}
