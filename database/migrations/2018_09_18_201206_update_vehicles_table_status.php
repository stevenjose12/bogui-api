<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVehiclesTableStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->integer('status')->after('user_id')->default('1')->comment('1: Activo, 0: Inactivo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->integer('status')->after('user_id');
        });
    }
}
