<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreatePasswordApp extends Migration {

        public function up() {
            Schema::create('password_app', function (Blueprint $table) {
                $table->increments('id');
                $table->string('email');
                $table->string('code');
                $table->enum('status',['0','1'])->comment('1: Activo, 0: Inactivo')->default('1');
                $table->timestamps();
            });
        }


        public function down() {
            Schema::dropIfExists('password_app');
        }
    }
