<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPaymentsTableForDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('routes_route_id_foreign');
            $table->integer('route_id')->nullable()->change();
            $table->integer('method')->comment('1: Tarjeta, 2: Efectivo, 3: Promocion, 4: Pago a Conductor')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('route_id')->unsigned()->change();
        });
    }
}
