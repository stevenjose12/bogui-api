<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationModalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('logo')->default('img/information-default-logo.png');
            $table->integer('type')->comment('1: Conductor, 2: Pasajero, 3: Ambos');
            $table->integer('status')->default(1)->comment('0: Desactivado, 1: Activo');
            $table->date('start_date')->nullable()->comment('Fecha de inicio');
            $table->date('end_date')->nullable()->comment('Fecha de finalizado');
            $table->integer('created_by')->comment('Usuario que la creo');
            $table->integer('updated_by')->comment('Ulitmo usuario que la modifico');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}
