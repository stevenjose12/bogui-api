<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAliveInTableUsers extends Migration {

    public function up() {

        Schema::table('users', function (Blueprint $table) {
            $table
                ->integer('alive')
                ->default(1)
                ->comment('1: vivo, 0: muerto. Se declara a un conductor muerto al transcurrir un periodo de tiempo sin emitir socket.');
        });

    }


    public function down() {

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('alive');
        });

    }

}