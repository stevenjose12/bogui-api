<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsTableRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropForeign('routes_driver_id_foreign');
            $table->dropColumn('latitud_destiny');
            $table->dropColumn('longitud_destiny');
            $table->dropColumn('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->integer('latitud_destiny')->unsigned();
            $table->integer('longitud_destiny')->unsigned();
            $table->integer('driver_id')->unsigned();
        });
    }
}
