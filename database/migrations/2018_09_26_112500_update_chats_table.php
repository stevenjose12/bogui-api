<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chats', function (Blueprint $table) {
            $table->dropForeign('chats_sender_id_foreign');
            $table->dropForeign('chats_reciever_id_foreign');
            $table->dropForeign('chats_route_id_foreign');
            $table->dropForeign('chats_motive_id_foreign');
            $table->dropColumn(['sender_id','reciever_id', 'message', 'file']);
            $table->integer('route_id')->unsigned()->nullable()->change();
            $table->integer('support')->nullable()->comment('0: No es soporte, 1: Soporte')->change();
            $table->integer('motive_id')->unsigned()->nullable()->comment('Id de un registro de support_motive')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chats', function (Blueprint $table) {
            $table->integer('sender_id')->unsigned()->comment('Remitente');
            $table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('reciever_id')->unsigned()->comment('Destinatario');
            $table->foreign('reciever_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('message');
            $table->string('file');
        });
    }
}
