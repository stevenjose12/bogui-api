<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->dropForeign(['vehicle_type_id']);
            $table->integer('vehicle_type_id')->unsigned()->nullable()->comment('Si es 0, es una tarifa para todos los tipos de vehiculos')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->integer('vehicle_type_id')->unsigned()->change();
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_type');
        });
    }
}
