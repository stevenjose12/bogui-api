<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableValidations extends Migration {

    public function up() {
        Schema::create('validations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->string("code");
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('validations');
    }

}