<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSupportType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('support_motive', function (Blueprint $table) {
            $table->enum('type',['1','2'])->after('description')->comment('1: Soporte, 2: Ayuda');
            $table->enum('type_user',['1','2','3'])->after('type')->comment('1: General, 2: Pasajero, 3: Conductor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('support_motive', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
