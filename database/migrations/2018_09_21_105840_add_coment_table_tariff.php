<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComentTableTariff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->string('minimum_tariff','tarifa minima, solo si cantidad de km de la carrera es menor al value_km_less')->change();
            $table->string('medium_tariff','tarifa media, solo si cantidad de km de la carrera es mayor al value_km_less y menor al value_km_grater')->change();
            $table->string('maximum_tariff','tarifa alta, solo si cantidad de km de la carrera es mayor al value_km_grater')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->string('minimum_tariff','tarifa minima, solo si cantidad de km de la carrera es menor al value_km_less')->change();
            $table->string('medium_tariff','tarifa media, solo si cantidad de km de la carrera es mayor al value_km_less y menor al value_km_grater')->change();
            $table->string('maximum_tariff','tarifa alta, solo si cantidad de km de la carrera es mayor al value_km_grater')->change();
        });
    }
}
