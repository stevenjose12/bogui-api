<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldPaidInTableRoute extends Migration {

    public function up() {
        Schema::table('routes', function (Blueprint $table) {
            $table->boolean('paid')->default("0")->comment("0: no pagado, 1: pagado");
        });
    }

    public function down() {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('paid');
        });
    }

}