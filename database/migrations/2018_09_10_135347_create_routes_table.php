<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitud_origin')->nullable();
            $table->string('longitud_origin')->nullable();
            $table->string('latitud_destiny')->nullable();
            $table->string('longitud_destiny')->nullable();
            $table->integer('status');
            $table->integer('time')->unsigned()->comment('En segundos');
            $table->integer('vehicle_type_id')->unsigned();
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_type');
            $table->integer('tariff_id')->unsigned();
            $table->foreign('tariff_id')->references('id')->on('tariffs');
            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('users');
            $table->integer('canceled_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
