<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentPendingDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_documents', function (Blueprint $table) {
            $table->text('comment')->after('status')->nullable()->comment('Motivo por el cual se rechaza el documento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_documents', function (Blueprint $table) {
            $table->dropColumn('upload');
        });
    }
}
