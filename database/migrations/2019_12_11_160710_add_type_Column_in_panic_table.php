<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnInPanicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panic', function (Blueprint $table) {
            $table->integer('type')
                ->after('status')
                ->comment('1: Conductor, 2: Pasajero')
                ->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panic', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
