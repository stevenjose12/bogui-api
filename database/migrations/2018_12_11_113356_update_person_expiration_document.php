<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonExpirationDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->date('expiration_permit')->after('permit')->nullable();
            $table->date('expiration_criminal_records')->after('criminal_records')->nullable();
            $table->date('expiration_license')->after('license')->nullable();
            $table->date('expiration_document')->after('document_photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn('expiration_permit');
            $table->dropColumn('expiration_criminal_records');
            $table->dropColumn('expiration_license');
            $table->dropColumn('expiration_document');
        });
    }
}
