<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsFileDescriptionTableVehicleType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_type', function (Blueprint $table) {
            $table->text('description')->after('registered_by');
            $table->string('photo')->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_type', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('photo');
        });
    }
}
