<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDistanceTableRoute extends Migration {

    public function up() {
        Schema::table('routes', function (Blueprint $table) {
            $table->decimal('distance', 15, 2)
                ->default(0)
                ->after('cost')
                ->comment('distancia en km');
        });
    }

    public function down() {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('distance');
        });
    }

}