<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id')->unsigned()->comment('Remitente');
            $table->foreign('sender_id')->references('id')->on('users');
            $table->integer('reciever_id')->unsigned()->comment('Destinatario');
            $table->foreign('reciever_id')->references('id')->on('users');
            $table->string('message');
            $table->string('file');
            $table->integer('route_id')->unsigned();
            $table->foreign('route_id')->references('id')->on('routes');
            $table->integer('support')->unsigned()->comment('0: No es soporte, 1: Soporte');
            $table->integer('motive_id')->unsigned()->comment('Motivo soporte');
            $table->foreign('motive_id')->references('id')->on('support_motive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
