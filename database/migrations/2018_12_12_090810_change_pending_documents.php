<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePendingDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_documents', function (Blueprint $table) {
            $table->dropColumn('file');
            $table->integer('status')
                ->comment('0: Pendiente de Revisión, 1: Aceptado, 2: Rechazado')
                ->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_documents', function (Blueprint $table) {
            $table->string('file')->nullable();
        });
    }
}
