<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPriceTableRoute extends Migration {

    public function up() {
        Schema::table('routes', function (Blueprint $table) {
            $table->double('cost', 10, 2)->after("time");
        });
    }

    public function down() {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
    }

}