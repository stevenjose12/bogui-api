<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeSpecialTableDestinies extends Migration {

    public function up() {
        Schema::table('destinies', function (Blueprint $table) {
            $table->integer('type_special')->after("type");
        });
    }

    public function down() {
        Schema::table('destinies', function (Blueprint $table) {
            $table->dropColumn("type_special");
        });
    }

}