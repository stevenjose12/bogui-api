<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('type')->comment('
                1: Carrera Aceptada, 
                2: Carrera Finalizada,
                3: Carrera Cancelada, 
                4: Destino Cambiado, 
                5: Solicitud de Conductor Aceptada,
                6: Documentos Requeridos,
                7: Documentos Aceptados,
                8: Documentos Rechazados,
                9: Documentos Enviados (Admin),
                10: Documentos Expirados Proximamente')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('type')->comment('1: Carrera Aceptada, 2: Carrera Finalizada, 3: Carrera Cancelada, 4: Destino Cambiado, 5: Solicitud de Conductor Aceptada')->change();
        });
    }
}
