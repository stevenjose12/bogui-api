<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldTableUser extends Migration
{

    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password', 255)->change();
        });
    }

    public function down()  {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->change();
        });
    }

}