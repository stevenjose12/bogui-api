<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRouteIdTableNotifications extends Migration {

    public function up() {

        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('route_id')->nullable();
        });

    }

    public function down() {

        Schema::table('notifications', function (Blueprint $table) {
            $table->dropColumn('route_id');
        });
        
    }

}
