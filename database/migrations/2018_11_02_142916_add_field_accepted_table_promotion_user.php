<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAcceptedTablePromotionUser extends Migration {

    public function up() {
        Schema::table('promotion_user', function (Blueprint $table) {
            $table->boolean('accepted')->default("0")->comment("0: no pagado, 1: pagado");
        });
    }

    public function down() {
        Schema::table('promotion_user', function (Blueprint $table) {
            $table->dropColumn('accepted');
        });
    }

}
