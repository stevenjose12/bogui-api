<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAirportTableRoutes extends Migration {

    public function up() {
        Schema::table('routes', function (Blueprint $table) {
            $table->integer('airport')
                ->default(0)
                ->after('tariff_id')
                ->comment('n° de puntos cerca del aeropuerto');
        });
    }

    public function down() {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('airport');
        });
    }

}