<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UploadFieldPendingDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_documents', function (Blueprint $table) {
            $table->integer('upload')->default(0)->comment('1: El usuario ya subio el archivo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_documents', function (Blueprint $table) {
            $table->dropColumn('upload');
        });
    }
}
