<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefreshToken extends Migration {

    public function up() {

        Schema::create('refresh_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->integer('user_id');
            $table->string('device_id');
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('refresh_tokens');
    }

}