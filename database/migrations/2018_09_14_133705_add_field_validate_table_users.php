<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldValidateTableUsers extends Migration {

    public function up() {
        Schema::table("users", function (Blueprint $table) {
            $table->boolean("validate")
                ->default(0)
                ->comment("0: no verificado; 1: verificado");
        });
    }

    public function down() {
        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("validate");
        });
    }

}