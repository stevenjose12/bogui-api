<ul class="leftside-navigation" id="menu">
    

    @if(Auth::user()->hasPermission('module-drivers') || Auth::user()->hasRole('super-admin'))
    <li>
        <a href="javascript:void(0)" class="collapsible-label1 waves-default tooltipped" data-position="right" data-delay="50" data-tooltip="Conductores">
            <i class="material-icons left-icon">assignment_ind</i>Conductores
            <badge-menu-read set-user></badge-menu-read>
        </a>
        <ul class="collapsible-body">
            <li><a href="{{ url('admin/users') }}">Consultar Conductores</a></li>
            <li><a href="{{ url('admin/earnings-drivers')}}">Reporte de Ganancias</a></li>
            {{-- <li><a href="{{ url('admin/payments-drivers')}}">Reporte de Pagos</a></li> --}}
            <li><a href="{{ url('admin/users-routes')}}">Consultar Viajes</a></li>
            <li><a href="{{ url('admin/users-banks')}}">Cuentas Bancarias</a></li>
            <li><a href="{{ url('admin/users-ratings')}}">Calificaciones</a></li>
        </ul>
    </li>
    @endif

    {{-- @if(Auth::user()->hasPermission('module-vehicles') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="{{ url('admin/vehicles') }}" class="menu waves-effect waves-default tooltipped" data-position="right" data-delay="50" data-tooltip="Vehiculos">
                <i class="material-icons left-icon">directions_car</i>Vehiculos
            </a>
        </li>
    @endif --}}

    @if(Auth::user()->hasPermission('module-passengers') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="javascript:void(0)" class="collapsible-label1 waves-default tooltipped" data-position="right" data-delay="50" data-tooltip="Pasajeros">
                    <i class="material-icons left-icon">person</i>Pasajeros
                    <badge-menu-read set-pass></badge-menu-read>
                </a>
            <ul class="collapsible-body">
                <li><a href="{{ url('admin/passengers') }}">Consultar Pasajeros</a></li>
                <li><a href="{{ url('admin/passengers-routes')}}">Consultar Viajes</a></li>
                <li><a href="{{ url('admin/earnings-passengers')}}">Reporte de Ingresos</a></li>
                <li><a href="{{ url('admin/passengers-ratings')}}">Calificaciones</a></li>
            </ul>
        </li>
    @endif

    @if(Auth::user()->hasPermission('module-support') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="{{ url('admin/chats') }}" class="collapsible-label1 tooltipped" data-position="right" data-delay="50" data-tooltip="Soporte">
                <i class="material-icons left-icon">chat</i>Chats de Soporte y Ayuda
                <badge-menu-read set-chat></badge-menu-read>
            </a>
        </li>
    @endif

    @if(Auth::user()->hasPermission('module-moderators') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="{{ url('admin/moderators') }}" class="collapsible-label1 tooltipped" data-position="right" data-delay="50" data-tooltip="Moderadores">
                <i class="material-icons left-icon">supervisor_account</i>Moderadores
            </a>
        </li>
    @endif

    @if(Auth::user()->hasPermission('module-promotions') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="{{ url('admin/promotions') }}" class="collapsible-label1 tooltipped" data-position="right" data-delay="50" data-tooltip="Promociones">
                <i class="material-icons left-icon">receipt</i>Promociones
            </a>
        </li>
    @endif

    @if(Auth::user()->hasPermission('module-ratings') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="{{ url('admin/ratings-app') }}" class="collapsible-label1 tooltipped" data-position="right" data-delay="50" data-tooltip="Soporte">
                <i class="material-icons left-icon">star</i>Calificación de Bogui
            </a>
        </li>
    @endif

    {{-- @if(Auth::user()->hasPermission('module-reports') || Auth::user()->hasRole('super-admin'))
    <li>
        <a href="javascript:void(0)" class="collapsible-label1 waves-default tooltipped" data-position="right" data-delay="50" data-tooltip="Reportes">
            <i class="material-icons left-icon">library_books</i>Reportes
        </a>
        <ul class="collapsible-body">
            <li><a href="{{ url('/admin/routes') }}"">Historial de Rutas</a></li>
            <li><a href="{{ url('/admin/ratings') }}"">Reporte de Calificaciones</a></li>
        </ul>
    </li>
    @endif --}}

    @if(Auth::user()->hasPermission('module-notifications') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="javascript:void(0)" class="collapsible-label1 tooltipped" data-position="right" data-delay="50" data-tooltip="Notificaciones">
                <i class="material-icons left-icon">notifications</i>Notificaciones
                <badge-menu-read set-notif></badge-menu-read>
            </a>
            <ul class="collapsible-body">
                    <li>
                        <a href="{{ url('admin/notifications') }}">Recibidas <badge-menu-read set-notif></badge-menu-read>
                        </a>
                    </li>
                    <li><a href="{{ url('admin/send-notifications') }}">Enviar</a></li>
            </ul>
        </li>
    @endif

    @if(Auth::user()->hasPermission('module-notifications') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="{{ url('admin/panics') }}" class="collapsible-label1 tooltipped" data-position="right" data-delay="50"
                data-tooltip="Alertas de emergencia">
                <i class="material-icons left-icon">warning</i>Alertas de emergencia
                {{-- <badge-menu-read set-notif></badge-menu-read> --}}
            </a>
        </li>
    @endif

    @if(Auth::user()->hasPermission('module-configuration') || Auth::user()->hasRole('super-admin'))
        <li>
            <a href="javascript:void(0)" class="collapsible-label1 waves-default tooltipped" data-position="right" data-delay="50" data-tooltip="Reportes">
                <i class="material-icons left-icon">settings</i>Configuración General
            </a>
            <ul class="collapsible-body">
                    <li><a href="{{ url('admin/tariffs') }}">Tarifas y Comisiones</a></li>
                    <li><a href="{{ url('admin/support') }}">Temas de Soporte y Ayuda</a></li>
                    <li><a href="{{ url('admin/search') }}">Radio de Busqueda</a></li>
                    <li><a href="{{ url('admin/nightmode') }}">Modo Nocturno</a></li>
                    <li><a href="{{ url('admin/information') }}">Ventana de Informacion</a></li>
                    {{-- @if(Auth::user()->hasRole('super-admin'))
                        <li><a href="{{ url('admin/webpay') }}">Transacciones Webpay</a></li>
                    @endif --}}
            </ul>
        </li>
    @endif

    <li>
        <a href="{{ url('/logout') }}" class="collapsible-label1 tooltipped" data-position="right" data-delay="50" data-tooltip="Tarifas">
            <i class="material-icons left-icon">exit_to_app</i>Salir
        </a>
    </li>
    

    {{-- <li><a href="javascript:void(0)" class="collapsible-label1 waves-effect waves-default tooltipped" data-position="right" data-delay="50" data-tooltip="Material UI"><i class="material-icons left-icon">build</i><i class="material-icons right-icon">arrow_drop_down</i>Material UI</a>
        <ul class="collapsible-body">
            <li><a href="buttons.html" class="waves-effect waves-default">Buttons</a></li>
            <li><a href="breadcrumbs.html" class="waves-effect waves-default">Breadcrumbs</a></li>
            <li><a href="collections.html" class="waves-effect waves-default">Collections</a></li>
        </ul>
    </li> --}}
</ul>