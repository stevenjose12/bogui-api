<nav class="page-header z-depth-3" id="header-main">
    <div class="nav-wrapper">
        <a id="logo-container" href="#" class="brand-logo hide-on-med-and-down">{{ config('app.name') }}</a>

        <ul class="right">

            @if(Auth::user()->hasPermission('module-notifications') || Auth::user()->hasPermission('module-support') || Auth::user()->hasRole('super-admin'))
                <li>
                    <a href="#" data-tooltip="Notificaciones" class="hrz-menu dropdown-button dropdown-belowOrigin dropdown-widthChange waves-effect waves-default">
                        <i class="material-icons header-notification-icon">notifications</i><badge-header-read set-header></badge-header-read>
                    </a>
                </li>
            @endif

            {{--user account--}}
            <li id="user-account-box"><a href="javascript:void(0)" class="hrz-menu dropdown-button dropdown-belowOrigin dropdown-widthChange waves-effect waves-default" data-activates='user-account-dropdown'><i class="material-icons">account_circle</i></a></li>
        </ul>

        {{--user account dropdown--}}
        <ul id='user-account-dropdown' class='dropdown-content'>
            <li><a href="{{ url('admin/profile') }}"><i class="material-icons">vpn_key</i>Cambiar Contraseña</a></li>
            <li class="divider"></li>
            {{-- <li><a href="{{ url('logout') }}"><i class="material-icons">power_settings_new</i>Salir</a></li> --}}
        </ul>
        {{--end user account dropdown--}}
    </div>
</nav>