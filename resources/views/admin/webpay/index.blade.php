@extends("layouts.admin")

@section("title", "Creación de Temas de Soporte")

@section('content')

<webpay-index url="{{ url('/admin/webpay') }}" :webpays="{{ $webpays }}"></webpay-index>

@endsection