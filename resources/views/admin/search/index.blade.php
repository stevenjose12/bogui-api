@extends("layouts.admin")

@section("title", "Radio de Busqueda")

@section('content')

    <search-index url="{{ url('/admin/search') }}" :set-table="{{ $search }}"></search-index>

@endsection