@extends('layouts.admin')

@section("title", "Detalles de Rutas")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Detalles de las Rutas del Pasajero {{$id}}</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <passenger-routes-form
                        :set-form="{{ $routes }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/passengers-routes') }}"
                        url-base="{{ url('/') }}"
                    ></passenger-routes-form>
                </div>
            </div>
        </div>
    </article>

@endsection