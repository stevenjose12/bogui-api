@extends("layouts.admin")

@section("title", "Clientes")

@section('content')

<passengers-index url="{{ url('/admin/passengers') }}" :set-table="{{ $passengers }}"></passengers-index>

@endsection
