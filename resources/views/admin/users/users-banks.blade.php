@extends("layouts.admin")

@section("title", "Pagos a los Conductores")

@section('content')
	<article class="row">
	    <div class="row">
	        <div class="col s12 center-align">
	            <h1>Cuentas Bancarias</h1>
	        </div>
	    </div>
        <bank-users-read url="{{ url('/admin/finances') }}" :set-table="{{ $users }}"></bank-users-read>
	</article>
@endsection