@extends("layouts.admin")

@section("title", "Conductores")

@section('content')

<users-index url="{{ url('/admin/users') }}" :set-table="{{ $users }}" driver_id={{ $driver_id }}></users-index>

@endsection