@extends('layouts.admin')

@section("title", "Detalles de Rutas")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Detalles de las Rutas del Conductor {{$id}}</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <user-routes-form
                        :set-form="{{ $routes }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/users-routes') }}"
                        url-base="{{ url('/') }}"
                    ></user-routes-form>
                </div>
            </div>
        </div>
    </article>

@endsection