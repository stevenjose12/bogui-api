@extends("layouts.admin")

@section("title", "Enviar Notificaciones")

@section('content')

    <send-notifications-index url="{{ url('/admin/send-notifications') }}" :set-users="{{ $users }}"></send-notifications-index>

@endsection