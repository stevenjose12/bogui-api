@extends('layouts.admin')

@section("title", "Edición de Tema de Soporte")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Editar Tema de Soportes</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <support-form
                        :set-form="{{ $support }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/support') }}"
                        url-base="{{ url('/') }}"
                    ></support-form>
                </div>
            </div>
        </div>
    </article>

@endsection