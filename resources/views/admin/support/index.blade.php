@extends("layouts.admin")

@section("title", "Creación de Temas de Soporte")

@section('content')

<support-index url="{{ url('/admin/support') }}" :supports="{{ $supports }}"></support-index>

@endsection