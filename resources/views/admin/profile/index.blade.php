@extends("layouts.admin")

@section("title", "Perfil")

@section('content')
<profile url="{{ url('/admin/profile') }}" :user="{{ $user }}"></profile>
@endsection