@extends("layouts.admin")

@section("title", "Chats")

@section('content')
    <chats-index url="{{ url('/admin/chats') }}" :chats="{{ $chats }}" :histories="{{ $histories }}" :user="{{ $user }}"></chats-index>
@endsection