<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Datos para Depositos</title>
		<style type="text/css">
		</style>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th style="text-align: center;">ID Conductor</th>
					<th style="text-align: center;">Nombres</th>
					<th style="text-align: center;">Apellidos</th>
					<th style="text-align: center;">Estado</th>
					<th style="text-align: center;">Fecha Inicio</th>
					<th style="text-align: center;">Fecha Cierre</th>
					<th style="text-align: center;">Banco</th>
					<th style="text-align: center;">Tipo Cuenta</th>
					<th style="text-align: center;">Nro. Cuenta</th>
					<th style="text-align: center;">RUT</th>
					<th style="text-align: center;">Correo Electronico</th>
					<th style="text-align: center;">Valor Deposito Semanal</th>
					<th style="text-align: center;">Valor Deposito Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($drivers as $driver)
					<tr>
						<td style="text-align: left;">{{ $driver->id }}</td>
						<td style="text-align: right;">{{ $driver->person->name }}</td>
						<td style="text-align: right;">{{ $driver->person->lastname }}</td>
						@if($driver->status == '1' && $driver->deleted_at == null)
							<td style="text-align: right;">Activo</td>
						@elseif($driver->status == '0' && $driver->deleted_at == null)
							<td style="text-align: right;">Inactivo</td>
						@elseif($driver->deleted_at != null)
							<td style="text-align: right;">Eliminado</td>
						@endif
						<td style="text-align: right;">{{ Carbon\Carbon::parse($from)->format('d/M/Y') }}</td>
						<td style="text-align: right;">{{ Carbon\Carbon::parse($to)->format('d/M/Y') }}</td>
						<td style="text-align: right;">{{ $driver->bank_user->bank->name }}</td>
						<td style="text-align: right;">{{ $driver->bank_user->account->name }}</td>
						<td style="text-align: left;">{{ $driver->bank_user->number }}</td>
						<td style="text-align: left;">{{ $driver->person->document }}</td>
						<td style="text-align: right;">{{ $driver->email }}</td>
						<td style="text-align: right;">CLP {{ $driver->balance }}</td>
						<td style="text-align: right;">CLP {{ $driver->total_balance }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th colspan="12">Total Pago Semana</th>
					<td style="font-weight: normal;">CLP {{$total}}</td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>