<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Resumen Semanal</title>
    <style type="text/css">

    </style>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th>Desde:</th>
          <th></th>
          <th>{{ $from }}</th>
        </tr>
        <tr>
          <th>Hasta:</th>
          <th></th>
          <th>{{ $to }}</th>
        </tr>
        <tr>
          <th>Ganancias Generadas</th>
          <th></th>
          <th>CLP {{ $weekly['bogui_earning'] }}</th>
        </tr>
        <tr>
          <th>Distancia Recorrida</th>
          <th></th>
          <th>{{ $weekly['distance_traveled'] }} KM</th>
        </tr>
        <tr></tr>
      </thead>
    </table>
  </body>
</html>