<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Resumen de Viajes</title>
    <style type="text/css">

    </style>
  </head>
  <body>
  <table>
    <thead>
      <tr>
        <th>Identificacion de Viaje</th>
        <th>Estado de la Ruta</th>
        <th>Metodo de Pago</th>
        <th>Costo del Viaje</th>
        <th>Comision de Bogui</th>
        <th>Pago por Promoción</th>
        <th>Diferencia Pago por Promoción</th>
        <th>Ganancia del Conductor</th>
        <th>Fecha de la Ruta</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($routes as $route)
        <tr>
          <td style="">{{ $route->id }}</td>
          <td style="">Culminada</td>
          <td style="">{{ $route->payment_type }}</td>
          <td style="">CLP {{ $route->cost }}</td>
          <td style="">CLP {{ $route->commission }}</td>
            @if($route->promo > 0)
              <td style="">CLP {{ $route->promo }}</td>
              <td style="">CLP {{ $route->difference }}</td>
            @else
              <td style="">---</td>
              <td style="">---</td>
            @endif
          <td style="">CLP {{ $route->balance }}</td>
          <td style="">{{ Carbon\Carbon::parse($route->created_at)->format('d-m-Y') }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
  </body>
</html>