@extends('layouts.admin')

@section("title", "Edición de Moderador")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Editar Moderadores</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <mods-form
                        :set-form="{{ $user }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/moderators') }}"
                        url-base="{{ url('/') }}"
                    ></mods-form>
                </div>
            </div>
        </div>
    </article>

@endsection