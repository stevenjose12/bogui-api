@extends("layouts.admin")

@section("title", "Creación de Moderador")

@section('content')

<mods-index url="{{ url('/admin/moderators') }}" :mods="{{ $mods }}"></mods-index>

@endsection