@extends("layouts.admin")

@section("title", "Calificaciones Bogui")

@section('content')
	<article class="row">
	    <div class="row">
	        <div class="col s12 center-align">
	            <h1>Calificaciones Bogui</h1>
	        </div>
	    </div>
        <ratings-app-read url="{{ url('/admin/ratings') }}" :set-table2="{{ $bogui }}"></ratings-app-read>
	</article>
@endsection