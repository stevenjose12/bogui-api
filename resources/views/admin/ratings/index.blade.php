@extends("layouts.admin")

@section("title", "Calificaciones")

@section('content')

<ratings-index url="{{ url('/admin/ratings') }}" :set-table="{{ $users }}" :set-table2="{{ $bogui }}"></ratings-index>

@endsection