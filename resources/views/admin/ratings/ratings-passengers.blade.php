@extends("layouts.admin")

@section("title", "Calificaciones Pasajeros")

@section('content')
	<article class="row">
	    <div class="row">
	        <div class="col s12 center-align">
	            <h1>Calificaciones Pasajeros</h1>
	        </div>
	    </div>
        <ratings-read url="{{ url('/admin/ratings') }}" :set-table="{{ $users }}"></ratings-read>
	</article>
@endsection