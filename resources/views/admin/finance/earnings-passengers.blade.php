@extends("layouts.admin")

@section("title", "Ingresos por Pasajeros")

@section('content')
	<article class="row">
	    <div class="row">
	        <div class="col s12 center-align">
	            <h1>Reporte de Ingresos por Usuario</h1>
	        </div>
	    </div>
		<finance-earnings-passenger url="{{ url('/admin/finances') }}" :passengers="{{ $passengers }}"></finance-earnings-passenger>
	</article>
@endsection