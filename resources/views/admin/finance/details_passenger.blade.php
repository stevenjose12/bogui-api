@extends('layouts.admin')

@section("title", "Detalle de Ingresos del Pasajero")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Detalles del Pasajero {{ $passenger->id }}</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <finance-details-passenger
                        :set-form="{{ $passenger }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/earnings-passengers') }}"
                        url-base="{{ url('/') }}"
                    ></finance-details-passenger>
                </div>
            </div>
        </div>
    </article>

@endsection