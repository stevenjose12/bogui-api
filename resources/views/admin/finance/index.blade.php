@extends("layouts.admin")

@section("title", "Finanzas")

@section('content')
    <finance-index url="{{ url('/admin/finances') }}" :drivers="{{ $drivers }}" :passengers="{{ $passengers }}"></finance-index>
@endsection