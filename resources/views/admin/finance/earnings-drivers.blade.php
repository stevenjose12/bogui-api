@extends("layouts.admin")

@section("title", "Ganancias de los Conductores")

@section('content')
	<article class="row">
	    <div class="row">
	        <div class="col s12 center-align">
	            <h1>Reporte de Ganancias por Conductor</h1>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col s12">
	            <a href="finances/weekly-report-bogui-excel-generate" class="btn btn-back" style="position: unset;">
	                <label class="label-back-cont">Generar Reporte</label>
	            </a>
	        </div>
        </div>
		<finance-earnings-driver url="{{ url('/admin/finances') }}" :drivers="{{ $drivers }}"></finance-earnings-driver>
	</article>
@endsection