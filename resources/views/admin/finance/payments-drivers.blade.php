@extends("layouts.admin")

@section("title", "Pagos a los Conductores")

@section('content')
	<article class="row">
	    <div class="row">
	        <div class="col s12 center-align">
	            <h1>Reporte de Pagos a los Conductores</h1>
	        </div>
	    </div>
        <finance-payments-driver url="{{ url('/admin/finances') }}"></finance-payments-driver>
	</article>
@endsection