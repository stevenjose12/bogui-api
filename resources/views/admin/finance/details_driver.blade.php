@extends('layouts.admin')

@section("title", "Detalles Ganancias del Conductor")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Detalles del Conductor {{ $driver->id }}</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <finance-details-driver
                        :set-form="{{ $driver }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/earnings-drivers') }}"
                        url-base="{{ url('/') }}"
                    ></finance-details-driver>
                </div>
            </div>
        </div>
    </article>

@endsection