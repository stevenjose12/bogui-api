@extends("layouts.admin")

@section("title", "Vehiculos")

@section('content')

<vehicles-index url="{{ url('/admin/vehicles') }}" :set-table="{{ $vehicles }}"></vehicles-index>

@endsection
