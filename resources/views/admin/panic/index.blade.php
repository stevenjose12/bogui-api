@extends("layouts.admin")

@section("title", "Alertas de emergencia")

@section('content')

<panic-index url="{{ url('/admin/panics') }}" panics="{{ $panics }}"></panic-index>

@endsection
