@extends("layouts.admin")

@section("title", "Informacion")

@section('content')

    <information-index url="{{ url('/admin/information') }}" :set-table="{{ $information }}"></information-index>

@endsection