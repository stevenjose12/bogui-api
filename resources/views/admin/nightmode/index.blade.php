@extends("layouts.admin")

@section("title", "Modo Nocturno")

@section('content')

    <nightmode-index url="{{ url('/admin/nightmode') }}" :set-table="{{ $nightmode }}"></nightmode-index>

@endsection