@extends("layouts.admin")

@section("title", "Notificaciones")

@section('content')

    <notifications-index url="{{ url('/admin/notifications') }}" :set-table="{{ $notifications }}"></notifications-index>

@endsection
