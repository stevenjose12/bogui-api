@extends('layouts.admin')

@section("title", "Detalle de Ruta")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Detalles de la Ruta {{ $route->id }}</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <routes-form
                        :set-form="{{ $route }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/users-routes') }}"
                        url-base="{{ url('/') }}"
                    ></routes-form>
                </div>
            </div>
        </div>
    </article>

@endsection
