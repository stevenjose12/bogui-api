@extends("layouts.admin")

@section("title", "Viajes de Conductores")

@section('content')

<article class="row">
    <div class="row">
        <div class="col s12 center-align">
            <h1>Viajes de Conductores</h1>
        </div>
    </div>
    <div class="row">   
        <div class="col s12">
            <div class="row">
				<routes-read-users url="{{ url('/admin/routes') }}" :routes="{{ $routes }}"></routes-read-users>
			</div>
		</div>
	</div>
</article>

@endsection