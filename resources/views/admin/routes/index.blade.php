@extends("layouts.admin")

@section("title", "Creación de Rutas")

@section('content')

<routes-index url="{{ url('/admin/routes') }}" :routes="{{ $routes }}"></routes-index>

@endsection