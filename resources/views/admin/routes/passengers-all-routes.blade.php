@extends("layouts.admin")

@section("title", "Viajes de Pasajeros")

@section('content')
<article class="row">
    <div class="row">
        <div class="col s12 center-align">
            <h1>Viajes de Pasajeros</h1>
        </div>
    </div>
    <div class="row">   
        <div class="col s12">
            <div class="row">
				<routes-read-passengers url="{{ url('/admin/routes') }}" :routes="{{ $routes }}"></routes-read-passengers>
			</div>
		</div>
	</div>
</article>

@endsection