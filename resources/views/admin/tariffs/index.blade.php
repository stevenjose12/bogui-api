@extends("layouts.admin")

@section("title", "Tarifas")

@section('content')

    <tariffs-index url="{{ url('/admin/tariffs') }}" url2="{{ url('/admin/types-tariff') }}" :set-table="{{ $tariffs }}" :set-type="{{ $types }}" :set-commission="{{ $commissions }}"></tariffs-index>

@endsection
