@extends('layouts.admin')

@section("title", "Edición de Promocion")

@section('content')

    <article class="row">
        <div class="row">
            <div class="col s12 center-align">
                <h1>Editar Promociones</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col s12">
                <div class="row">
                    <promotions-form
                        :set-form="{{ $promotions }}"
                        url="{{ url('/admin') }}"
                        url-back="{{ url('admin/promotions') }}"
                        url-base="{{ url('/') }}"
                    ></promotions-form>
                </div>
            </div>
        </div>
    </article>

@endsection