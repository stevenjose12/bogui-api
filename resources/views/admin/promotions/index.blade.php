@extends("layouts.admin")

@section("title", "Creación de Promocion")

@section('content')

<promotions-index url="{{ url('/admin/promociones') }}" :promotions="{{ $promotions }}"></promotions-index>

@endsection