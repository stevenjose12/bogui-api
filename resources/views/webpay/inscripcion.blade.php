<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Bogui</title>
</head>
<body>	
	<script src="{{ \Request::getSchemeAndHttpHost() }}:11021/socket.io/socket.io.js" type="text/javascript"></script>
	<script>
		window.onload = function() {
			var Socket = io('{{ \Request::getSchemeAndHttpHost() }}:11021');
			var data = JSON.parse('{!! json_encode($data) !!}');
			Socket.emit('webpay-inscripcion',data)
		}		
	</script>
</body>
</html>