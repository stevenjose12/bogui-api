@extends('page.layouts.master')

@section('title')
	@lang('Page.Nosotros.Title')
@stop

@section('content')
	<div class="contenido" id="nosotros">
		
		{{-- Que es Bogui --}}

		<div class="que-es">
			<div class="row">
				<div class="col-md-6">
					<div class="container-que-es">
						<h1>@lang('Page.Nosotros.QueEs')</h1>
						<p>{!! nl2br(\App::getLocale() == 'es' ? $nosotros->content : $nosotros->content_english) !!}</p>
						<div class="text-center">
							<a href="{{ URL('/solicitar') }}">
								@lang('Page.Nosotros.ComoPedir') <i class="fa fa-chevron-right"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/nosotros1.png') }}
				</div>
			</div>
		</div>

		{{-- Misión --}}

		<div id="mision" class="mision">
			<div class="row">
				<div class="col-md-6 mision-bg" style="background-image: url({{ URL('page-public/img/beneficios_conductor.jpg') }})">
					
				</div>
				<div class="col-md-6">
					<div class="container-mision">
						<h1>@lang('Page.Nosotros.Mision')</h1>
						<p>{!! nl2br(\App::getLocale() == 'es' ? $nosotros->mission : $nosotros->mission_english) !!}</p>
					</div>
				</div>
			</div>
		</div>

		{{-- Beneficios --}}

		<div class="beneficios">
			<div class="row">
				<div class="col-lg-6">
					<h1>@lang('Page.Nosotros.BeneficiosCon') <strong>Bogui</strong></h1>
					<p>{!! nl2br(\App::getLocale() == 'es' ? $nosotros->benefits : $nosotros->benefits_english) !!}</p>
					<ul>
						@foreach($beneficios as $beneficio)
							<li><strong>- {{ \App::getLocale() == 'es' ? $beneficio->title : $beneficio->title_english }},</strong> {!! nl2br(\App::getLocale() == 'es' ? $beneficio->content : $beneficio->english) !!}</li>
						@endforeach
					</ul>
				</div>
				<div class="col-lg-6">
					{{ HTML::Image('page-public/img/phone2.jpg') }}
				</div>
			</div>
		</div>

		{{-- Bogui cerca de ti --}}

		<div class="cerca" style="background-image: url({{ URL('page-public/img/nosotros2.png') }})">
			<h1>@lang('Page.Nosotros.NoImporta') <strong>@lang('Page.Nosotros.Siempre') <span class="red">Bogui @lang('Page.Nosotros.Cerca')</span></strong></h1>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			var hash = window.location.hash.substr(1);
			if (hash != '') {
				$('html, body').animate({
			        scrollTop: $('#' + hash).offset().top - 35
			    }, 500);
			}
		});
	</script>
@stop