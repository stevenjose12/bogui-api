@extends('page.layouts.master')

@section('title')
	@lang('Page.BeneficiosConductor.Title')
@stop

@section('content')
	<div class="contenido" id="beneficios-conductor">
		
		{{-- Beneficios --}}

		<div class="beneficios">
			<div class="row">
				<div class="col-md-6">
					<h1>@lang('Page.BeneficiosConductor.BeneficiosDeSer') <strong>@lang('Page.BeneficiosConductor.ConductorBogui')</strong></h1>
					<p>@lang('Page.BeneficiosConductor.Preocupa')</p>
					<ul>
						@foreach($beneficios as $beneficio)
							<li><strong>{{ \App::getLocale() == 'es' ? $beneficio->title : $beneficio->title_english }}</strong> {{ \App::getLocale() == 'es' ? $beneficio->content : $beneficio->english }}</li>
						@endforeach
					</ul>
					<p class="text-center bold">@lang('Page.BeneficiosConductor.Descarga')</p>
				</div>
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/beneficios_conductor.png') }}
				</div>
			</div>
		</div>

		{{-- Tú pones las ganancias --}}

		<div class="ganancias" style="background-image: url({{ URL('page-public/img/beneficios_conductor.jpg') }})">
			<div class="container-mensaje">
				{{ HTML::Image('page-public/img/icons/arrow_white.png') }}
				<h1><strong>@lang('Page.BeneficiosConductor.TuPones')</strong></h1>
				<h1>@lang('Page.BeneficiosConductor.NosotrosPonemos')</h1>
				<h1><strong>@lang('Page.BeneficiosConductor.Tecnologia')</strong></h1>
			</div>
		</div>
	</div>
@stop