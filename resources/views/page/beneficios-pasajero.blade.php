@extends('page.layouts.master')

@section('title')
	@lang('Page.BeneficiosPasajero.Title')
@stop

@section('content')
	<div class="contenido" id="beneficios-pasajero">
		
		{{-- Beneficios --}}

		<div class="beneficios">
			<div class="row">
				<div class="col-md-6">
					<h1><strong>@lang('Page.BeneficiosPasajero.Beneficios')</strong> @lang('Page.BeneficiosPasajero.DeViajar') <strong>Bogui</strong></h1>
					<p>@lang('Page.BeneficiosPasajero.PideTuBogui')</p>
					<p class="buen-viaje">@lang('Page.BeneficiosPasajero.BuenViaje')</p>
					@if (!Auth::check())
						<a href="{{ URL('/register?seccion=1') }}">
							<button class="btn btn-default">
								@lang('Page.BeneficiosPasajero.Registrate')
							</button>
						</a>
					@endif
				</div>
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/pedir.jpg') }}
				</div>
			</div>
		</div>

		{{-- Lista Beneficios --}}

		<div class="lista-beneficios">
			<div class="row">
				@foreach($beneficios as $key => $beneficio)
					<div class="col-md-3">
						{{ HTML::Image('page-public/img/icons/beneficios'.($key + 1).'.svg') }}
						<h2>{{ \App::getLocale() == 'es' ? $beneficio->title : $beneficio->title_english }}</h2>
						<p>{!! nl2br(\App::getLocale() == 'es' ? $beneficio->content : $beneficio->english) !!}</p>
					</div>
				@endforeach
			</div>
		</div>

		{{-- Viajar Seguro --}}

		<div class="viajar-seguro" style="background-image: url({{ URL('page-public/img/beneficios.jpg') }})">
			<div class="container-mensaje">
				{{ HTML::Image('page-public/img/icons/arrow_white.png') }}
				<h1><strong>@lang('Page.BeneficiosPasajero.ViajaSeguro')</strong></h1>
				<h1>@lang('Page.BeneficiosPasajero.ViajaCon') <strong>Bogui</strong></h1>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('img[src$=".svg"]').each(function() {
		        var $img = jQuery(this);
		        var imgURL = $img.attr('src');
		        var attributes = $img.prop("attributes");

		        $.get(imgURL, function(data) {

		            var $svg = jQuery(data).find('svg');

		            $svg = $svg.removeAttr('xmlns:a');

		            $.each(attributes, function() {
		                $svg.attr(this.name, this.value);
		            });

		            $img.replaceWith($svg);
		        }, 'xml');
		    });

		    setTimeout(function() {
		    	$('svg > title').remove();
		    });
		});
	</script>
@stop