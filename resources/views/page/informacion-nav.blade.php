<div class="nav-informacion">
	<h3>@lang('Page.Informacion.Title')</h3>
	<ul>
		<li v-on:click="seccion = 1" :class="{ active: seccion == 1 }">
			<i class="fa fa-chevron-left"></i>
			@lang('Page.Informacion.Terminos')
		</li>
		<li v-on:click="seccion = 2" :class="{ active: seccion == 2 }">
			<i class="fa fa-chevron-left"></i>
			@lang('Page.Informacion.Privacidad')
		</li>
	</ul>
</div>