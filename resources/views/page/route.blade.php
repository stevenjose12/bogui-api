<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
	{{ HTML::Style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::Style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::Style('bower_components/sweetalert/dist/sweetalert.css') }}
	{{ HTML::Style('bower_components/hold-on/src/css/HoldOn.min.css') }}
	{{ HTML::Style('css/ruta.css') }}
	<title>Bogui | Visualización de ruta</title>
</head>
<body>
	<div id="app">
	<view-route :route="{{ isset($route) ? $route : '""' }}" :driver="{{ isset($driver) ? $driver : '""' }}" :id="{{ isset($id) ? $id : '""' }}"></view-route>
	</div>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyB2AYKCUTc1BJJaL_SVBT7TwTPI85lCW5A&libraries=places"></script>
	{{ HTML::Script('bower_components/jquery/dist/jquery.min.js') }}
	{{ HTML::Script('bower_components/bootstrap/dist/js/bootstrap.min.js') }}
	{{ HTML::Script('bower_components/vue/dist/vue.min.js') }}
	{{ HTML::Script('bower_components/axios/dist/axios.min.js') }}
	{{ HTML::Script('bower_components/sweetalert/dist/sweetalert.min.js') }}
	{{ HTML::Script('bower_components/hold-on/src/js/HoldOn.min.js') }}
	{{ HTML::Script('page-public/js/loader.js') }}
	{{ HTML::Script('page-public/js/dropdown.js') }}
	{{ HTML::Script('page-public/js/scroll.js') }}
	{{ HTML::Script('js/ruta.js') }}
</body>
</html>