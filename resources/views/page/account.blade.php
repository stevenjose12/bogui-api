@extends('page.layouts.master')

@section('title')
	@lang('Page.Account.Title')
@stop

@section('content')
	<div class="contenido" id="account">
		@if (Auth::user()->level == '3')
			<div class="container-saludo">
				{{ HTML::Image('img/logo_pasajero.png') }}
				<h1>@lang('Page.Account.Bienvenido',['user' => Auth::user()->person->name.' '.Auth::user()->person->lastname])</h1>
				<h3>@lang('Page.Account.Registrado')</h3>
				<p>@lang('Page.Account.DescriptionPasajero')</p>
				<div class="row tiendas">
{{-- 					<div class="col-md-6 appstore">
						<a href="{{ $_pasajero->apple }}">
							{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/apple.png' : 'page-public/img/icons/apple_en.png') }}
						</a>
					</div> --}}
					<div class="col-md-6 offset-md-3">
						<a href="{{ $_pasajero->google }}">
							{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/google.png' : 'page-public/img/icons/google_en.png') }}
						</a>
					</div>
				</div>
			</div>
		@elseif (Auth::user()->level == '2')
			<div class="container-saludo">
				{{ HTML::Image('img/logo_conductor.png') }}
				<h1>@lang('Page.Account.Bienvenido',['user' => Auth::user()->person->name.' '.Auth::user()->person->lastname])</h1>
				<h3>@lang('Page.Account.Registrado')</h3>
				<p>@lang('Page.Account.DescriptionConductor')</p>
				<div class="row tiendas">
{{-- 					<div class="col-md-6 appstore">
						<a href="{{ $_conductor->apple }}">
							{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/apple.png' : 'page-public/img/icons/apple_en.png') }}
						</a>
					</div> --}}
					<div class="col-md-6 offset-md-3">
						<a href="{{ $_conductor->google }}">
							{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/google.png' : 'page-public/img/icons/google_en.png') }}
						</a>
					</div>
				</div>
			</div>
		@endif
	</div>
@stop