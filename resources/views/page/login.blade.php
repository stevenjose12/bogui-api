@extends('page.layouts.master')

@section('title')
	@lang('Page.Login.Title')
@stop

@section('content')
	<div class="contenido" id="login">
		
		{{ Form::open(['v-on:submit.prevent' => 'submit()']) }}
			<div class="container-form">
				<div class="container-mensaje text-center">
					<h2>@lang('Page.Login.Title')</h2>
				</div>
				<div class="container-login">
					<div class="form-group">
						{{ Form::text('email','',['class' => 'form-control','placeholder' => Lang::get('Page.Login.Email'),'v-model' => 'form.email']) }}
					</div>
					<div class="form-group">
						{{ Form::password('password',['class' => 'form-control','placeholder' => Lang::get('Page.Login.Password'),'v-model' => 'form.password']) }}
					</div>
					<div style="padding-bottom: 15px;">
						<div class="form-check-inline">
						  <label class="form-check-label">
						    <input type="radio" value="3" v-model="form.type" class="form-check-input" name="radio-soy"> Pasajero
						  </label>
						</div>
						<div class="form-check-inline">
						  <label class="form-check-label">
						    <input type="radio" value="2" v-model="form.type" class="form-check-input" name="radio-soy"> Conductor
						  </label>
						</div>
					</div>
					<a href="{{ URL('reset') }}">
						<p>@lang('Page.Login.Olvide')</p>
					</a>
					<div class="text-center">
						<button class="btn btn-default" type="submit">
							@lang('Page.Login.Aceptar')
						</button>
					</div>
				</div>
				<div class="container-mensaje text-center">
					<p>@lang('Page.Login.Cuenta')</p>
					<a href="{{ URL('register') }}">
						<h3>@lang('Page.Login.Registro')</h3>
					</a>
				</div>
			</div>			
		{{ Form::close() }}

	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		var vue = new Vue({
			el: '#login',
			data: {
				form: {
					type: 3
				}
			},
			methods: {
				submit() {
					setLoader();
					axios.post('{{ URL('home/login') }}',this.form)
						.then(function(res) {
							if (res.data.result) {
								window.location = res.data.url;
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				}
			}
		})
	</script>
@stop