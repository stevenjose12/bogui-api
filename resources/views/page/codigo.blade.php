@extends('page.layouts.master')

@section('title')
	@lang('Page.Codigo.Title')
@stop

@section('content')
	<div class="contenido" id="codigo">
		
		{{ Form::open(['v-on:submit.prevent' => 'submit()']) }}
			<div class="container-form">
				<p>@lang('Page.Codigo.SMS')</p>
				<div class="form-group">
					{{ Form::text('codigo','',['class' => 'form-control','placeholder' => Lang::get('Page.Codigo.Codigo'),'v-model' => 'form.codigo']) }}
				</div>
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-default" type="button" v-on:click="reenviar()">
							@lang('Page.Codigo.Reenviar')
						</button>
					</div>
					<div class="col-md-6">
						<button class="btn btn-default" type="submit">
							@lang('Page.Codigo.Continuar')
						</button>
					</div>
				</div>
			</div>
		{{ Form::close() }}

	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		var vue = new Vue({
			el: '#codigo',
			data: {
				form: {
					codigo: ''
				}
			},
			methods: {
				reenviar() {
					setLoader();
					axios.post('{{ URL('codigo/reenviar') }}')
						.then(function(res) {
							if (res.data.result) {
								swal('','{{ Lang::get('Page.Codigo.Success') }}','success');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				},
				submit() {
					setLoader();
					axios.post('{{ URL('codigo') }}',this.form)
						.then(function(res) {
							if (res.data.result) {
								window.location.href = res.data.url;
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				}
			}
		})
	</script>
@stop