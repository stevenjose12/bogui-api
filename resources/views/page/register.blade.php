@extends('page.layouts.master')

@section('title')
	@lang('Page.Register.Title')
@stop

@section('content')
	<div class="contenido" id="register" v-cloak>

		<div v-if="seccion == 0">
			<div class="select-form">
				<div class="container-form">
					<div class="container-mensaje text-center">
						<h2>@lang('Page.Register.RegisterComo')</h2>
					</div>
					<div class="container-login">
						<div class="row">
							<div class="col-md-6 text-center">
								{{ HTML::Image('page-public/img/icons/conductor.png') }}
								<button class="btn btn-default" v-on:click="seccion = 2; setDatepicker()">
									@lang('Page.Register.Conductor')
								</button>
							</div>
							<div class="col-md-6 text-center">
								{{ HTML::Image('page-public/img/icons/pasajero.png') }}
								<button class="btn btn-default" v-on:click="seccion = 1; setDatepicker()">
									@lang('Page.Register.Pasajero')
								</button>
							</div>
						</div>
					</div>
					<div class="container-mensaje text-center">
						<p>@lang('Page.Register.Cuenta')</p>
						<a href="{{ URL('login') }}">
							<h3>@lang('Page.Register.Login')</h3>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div v-if="seccion == 1" class="formulario">
			<button class="btn btn-default btn-regresar" v-on:click="seccion = 0">
				@lang('Page.Register.Regresar')
			</button>

			{{ Form::open(['v-on:submit.prevent' => 'submitPasajero()']) }}
				<div class="row row-avatar border-red-bottom" v-on:click="seccion = 2; setDatepicker()">
					<div class="col-xs-4">
						{{ HTML::Image('page-public/img/icons/conductor.png','',['class' => 'img-avatar']) }}
					</div>
					<div class="col-xs-6">
						<p class="p-dinero">@lang('Page.Register.GanarDinero')</p>
						<h4 class="p-red">@lang('Page.Register.RegisterConductor')</h4>
					</div>
					<div class="col-xs-2">
						{{ HTML::Image('page-public/img/icons/arrow_red.png','',['class' => 'arrow-red']) }}
					</div>
				</div>
				<div class="row row-avatar">
					<div class="col-xs-4">
						{{ HTML::Image('page-public/img/icons/pasajero.png','',['class' => 'img-avatar']) }}
					</div>
					<div class="col-xs-8">
						<h4 class="h4-conductor">@lang('Page.Register.RegisterPasajero')</h4>
					</div>
				</div>
				<div class="container-border">
					
					<div class="photo" :style="{ backgroundImage: 'url(' + (photo_preview_passenger != '' ? photo_preview_passenger : '{{ URL('page-public/img/icons/pasajero.png') }}') + ')' }"></div>
					
					<div class="form-group form-group-photo" :class="{ active: form_pasajero.photo }">
						{{ Form::label('photo',Lang::get('Page.Register.SubirFotoPerfil')) }}
						{{ Form::file('photo',['class' => 'form-control','v-on:change' => 'changeFile($event,1,"photo")']) }}						
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							{{ Form::text('nombre','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Nombre'),'v-model' => 'form_pasajero.nombre']) }}
						</div>
						<div class="form-group col-md-6">
							{{ Form::text('apellido','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Apellido'),'v-model' => 'form_pasajero.apellido']) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::text('email','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Email'),'v-model' => 'form_pasajero.email']) }}
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::select('code',$paises,null,['class' => 'form-control','placeholder' => Lang::get('Page.Register.Codigo'),'v-model' => 'form_pasajero.code']) }}
							</div>
						</div>
						<div class="col-md-9">
							<div class="form-group">
								{{ Form::number('telefono','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Telefono'),'v-model' => 'form_pasajero.telefono']) }}
							</div>
						</div>
					</div>					
					<div class="form-group">
						{{ Form::select('document_type',[
							'1' => 'RUT',
							'2' => Lang::get('Page.Register.Pasaporte'),
							'3' => Lang::get('Page.Register.Otro')
						],null,['class' => 'form-control','placeholder' => Lang::get('Page.Register.TipoDocumento'),'v-model' => 'form_pasajero.document_type']) }}
					</div>
					<div class="form-group">
						{{ Form::text('document','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Document'),'v-model' => 'rut_passenger']) }}
					</div>
					<div class="form-group" :class="{ validate: checkValidator('expiration_document')  }">
						{{ Form::text('expiration_document','',['data-model' => 'expiration_document', 'class' => 'form-control datepicker_expiration', 'placeholder' => "Expiración del RUT o Pasaporte", 'v-model' => 'form_pasajero.expiration_document']) }}
					</div>
					<div class="form-group">
						{{ Form::password('password',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Password'),'v-model' => 'form_pasajero.password']) }}
					</div>
					<div class="form-group">
						{{ Form::password('password_confirmation',['class' => 'form-control','placeholder' => Lang::get('Page.Register.RepeatPassword'),'v-model' => 'form_pasajero.password_confirmation']) }}
					</div>
					{{-- <div class="form-group form-group-file" :class="{ active: form_pasajero.document_photo }">
						{{ Form::label('document_photo',Lang::get('Page.Register.SubirFotoRut')) }}
						{{ Form::file('document_photo',['class' => 'form-control','v-on:change' => 'changeFile($event,1,"document_photo")']) }}						
					</div> --}}

					<p>@lang('Page.Register.AlContinuar') <a href="{{ URL('informacion?seccion=1') }}" target="_blank">@lang('Page.Register.Terminos')</a> @lang('Page.Register.Reconoces') <a href="{{ URL('informacion?seccion=2') }}" target="_blank">@lang('Page.Register.Politica')</a></p>
					<div class="text-center">
						<button class="btn btn-default">
							@lang('Page.Register.Aceptar')
						</button>
					</div>
				</div>
			{{ Form::close() }}
		</div>

		<div v-if="seccion == 2" class="formulario">
			<button class="btn btn-default btn-regresar" v-on:click="seccion = 0; setYear()" v-if="seccion_conductor == 1">
				@lang('Page.Register.Regresar')
			</button>

			<button class="btn btn-default btn-regresar" v-if="seccion_conductor == 2" v-on:click="seccion_conductor = 1; setDatepicker()">
				@lang('Page.Register.Regresar')
			</button>

			<button class="btn btn-default btn-regresar" v-if="seccion_conductor == 3" v-on:click="seccion_conductor = 2; setDatepicker(); iniciar();">
				@lang('Page.Register.Regresar')
			</button>

			{{ Form::open(['v-on:submit.prevent' => 'submit()']) }}
				<div class="row row-avatar border-red-bottom" v-on:click="seccion = 1; setDatepicker()" v-if="seccion_conductor == 1">
					<div class="col-xs-4">
						{{ HTML::Image('page-public/img/icons/pasajero.png','',['class' => 'img-avatar']) }}
					</div>
					<div class="col-xs-6">
						<p>@lang('Page.Register.Viajar')</p>
						<h4 class="p-red">@lang('Page.Register.RegisterPasajero')</h4>
					</div>
					<div class="col-xs-2">
						{{ HTML::Image('page-public/img/icons/arrow_red.png','',['class' => 'arrow-red']) }}
					</div>
				</div>
				<div class="row row-avatar">
					<div class="col-xs-4">
						{{ HTML::Image('page-public/img/icons/conductor.png','',['class' => 'img-avatar']) }}
					</div>
					<div class="col-xs-8">
						<h4 class="h4-conductor">@lang('Page.Register.RegisterConductor')</h4>
					</div>
				</div>
				<div class="container-border">

					<div v-if="seccion_conductor == 1">
						<div class="photo" :style="{ backgroundImage: 'url(' + (photo_preview_driver != '' ? photo_preview_driver : '{{ URL('page-public/img/icons/conductor.png') }}') + ')' }"></div>
						
						<div class="form-group form-group-photo" :class="{ active: form.photo, validate: checkValidator('photo') }">
							{{ Form::label('photo',Lang::get('Page.Register.SubirFotoPerfil')) }}
							{{ Form::file('photo',['class' => 'form-control', 'v-on:change' => 'changeFile($event,2,"photo")']) }}						
						</div>
						<div class="row">
							<div class="form-group col-md-6" :class="{ validate: checkValidator('nombre')  }">
								{{ Form::text('nombre','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Nombre'),'v-model' => 'form.nombre']) }}
							</div>
							<div class="form-group col-md-6" :class="{ validate: checkValidator('apellido') }">
								{{ Form::text('apellido','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Apellido'),'v-model' => 'form.apellido']) }}
							</div>
						</div>
						<div class="form-group" :class="{ validate: checkValidator('email')  }">
							{{ Form::text('email','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Email'),'v-model' => 'form.email']) }}
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="{ validate: checkValidator('code')  }">
									{{ Form::select('code',$paises,null,['class' => 'form-control','placeholder' => Lang::get('Page.Register.Codigo'),'v-model' => 'form.code']) }}
								</div>
							</div>
							<div class="col-md-9">
								<div class="form-group" :class="{ validate: checkValidator('telefono')  }">
									{{ Form::number('telefono','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Telefono'),'v-model' => 'form.telefono']) }}
								</div>
							</div>
						</div>				
						<div class="form-group" :class="{ validate: checkValidator('document_type')  }">
							{{ Form::select('document_type',[
								'1' => 'RUT',
								'2' => Lang::get('Page.Register.Pasaporte'),
								'3' => Lang::get('Page.Register.Otro')
							],null,['class' => 'form-control','placeholder' => Lang::get('Page.Register.TipoDocumento'),'v-model' => 'form.document_type']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('document')  }">
							{{ Form::text('document','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Document'),'v-model' => 'rut_driver']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('password')  }">
							{{ Form::password('password',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Password'),'v-model' => 'form.password']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('password_confirmation')  }">
							{{ Form::password('password_confirmation',['class' => 'form-control','placeholder' => Lang::get('Page.Register.RepeatPassword'),'v-model' => 'form.password_confirmation']) }}
						</div>
						<div class="form-group form-group-file" :class="{ active: form.document_photo, validate: checkValidator('document_photo') }">
							{{ Form::label('document_photo',Lang::get('Page.Register.SubirFotoRut')) }}
							{{ Form::file('document_photo',['class' => 'form-control','v-on:change' => 'changeFile($event,2,"document_photo")']) }}						
						</div>
						{{-- <div class="form-group" :class="{ validate: checkValidator('expiration_document')  }">
							{{ Form::text('expiration_document','',['data-model' => 'expiration_document', 'class' => 'form-control datepicker_expiration','placeholder' => "Expiración del RUT o Pasaporte",'v-model' => 'form.expiration_document']) }}
						</div> --}}
						<div class="form-group form-group-file" :class="{ active: form.certificado, validate: checkValidator('certificado') }">
							{{ Form::label('certificado',Lang::get('Page.Register.SubirFotoAntecedentes')) }}
							{{ Form::file('certificado',['class' => 'form-control','v-on:change' => 'changeFile($event,2,"certificado")']) }}						
						</div>
						{{-- <div class="form-group" :class="{ validate: checkValidator('expiration_criminal_records')  }">
							{{ Form::text('expiration_criminal_records','',['data-model' => 'expiration_criminal_records', 'class' => 'form-control datepicker_expiration','placeholder' => "Expiración del Certificado de Antecedentes",'v-model' => 'form.expiration_criminal_records']) }}
						</div> --}}
						<div class="text-center">
							<button class="btn btn-default" type="button" v-on:click="validate(1)">
								@lang('Page.Register.Continuar')
							</button>
						</div>
					</div>

					<div v-if="seccion_conductor == 2">
						<h3 class="info">@lang('Page.Register.InfoVehiculo')</h3>
						
						<div class="photo" :style="{ backgroundImage: 'url(' + (photo_preview_vehicle != '' ? photo_preview_vehicle : '{{ URL('page-public/img/icons/auto.png') }}') + ')' }"></div>
						
						<div class="form-group form-group-photo" :class="{ active: form.photo_vehicle, validate: checkValidator('photo_vehicle') }">
							{{ Form::label('vehicle_photo',Lang::get('Page.Register.SubirFotoVehiculo')) }}
							{{ Form::file('vehicle_photo',['class' => 'form-control', 'v-on:change' => 'changeFile($event,2,"photo_vehicle")']) }}						
						</div>
						
						<div class="form-group" :class="{ validate: checkValidator('marca')  }">
							{{ Form::text('marca','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Marca'),'v-model' => 'form.marca']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('modelo')  }">
							{{ Form::text('modelo','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Modelo'),'v-model' => 'form.modelo']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('patente')  }">
							{{ Form::text('patente','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Patente'),'v-model' => 'form.patente']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('year')  }">
							<select id="year" name="year" class="form-control">
								@for($i = 2000; $i <= date("Y"); $i++)
								<option value="{{ $i }}">{{ $i }}</option>
								@endfor
							</select>
							{{-- Form::number('year', '', ['class' => 'yearpicker form-control', 'placeholder' => Lang::get('Page.Register.Year'), 'id' => 'year', 'readonly' => 'true']) --}}
						</div>
						<div class="form-group form-group-file" :class="{ active: form.licencia, validate: checkValidator('licencia') }">
							{{ Form::label('licencia',Lang::get('Page.Register.SubirFotoLicencia')) }}
							{{ Form::file('licencia',['class' => 'form-control', 'v-on:change' => 'changeFile($event,2,"licencia")']) }}						
						</div>
						{{-- <div class="form-group" :class="{ validate: checkValidator('expiration_criminal_records')  }">
							{{ Form::text('expiration_license','',['data-model' => 'expiration_license', 'class' => 'form-control datepicker_expiration','placeholder' => "Expiración de Licencia de Conducir",'v-model' => 'form.expiration_license']) }}
						</div> --}}
						<div class="form-group form-group-file" :class="{ active: form.permiso, validate: checkValidator('permiso') }">
							{{ Form::label('permiso',Lang::get('Page.Register.SubirFotoPermiso')) }}
							{{ Form::file('permiso',['class' => 'form-control', 'v-on:change' => 'changeFile($event,2,"permiso")']) }}						
						</div>
						<div class="form-group" :class="{ validate: checkValidator('expiration_permit')  }">
							{{ Form::text('expiration_permit','',['data-model' => 'expiration_permit', 'class' => 'form-control datepicker_expiration','placeholder' => "Expiración del Permiso de Circulación",'v-model' => 'form.expiration_permit']) }}
						</div>
						<div class="text-center">
							<button class="btn btn-default" type="button" v-on:click="validate(2)">
								@lang('Page.Register.Continuar')
							</button>
						</div>
					</div>

					<div v-if="seccion_conductor == 3">
						<h3 class="info">@lang('Page.Register.DatosBancarios')</h3>
						
						<div class="form-group" :class="{ validate: checkValidator('banco') }">
							{{ Form::select('banco',$bancos,'',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Banco'),'v-model' => 'form.banco']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('type_account') }">
							{{ Form::select('type_account',$cuentas,'',['class' => 'form-control','placeholder' => Lang::get('Page.Register.TipoCuenta'),'v-model' => 'form.type_account']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('rut_banco') }">
							{{ Form::text('rut_banco','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.BancoRut'),'v-model' => 'form.rut_banco']) }}
						</div>
						<div class="form-group" :class="{ validate: checkValidator('account') }">
							{{ Form::number('account','',['class' => 'form-control','placeholder' => Lang::get('Page.Register.Account'),'v-model' => 'form.account']) }}
						</div>

						<p>@lang('Page.Register.AlContinuar') <a href="{{ URL('informacion?seccion=1') }}" target="_blank">@lang('Page.Register.Terminos')</a> @lang('Page.Register.Reconoces') <a href="{{ URL('informacion?seccion=2') }}" target="_blank">@lang('Page.Register.Politica')</a></p>
						<div class="text-center">
							<button class="btn btn-default" type="button" v-on:click="validate(3)">
								@lang('Page.Register.Aceptar')
							</button>
						</div>
					</div>					
				</div>
			{{ Form::close() }}
		</div>
	</div>
@stop

@section('styles')
	<style>
		.form-control:disabled, .form-control[readonly] {
			background-color: #fff !important;
		}
	</style>
	{{ HTML::Style('page-public/css/yearpicker.css') }}
	{{ HTML::Style('bower_components/jquery-ui/themes/base/jquery-ui.min.css') }}
@stop

@section('scripts')
	{{ HTML::Script('bower_components/jquery-ui/jquery-ui.min.js') }}
	{{ HTML::Script('bower_components/moment/min/moment.min.js') }}
	{{ HTML::Script('page-public/js/yearpicker.js') }}
	{{ HTML::Script('page-public/js/datepicker.js') }}
	{{ HTML::Script(\Request::getSchemeAndHttpHost().':11021/socket.io/socket.io.js') }}
	{{-- HTML::Script('http://192.168.1.4:11020/socket.io/socket.io.js') --}}
	<script type="text/javascript">
		var _socket = io.connect('{{ \Request::getSchemeAndHttpHost() }}' + ':11021');
		/*var _socket = io.connect('http://192.168.1.4:11020');*/

		var vue = new Vue({
			el: '#register',
			data: {
				seccion: '{{ Request::has('seccion') ? Request::get('seccion') : 0 }}',
				seccion_conductor: 1,
				RE_RUT: /((([0-9.]+){6})-([0-9K]+?))/g,
				form: {
					document_photo: '',
					code: '{{ Request::has('code') ? Request::get('code') : '' }}',
					document_type: '{{ Request::has('document_type') ? Request::get('document_type') : '' }}',
					photo: '',
					photo_preview: '',
					certificado: '', 
					permiso: '',
					licencia: '',
					photo_vehicle: '',
					photo_vehicle_preview: '',
					banco: '',
					type_account: '',
					nombre: '{{ Request::has('nombre') ? Request::get('nombre') : '' }}',
					apellido: '{{ Request::has('apellido') ? Request::get('apellido') : '' }}',
					telefono: '{{ Request::has('telefono') ? Request::get('telefono') : '' }}',
					document: '{{ Request::has('document') ? Request::get('document') : '' }}',
					email: '{{ Request::has('email') ? Request::get('email') : '' }}',
					year: '',
					// expiration_license: '',
					expiration_permit: '',
					//expiration_document: '',
					// expiration_criminal_records: ''
				},
				form_pasajero: {
					document_photo: '',
					code: '',
					document_type: '',
					photo: '',
					photo_preview: '',
					expiration_document: ''					
				},
				photo_preview_passenger: '',
				photo_preview_driver: '',
				photo_preview_vehicle: '',
				form_validator: [],
				rut_passenger: '',
				rut_driver: '',
				regex: '((([0-9.]+){6})-([0-9K]+?))',
				showMsg: false,
				msg: 'El RUT debe poseer el siguiente formato: XX.XXX.XXX-X o X.XXX.XXX-X, el ultimo valor puede ser K o del 0 al 9.'
			},
			created: function() {
				this.setDatepicker();
			},
			watch: {
			    rut_passenger (value){
			        if (this.form_pasajero.document_type == '1') {
			        	let check_regex = new RegExp(this.regex)
			            value = value.split('.').join("");
			            value = value.split(' ').join("");
		            	value = value.split('-').join("");
			            if (value.length == 8) {
		                    value = value.slice(0, 1) + "." + value.slice(1,4) + "." + value.slice(4,7) + '-' + value.slice(7,9);
			                if (check_regex.test(value)) {
			                    this.showMsg = false;
			                } else {
			                    this.showMsg = true;
			                }
			            } else if (value.length == 9) {
		                    value = value.slice(0, 2) + "." + value.slice(2,5) + "." + value.slice(5,8) + '-' + value.slice(8,10);
			                if (check_regex.test(value)) {
			                    this.showMsg = false;
			                } else {
			                    this.showMsg = true;
			                }
			            } else {
			                this.showMsg = true;
			            }
			        } else {
			            this.showMsg = false;
			        }
		            this.rut_passenger = value.toUpperCase()
		            this.form_pasajero.document = this.rut_passenger
			    },
			    rut_driver (value){
			        if (this.form.document_type == '1') {
			        	let check_regex = new RegExp(this.regex)
			            value = value.split('.').join("");
			            value = value.split(' ').join("");
		            	value = value.split('-').join("");
			            if (value.length == 8) {
		                    value = value.slice(0, 1) + "." + value.slice(1,4) + "." + value.slice(4,7) + '-' + value.slice(7,9);
			                if (check_regex.test(value)) {
			                    this.showMsg = false;
			                } else {
			                    this.showMsg = true;
			                }
			            } else if (value.length == 9) {
		                    value = value.slice(0, 2) + "." + value.slice(2,5) + "." + value.slice(5,8) + '-' + value.slice(8,10);
			                if (check_regex.test(value)) {
			                    this.showMsg = false;
			                } else {
			                    this.showMsg = true;
			                }
			            } else {
			                this.showMsg = true;
			            }
			        } else {
			            this.showMsg = false;
			            console.log('no es tipo RUT Driver')
			        }
		            this.rut_driver = value.toUpperCase()
		            this.form.document = this.rut_driver
			    }
			},		
			methods: {
				setDatepicker() {
					$(document).ready(function() {
						$('.datepicker_expiration').datepicker({
							maxDate: "+3600",
							minDate: "0",
							dateFormat: 'dd-mm-yy',
							changeYear: true,
							changeMonth: true
						}).change(function() {
							var model = $(this).attr('data-model');
							vue.form[model] = $(this).val();
							vue.form_pasajero[model] = $(this).val();
						});
					});
				},
				iniciar() {
					$(document).ready(function() {
						setTimeout(function() {
							$('.yearpicker').yearpicker({
								startYear: 2000,
								endYear: 2019
							});

							if (vue.form.year != '') {
								setTimeout(function() {
									$('#year').val(vue.form.year);
								},100);
							}
						},100);				
					});
				},
				setYear() {
					vue.form.year = $('#year').val();
				},
				dataURItoBlob(dataURI) {
				    // convert base64/URLEncoded data component to raw binary data held in a string
				    var byteString;
				    if (dataURI.split(',')[0].indexOf('base64') >= 0)
				        byteString = atob(dataURI.split(',')[1]);
				    else
				        byteString = unescape(dataURI.split(',')[1]);

				    // separate out the mime component
				    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

				    // write the bytes of the string to a typed array
				    var ia = new Uint8Array(byteString.length);
				    for (var i = 0; i < byteString.length; i++) {
				        ia[i] = byteString.charCodeAt(i);
				    }

				    return new Blob([ia], {type:mimeString});
				},
				changeFile(e,type,field) {
					var files = e.target.files || e.dataTransfer.files;
					var format = files[0].name.split('.');
					var permitidos = ['jpg','png','gif','jpeg'];
					if (permitidos.indexOf(format[format.length - 1].toLowerCase()) == -1) {
						swal('','{{ Lang::get('Page.Register.FormatoNoValido') }}','warning');
						return false;
					}
		            if (!files.length)
						return;
						
		            var reader = new FileReader();
					let tipo = format;
					
	            	switch(tipo) {
						case 'jpg':
							tipo = 'image/jpeg';
							break;

						case 'png':
							tipo = 'image/png';
							break;

						case 'gif':
							tipo = 'image/gif';
							break;

						case 'jpeg':
							tipo = 'image/jpeg';
							break;

						default: 
							tipo = 'image/jpeg';
							break;
					}
		            if (type == 1) {
		            	// this.form_pasajero[field] = files[0];
		            	reader.readAsDataURL(files[0]);
		            	reader.onload = function(file) {
				            vue.form_pasajero[field + '_preview'] = file.target.result;
				            if (field == 'photo') {
				            	vue.photo_preview_passenger = file.target.result
				            }
				            	var tempImg = new Image();
					            tempImg.src = reader.result;
					            tempImg.onload = function() {
								    var MAX_WIDTH = 1000;
								    var MAX_HEIGHT = 1000;
								    var tempW = tempImg.width;
								    var tempH = tempImg.height;

								    if (tempW > tempH) {
								        if (tempW > MAX_WIDTH) {
								        tempH *= MAX_WIDTH / tempW;
								        tempW = MAX_WIDTH;
								        }
								    } else {
								        if (tempH > MAX_HEIGHT) {
								        tempW *= MAX_HEIGHT / tempH;
								        tempH = MAX_HEIGHT;
								        }
								    }
								    var resizedCanvas = document.createElement('canvas');
								    resizedCanvas.width = tempW;
								    resizedCanvas.height = tempH;
								    var ctx = resizedCanvas.getContext("2d");
								    ctx.drawImage(this, 0, 0, tempW, tempH);
								    var dataURL = resizedCanvas.toDataURL(tipo);
								    vue.form_pasajero[field] = vue.dataURItoBlob(dataURL);
								}
				        }
		            }
		            else {
		            	// this.form[field] = files[0];
		            	reader.readAsDataURL(files[0]);
		            	reader.onload = function(file) {
				            vue.form[field + '_preview'] = file.target.result;
				            if (field == 'photo') {
				            	vue.photo_preview_driver = file.target.result
				            }
				            if (field == 'photo_vehicle') {
				            	vue.photo_preview_vehicle = file.target.result
				            }
				            var tempImg = new Image();
				            tempImg.src = reader.result;

			            	tempImg.onload = function() {
							    var MAX_WIDTH = 1000;
							    var MAX_HEIGHT = 1000;
							    var tempW = tempImg.width;
							    var tempH = tempImg.height;

							    if (tempW > tempH) {
							        if (tempW > MAX_WIDTH) {
							        tempH *= MAX_WIDTH / tempW;
							        tempW = MAX_WIDTH;
							        }
							    } else {
							        if (tempH > MAX_HEIGHT) {
							        tempW *= MAX_HEIGHT / tempH;
							        tempH = MAX_HEIGHT;
							        }
							    }
							    var resizedCanvas = document.createElement('canvas');
							    resizedCanvas.width = tempW;
							    resizedCanvas.height = tempH;
							    var ctx = resizedCanvas.getContext("2d");
							    ctx.drawImage(this, 0, 0, tempW, tempH);
							    var dataURL = resizedCanvas.toDataURL(tipo);
							    vue.form[field] = vue.dataURItoBlob(dataURL);
							}
				        }
		            }
				},
				submit() {
					var _document = this.form.document;
				    switch (this.form.document_type) {

				      case "1": // RUT
				        _document = _document.split('.').join("");
				        var documentValidate = _document.match(this.regex);
				        if (!documentValidate) {

				          swal('',"El formato de RUT no es válido (55555555-X)",'warning');
						  return;
						  
				        } else {

				        	if (_document.length == 9 || _document.length == 10) {
								this.rut_driver = _document
								
				        	} else {

				        	    swal("", "El formato de RUT no es válido (55555555-X)",'warning');
								this.showMsg = true;
								
				        	    return;
				        	}
						}
						
				        break;

				    }

				    var rut = this.form.document.replace(/[ .]/g, "").toLowerCase();

					var data = this.form;
					delete data['certificado_preview'];
					delete data['document_photo_preview'];
					delete data['licencia_preview'];
					delete data['permiso_preview'];
					delete data['photo_preview'];
					delete data['photo_vehicle_preview'];

					// data.document = rut;

					setLoader();
					// this.form.year = $('#year').val();
					axios.post(`{{ URL('register/conductor') }}`,this.createdFormData(data),{
		                headers: {
		                    'Content-Type': 'multipart/form-data'
		                }
		            })
						.then(function(res) {
							if (res.data.result) {
								swal('','{{ Lang::get('Page.Register.Success') }}','success');
								_socket.emit('new-user',{
									id: res.data.user_id
								});
								setTimeout(function() {
									window.location.href = res.data.url;
								},1000);
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				},
				submitPasajero() {
					var _document = this.form_pasajero.document;
	
				    switch (this.form_pasajero.document_type) {

				      case "1": // RUT
				      	_document = _document.split('.').join("");
				        var documentValidate = _document.match(this.regex);
				        if (!documentValidate) {

				          swal('',"El formato de RUT no es válido (55555555-X)",'warning');
						  return;
						  
				        }  else {

		                    if (_document.length == 9 || _document.length == 10) {
		                        this.rut_passenger = _document
		                    } else {
		                        swal("", "El formato de RUT no es válido (55555555-X)",'warning');
		                        this.showMsg = true;
		                        return;
		                    }
		                }
				        break;

				    }

				    var rut = this.form_pasajero.document.replace(/[ .]/g, "").toLowerCase();					

					var data = this.form_pasajero;
					delete data['document_photo_preview'];
					delete data['photo_preview'];

					setLoader();

					axios.post('{{ URL('register') }}',this.createdFormData(data),{
		                 headers: {
		                    'Content-Type': 'multipart/form-data'
		                }
		            })
						.then(function(res) {
							if (res.data.result) {
								swal('','{{ Lang::get('Page.Register.Success') }}','success');
								_socket.emit('new-pass',{
									id: res.data.user_id
								});
								setTimeout(function() {
									window.location.href = res.data.url;
								},1000);
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				},
				createdFormData(data) {
	                var formdata = new FormData();
	                for (var key in data) {
	                    formdata.append(key, data[key]);
	                }
	                return formdata;
	            },
	            checkValidator(name) {
	            	return this.form_validator.indexOf(name) != -1;
	            },
	            checkEmail(email) {
				    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  					return re.test(email);
	            },
	            validate(num) {

					this.form_validator = [];
					
					var continuar = true;
					
	            	switch(num) {
	            		case 1:
	            			var campos = [
	            				'nombre',
	            				'photo',
	            				'email',
	            				'apellido',
	            				'code',
	            				'telefono',
	            				'document_type',
	            				'document',
	            				'password',
	            				'password_confirmation',
	            				'document_photo',
	            				'certificado',
	            				//'expiration_document',
	            				// 'expiration_criminal_records'
	            			];
	            		break;

	            		case 2:
	            			var campos = [
	            				'photo_vehicle',
	            				'marca',
	            				'modelo',
	            				'patente',
	            				'year',
	            				'licencia',
	            				'permiso',
	            				'expiration_permit',
	            				// 'expiration_license'
	            			];
	            		break;
	            			

	            		case 3:
	            			var campos = [
	            				'banco',
	            				'rut_banco',
	            				'type_account',
	            				'account'
	            			];
	            		break;
					}
					
            		campos.forEach(function(item) {
						
						if ( (item == 'year') ) {

            				if ($('#year').val() == '' || $('#year').val() == null) {

            					vue.form_validator.push(item);
								continuar = false;
							}

            			} else if (vue.form[item] == '' || vue.form[item] == null) {

							vue.form_validator.push(item);
							
        					continuar = false;
        				}
        			});

        			if (continuar) {
        				switch(num) {
        					case 1:
        						if (vue.form.password != vue.form.password_confirmation) {
	            					swal('','{{ Lang::get('Page.Register.NoCoinciden') }}','warning');
	            					return false;
								}
								if (vue.form.password.length < 6 || vue.form.password_confirmation < 6) {
									swal('','{{ Lang::get('Page.Register.PassCorto') }}','warning');
	            					return false;
								} 
	            				if (!vue.checkEmail(vue.form.email)) {
	            					swal('','{{ Lang::get('Page.Register.CorreoNoValido') }}','warning');
	            					return false;
	            				}
								vue.seccion_conductor = 2;
								vue.setDatepicker();
	            				vue.iniciar();			
        					break

        					case 2:
        						vue.setYear();
        						vue.seccion_conductor = 3;
        					break;

        					case 3: 
								vue.submit();
        					break;
        				}
        			} else {
        				swal('','{{ Lang::get('Page.Register.Verifique') }}','warning');
        			}
	            }
			}
		});
	</script>
@stop