@extends('page.layouts.master')

@section('title')
	@lang('Page.Solicitar.Title')
@stop

@section('content')
	<div class="contenido" id="solicitar">
		
		{{-- Proceso --}}

		<div class="proceso">
			<div class="row">
				<div class="col-md-4 item-line">
					{{ HTML::Image('page-public/img/icons/app1.png') }}
					<h3>@lang('Page.Home.DescargaApp')</h3>
					<p>@lang('Page.Home.NuestraApp')</p>
				</div>
				<div class="col-md-4 item-line">
					{{ HTML::Image('page-public/img/icons/app2.png') }}
					<h3>@lang('Page.Home.Pide')</h3>
					<p>@lang('Page.Home.PuntoPartida')</p>
				</div>
				<div class="col-md-4">
					{{ HTML::Image('page-public/img/icons/money.png') }}
					<h3>@lang('Page.Home.Llegas')</h3>
					<p>@lang('Page.Home.TePermite')</p>
				</div>
			</div>
		</div>

		{{-- Promociones --}}
	
		<div id="promociones" class="promociones">
			<div class="row">
				<div class="col-md-6">
					<h1>@lang('Page.Solicitar.Promociones')</h1>
					<p>@lang('Page.Solicitar.PromocionesContent')</p>
					<p>@lang('Page.Solicitar.Cita')</p>
					<p class="text-center bold">@lang('Page.Solicitar.Descarga')</p>
					<div class="row container-tiendas">
{{-- 						<div class="col-md-6 appstore">
							<a href="https://play.google.com/store/apps/details?id=com.limonbyte.bogui">
								{{ HTML::Image('page-public/img/icons/apple.png') }}
							</a>
						</div> --}}
{{-- 						<div class="col-md-6">
							<a href="https://play.google.com/store/apps/details?id=com.limonbyte.bogui">
								{{ HTML::Image('page-public/img/icons/google.png') }}
							</a>
						</div> --}}
					</div>
					<div class="text-center container-tiendas">
						<a href="https://play.google.com/store/apps/details?id=com.limonbyte.bogui">
							{{ HTML::Image('page-public/img/icons/google.png') }}
						</a>
					</div>
				</div>
				<div class="col-md-6 text-center">
					{{ HTML::Image('page-public/img/phone1.jpg') }}
				</div>
			</div>
		</div>

	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			var hash = window.location.hash.substr(1);
			if (hash != '') {
				$('html, body').animate({
			        scrollTop: $('#' + hash).offset().top - 35
			    }, 500);
			}
		});
	</script>
@stop