@extends('page.layouts.master')

@section('title')
	@lang('Page.Home.Title')
@stop

@section('content')
	<div class="contenido" id="home">

		{{-- Banner --}}

		<div class="banner">
			<div class="row">
				<div class="col-lg-6 row-mensaje">
					<div class="container-mensaje">
						{{ HTML::Image('page-public/img/icons/arrow_white.png') }}
						<h1><strong>@lang('Page.Home.GeneraDinero')</strong></h1>
						<h1>@lang('Page.Home.CualquierLugar') <strong>Chile</strong></h1>
					</div>
				</div>
				<div class="col-lg-6">
					@if (!Auth::check())
						{{ Form::open(['method' => 'get','url' => 'register']) }}
							{{ Form::hidden('seccion','2') }}
							<a href="{{ URL('register?seccion=1') }}">
								<div class="row row-avatar border-red-bottom">
									<div class="col-xs-4">
										{{ HTML::Image('page-public/img/icons/pasajero.png','',['class' => 'img-avatar']) }}
									</div>
									<div class="col-xs-6">
										<p>@lang('Page.Home.QuieresViajar')</p>
										<h4 class="p-red">@lang('Page.Home.RegisterPasajero')</h4>
									</div>
									<div class="col-xs-2">
										{{ HTML::Image('page-public/img/icons/arrow_red.png','',['class' => 'arrow-red']) }}
									</div>
								</div>
							</a>
							<div class="row row-avatar">
								<div class="col-xs-4">
									{{ HTML::Image('page-public/img/icons/conductor.png','',['class' => 'img-avatar']) }}
								</div>
								<div class="col-xs-8">
									<h4 class="h4-conductor">@lang('Page.Home.RegisterConducir')</h4>
								</div>
							</div>
							<div class="container-border">
								<div class="row">
									<div class="form-group col-md-6">
										{{ Form::text('nombre','',['class' => 'form-control','placeholder' => Lang::get('Page.Home.Nombre'),'v-model' => 'form.nombre']) }}
									</div>
									<div class="form-group col-md-6">
										{{ Form::text('apellido','',['class' => 'form-control','placeholder' => Lang::get('Page.Home.Apellido'),'v-model' => 'form.apellido']) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form::text('email','',['class' => 'form-control','placeholder' => Lang::get('Page.Home.Email'),'v-model' => 'form.email']) }}
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											{{ Form::select('code',$paises,null,['class' => 'form-control','placeholder' => Lang::get('Page.Home.Codigo'),'v-model' => 'form.code']) }}
										</div>
									</div>
									<div class="col-md-9">
										<div class="form-group">
											{{ Form::number('telefono','',['class' => 'form-control','placeholder' => Lang::get('Page.Home.Telefono'),'v-model' => 'form.telefono']) }}
										</div>
									</div>
								</div>		
								<div class="form-group">
									{{ Form::select('document_type',[
										'1' => 'RUT',
										'2' => 'Pasaporte',
										'3' => 'Otro'
									],null,['class' => 'form-control','placeholder' => Lang::get('Page.Home.TipoDocumento'),'v-model' => 'form.document_type']) }}
								</div>
								<div class="form-group">
									{{ Form::text('document','',['class' => 'form-control','placeholder' => Lang::get('Page.Home.Documento'),'v-model' => 'rut_driver']) }}
								</div>
								<p>@lang('Page.Home.AlContinuar') <a href="{{ URL('informacion?seccion=1') }}" target="_blank">@lang('Page.Home.Terminos')</a> @lang('Page.Home.Reconoces') <a href="{{ URL('informacion?seccion=2') }}" target="_blank">@lang('Page.Home.Politica')</a></p>
								<div class="text-center">
									<button class="btn btn-default" :disabled="validate() > 0">
										@lang('Page.Home.Continuar')
									</button>
								</div>
							</div>
						{{ Form::close() }}
					@endif
				</div>
			</div>
		</div>
		
		{{-- Proceso --}}

		<div class="proceso">
			<div class="row">
				<div class="col-md-4 item-line">
					{{ HTML::Image('page-public/img/icons/app1.png') }}
					<h3>@lang('Page.Home.DescargaApp')</h3>
					<p>@lang('Page.Home.NuestraApp')</p>
				</div>
				<div class="col-md-4 item-line">
					{{ HTML::Image('page-public/img/icons/app2.png') }}
					<h3>@lang('Page.Home.Pide')</h3>
					<p>@lang('Page.Home.PuntoPartida')</p>
				</div>
				<div class="col-md-4">
					{{ HTML::Image('page-public/img/icons/money.png') }}
					<h3>@lang('Page.Home.Llegas')</h3>
					<p>@lang('Page.Home.TePermite')</p>
				</div>
			</div>
		</div>

		{{-- Tarifas --}}

		<div class="tarifas" style="background-image: url('{{ URL('img/tarifas.jpg') }}')">
			<h2 class="text-center">Nuestras Tarifas</h2>
			<div class="container-table">
				<table class="table">
					<thead>
						<tr>
							<th>Categoría</th>
							<th>Tarifa Base</th>
							<th>Tarifa Mínima</th>
							<th>Tarifa {{ $distance_min }}KM a {{ $distance_max }}KM</th>
							<th>Tarifa mayor a {{ $distance_max }}KM</th>
						</tr>
					</thead>
					<tbody>
						@for($i = 0; $i < count($tarifas); $i++)
							<tr>
								<td class="categoria">
									<p><strong>{{ TariffType::get($i) }}</strong></p>
									@if ($i == 4)
										<p class="minute">Por minuto</p>
									@endif
								</td>
								<td>{{ $tarifas[$i]->base_tariff > 0 ? Money::get($tarifas[$i]->base_tariff) : 'NO APLICA' }}</td>
								<td>{{ $tarifas[$i]->minimum_tariff > 0 ? Money::get($tarifas[$i]->minimum_tariff) : 'NO APLICA' }}</td>
								<td>{{ $tarifas[$i]->medium_tariff > 0 ? Money::get($tarifas[$i]->medium_tariff) : 'NO APLICA' }}</td>
								<td>{{ $tarifas[$i]->maximum_tariff > 0 ? Money::get($tarifas[$i]->maximum_tariff) : 'NO APLICA' }}</td>
							</tr>
						@endfor
					</tbody>
				</table>
			</div>
		</div>

		<div class="tarifas-accordion" style="background-image: url('{{ URL('img/tarifas.jpg') }}')">
			<h2 class="text-center">Nuestras Tarifas</h2>
			<div id="accordion" class="accordion">
				@for ($i = 0; $i < count($tarifas); $i++)
					  <h3>{{ TariffType::get($i) }} @if ($i == 4) (Por minuto) @endif</h3>
					  <div>
					    <p><strong>Tarifa Base:</strong> {{ $tarifas[$i]->base_tariff > 0 ? Money::get($tarifas[$i]->base_tariff) : 'NO APLICA' }}</p>
						<p><strong>Tarifa Mínima:</strong> {{ $tarifas[$i]->minimum_tariff > 0 ? Money::get($tarifas[$i]->minimum_tariff) : 'NO APLICA' }}</p>
						<p><strong>Tarifa 1.5KM a 50KM:</strong> {{ $tarifas[$i]->medium_tariff > 0 ? Money::get($tarifas[$i]->medium_tariff) : 'NO APLICA' }}</p>
						<p><strong>Tarifa mayor a 50KM:</strong> {{ $tarifas[$i]->maximum_tariff > 0 ? Money::get($tarifas[$i]->maximum_tariff) : 'NO APLICA' }}</p>
					  </div>
				@endfor
			</div>
		</div>

		{{-- Beneficios --}}

		<div class="beneficios">
			<div class="row">
				<div class="col-md-6">
					<h1><strong>@lang('Page.Home.Beneficios')</strong> @lang('Page.Home.DeViajar') <strong>Bogui</strong></h1>
					<p>@lang('Page.Home.PideTuBogui')</p>
					<p>@lang('Page.Home.BuenViaje')</p>
					<a href="{{ URL('/beneficios/pasajero') }}">
						<button class="btn btn-default">
							@lang('Page.Home.LeerMas')
						</button>
					</a>
				</div>
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/pedir.jpg') }}
				</div>
			</div>
		</div>

		{{-- Chile es uno solo --}}

		<div class="chile-solo">
			<div class="row">
				<div class="col-md-7">
					<div class="container-chile">
						<h2>@lang('Page.Home.UnoSolo')</h2>
						<h3>@lang('Page.Home.NorteSur')</h3>
						<p>@lang('Page.Home.ChileDescripcion')</p>
						<p class="bold"></p>
					</div>
				</div>
				<div class="col-md-5 container-img-chile">
					{{ HTML::Image('page-public/img/icons/chile.png') }}
				</div>
			</div>
		</div>

		{{-- Promociones --}}

		<div class="promociones">
			<div class="row">
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/phone1.jpg') }}
				</div>
				<div class="col-md-6">
					<h1>@lang('Page.Home.Conoces') <strong>@lang('Page.Home.Promociones')</strong>?</h1>
					<a href="{{ URL('/solicitar#promociones') }}">
						<button class="btn btn-default">
							@lang('Page.Home.Conocer') <i class="fa fa-chevron-right"></i>
						</button>
					</a>
				</div>
			</div>
		</div>

		{{-- Solicitar --}}

		<div class="solicitar" style="background-image: url({{ URL('page-public/img/panoramica.jpg') }})">
			<div class="row">
				<div class="col-md-6">
					<h1>@lang('Page.Home.Necesitas')<br /><strong>@lang('Page.Home.NecesitasSolicitar')</strong>?</h1>
				</div>
				<div class="col-md-6">
					<div class="text-center">
						<a href="{{ $_pasajero->google }}">
							{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/google.png' : 'page-public/img/icons/google_en.png') }}
						</a>
					</div>
					<div class="row">
<!-- 						<div class="col-lg-6 appstore">
							<a href="{{ $_pasajero->apple }}">
								{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/apple.png' : 'page-public/img/icons/apple_en.png') }}
							</a>
						</div> -->
						<!-- <div class="col-lg-6 offset-lg-3">
							<a href="{{ $_pasajero->google }}">
								{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/google.png' : 'page-public/img/icons/google_en.png') }}
							</a>
						</div> -->
					</div>
				</div>
			</div>
		</div>

		{{-- Edad --}}

		<div class="edad">
			<div class="row">
				<div class="col-md-6">
					<div class="container-edad">
						<h3>@lang('Page.Home.EdadLimite') <strong>@lang('Page.Home.IngresosExtra')</strong></h3>
						<p>@lang('Page.Home.EdadDescripcion')</p>
						<a href="{{ URL('/consigue-empleo#edad') }}">
							<button class="btn btn-default">
								@lang('Page.Home.LeerMas')
							</button>
						</a>
					</div>
				</div>
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/edad.jpg') }}
				</div>
			</div>
		</div>
	</div>
@stop

@section('styles')
	{{ HTML::Style('bower_components/jquery-ui/themes/base/jquery-ui.min.css') }}
@stop

@section('scripts')
	{{ HTML::Script('bower_components/jquery-ui/jquery-ui.min.js') }}
	<script type="text/javascript">
		$(document).ready(function() {
			$('.accordion').accordion();
		});

		new Vue({
			el: '#home',
			data: {
				regex: '((([0-9.]+){6})-([0-9K]+?))',
				showMsg: false,
				rut_driver: '',
				form: {
					nombre: '',
					apellido: '',
					document_type: '',
					code: '',
					email: '',
					document: '',
					telefono: ''
				}
			},
			methods: {
				validate() {
					var continuar = false;
					var vue = this;
					Object.keys(this.form).forEach(function(key) {
						if (vue.form[key].trim() == '' || vue.form[key] == null) {
							continuar = true;
						}
					});
					return continuar;
				}
			},
			watch: {
				rut_driver(value) {
			        if (this.form.document_type == '1') {
			        	let check_regex = new RegExp(this.regex)
			            value = value.split('.').join("");
			            value = value.split(' ').join("");
		            	value = value.split('-').join("");
			            if (value.length == 8) {
		                    value = value.slice(0, 1) + "." + value.slice(1,4) + "." + value.slice(4,7) + '-' + value.slice(7,9);
			                if (check_regex.test(value)) {
			                    this.showMsg = false;
			                } else {
			                    this.showMsg = true;
			                }
			            } else if (value.length == 9) {
		                    value = value.slice(0, 2) + "." + value.slice(2,5) + "." + value.slice(5,8) + '-' + value.slice(8,10);
			                if (check_regex.test(value)) {
			                    this.showMsg = false;
			                } else {
			                    this.showMsg = true;
			                }
			            } else {
			                this.showMsg = true;
			            }
			        } else {
			            this.showMsg = false;
			            console.log('no es tipo RUT Driver')
			        }
		            this.rut_driver = value.toUpperCase()
		            this.form.document = this.rut_driver
			    }
			}
		});
	</script>
@stop