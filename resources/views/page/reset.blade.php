@extends('page.layouts.master')

@section('title')
	@lang('Page.Reset.Title')
@stop

@section('content')
	<div class="contenido" id="reset" v-cloak>
		
		<div v-if="seccion == 0">
			<a href="{{ URL('login') }}">
				<button class="btn btn-default btn-regresar">
					@lang('Page.Reset.Regresar')
				</button>
			</a>
			{{ Form::open(['v-on:submit.prevent' => 'sendCode()']) }}
				<div class="container-form">
					<div class="container-mensaje text-center">
						<h2>@lang('Page.Reset.Title')</h2>
					</div>
					<div class="container-login">
						<p class="text-center">@lang('Page.Reset.IngreseCorreo')</p>
						<div class="form-group">
							{{ Form::text('email','',['class' => 'form-control','placeholder' => Lang::get('Page.Reset.Email'),'v-model' => 'form.email']) }}
						</div>
						<div style="padding-bottom: 15px;">
							<div class="form-check-inline">
							  <label class="form-check-label">
							    <input type="radio" value="3" v-model="form.type" class="form-check-input" name="radio-soy"> Pasajero
							  </label>
							</div>
							<div class="form-check-inline">
							  <label class="form-check-label">
							    <input type="radio" value="2" v-model="form.type" class="form-check-input" name="radio-soy"> Conductor
							  </label>
							</div>
						</div>
						<div class="text-center">
							<button class="btn btn-default" type="submit" :disabled="form.email == '' || form.email == null">
								@lang('Page.Reset.Continuar')
							</button>
						</div>
					</div>
				</div>			
			{{ Form::close() }}
		</div>

		<div v-if="seccion == 1">
			<button class="btn btn-default btn-regresar" v-on:click="seccion = 0">
				@lang('Page.Reset.Regresar')
			</button>
			{{ Form::open(['v-on:submit.prevent' => 'checkCode()']) }}
				<div class="container-form">
					<div class="container-mensaje text-center">
						<h2>@lang('Page.Reset.Title')</h2>
					</div>
					<div class="container-login">
						<p class="text-center">@lang('Page.Reset.IngreseCodigo')</p>
						<div class="form-group">
							{{ Form::text('codigo','',['class' => 'form-control','placeholder' => Lang::get('Page.Reset.CodigoSeguridad'),'v-model' => 'form.codigo']) }}
						</div>
						<div style="padding-bottom: 15px;">
							<div class="form-check-inline">
							  <label class="form-check-label">
							    <input type="radio" value="3" v-model="form.type" class="form-check-input" disabled name="radio-soy"> Pasajero
							  </label>
							</div>
							<div class="form-check-inline">
							  <label class="form-check-label">
							    <input type="radio" value="2" v-model="form.type" class="form-check-input" disabled name="radio-soy"> Conductor
							  </label>
							</div>
						</div>
						<div class="text-center">
							<button class="btn btn-default" type="submit" :disabled="form.codigo == '' || form.codigo == null">
								@lang('Page.Reset.Continuar')
							</button>
						</div>
					</div>
				</div>			
			{{ Form::close() }}
		</div>

		<div v-if="seccion == 2">
			<button class="btn btn-default btn-regresar" v-on:click="seccion = 1">
				@lang('Page.Reset.Regresar')
			</button>
			{{ Form::open(['v-on:submit.prevent' => 'submit()']) }}
				<div class="container-form">
					<div class="container-mensaje text-center">
						<h2>@lang('Page.Reset.Title')</h2>
					</div>
					<div class="container-login">
						<p class="text-center">@lang('Page.Reset.IngresePass')</p>
						<div class="form-group">
							{{ Form::password('password',['class' => 'form-control','placeholder' => Lang::get('Page.Reset.Password'),'v-model' => 'form.password']) }}
						</div>
						<div class="form-group">
							{{ Form::password('password_confirmation',['class' => 'form-control','placeholder' => Lang::get('Page.Reset.RepeatPassword'),'v-model' => 'form.password_confirmation']) }}
						</div>
						<div style="padding-bottom: 15px;">
							<div class="form-check-inline">
							  <label class="form-check-label">
							    <input type="radio" value="3" v-model="form.type" class="form-check-input" disabled name="radio-soy"> Pasajero
							  </label>
							</div>
							<div class="form-check-inline">
							  <label class="form-check-label">
							    <input type="radio" value="2" v-model="form.type" class="form-check-input" disabled name="radio-soy"> Conductor
							  </label>
							</div>
						</div>
						<div class="text-center">
							<button class="btn btn-default" type="submit" :disabled="(form.password == '' || form.password == null) || (form.password_confirmation == '' || form.password_confirmation == null)">
								@lang('Page.Reset.Continuar')
							</button>
						</div>
					</div>
				</div>			
			{{ Form::close() }}
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		var vue = new Vue({
			el: '#reset',
			data: {
				seccion: 0,
				form: {
					email: '',
					password: '',
					password_confirmation: '',
					codigo: '',
					type: 3				
				}
			},
			methods: {
				sendCode() {
					setLoader();
					axios.post('{{ URL('reset/code') }}',this.form)
						.then(function(res) {
							if (res.data.result) {
								swal('','{{ Lang::get('Page.Reset.SuccessCodigo') }}','success');
								vue.seccion = 1;
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				},
				checkCode() {
					setLoader();
					axios.post('{{ URL('reset/code/check') }}',this.form)
						.then(function(res) {
							if (res.data.result) {
								vue.seccion = 2;
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				},
				submit() {
					if (this.form.password != this.form.password_confirmation) {
						swal('','{{ Lang::get('Page.Reset.NoCoinciden') }}','warning');
						return false;
					}
					setLoader();
					axios.post('{{ URL('reset') }}',this.form)
						.then(function(res) {
							if (res.data.result) {
								swal('','{{ Lang::get('Page.Reset.Success') }}','success');
								setTimeout(function() {
									window.location.href = res.data.url;
								},1000);
							}
							else {
								swal('',res.data.error,'warning');
							}
						})
						.catch(function(err) {
							swal('','{{ Lang::get('General.Error') }}','warning');
						})
						.then(function() {
							quitLoader();
						});
				}
			}
		});
	</script>
@stop