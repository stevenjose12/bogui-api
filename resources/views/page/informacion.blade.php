@extends('page.layouts.master')

@section('title')
	@lang('Page.Informacion.Title')
@stop

@section('content')
	<div class="contenido" id="informacion" v-cloak>
		<div class="row">
			<div class="col-md-6 nav-responsive">
				@include('page.informacion-nav')
			</div>
			<div class="col-md-6">
				<h3>Bogui</h3>
				<div v-if="seccion == 1">
					<h2>@lang('Page.Informacion.Terminos')</h2>
					<p>{!! str_replace("<br />", "<br /><br />", nl2br(\App::getLocale() == 'es' ? $terminos->content : $terminos->english)) !!}</p>
				</div>
				<div v-if="seccion == 2">
					<h2>@lang('Page.Informacion.Privacidad')</h2>
					<p>{!! str_replace("<br />", "<br /><br />", nl2br(\App::getLocale() == 'es' ? $privacidad->content : $privacidad->english)) !!}</p>
				</div>
			</div>
			<div class="col-md-6 nav-no-responsive">
				@include('page.informacion-nav')
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		new Vue({
			el: '#informacion',
			data: {
				seccion: '{{ Request::has('seccion') ? Request::get('seccion') : '1' }}'
			}
		});
	</script>
@stop