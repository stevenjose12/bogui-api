@extends('page.layouts.master')

@section('title')
	@lang('Page.ConsigueEmpleo.Title')
@stop

@section('content')
	<div class="contenido" id="consigue-empleo">

		{{-- Consigue Empleo --}}

		<div class="consigue-empleo">
			<div class="row">
				<div class="col-md-6">
					<h1><span class="red">@lang('Page.ConsigueEmpleo.ConsigueEmpleo')</span><br />@lang('Page.ConsigueEmpleo.ConNosotros')</h1>
					<p>@lang('Page.ConsigueEmpleo.Content')</p>
					<ul>
						@foreach($condiciones as $condicion)
							<li>{{ \App::getLocale() == 'es' ? $condicion->content : $condicion->english }}</li>
						@endforeach
					</ul>
				</div>
				<div class="col-md-6 text-center">
					{{ HTML::Image('page-public/img/empleo.jpg') }}
				</div>
			</div>
		</div>
		
		{{-- Edad --}}

		<div id="edad" class="edad">
			<div class="row">
				<div class="col-md-6">
					{{ HTML::Image('page-public/img/edad.jpg') }}
				</div>
				<div class="col-md-6">
					<div class="container-edad">
						<h3>@lang('Page.ConsigueEmpleo.Limite') <strong>@lang('Page.ConsigueEmpleo.IngresosExtra')</strong></h3>
						<p>@lang('Page.ConsigueEmpleo.PersonaMayor')</p>
						@if (!Auth::check())
							<p class="bold text-center">
								@lang('Page.ConsigueEmpleo.QueEsperas')
							</p>
							<a href="{{ URL('/register?seccion=2') }}">
								<button class="btn btn-default">
									@lang('Page.ConsigueEmpleo.Registrate')
								</button>
							</a>
						@endif
					</div>
				</div>				
			</div>
		</div>

		{{-- Banner --}}

		<div class="banner">
			{{ HTML::Image('page-public/img/empleo_conductor.jpg') }}
		</div>
	</div>
@stop