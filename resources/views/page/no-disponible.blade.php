<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
	{{ HTML::Style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::Style('css/no-disponible.css') }}
	<title>Bogui | Ruta no Disponible</title>
</head>
<body>
	<div class="ruta__info__footer">
		<div class="row">
			<div class="col-4">
				<div class="ruta__info__footer__logo">
					<img src="{{ URL('img/logo_sin_lienzo.png') }}" />
				</div>
			</div>
			<div class="col-8">
				<div class="ruta__info__footer__mas">
					<a href="{{ URL('') }}" target="_blank">
						Conoce más de Bogui
						<img src="{{ URL('img/arrow.png') }}" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="text-center mapa">
		{{ HTML::Image('img/mapa-no-disponible.png') }}
	</div>
	<h3>Lo sentimos, la ruta no se encuentra disponible</h3>
</body>
</html>