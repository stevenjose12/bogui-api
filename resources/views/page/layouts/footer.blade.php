<footer>
	<div class="row row-links">
{{-- 		<div class="col-md-3">
			<h3>@lang('General.Footer.CambiarIdioma')</h3>
			<ul>
				<li><a href="{{ URL('lang/en') }}">@lang('General.Footer.Ingles')</a></li>
				<li><a href="{{ URL('lang/es') }}">@lang('General.Footer.Spanish')</a></li>
			</ul>
		</div> --}}
		<div class="col-md-4">
			<h3>Bogui</h3>
			<ul>
				<li><a href="{{ Request::is('nosotros') ? URL('#nosotros') : URL('nosotros#nosotros') }}">@lang('General.Footer.Nosotros')</a></li>
				<li><a href="{{ Request::is('nosotros') ? URL('#mision') : URL('nosotros#mision') }}">@lang('General.Footer.Mision')</a></li>
				<li><a href="{{ URL('informacion?seccion=2') }}">@lang('General.Footer.Privacidad')</a></li>
				<li><a href="{{ URL('informacion?seccion=1') }}">@lang('General.Footer.Terminos')</a></li>
			</ul>
		</div>
		<div class="col-md-4">
			<h3>@lang('General.Footer.Conductor')</h3>
			<ul>
				@if (!Auth::check())
					<li><a href="{{ Request::is('register') ? URL('#register') : URL('register?seccion=2') }}">@lang('General.Header.RegisterConductor')</a></li>
				@endif
				<li><a href="{{ Request::is('beneficios/conductor') ? URL('#beneficios-conductor') : URL('beneficios/conductor') }}">@lang('General.Footer.BeneficiosSocio')</a></li>
				<li><a href="{{ Request::is('consigue-empleo') ? URL('#consigue-empleo') : URL('consigue-empleo') }}">@lang('General.Header.Empleo')</a></li>
			</ul>
		</div>
		<div class="col-md-4">
			<h3>@lang('General.Footer.Pasajero')</h3>
			<ul>
				@if (!Auth::check())
					<li><a href="{{ Request::is('register') ? URL('#register') : URL('register?seccion=1') }}">@lang('General.Header.RegisterPasajero')</a></li>
				@endif
				<li><a href="{{ Request::is('solicitar') ? URL('#solicitar') : URL('solicitar') }}">@lang('General.Header.Solicitar')</a></li>
				<li><a href="{{ Request::is('beneficios/pasajero') ? URL('#beneficios-pasajero') : URL('beneficios/pasajero') }}">@lang('General.Header.BeneficiosPasajero')</a></li>
				<li><a href="{{ Request::is('solicitar') ? URL('#promociones') : URL('solicitar#promociones') }}">@lang('General.Header.Promociones')</a></li>
			</ul>
		</div>
	</div>
	<div class="tiendas-footer text-center">
		<p><strong>@lang('General.Footer.Ayuda'):</strong> <span>{{ $_contact->phone }}</span></p>
		<p><strong>@lang('General.Footer.Enlace')</strong> <img src="{{ URL('page-public/img/icons/puntero.png') }}" class="puntero" /> <a target="_blank" href="{{ $_contact->telegram }}">{{ $_contact->telegram }}</a></p>
		<div class="row">
{{-- 			<div class="col-md-6  appstore">
				<a href="{{ $_pasajero->apple }}">
					{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/apple.png' : 'page-public/img/icons/apple_en.png') }}
				</a>
			</div> --}}
			<div class="col-md-6 offset-md-3">
				<a href="{{ $_pasajero->google }}">
					{{ HTML::Image(\App::getLocale() == 'es' ? 'page-public/img/icons/google.png' : 'page-public/img/icons/google_en.png') }}
				</a>
			</div>
		</div>
	</div>
<!-- 	<p class="copy text-center">&copy; {{ \Carbon\Carbon::now()->format('Y') }} Designed by <a target="_blank" href="https://www.limonbyte.com">LimónByte</a> | @lang('General.Footer.Derechos').</p> -->
</footer>