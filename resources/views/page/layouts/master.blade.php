<!DOCTYPE html>
<html lang="es">
<head>
	<title>Bogui | @yield('title')</title>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
	{{ HTML::Style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::Style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::Style('bower_components/sweetalert/dist/sweetalert.css') }}
	{{ HTML::Style('bower_components/hold-on/src/css/HoldOn.min.css') }}
	{{ HTML::Style('page-public/css/main.css?1.0') }}
	@yield('styles')
</head>
<body>
	<div class="container-fluid" style="overflow: hidden; padding: 0px !important;">

		@include('page.layouts.header')
			
			@yield('content')

		@include('page.layouts.footer')

	</div>
	
	{{ HTML::Script('bower_components/jquery/dist/jquery.min.js') }}
	{{ HTML::Script('bower_components/bootstrap/dist/js/bootstrap.min.js') }}
	{{ HTML::Script('bower_components/vue/dist/vue.min.js') }}
	{{ HTML::Script('bower_components/axios/dist/axios.min.js') }}
	{{ HTML::Script('bower_components/sweetalert/dist/sweetalert.min.js') }}
	{{ HTML::Script('bower_components/hold-on/src/js/HoldOn.min.js') }}
	{{ HTML::Script('page-public/js/loader.js') }}
	{{ HTML::Script('page-public/js/dropdown.js') }}
	{{ HTML::Script('page-public/js/scroll.js') }}
	@yield('scripts')
</body>
</html>