<header>   
	<nav class="navbar navbar-expand-md fixed-top bg-light">
	   <a class="navbar-brand" href="{{ URL('/') }}">
	    {{ HTML::Image('page-public/img/icons/logo.png') }}
	   </a>

	   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
	     <i class="fa fa-bars"></i>
	   </button>
		
	   <div class="collapse navbar-collapse" id="collapsibleNavbar">
		   <ul class="navbar-nav">
			   	<li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          @lang('General.Header.Viajar')
			        </a>
			        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			          @if (!Auth::check())
			          	<a class="dropdown-item" href="{{ Request::is('register') ? URL('#register') : URL('register?seccion=1') }}">@lang('General.Header.RegisterPasajero')</a>
			          @endif
			          <a class="dropdown-item" href="{{ Request::is('beneficios/pasajero') ? URL('#beneficios-pasajero') : URL('beneficios/pasajero') }}">@lang('General.Header.BeneficiosPasajero')</a>
			          <a class="dropdown-item" href="{{ Request::is('solicitar') ? URL('#promociones') : URL('solicitar#promociones') }}">@lang('General.Header.Promociones')</a>
			          <a class="dropdown-item" href="{{ Request::is('solicitar') ? URL('#solicitar') : URL('solicitar#solicitar') }}">@lang('General.Header.Solicitar')</a>
			        </div>
			    </li>
			    <li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          @lang('General.Header.Conducir')
			        </a>
			        <div class="dropdown-menu">
			          @if (!Auth::check())
			         	 <a class="dropdown-item" href="{{ Request::is('register') ? URL('#register') : URL('register?seccion=2') }}">@lang('General.Header.RegisterConductor')</a>
			          @endif
			          <a class="dropdown-item" href="{{ Request::is('beneficios/conductor') ? URL('#beneficios-conductor') : URL('beneficios/conductor') }}">@lang('General.Header.BeneficiosConductor')</a>
			          <a class="dropdown-item" href="{{ Request::is('consigue-empleo') ? URL('#consigue-empleo') : URL('consigue-empleo') }}">@lang('General.Header.Empleo')</a>
			        </div>
			    </li>
			</ul>
			<ul class="navbar-nav ml-auto">
				@if (!Auth::check())
				    <li class="nav-item">
				      <a class="nav-link" href="{{ URL('/login') }}">@lang('General.Header.Iniciar')</a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link ser-bogui" href="{{ Request::is('/register') ? URL('#register') : URL('/register?seccion=2') }}">@lang('General.Header.SerConductor')</a>
				    </li>
				@else
					<li class="nav-item">
				      <a class="nav-link" href="{{ URL('/home/logout') }}">@lang('General.Header.Cerrar')</a>
				    </li>
					<li class="nav-item">
				      <a class="nav-link ser-bogui" href="{{ Request::is('/account') ? URL('#account') : URL('/account') }}">@lang('General.Header.Cuenta')</a>
				    </li>
				@endif
		   </ul>
	   </div>
	</nav>
</header>