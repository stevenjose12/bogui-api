<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Ha finalizado tu viaje</title>
    <style type="text/css">
        .container {
            text-align: center;
            font-family: Calibri;
            padding: 40px;
        }
        img {
            width: 350px;
        }
        .title {
            text-transform: uppercase;
            font-weight: 300;
            font-size: 30px;
            margin-top: 40px;
        }
        button {
            margin-top: 20px;
            text-transform: uppercase;
            font-weight: 300;
            width: 250px;
            border-radius: 3px;
            padding: 15px;
            text-align: center;
            outline: 0px !important;
            font-size: 16px !important;
            border: 0px !important;
            background-color: #43A047 !important;
            color: #f4f4f4 !important;
            cursor: pointer;
        }
        .codigo {
            font-size: 30px;
            text-transform: uppercase;
            font-weight: bold;
        }
        table, tr, th, td {
            text-align: left;
        }
        td {
            padding-left: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <img src="{{ URL('img/logo.png') }}" />
        <h4 class="title">Ha finalizado tu viaje</h4>
        <p>Pasajero Bogui, ha finalizado tu viaje. Te enviamos su información:</p>
        <table>
            <tr>
                <th>ID</th>
                <td>{{ $route->id }}</td>
            </tr>
            <tr>
                <th>Fecha - Hora</th>
                <td>{{ \Carbon\Carbon::parse($route->updated_at)->format('d-m-Y H:i') }}</td>
            </tr>
            <tr>
                <th>Partida</th>
                <td>{{ $route->description }}</td>
            </tr>
            @if ($route->has_parada)
                <tr>
                    <th>Parada</th>
                    <td>{{ $route->parada_descripcion }}</td>
                </tr>
            @endif
            <tr>
                <th>Destino</th>
                <td>{{ $route->destino_descripcion }}</td>
            </tr>
            <tr>
                <th>Costo</th>
                <td>{{ Money::get($route->cost) }}</td>
            </tr>
            <tr>
                <th>Método de Pago</th>
                <td>{{ $route->payment_type }}</td>
            </tr>
            <tr>
                <th>Distancia</th>
                <td>{{ number_format($route->distance,2,'.',',') }} Km</td>
            </tr>
            <tr>
                <th>Conductor</th>
                <td>{{ $route->driver->person->name.' '.$route->driver->person->lastname }}</td>
            </tr>
            <tr>
                <th>Tipo de Vehículo</th>
                <td>{{ $route->vehicleCategoryName }}</td>
            </tr>
        </table>
    </div>
</body>
</html>