@component('mail::message')

# Bienvenido {{ $user->person->name }} {{ $user->person->lastname }}

Su nivel de acceso es:
	@if($user->level == '1') 
		Administrador 
	@elseif($user->level == '2') 
		Conductor 
	@elseif($user->level == '3') 
		Pasajero 
	@else
		Moderador
	@endif

Estos son sus datos de acceso.

Indentificación: {{ $user->id }}
<br>
Correo electronico: {{ $user->email }}
<br>
Contraseña: {{ $password }}

Gracias,<br>
<strong>{{ config('app.name') }}</strong>
@endcomponent