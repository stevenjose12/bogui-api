<?php

	return [

		'Account' => [
			'Title' => 'Mi Cuenta',
			'Bienvenido' => '¡Bienvenido :user!',
			'Registrado' => '¡Ya estas Registrado!',
			'DescriptionPasajero' => '¡Gracias por crear tu perfil de pasajero! ¡Ya casi estás! Descarga nuestra App de Pasajero para solicitar tus servicios en Bogui.',
			'DescriptionConductor' => '¡Gracias por crear tu perfil de socio! ¡Ya casi estás! Descarga nuestra App de Conductor para continuar.'
		],

		'Login' => [
			'Title' => 'Iniciar Sesión',
			'Olvide' => 'Olvidé mi contraseña',
			'Aceptar' => 'Aceptar',
			'Email' => 'Correo Electrónico',
			'Password' => 'Contraseña',
			'Cuenta' => '¿No tienes cuenta?',
			'Registro' => 'Regístrate'
		],

		'Reset' => [
			'Title' => 'Restablecer Contraseña',
			'Regresar' => 'Regresar',
			'IngreseCorreo' => 'Se enviará un código a su correo',
			'IngreseCodigo' => 'Introduzca el código para continuar',
			'IngresePass' => 'Introduzca la nueva contraseña',
			'Continuar' => 'Continuar',
			'Password' => 'Contraseña',
			'RepeatPassword' => 'Repetir Contraseña',
			'CodigoSeguridad' => 'Código de Seguridad',
			'Email' => 'Correo Electrónico',
			'NoCoinciden' => 'Las contraseñas no coinciden',
			'Success' => 'Se ha cambiado la contraseña correctamente',
			'SuccessCodigo' => 'Se ha enviado un código al correo electrónico'
		],

		'Codigo' => [
			'Title' => 'Código de Verificación',
			'Codigo' => 'Código',
			'Reenviar' => 'Reenviar Código',
			'SMS' => 'Se envió un código de seguridad por SMS para confirmar tu cuenta',
			'Success' => 'Se ha enviado el código de verificación',
			'Bienvenido' => 'Bienvenido a Bogui, su código de verificación es',
			'Continuar' => 'Continuar'
		],

		'Register' => [
			'Title' => 'Regístrarse',
			'RegisterComo' => 'Regístrate como', 
			'Conductor' => 'Conductor',
			'Pasajero' => 'Pasajero',
			'Login' => 'Iniciar Sesión',
			'Cuenta' => '¿Ya tienes cuenta?',
			'Regresar' => 'Regresar',
			'Viajar' => '¿Quieres viajar sin problemas?',
			'GanarDinero' => '¿Quieres ganar dinero?',
			'RegisterConductor' => 'Regístrate como Conductor',
			'RegisterPasajero' => 'Regístrate como Pasajero',
			'SubirFotoPerfil' => 'Subir Foto del Perfil',
			'Nombre' => 'Nombre',
			'Apellido' => 'Apellido',
			'Email' => 'Email',
			'Codigo' => 'Código',
			'Telefono' => 'Teléfono',
			'TipoDocumento' => 'Tipo de Documento',
			'Document' => 'Ingresa tu RUT o Pasaporte',
			'Password' => 'Contraseña',
			'RepeatPassword' => 'Repetir Contraseña',
			'Pasaporte' => 'Pasaporte',
			'Otro' => 'Otro',
			'Aceptar' => 'Aceptar',
			'AlContinuar' => 'Al continuar, aceptas los',
			'Terminos' => 'Términos y Condiciones',
			'Reconoces' => 'de Bogui y reconoces haber leído su',
			'Politica' => 'política de privacidad',
			'SubirFotoRut' => 'Subir imagen de RUT o Pasaporte',
			'SubirFotoAntecedentes' => 'Subir certificado de Antecedentes',
			'Continuar' => 'Continuar',
			'InfoVehiculo' => 'Información del Vehículo',
			'SubirFotoPermiso' => 'Subir permiso de Circulación',
			'SubirFotoLicencia' => 'Subir Licencia de Conducir',
			'Year' => 'Año de Modelo',
			'Marca' => 'Marca de Vehículo',
			'Modelo' => 'Modelo de Vehículo',
			'DatosBancarios' => 'Datos Bancarios',
			'Banco' => 'Banco',
			'BancoRut' => 'Número de RUT',
			'TipoCuenta' => 'Tipo de Cuenta',
			'Account' => 'Número de Cuenta',
			'Patente' => 'Patente',
			'SubirFotoVehiculo' => 'Subir Foto del Vehículo',
			'FormatoNoValido' => 'El formato de la imagen no es válido',
			'Success' => 'Se ha registrado correctamente',
			'Bienvenido' => 'Bienvenido a Bogui, su código de verificación es',
			'Verifique' => 'Por favor, verifique los campos',
			'CorreoNoValido' => 'El correo electrónico no es válido',
			'NoCoinciden' => 'Las contraseñas no coinciden',
			'AnioMaximo' => 'El año del vehículo no puede ser mayor al ' . date('Y'),
			'AnioMinimo' => 'El año del vehículo no puede ser menor al ' . date('Y'),
			'PassCorto' => 'La contraseña debe tener al menos 6 caracteres'
		],

		'Informacion' => [
			'Title' => 'Información Legal',
			'Terminos' => 'Términos y Condiciones',
			'Privacidad' => 'Privacidad'
		],

		'Home' => [
			'Title' => 'Inicio',
			'GeneraDinero' => 'Genera dinero en',
			'CualquierLugar' => 'cualquier lugar de',
			'RegisterPasajero' => 'Regístrate como Pasajero',
			'RegisterConducir' => 'Regístrate para Conducir',
			'QuieresViajar' => '¿Quieres viajar sin problemas?',
			'Nombre' => 'Nombre',
			'Apellido' => 'Apellido',
			'Email' => 'Correo Electrónico',
			'Codigo' => 'Código',
			'Telefono' => 'Teléfono',
			'TipoDocumento' => 'Tipo de Documento',
			'Documento' => 'Ingresa tu RUT o Pasaporte',
			'AlContinuar' => 'Al continuar, aceptas los',
			'Terminos' => 'Términos y Condiciones',
			'Reconoces' => 'de Bogui y reconoces haber leído su',
			'Politica' => 'política de privacidad',
			'Continuar' => 'Continuar',
			'LeerMas' => 'Leer Más',
			'DescargaApp' => 'Descarga nuestra App y Regístrate',
			'Llegas' => 'Llegas a tu destino y procedes a pagar',
			'Pide' => 'Pide tu Bogui y selecciona tu destino',
			'NuestraApp' => 'Descarga nuestra app para pasajero y llena los campos de registro',
			'PuntoPartida' => 'Selecciona tu punto de partida, si necesitas hacer una parada adicional y el destino',
			'TePermite' => 'Bogui te permite decidir si desea pagar en efectivo o tarjeta de crédito. Y listo, así de fácil es viajar con Bogui',
			'BuenViaje' => '¡Buen Viaje!',
			'DeViajar' => 'de viajar',
			'Beneficios' => 'Beneficios',
			'PideTuBogui' => 'Pide tu Bogui desde la comodidad de tu hogar, conoce la tarifa de tu viaje antes de pedirlo, te mostramos en tiempo real donde se encuentra tu conductor, en minutos lo tendrás donde tu lo solicites esperando por ti, descarga la app Bogui para pasajeros y úsala con total confianza.',
			'Tecnologia' => '¡Bienvenidos a la tecnología!',
			'UnoSolo' => 'Chile es uno solo',
			'NorteSur' => 'Bogui esta disponible de Norte a Sur',
			'ChileDescripcion' => 'Bogui es la única aplicación que está disponible en todos los rincones de Chile, porque sabemos que viajar en lugares remotos resulta más complicado hemos implementado nuestra cobertura a todos los rincones de Chile, queremos dar la oportunidad a conductores y pasajeros de lugares remotos a vivir y disfrutar de un servicio creado para todos. Solo necesitas tener cobertura de internet en tu dispositivo.',
			'Conocer' => 'Conocer',
			'Conoces' => '¿Conoces nuestras',
			'Promociones' => 'promociones',
			'IngresosExtra' => 'ingresos extra',
			'EdadLimite' => 'La edad no es un limite para generar',
			'Necesitas' => '¿Necesitas',
			'NecesitasSolicitar' => 'solicitar un Bogui',
			'EdadDescripcion' => 'Si eres una persona mayor, estás jubilado, tienes alguna incapacidad que no te permite optar por otro tipo de trabajo, Sientes que las puertas están cerradas para ti , no te preocupes, Bogui también está diseñada para ti.'
		],

		'Nosotros' => [
			'Title' => 'Nosotros',
			'Mision' => 'Misión',
			'BeneficiosCon' => 'Beneficios con',
			'NoImporta' => 'No importa donde te encuentres,',
			'Cerca' => 'cerca de ti',
			'Siempre' => 'siempre habra un',
			'QueEs' => '¿Qué es Bogui?',
			'ComoPedir' => '¿Como pedir un Bogui?'
		],

		'ConsigueEmpleo' => [
			'Title' => 'Consigue Empleo con Nosotros',
			'Content' => 'Si tienes un vehículo y necesitas aumentar tus ingresos descarga Bogui conductor y regístrate, un equipo nuestro revisará tu solicitud y pronto estarás listo para ganar dinero extra, los requerimientos básicos son vehículos que cumplan con las siguientes características:',
			'ConsigueEmpleo' => 'Consigue Empleo',
			'ConNosotros' => 'Con Nosotros',
			'Limite' => 'La edad no es un limite para generar',
			'IngresosExtra' => 'Ingresos Extra',
			'PersonaMayor' => 'Si eres una persona mayor, estás jubilado, tienes alguna incapacidad que no te permite optar por otro tipo de trabajo, Sientes que las puertas están cerradas para ti , no te preocupes, Bogui también está diseñada para ti, descarga la aplicación para conductores, llena los datos de Registro y un equipo nuestro evaluará tus capacidades para conducir, si apruebas tendrás la oportunidad de generar ingresos acorde a tu necesidad.',
			'QueEsperas' => '¿Qué esperas?',
			'Registrate' => 'Regístrarse'
		],

		'Solicitar' => [
			'Title' => 'Solicitar un Bogui',
			'Promociones' => 'Promociones',
			'Descarga' => '¡Descarga la app y entérate!',
			'PromocionesContent' => 'Bogui es una empresa que se creó para innovar y una de esas innovaciones es acompañarte en tu vida cotidiana facilitando la forma de moverte en tu ciudad No te pierdas las promociones que tendremos para ti visita constantemente nuestra página y entérate de todas las novedades que iremos dando a conocer , convenios, regalías, promociones, concursos y muchas cosas más',
			'Cita' => '“Tú eres importante para nosotros, por eso queremos premiar tu colaboración, con una importante promoción de incorporación para nuestros colaboradores”'
		],

		'BeneficiosPasajero' => [
			'Title' => 'Beneficios de Viajar con Bogui',
			'ViajaSeguro' => 'Viaja seguro en Chile',
			'ViajaCon' => 'Viaja con',
			'Registrate' => 'Regístrarse',
			'PideTuBogui' => 'Pide tu Bogui desde la comodidad de tu hogar, conoce la tarifa de tu viaje antes de pedirlo, te mostramos en tiempo real donde se encuentra tu conductor, en minutos lo tendrás donde tu lo solicites esperando por ti, descarga la app Bogui para pasajeros y úsala con total confianza, te garantizamos que nuestros conductores son meticulosamente seleccionados y te darán un servicio de calidad, para que te sientas aún más seguro comparte tu viaje con quien tú quieras, haz un seguimiento en tiempo real de tu viaje. Bogui te permite conocer los datos de tu conductor y tu vehículo antes de abordar.',
			'BuenViaje' => '¡Buen Viaje!',
			'Beneficios' => 'Beneficios',
			'DeViajar' => 'de viajar con'
		],

		'BeneficiosConductor' => [
			'Title' => 'Beneficios de ser Conductor Bogui',
			'TuPones' => 'Tú pones las ganas',
			'NosotrosPonemos' => 'nosotros ponemos',
			'Tecnologia' => 'la tecnología',
			'Descarga' => '¡Descarga la app ahora y conviértete en un conductor Bogui!',
			'BeneficiosDeSer' => 'Beneficios de ser',
			'ConductorBogui' => 'Conductor Bogui',
			'Preocupa' => 'Bogui es la única aplicación que realmente se preocupa de ti, ha sido diseñada y creada para ofrecerte una serie de beneficios que harán que tu vida cotidiana sea diferente de ahora en adelante.'
		]
	];