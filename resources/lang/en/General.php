<?php

	return [

		'Error' => 'Se ha producido un error',

		'Header' => [
			'Viajar' => 'Viajar',
			'Conducir' => 'Conducir',
			'RegisterPasajero' => 'Regístrate como Pasajero',
			'Promociones' => 'Promociones',
			'Solicitar' => 'Solicitar un Servicio',
			'BeneficiosPasajero' => 'Beneficios de Viajar con Bogui',
			'RegisterConductor' => 'Regístrate como Conductor',
			'BeneficiosConductor' => 'Beneficios de Conducir',
			'Empleo' => 'Consigue empleo con Bogui',
			'Cerrar' => 'Cerrar Sesión',
			'Iniciar' => 'Iniciar Sesión',
			'SerConductor' => 'Ser Conductor Bogui',
			'Cuenta' => 'Mi Cuenta'
		],

		'Footer' => [
			'CambiarIdioma' => 'Cambiar de Idioma',
			'Ingles' => 'Ingles',
			'Spanish' => 'Español',
			'Privacidad' => 'Privacidad',
			'Mision' => 'Misión',
			'Nosotros' => '¿Quiénes Somos?',
			'Terminos' => 'Términos y Condiciones',
			'Conductor' => 'Conductor',
			'Pasajero' => 'Pasajero',
			'Ayuda' => 'Teléfono de Ayuda',
			'Enlace' => 'Enlace a Telegram',
			'Derechos' => 'Todos los derechos reservados',
			'BeneficiosSocio' => 'Beneficios de ser socio Bogui'
 		]
	];