import Vue from 'vue'
import io from 'socket.io-client';
import { APP_URL, SOCKET_URL } from './.env';

window.baseURL = APP_URL;

Vue.component("view-route", require("./public/routes/View"));
Vue.component("view-followpassenger", require("./public/followPassengers/index"));

const socket = io(SOCKET_URL);

const app = new Vue({
    el: '#app'
});