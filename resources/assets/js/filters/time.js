const time = {}

function convertToLatinFormat(date, format) {
	let hours = Math.floor(date / 3600);
	date = date - hours * 3600;
	let minutes = Math.floor(date / 60);
	let seconds = date - minutes * 60;
	date = hours+':'+minutes+':'+seconds

    return moment(date, 'HH:mm:ss').format(format);
}

time.install = function (Vue) {
    Vue.filter("time", (val, format = "HH:mm:ss") => {
      return convertToLatinFormat(val, format);
    });
}

export default time