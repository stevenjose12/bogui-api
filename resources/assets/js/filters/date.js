const date = {}

function convertToLatinFormat(date, format) {
    return moment(date).format(format);
}

date.install = function (Vue) {
    Vue.filter("date", (val, format = "DD-MM-YYYY") => {
      return convertToLatinFormat(val, format);
    });
}

export default date
