-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-12-2019 a las 13:14:12
-- Versión del servidor: 5.7.28-0ubuntu0.16.04.2
-- Versión de PHP: 5.6.40-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `boggui_react`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `about`
--

CREATE TABLE `about` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mission` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mission_english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefits` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefits_english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `about`
--

INSERT INTO `about` (`id`, `content`, `content_english`, `mission`, `mission_english`, `benefits`, `benefits_english`, `created_at`, `updated_at`) VALUES
(1, 'Bogui es una empresa Chilena que provee una poderosa herramienta tecnológica para tu Smartphone, la que permite conectar a un conductor con vehículo y a un pasajero con la necesidad de moverse de un lugar a otro, ésta funciona en tiempo real y te muestra información importante tanto del conductor como del pasajero, brindándote la tranquilidad y la confianza que necesitas para moverte de un lugar a otro, para los conductores es una alternativa muy sencilla para generar ingresos extras sin necesidad de cumplir horarios ni contratos, la flexibilidad que te ofrece Bogui es muy importante para ti porque eres tú quien maneja los tiempos según tus necesidades de generar ingresos.\r\n\r\nBogui se crea pensando en la constante problemática que existe en el transporte de pasajeros, como incomodidad, acosos, robos, engaños, asaltos y muchas cosas más que afectan a los usuarios que sólo tienen la necesidad de moverse por la ciudad, ya sea por trabajo o placer o simplemente la necesidad, pensando en esta diaria problemática es que Bogui implementa una importante herramienta tecnológica reflejada en una aplicación móvil, el proceso de registro para crear una cuenta tanto de pasajero como de conductor es muy meticulosa, seleccionamos e identificamos cada una de las cuentas con Rut , realizamos la verificación de datos para garantizar la seguridad y la tranquilidad a la hora de viajar, te invitamos a probar y vivir esta nueva experiencia en tus viajes cotidianos.', 'Bogui es una empresa Chilena que provee una poderosa herramienta tecnológica para tu Smartphone, la que permite conectar a un conductor con vehículo y a un pasajero con la necesidad de moverse de un lugar a otro, ésta funciona en tiempo real y te muestra información importante tanto del conductor como del pasajero, brindándote la tranquilidad y la confianza que necesitas para moverte de un lugar a otro, para los conductores es una alternativa muy sencilla para generar ingresos extras sin necesidad de cumplir horarios ni contratos, la flexibilidad que te ofrece Bogui es muy importante para ti porque eres tú quien maneja los tiempos según tus necesidades de generar ingresos.\r\n\r\nBogui se crea pensando en la constante problemática que existe en el transporte de pasajeros, como incomodidad, acosos, robos, engaños, asaltos y muchas cosas más que afectan a los usuarios que sólo tienen la necesidad de moverse por la ciudad, ya sea por trabajo o placer o simplemente la necesidad, pensando en esta diaria problemática es que Bogui implementa una importante herramienta tecnológica reflejada en una aplicación móvil, el proceso de registro para crear una cuenta tanto de pasajero como de conductor es muy meticulosa, seleccionamos e identificamos cada una de las cuentas con Rut , realizamos la verificación de datos para garantizar la seguridad y la tranquilidad a la hora de viajar, te invitamos a probar y vivir esta nueva experiencia en tus viajes cotidianos.', 'Nuestra misión es ofrecer mucho más que un viaje a nuestros clientes, estamos enfocados en que tus viajes sean diferentes, confortables, transparentes. Queremos evitarte el trágico y estresante viaje en el transporte público, nuestras tarifas son claras y transparentes, no queremos que tengas más sorpresas al llegar a tu destino descarga la app y vive un viaje diferente.', 'Nuestra misión es ofrecer mucho más que un viaje a nuestros clientes, estamos enfocados en que tus viajes sean diferentes, confortables, transparentes. Queremos evitarte el trágico y estresante viaje en el transporte público, nuestras tarifas son claras y transparentes, no queremos que tengas más sorpresas al llegar a tu destino descarga la app y vive un viaje diferente.', 'Bogui es una aplicación móvil que permite conectar un conductor con un pasajero, un Bogui corresponde a un vehículo privado con conductor y siempre habrá uno disponible para ti, para llevarte al lugar que tú prefieras, los principales beneficios que te ofrecemos son:', 'Bogui es una aplicación móvil que permite conectar un conductor con un pasajero, un Bogui corresponde a un vehículo privado con conductor y siempre habrá uno disponible para ti, para llevarte al lugar que tú prefieras, los principales beneficios que te ofrecemos son:', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_type`
--

CREATE TABLE `accounts_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `accounts_type`
--

INSERT INTO `accounts_type` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cuenta Corriente', NULL, NULL, NULL),
(2, 'Cuenta de Ahorro', NULL, NULL, NULL),
(3, 'Cuenta Vista', NULL, NULL, NULL),
(4, 'Cuenta Rut', NULL, NULL, NULL),
(5, 'Chequera Electrónica', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_notifications`
--

CREATE TABLE `admin_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `admin_notifications`
--

INSERT INTO `admin_notifications` (`id`, `title`, `file`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'hola a todos', NULL, 'hola a todos', NULL, '2019-12-28 16:09:08', '2019-12-28 16:09:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_notifications_users`
--

CREATE TABLE `admin_notifications_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `admin_notifications_users`
--

INSERT INTO `admin_notifications_users` (`id`, `notification_id`, `user_id`, `viewed`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, NULL, '2019-12-28 16:09:08', '2019-12-28 16:09:08'),
(2, 1, 2746, 0, NULL, '2019-12-28 16:09:08', '2019-12-28 16:09:08'),
(3, 1, 2747, 0, NULL, '2019-12-28 16:09:08', '2019-12-28 16:09:08'),
(4, 1, 2748, 0, NULL, '2019-12-28 16:09:08', '2019-12-28 16:09:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `airports`
--

CREATE TABLE `airports` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `airports`
--

INSERT INTO `airports` (`id`, `name`, `lat`, `lng`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Aeropuerto Internacional Comodoro Arturo Merino Benítez', '-33.392777777778', '-70.785555555556', '2018-10-10 04:00:00', '2018-10-10 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banks`
--

INSERT INTO `banks` (`id`, `name`, `registered_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Banco Santander-Chile', 1, NULL, NULL, NULL),
(2, 'Banco de Chile', 1, NULL, NULL, NULL),
(3, 'Banco Internacional', 1, NULL, NULL, NULL),
(4, 'Scotiabank Chile', 1, NULL, NULL, NULL),
(5, 'Banco de Crédito e Inversiones', 1, NULL, NULL, NULL),
(6, 'Corpbanca', 1, NULL, NULL, NULL),
(7, 'Banco Bice', 1, NULL, NULL, NULL),
(8, 'HSBC Bank (Chile)', 1, NULL, NULL, NULL),
(9, 'Banco Itaú Chile', 1, NULL, NULL, NULL),
(10, 'Banco Security', 1, NULL, NULL, NULL),
(11, 'Banco Falabella', 1, NULL, NULL, NULL),
(12, 'Deutsche Bank (Chile)', 1, NULL, NULL, NULL),
(13, 'Banco Ripley', 1, NULL, NULL, NULL),
(14, 'Rabobank Chile', 1, NULL, NULL, NULL),
(15, 'Banco Consorcio', 1, NULL, NULL, NULL),
(16, 'Banco Penta', 1, NULL, NULL, NULL),
(17, 'Banco Paris', 1, NULL, NULL, NULL),
(18, 'Banco Bilbao Vizcaya Argentaria, Chile.', 1, NULL, NULL, NULL),
(19, 'Banco do Brasil S.A.', 1, NULL, NULL, NULL),
(20, 'JP Morgan Chase Bank', 1, NULL, NULL, NULL),
(21, 'Banco de la Nación Argentina', 1, NULL, NULL, NULL),
(22, 'The Bank of Tokyo-Mitsubishi', 1, NULL, NULL, NULL),
(23, 'DnB Bank Asa.', 1, NULL, NULL, NULL),
(24, 'Banco Estado', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banks_user`
--

CREATE TABLE `banks_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `identification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_swedish_ci NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_swedish_ci NOT NULL,
  `type` int(11) NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banks_user`
--

INSERT INTO `banks_user` (`id`, `user_id`, `identification`, `number`, `type`, `bank_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2746, '162848-k', '3120008546009489', 1, 15, '2019-12-28 13:44:55', '2019-12-28 13:44:55', NULL),
(2, 2747, '134642', '00882007486', 3, 15, '2019-12-28 14:24:55', '2019-12-28 14:24:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `benefits`
--

CREATE TABLE `benefits` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_english` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `benefits`
--

INSERT INTO `benefits` (`id`, `title`, `title_english`, `content`, `english`, `created_at`, `updated_at`) VALUES
(1, 'Disponibilidad 24/7', 'Disponibilidad 24/7', 'no importa dónde te encuentres ni la hora que sea, sólo abre tu aplicación y con pocos clics solicita tu servicio, en pocos minutos ya tendrás un conductor Bogui a tu servicio.', 'no importa dónde te encuentres ni la hora que sea, sólo abre tu aplicación y con pocos clics solicita tu servicio, en pocos minutos ya tendrás un conductor Bogui a tu servicio.', NULL, NULL),
(2, 'Comodidad', 'Comodidad', 'viaja con la comodidad que mereces nos preocupamos que nuestros Bogui cumplan con ciertos estándares de seguridad y Confort para que tu viaje sea placentero.', 'viaja con la comodidad que mereces nos preocupamos que nuestros Bogui cumplan con ciertos estándares de seguridad y Confort para que tu viaje sea placentero.', NULL, NULL),
(3, 'Economía', 'Economía', 'Bogui te garantiza tarifas claras y económicas para que te sientas tranquilo, para ellos siempre te mostraremos la tarifa de tu viaje antes de solicitar el servicio ( las tarifas pueden variar si haces cambios durante el trayecto como modificar tu destino o hacer una parada adicional)', 'Bogui te garantiza tarifas claras y económicas para que te sientas tranquilo, para ellos siempre te mostraremos la tarifa de tu viaje antes de solicitar el servicio ( las tarifas pueden variar si haces cambios durante el trayecto como modificar tu destino o hacer una parada adicional)', NULL, NULL),
(4, 'Flexibilidad', 'Flexibilidad', 'Bogui te proporciona esa gran ventaja que necesitas, es personalizable a tu necesidad, no importa el lugar ni la hora para solicitar un Bogui, pídelo desde el lugar que mas te acomode y míralo venir en tiempo real, siempre habrá uno disponible.', 'Bogui te proporciona esa gran ventaja que necesitas, es personalizable a tu necesidad, no importa el lugar ni la hora para solicitar un Bogui, pídelo desde el lugar que mas te acomode y míralo venir en tiempo real, siempre habrá uno disponible.', NULL, NULL),
(5, 'Profesionalismo', 'Profesionalismo', 'nos aseguramos que todos nuestros conductores estén correctamente capacitados para conducir, transportar personas y estén familiarizados con tu ciudad, queremos que tengas una grata experiencia.', 'nos aseguramos que todos nuestros conductores estén correctamente capacitados para conducir, transportar personas y estén familiarizados con tu ciudad, queremos que tengas una grata experiencia.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(10) UNSIGNED NOT NULL,
  `route_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `blacklist`
--

INSERT INTO `blacklist` (`id`, `driver_id`, `route_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2746, 2, NULL, '2019-12-28 14:48:50', '2019-12-28 14:48:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category_notification`
--

CREATE TABLE `category_notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_type_id` int(10) UNSIGNED NOT NULL,
  `category` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `category_notification`
--

INSERT INTO `category_notification` (`id`, `vehicle_type_id`, `category`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-09-25 20:04:20', '2018-09-25 20:04:20'),
(2, 2, 1, '2018-09-25 20:04:20', '2018-09-25 20:04:20'),
(3, 2, 2, '2018-09-25 20:04:20', '2018-09-25 20:04:20'),
(4, 3, 2, '2018-09-25 20:04:20', '2018-09-25 20:04:20'),
(5, 3, 3, '2018-09-25 20:04:20', '2018-09-25 20:04:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `route_id` int(10) UNSIGNED DEFAULT NULL,
  `support` int(11) DEFAULT NULL COMMENT '0: No es soporte, 1: Soporte',
  `motive_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id de un registro de support_motive',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `chats`
--

INSERT INTO `chats` (`id`, `route_id`, `support`, `motive_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 4, 0, NULL, NULL, '2019-12-28 16:10:57', '2019-12-28 16:10:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat_user`
--

CREATE TABLE `chat_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_id` int(10) UNSIGNED NOT NULL COMMENT 'Sala de Chat',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'Usuario',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `chat_user`
--

INSERT INTO `chat_user` (`id`, `chat_id`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 2748, NULL, '2019-12-28 16:10:57', '2019-12-28 16:10:57'),
(2, 1, 2746, NULL, '2019-12-28 16:10:57', '2019-12-28 16:10:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `commissions`
--

CREATE TABLE `commissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `registered_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `commissions`
--

INSERT INTO `commissions` (`id`, `amount`, `registered_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 10, 1, NULL, '2018-09-27 15:03:42', '2019-12-10 02:34:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `telegram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contact`
--

INSERT INTO `contact` (`id`, `telegram`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'https://t.me/bogui', '2 264 904 59', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afganistán', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Argelia', 213),
(4, 'AS', 'Samoa Americana', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguila', 1264),
(8, 'AQ', 'Antártida', 0),
(9, 'AG', 'Antigua y Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaiyán', 994),
(16, 'BS', 'Las Bahamas', 1242),
(17, 'BH', 'Bahrein', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarús', 375),
(21, 'BE', 'Bélgica', 32),
(22, 'BZ', 'Belice', 501),
(23, 'BJ', 'Benín', 229),
(24, 'BM', 'Islas Bermudas', 1441),
(25, 'BT', 'Bhután', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia y Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Isla Bouvet', 0),
(30, 'BR', 'Brasil', 55),
(31, 'IO', 'Territorio Británico del Océano Índico', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Camboya', 855),
(37, 'CM', 'Camerún', 237),
(38, 'CA', 'Canadá', 1),
(39, 'CV', 'Cabo Verde', 238),
(40, 'KY', 'Islas Caimán', 1345),
(41, 'CF', 'República Centroafricana', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Isla de Navidad', 61),
(46, 'CC', 'Islas Cocos (Keeling)', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoras', 269),
(49, 'CG', 'República del Congo', 242),
(50, 'CD', 'República Democrática del Congo', 242),
(51, 'CK', 'Islas Cook', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Costa de Marfil (Costa de Marfil)', 225),
(54, 'HR', 'Croacia (Croacia)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Chipre', 357),
(57, 'CZ', 'Republica checa', 420),
(58, 'DK', 'Dinamarca', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'República Dominicana', 1809),
(62, 'TP', 'Timor Oriental', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egipto', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Guinea Ecuatorial', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Etiopía', 251),
(70, 'XA', 'Territorios Externos de Australia', 61),
(71, 'FK', 'Islas Malvinas', 500),
(72, 'FO', 'Islas Faroe', 298),
(73, 'FJ', 'Islas Fiji', 679),
(74, 'FI', 'Finlandia', 358),
(75, 'FR', 'Francia', 33),
(76, 'GF', 'Guayana Francesa', 594),
(77, 'PF', 'Polinesia francés', 689),
(78, 'TF', 'Territorios Franceses del Sur', 0),
(79, 'GA', 'Gabón', 241),
(80, 'GM', 'Gambia', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Alemania', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Grecia', 30),
(86, 'GL', 'Tierra Verde', 299),
(87, 'GD', 'Granada', 1473),
(88, 'GP', 'Guadalupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey y Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea Bissau', 245),
(94, 'GY', 'Guayana', 592),
(95, 'HT', 'Haití', 509),
(96, 'HM', 'Islas Heard y McDonald', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'RAE de Hong Kong.', 852),
(99, 'HU', 'Hungría', 36),
(100, 'IS', 'Islandia', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Irak', 964),
(105, 'IE', 'Irlanda', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italia', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japón', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordán', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenia', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Corea del Norte', 850),
(116, 'KR', 'Corea del Sur', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kirguistán', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Letonia', 371),
(121, 'LB', 'Líbano', 961),
(122, 'LS', 'Lesoto', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libia', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lituania', 370),
(127, 'LU', 'Luxemburgo', 352),
(128, 'MO', 'RAE de Macau', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malasia', 60),
(133, 'MV', 'Maldivas', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Hombre (Isla de)', 44),
(137, 'MH', 'Islas Marshall', 692),
(138, 'MQ', 'Martinica', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauricio', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldavia', 373),
(145, 'MC', 'Mónaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Marruecos', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Antillas Holandesas', 599),
(155, 'NL', 'Países Bajos', 31),
(156, 'NC', 'Nueva Caledonia', 687),
(157, 'NZ', 'Nueva Zelanda', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Níger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Isla Norfolk', 672),
(163, 'MP', 'Islas Marianas del Norte', 1670),
(164, 'NO', 'Noruega', 47),
(165, 'OM', 'Omán', 968),
(166, 'PK', 'Pakistán', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Territorio Palestino, Ocupado', 970),
(169, 'PA', 'Panamá', 507),
(170, 'PG', 'Papúa Nueva Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Filipinas', 63),
(174, 'PN', 'Isla Pitcairn', 0),
(175, 'PL', 'Polonia', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Katar', 974),
(179, 'RE', 'Reunión', 262),
(180, 'RO', 'Rumania', 40),
(181, 'RU', 'Rusia', 70),
(182, 'RW', 'Ruanda', 250),
(183, 'SH', 'Santa Helena', 290),
(184, 'KN', 'San Cristóbal y Nieves', 1869),
(185, 'LC', 'Santa Lucía', 1758),
(186, 'PM', 'San Pedro y Miquelón', 508),
(187, 'VC', 'San Vicente y las Granadinas', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Santo Tomé y Príncipe', 239),
(191, 'SA', 'Arabia Saudita', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leona', 232),
(196, 'SG', 'Singapur', 65),
(197, 'SK', 'Eslovaquia', 421),
(198, 'SI', 'Eslovenia', 386),
(199, 'XG', 'Territorios más pequeños del Reino Unido', 44),
(200, 'SB', 'Islas Salomón', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'Sudáfrica', 27),
(203, 'GS', 'Georgia del sur', 0),
(204, 'SS', 'Sudán del Sur', 211),
(205, 'ES', 'España', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudán', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Islas Svalbard y Jan Mayen', 47),
(210, 'SZ', 'Swazilandia', 268),
(211, 'SE', 'Suecia', 46),
(212, 'CH', 'Suiza', 41),
(213, 'SY', 'Siria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tayikistán', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Tailandia', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad y Tobago', 1868),
(222, 'TN', 'Túnez', 216),
(223, 'TR', 'Turquía', 90),
(224, 'TM', 'Turkmenistán', 7370),
(225, 'TC', 'Islas Turcas y Caicos', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ucrania', 380),
(229, 'AE', 'Emiratos Árabes Unidos', 971),
(230, 'GB', 'Reino Unido', 44),
(231, 'US', 'Estados Unidos', 1),
(232, 'UM', 'Islas menores alejadas de los Estados Unidos', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistán', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Estado de la Ciudad del Vaticano (Santa Sede)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Islas Vírgenes (británicas)', 1284),
(240, 'VI', 'Islas Vírgenes (EE.UU.)', 1340),
(241, 'WF', 'Islas Wallis y Futuna', 681),
(242, 'EH', 'Sahara Occidental', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabue', 263);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deleted_chats`
--

CREATE TABLE `deleted_chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `chat_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinies`
--

CREATE TABLE `destinies` (
  `id` int(10) UNSIGNED NOT NULL,
  `route_id` int(10) UNSIGNED NOT NULL,
  `latitud` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitud` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1 = parada, 2 = destino',
  `type_special` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `destinies`
--

INSERT INTO `destinies` (`id`, `route_id`, `latitud`, `longitud`, `deleted_at`, `created_at`, `updated_at`, `type`, `type_special`, `description`) VALUES
(1, 1, '10.315707117989124', '-67.643665317446', NULL, '2019-12-28 14:47:36', '2019-12-28 14:47:36', 2, 0, 'Vía Ocumare de la Costa, El Limón 2105, Aragua, Venezuela'),
(2, 2, '10.310850600239078', '-67.64150680974126', NULL, '2019-12-28 14:48:32', '2019-12-28 14:48:32', 2, 0, '281 Vía Ocumare de la Costa, El Limón 2105, Aragua, Venezuela'),
(3, 3, '10.306920932167575', '-67.63930136337876', NULL, '2019-12-28 16:01:44', '2019-12-28 16:01:44', 2, 0, 'AV. Universidad Local 18-3, El Progreso, Sector Arias Blanco, El Limón 2105, Aragua, Venezuela'),
(6, 4, '10.287296589118073', '-67.62441460043192', NULL, '2019-12-28 16:11:24', '2019-12-28 16:11:24', 2, 0, 'Avenida Universidad, El Limón 2105, Aragua, Venezuela');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `driver_benefits`
--

CREATE TABLE `driver_benefits` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_english` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `driver_benefits`
--

INSERT INTO `driver_benefits` (`id`, `title`, `title_english`, `content`, `english`, `created_at`, `updated_at`) VALUES
(1, 'Tu tiempo', 'Tu tiempo', 'es muy importante Por eso no te lo exigimos, tú decides cuándo y dónde generar ingresos.', 'es muy importante Por eso no te lo exigimos, tú decides cuándo y dónde generar ingresos.', NULL, NULL),
(2, 'Mejores ganancias', 'Mejores ganancias', 'garantizadas, somos la aplicación que siempre te cobrará la comisión más baja para asegurarnos que tus ingresos sean más altos, recibe pagos semanales directamente a tu cuenta o como tú lo prefieras.', 'garantizadas, somos la aplicación que siempre te cobrará la comisión más baja para asegurarnos que tus ingresos sean más altos, recibe pagos semanales directamente a tu cuenta o como tú lo prefieras.', NULL, NULL),
(3, 'Soporte 24/7', 'Soporte 24/7', 'Te brindamos un respuesta a todas tus inquietudes y resolveremos todas aquellas que nos competan.', 'Te brindamos un respuesta a todas tus inquietudes y resolveremos todas aquellas que nos competan.', NULL, NULL),
(4, 'Seguridad,', 'Seguridad,', 'te garantizamos que las personas que trasladas están plenamente registradas en nuestra base de datos, por lo que tú servicio lo puedes dar con toda la confianza', 'te garantizamos que las personas que trasladas están plenamente registradas en nuestra base de datos, por lo que tú servicio lo puedes dar con toda la confianza', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employment_conditions`
--

CREATE TABLE `employment_conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `employment_conditions`
--

INSERT INTO `employment_conditions` (`id`, `content`, `english`, `created_at`, `updated_at`) VALUES
(1, 'Sedan', 'Sedan', NULL, NULL),
(2, 'Hatchback 5 puertas', 'Hatchback 5 puertas', NULL, NULL),
(3, 'Suv y subcompactos para 4 o 6 pasajeros', 'Suv y subcompactos para 4 o 6 pasajeros', NULL, NULL),
(4, 'Van y Minivan mínimo 6 pasajeros', 'Van y Minivan mínimo 6 pasajeros', NULL, NULL),
(5, 'Aire acondicionado', 'Aire acondicionado', NULL, NULL),
(6, 'Calefacción', 'Calefacción', NULL, NULL),
(7, 'Radio', 'Radio', NULL, NULL),
(8, 'Al menos dos airbags', 'Al menos dos airbags', NULL, NULL),
(9, 'Cinturón de seguridad en todos los asientos', 'Cinturón de seguridad en todos los asientos', NULL, NULL),
(10, 'Año 2010 en adelante', 'Año 2010 en adelante', NULL, NULL),
(11, 'Permiso de circulación al día', 'Permiso de circulación al día', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `information`
--

CREATE TABLE `information` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'img/information-default-logo.png',
  `type` int(11) NOT NULL COMMENT '1: Conductor, 2: Pasajero, 3: Ambos',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0: Desactivado, 1: Activo',
  `start_date` date DEFAULT NULL COMMENT 'Fecha de inicio',
  `end_date` date DEFAULT NULL COMMENT 'Fecha de finalizado',
  `created_by` int(11) NOT NULL COMMENT 'Usuario que la creo',
  `updated_by` int(11) NOT NULL COMMENT 'Ulitmo usuario que la modifico',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `information`
--

INSERT INTO `information` (`id`, `title`, `description`, `logo`, `type`, `status`, `start_date`, `end_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '¡Bienvenido a Bogui!', 'La mejor opción de transporte', 'img/information-default-logo.png', 1, 1, NULL, NULL, 1, 1, '2019-10-15 03:00:00', '2019-10-15 03:00:00', NULL),
(2, '¡Bienvenido a Bogui!', 'La mejor opción de Transporte', 'img/img_welcome.jpg', 2, 1, NULL, NULL, 1, 1, '2019-10-15 03:00:00', '2019-10-15 03:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_id` int(10) UNSIGNED NOT NULL COMMENT 'Sala de Chat',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'Usuario',
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ruta del archivo',
  `seen` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Visto por el destinatario',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_10_134551_create_persons_table', 1),
(4, '2018_09_10_134932_create_promotions_table', 1),
(5, '2018_09_10_135343_create_promotion_user_table', 1),
(6, '2018_09_10_135344_create_vehicle_type_table', 1),
(7, '2018_09_10_135345_create_vehicles_table', 1),
(8, '2018_09_10_135346_create_tariffs_table', 1),
(9, '2018_09_10_135347_create_routes_table', 1),
(10, '2018_09_10_135348_create_route_user_table', 1),
(11, '2018_09_10_135349_create_payments_table', 1),
(12, '2018_09_10_140157_create_notifications_table', 1),
(13, '2018_09_10_143437_create_payment_promotion_table', 1),
(14, '2018_09_10_145503_create_ratings_table', 1),
(15, '2018_09_10_1499443_create_destinies_table', 1),
(16, '2018_09_10_150244_create_commissions_table', 1),
(17, '2018_09_10_150456_create_support_motive_table', 1),
(18, '2018_09_10_150733_create_chats_table', 1),
(19, '2018_09_12_183706_add_level_to_users', 2),
(20, '2018_09_12_193233_add_status_to_users', 3),
(21, '2018_09_12_193526_add_status_to_vehicles', 3),
(22, '2018_09_13_153734_alter_vehicles_table', 4),
(23, '2018_09_13_154450_alter_persons_table', 4),
(24, '2018_09_13_154631_alter_promotion_user_table1', 4),
(25, '2018_09_13_154856_alter_routes_table', 4),
(26, '2018_09_13_154932_alter_tariffs_table', 5),
(27, '2018_09_13_172222_alter_tariffs_kms_colummns_table', 6),
(28, '2018_09_13_172507_alter_vehicles_nullable_table', 7),
(29, '2018_09_14_133705_add_field_validate_table_users', 8),
(30, '2018_09_14_143401_add_default_value_column_status_of_users', 9),
(31, '2018_09_14_152204_laratrust_setup_tables', 10),
(32, '2018_09_14_203732_drop_name_users_table', 11),
(35, '2018_09_17_135140_add_columns_file_description_table_vehicle_type', 12),
(38, '2018_09_18_133244_add_fields_table_persons', 13),
(39, '2018_09_18_150734_create_password_app', 14),
(40, '2018_09_18_190952_create_banks_table', 15),
(41, '2018_09_18_191008_create_banks_user_table', 16),
(43, '2018_09_18_201206_update_vehicles_table_status', 17),
(44, '2018_09_18_203719_update_photos_driver', 18),
(45, '2018_09_18_193831_create_table_validations', 19),
(46, '2018_09_19_141828_update_field_table_user', 19),
(47, '2018_09_19_144000_update_field_status_table_user', 19),
(48, '2018_09_21_104119_commet_field_status_table_vehicles', 20),
(49, '2018_09_21_105840_add_coment_table_tariff', 21),
(50, '2018_09_24_130409_add_fild_type_table_destinies', 22),
(51, '2018_09_24_132503_add_fild_user_id_table_routes', 22),
(52, '2018_09_24_133747_drop_columns_table_routes', 22),
(53, '2018_09_24_143802_add_column_num_uses_to_table_promotions', 23),
(54, '2018_09_25_082348_add_field_driver_id_table_routes', 23),
(55, '2018_09_25_090155_create_table_vehicle_category', 24),
(56, '2018_09_25_111909_update_ratings_points', 24),
(57, '2018_09_25_103944_update_drop_foreign_keys_table_vehicle_category', 25),
(58, '2018_09_25_155300_create_category_notification', 25),
(59, '2018_09_25_155537_create_blacklist_table', 25),
(60, '2018_09_25_135720_update_routes_canceled_reason', 26),
(61, '2018_09_26_094357_create_column_default_navigator', 26),
(62, '2018_09_26_100134_column_status_driver', 26),
(63, '2018_09_26_131136_create_stores_table', 27),
(64, '2018_09_26_150902_update_route_status_route', 27),
(65, '2018_09_26_112500_update_chats_table', 28),
(66, '2018_09_26_112600_create_chat_user_table', 28),
(67, '2018_09_26_112616_create_messages_table', 28),
(68, '2018_09_26_152251_update_tariffs_table', 28),
(69, '2018_09_26_153843_create_tariff_type_table', 28),
(70, '2018_09_26_153857_create_types_table', 28),
(71, '2018_09_28_090650_alter_users_table', 29),
(72, '2018_09_28_113840_update_support_type', 30),
(73, '2018_10_03_103354_add_column_route_id_table_notifications', 31),
(76, '2018_10_03_145642_add_column_price_table_route', 32),
(77, '2018_10_03_145714_add_column_type_special_table_destinies', 32),
(78, '2018_10_08_144339_add_default_payment', 33),
(79, '2018_10_10_091244_create_table_airports', 34),
(80, '2018_10_10_095210_add_column_tariff_special_table_routes', 35),
(81, '2018_10_11_090251_add_viewed_notification', 36),
(82, '2018_10_11_092336_update_ratings_to_bogui', 36),
(83, '2018_10_11_135500_add_time_stop', 37),
(84, '2018_10_11_152722_add_payment_route', 38),
(85, '2018_10_15_080809_add_date_driver', 39),
(86, '2018_10_15_140539_accounts_type_table', 40),
(87, '2018_10_15_144918_change_enum_bank_user', 40),
(88, '2018_10_17_155624_modify_payments_table_for_drivers', 41),
(89, '2018_10_18_094731_add_column_debt_payments', 41),
(90, '2018_10_19_104847_create_search_radius_table', 42),
(91, '2018_10_21_203818_create_terms_table', 42),
(92, '2018_10_21_204133_create_privacy_table', 42),
(93, '2018_10_25_081201_add_route_photo', 43),
(94, '2018_10_26_144123_update_notifications_table', 44),
(95, '2018_10_29_094527_deleted_chats_table', 45),
(96, '2018_11_01_091829_update_stores_table', 46),
(97, '2018_11_01_094531_create_field_paid_in_table_route', 47),
(98, '2018_11_01_100500_create_weekly_debt_table', 48),
(99, '2018_11_01_085621_update_stores_table', 49),
(100, '2018_11_02_081740_create_contact_table', 49),
(101, '2018_11_02_132340_add_field_paid_table_promotion_user', 50),
(102, '2018_11_02_142916_add_field_accepted_table_promotion_user', 51),
(107, '2018_11_05_075438_create_about_table', 52),
(108, '2018_11_05_075504_create_benefits_table', 52),
(109, '2018_11_05_085626_create_employment_conditions_table', 53),
(110, '2018_11_05_091235_create_driver_benefits', 53),
(111, '2018_11_05_091253_create_passenger_benefits', 53),
(113, '2018_11_12_112310_create_last_status_driver', 54),
(114, '2018_11_20_163151_user_webpay_field', 55),
(115, '2018_11_21_092538_digits_credit_card', 56),
(116, '2018_11_21_093351_create_webpay_table', 57),
(117, '2018_11_21_094436_update_webpay_table', 58),
(118, '2018_11_22_081033_alter_tariffs_table_add_other_tariff', 59),
(119, '2018_11_22_113936_update_payments_stop', 60),
(121, '2018_12_05_161102_add_field_distance_table_route', 61),
(122, '2018_12_10_131028_add_field_airport_table_routes', 62),
(125, '2018_12_11_112449_create_pending_documents', 63),
(126, '2018_12_11_113356_update_person_expiration_document', 63),
(127, '2018_12_12_090810_change_pending_documents', 64),
(128, '2018_12_12_130435_upload_field_pending_documents', 65),
(129, '2018_12_13_083842_update_notifications', 66),
(130, '2018_12_13_085101_notification_global', 66),
(131, '2018_12_13_113407_add_comment_pending_documents_table', 67),
(132, '2018_12_14_134500_change_description_notification', 68),
(133, '2019_01_24_163123_create_admin_notifications', 69),
(134, '2019_01_24_163336_create_admin_notifications_users', 69),
(135, '2019_01_25_150842_create_night_mode_table', 70),
(137, '2019_02_01_102723_create_table_opentok', 71),
(138, '2019_02_06_104302_update_routes_description', 72),
(139, '2019_02_06_104321_update_destinies_description', 72),
(140, '2019_03_19_090727_user_id_field_password_app', 73),
(141, '2019_03_19_093217_create_discounts_table', 74),
(142, '2019_05_24_162639_add_field_alive_in_table_users', 74),
(143, '2019_07_22_094019_create_information_modal_table', 74),
(144, '2019_08_14_104554_add_column_permit_to_vehicles', 74),
(145, '2019_09_27_100841_add_vehicle_id_column_to_pending_documents_table', 74),
(146, '2019_10_30_140225_create_table_refresh_token', 75),
(147, '2019_11_01_143226_panic_options_table', 76),
(148, '2019_11_01_144242_panic_radius', 76),
(149, '2019_11_01_144617_panic_table', 76),
(150, '2019_11_01_144756_panic_details', 76),
(151, '2019_11_04_103038_latitude_longitude_panic', 76),
(152, '2019_12_03_101459_create_usersContacts_table', 77),
(153, '2019_12_03_145627_add_phoneCode_column_in_usersContacts_table', 77),
(154, '2019_12_11_160710_add_type_Column_in_panic_table', 78);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `night_mode`
--

CREATE TABLE `night_mode` (
  `id` int(10) UNSIGNED NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `night_mode`
--

INSERT INTO `night_mode` (`id`, `start`, `end`, `created_at`, `updated_at`) VALUES
(1, '18:40:00', '07:30:00', NULL, '2019-10-17 03:38:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL COMMENT '\n                1: Carrera Aceptada, \n                2: Carrera Finalizada,\n                3: Carrera Cancelada, \n                4: Destino Cambiado, \n                5: Solicitud de Conductor Aceptada,\n                6: Documentos Requeridos,\n                7: Documentos Aceptados,\n                8: Documentos Rechazados,\n                9: Documentos Enviados (Admin),\n                10: Documentos Expirados Proximamente',
  `description` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `route_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `description`, `user_id`, `viewed`, `route_id`, `deleted_at`, `created_at`, `updated_at`, `driver_id`) VALUES
(1, 5, 'Bienvenido a Bogui. ¡Ya puedes empezar a conducir!', 2746, 0, NULL, NULL, '2019-12-28 15:04:32', '2019-12-28 15:04:32', NULL),
(2, 5, 'Bienvenido a Bogui. ¡Ya puedes empezar a conducir!', 2747, 0, NULL, NULL, '2019-12-28 15:28:09', '2019-12-28 15:28:09', NULL),
(3, 1, NULL, 2748, 0, 4, NULL, '2019-12-28 17:10:56', '2019-12-28 17:10:56', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opentok`
--

CREATE TABLE `opentok` (
  `id` int(10) UNSIGNED NOT NULL,
  `route_id` int(10) UNSIGNED NOT NULL,
  `sessionId` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panic`
--

CREATE TABLE `panic` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1: Activo, 0: Resuelto',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1: Conductor, 2: Pasajero',
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `panic`
--

INSERT INTO `panic` (`id`, `user_id`, `status`, `type`, `latitude`, `longitude`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2748, 1, 2, '10.2970553', '-67.6316445', '2019-12-28 14:41:46', '2019-12-28 14:41:46', NULL),
(2, 2748, 1, 2, '10.2970492', '-67.6316296', '2019-12-28 14:44:52', '2019-12-28 14:44:52', NULL),
(3, 2746, 0, 1, '10.30077971414998', '-67.62988327031344', '2019-12-28 16:06:07', '2019-12-28 16:06:19', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panic_details`
--

CREATE TABLE `panic_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `panic_id` int(10) UNSIGNED NOT NULL,
  `response_id` int(10) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `panic_details`
--

INSERT INTO `panic_details` (`id`, `user_id`, `panic_id`, `response_id`, `comment`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2747, 3, NULL, NULL, '2019-12-28 16:06:37', '2019-12-28 16:06:37', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panic_options`
--

CREATE TABLE `panic_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `panic_options`
--

INSERT INTO `panic_options` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Falsa Alarma', NULL, NULL, NULL),
(2, 'El conductor esta bien', NULL, NULL, NULL),
(3, 'El conductor solucionó el problema', NULL, NULL, NULL),
(4, 'El conductor fue asaltado', NULL, NULL, NULL),
(5, 'Otro', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panic_radius`
--

CREATE TABLE `panic_radius` (
  `id` int(10) UNSIGNED NOT NULL,
  `distance` double(10,2) NOT NULL COMMENT 'Km',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `panic_radius`
--

INSERT INTO `panic_radius` (`id`, `distance`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 10.00, '2019-11-11 12:37:25', '2019-11-11 12:37:25', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passenger_benefits`
--

CREATE TABLE `passenger_benefits` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_english` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `passenger_benefits`
--

INSERT INTO `passenger_benefits` (`id`, `title`, `title_english`, `content`, `english`, `created_at`, `updated_at`) VALUES
(1, 'Monitorear el Viaje', 'Monitorear el Viaje', 'Bogui te permite ver en tiempo real a tu conductor desde el momento que acepta tu solicitud, además te mostramos toda la información necesaria de tu conductor y del vehículo. Solicita tu Bogui cuando estés listo.', 'Bogui te permite ver en tiempo real a tu conductor desde el momento que acepta tu solicitud, además te mostramos toda la información necesaria de tu conductor y del vehículo. Solicita tu Bogui cuando estés listo.', NULL, NULL),
(2, 'Métodos de Pago', 'Métodos de Pago', 'Bogui es una aplicación diseñada a medida de todos, si no cuentas con una tarjeta de crédito, no te preocupes, puedes pagar tu viaje con dinero en efectivo.', 'Bogui es una aplicación diseñada a medida de todos, si no cuentas con una tarjeta de crédito, no te preocupes, puedes pagar tu viaje con dinero en efectivo.', NULL, NULL),
(3, 'Comunicación', 'Comunicación', 'Porque la comunicación es importante hemos implementado la mensajería instantánea, Una vez que tu conductor acepte tu solicitud, pueden enviarle las instrucciones que desees por medio de mensaje de texto directamente desde tu aplicación.', 'Porque la comunicación es importante hemos implementado la mensajería instantánea, Una vez que tu conductor acepte tu solicitud, pueden enviarle las instrucciones que desees por medio de mensaje de texto directamente desde tu aplicación.', NULL, NULL),
(4, 'Seguridad', 'Seguridad', 'Te garantizamos que todos nuestros conductores están debidamente registrados y sus cuentas verificadas, queremos que sientas la diferencia de un servicio confiable.', 'Te garantizamos que todos nuestros conductores están debidamente registrados y sus cuentas verificadas, queremos que sientas la diferencia de un servicio confiable.', NULL, NULL),
(5, 'Compartir Ubicación (Viaje)', 'Compartir Ubicación (Viaje)', 'Para que te sientas aún más seguro y tranquilo, puedes compartir tu viaje con las personas que se preocupan por ti, viaja con nosotros y descubre nuevas experiencias.', 'Para que te sientas aún más seguro y tranquilo, puedes compartir tu viaje con las personas que se preocupan por ti, viaja con nosotros y descubre nuevas experiencias.', NULL, NULL),
(6, 'Evaluación', 'Evaluación', 'Tu opinión es importante para regular la calidad de nuestro servicio, por eso te pedimos que califiques a cada conductor que te mueva de un lugar a otro. Calificar su servicio es beneficio para todos.', 'Tu opinión es importante para regular la calidad de nuestro servicio, por eso te pedimos que califiques a cada conductor que te mueva de un lugar a otro. Calificar su servicio es beneficio para todos.', NULL, NULL),
(7, 'Promociones', 'Promociones', 'Visita nuestra página frecuentemente y entérate de una serie de promociones y convenios que tendremos especialmente diseñados para ti.', 'Visita nuestra página frecuentemente y entérate de una serie de promociones y convenios que tendremos especialmente diseñados para ti.', NULL, NULL),
(8, 'Soporte', 'Soporte', 'Puedes contactarte con nosotros directamente desde tu aplicación, un equipo de expertos está disponible las 24 horas del día, los 7 días a la semana , queremos resolver todas tus dudas.', 'Puedes contactarte con nosotros directamente desde tu aplicación, un equipo de expertos está disponible las 24 horas del día, los 7 días a la semana , queremos resolver todas tus dudas.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_app`
--

CREATE TABLE `password_app` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1: Activo, 0: Inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `method` int(11) NOT NULL COMMENT '1: Tarjeta, 2: Efectivo, 3: Promocion, 4: Pago a Conductor',
  `total` double(15,2) NOT NULL,
  `commission` double(15,2) NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `debt` decimal(15,2) DEFAULT NULL,
  `paid_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stop` double(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`id`, `method`, `total`, `commission`, `promotion_id`, `route_id`, `user_id`, `debt`, `paid_by`, `deleted_at`, `created_at`, `updated_at`, `stop`) VALUES
(1, 2, 1150.00, 50.00, NULL, 1, 2748, NULL, NULL, NULL, '2019-12-28 14:47:36', '2019-12-28 14:47:36', NULL),
(2, 2, 1100.00, 50.00, NULL, 2, 2748, NULL, NULL, NULL, '2019-12-28 14:48:32', '2019-12-28 14:48:32', NULL),
(3, 2, 1100.00, 50.00, NULL, 3, 2748, NULL, NULL, NULL, '2019-12-28 16:01:44', '2019-12-28 16:01:44', NULL),
(4, 2, 1100.00, 100.00, NULL, 4, 2748, NULL, NULL, NULL, '2019-12-28 16:10:46', '2019-12-28 16:11:09', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_promotion`
--

CREATE TABLE `payment_promotion` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `promotion_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pending_documents`
--

CREATE TABLE `pending_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1: Foto de Perfil, 2: Licencia de Conducir, 3: Foto de Vehículo, 4: Permiso de Circulación, 5: Certificado de Antecedentes, 6: Documento (RUT o Pasaporte)',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0: Pendiente de Revisión, 1: Aceptado, 2: Rechazado',
  `comment` text COLLATE utf8mb4_unicode_ci COMMENT 'Motivo por el cual se rechaza el documento',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `upload` int(11) NOT NULL DEFAULT '0' COMMENT '1: El usuario ya subio el archivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'module-drivers', 'Modulo Conductores', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(2, 'module-passengers', 'Modulo Clientes', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(3, 'module-tariffs', 'Modulo Tarifas', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(4, 'module-support', 'Modulo Chat', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(5, 'module-finances', 'Modulo Ganancias', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(6, 'module-vehicles', 'Modulo Vehiculos', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(7, 'module-promotions', 'Modulo Promociones', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(8, 'module-routes', 'Modulo Rutas', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(9, 'module-ratings', 'Modulo Calificaciones', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(10, 'module-reports', 'Modulo Reportes', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(11, 'module-moderators', 'Modulo Moderadores', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(12, 'module-configuration', 'Modulo Configuración', NULL, '2018-09-14 20:24:17', '2018-09-14 20:24:17'),
(13, 'module-notifications', 'Modulo Notificaciones', NULL, '2018-12-19 19:23:19', '2018-12-19 19:23:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persons`
--

CREATE TABLE `persons` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1: DNI, 2: Pasaporte, 3: Otro',
  `document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_swedish_ci DEFAULT NULL COMMENT 'Archivo',
  `document_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_document` date DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Archivo',
  `permit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_permit` date DEFAULT NULL,
  `criminal_records` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_criminal_records` date DEFAULT NULL,
  `license` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_license` date DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persons`
--

INSERT INTO `persons` (`id`, `name`, `lastname`, `phone`, `document_type`, `document`, `document_photo`, `expiration_document`, `photo`, `permit`, `expiration_permit`, `criminal_records`, `expiration_criminal_records`, `license`, `expiration_license`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Conductor', 'Bogui', '584124758521', '1', '32.178.905-7', 'drivers/files/bc433461765b4a70fdac8478fa212e5b20022002.jpg', NULL, 'drivers/profile/bc433461765b4a70fdac8478fa212e5b9170243.jpg', NULL, '2019-12-29', 'drivers/files/ed86e2a3e86bc5a7a6af0712797ed7b85161224.jpg', NULL, 'drivers/files/bc433461765b4a70fdac8478fa212e5b99042847.jpg', NULL, 2746, NULL, '2019-12-28 13:44:55', '2019-12-28 13:44:55'),
(2, 'Otro', 'Conductor', '12464318407940', '1', '1.324.579-5', 'drivers/files/2a968b875eaed0fc8fcce55d83a8d0ce19854619.jpg', NULL, 'drivers/profile/482b25dd1d63379f474d7bcdafc010c565354391.jpg', NULL, '2019-12-29', 'drivers/files/b5e3c521f4d615380d419fe4d7cd360f70632423.jpg', NULL, 'drivers/files/2a968b875eaed0fc8fcce55d83a8d0ce90449275.jpg', NULL, 2747, NULL, '2019-12-28 14:24:55', '2019-12-28 14:24:55'),
(3, 'pasajero', 'bogui', '4712348970', '1', '312.789-4', NULL, '2019-12-29', 'passengers/profile/DTr6rwjRDFu25vwPzS6vTeUlryBUVXm8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 2748, NULL, '2019-12-28 14:37:07', '2019-12-28 14:37:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privacy`
--

CREATE TABLE `privacy` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `privacy`
--

INSERT INTO `privacy` (`id`, `content`, `english`, `created_at`, `updated_at`) VALUES
(1, 'Bogui es una empresa de tecnología y lo que buscamos es conectar a las comunidades a través del transporte. Con estas políticas de privacidad pasamos a informar cual es el tipo de información que recibimos tanto de pasajeros como de conductores de Bogui y como la usamos para que estos dos entes se puedan conectar. A continuación pasamos a explicar cómo es la manera de conectar un pasajero con un conductor con la idea de ir mejorando la integración de las comunidades con un fin en común, mejorar la calidad de vida del día a día referente a la manera de moverse por tu ciudad.\r\nInformación que nos proporciona.\r\nRegistro.\r\nCuando usted se inscribe en una cuenta de Bogui, usted nos da su nombre, dirección de correo electrónico y número de teléfono. Si se inscribe a Bogui usando su cuenta de Facebook, obtendremos información básica de su perfil de Facebook como su nombre, foto de perfil, sexo, y amigos de Facebook.\r\nForma de pago.\r\nUsted como usuario cuando agrega una tarjeta de crédito a su cuenta personal de Bogui, es un tercero el que recepción y almacena los datos de su tarjeta con el propósito de que sus datos se encuentren seguros y bajo resguardo, Bogui no almacena sus datos financieros en nuestros servidores.\r\nComunicación.\r\nSi por alguna razón nos contacta directamente por medio de nuestro servicio de atención al cliente es probable que le solicitemos información adicional como sus datos personales para poder identificar si usted es el titular de alguna cuenta en particular, estos datos pueden ser (su nombre completo, su número personal de identificación, su número telefónico, su dirección de correo electrónico o algún contenido enviado como archivo adjunto o cualquier otro tipo de información que nos desee proporcionar con fines meramente para identificación.\r\nInformación para la aplicación de conductor.\r\nEn el momento que usted decide formar parte de la comunidad de conductores de Bogui, además de la información básica que se requiere para el registro, le pedimos proporcionar información adicional personal como una fotografía personal actualizada, un certificado de antecedentes personales, un certificado de conductor, datos de su carnet de conducir, una dirección física, datos financieros como un número de cuenta bancaria, tipo de cuenta bancaria, además se le solicitara información del vehículo que usara para prestar los servicios como, padrón del vehículo, revisión técnica del vehículo al día, certificado de gases vigente, permiso de circulación vigente, seguro obligatorio vigente, además de un seguro adicional por asiento y una fotografía del vehículo actualizada, toda esta información se almacena en nuestros servidores para mantener una verificación constante y actualizada para ayudar a mantener el orden y proteger a toda nuestra comunidad.\r\nInformación para la aplicación de pasajero.\r\nEn el momento que usted decide formar parte de la comunidad de pasajeros de Bogui, además de la información básica que se requiere para el registro, le pedimos proporcionar información adicional personal como una fotografía personal actualizada, una fotografía de su documento de identificación o de pasaporte (solamente para cruzar información y garantizar que su cuenta es fidedigna legal y transparente), una dirección física.\r\nPagos.\r\nEn Bogui queremos asegurarnos que todos nuestros conductores reciban sus pagos es por eso que mantenemos en nuestros servidores información bancaria proporcionada por usted al momento de registrarse como conductor de Bogui.\r\nInformación del terminal.\r\nBogui recibe información de los terminales de los usuarios, incluyendo la dirección IP, el tipo de navegador web, la versión del sistema operativo móvil, el operador y el fabricante del teléfono, las instalaciones de la aplicación, los identificadores de dispositivos, los identificadores de publicidad móvil, los tokens de notificación push y, Su identificador de Facebook. Recopilamos datos de sensores móviles de los\r\ndispositivos de los controladores (como velocidad, dirección, altura, aceleración o desaceleración) para mejorar la precisión de la ubicación y analizar los patrones de uso.\r\nInformación de llamada y texto.\r\nBogui trabaja con empresas externas que proveen el servicio de llamadas telefónicas y mensajes de texto entre conductores y pasajeros que se han enlazado para un viaje, Bogui recibe información de estas comunicaciones, incluyendo la fecha y hora de la llamada o mensaje SMS, los números de teléfono de las partes y el contenido de cualquier mensaje SMS.\r\nCalificaciones.\r\nEn Bogui queremos garantizar que los viajes que realicen los disfruten al máximo, para eso nuestro estricto control de calificaciones entre pasajeros y conductores es súper importante, esto nos ayuda a mantener la plataforma con conductores y pasajeros con buenas calificaciones ya que un conductor o pasajero mal evaluado no califica para formar parte de la comunidad Bogui.\r\nInformación de uso.\r\nPara ayudarnos a mejorar la plataforma Bogui, recibimos automáticamente toda la información de las interacciones que usted tiene con la plataforma, es decir hora y fecha de sus visitas, las acciones que ejecuta.\r\nInformación sobre la ubicación.\r\nBogui y sus aplicaciones tecnológicas se encargar de conectar un conductor con un pasajero, para esto recibimos su ubicación cuando usted abre la aplicación en su dispositivo móvil, También podemos recopilar la ubicación precisa de su dispositivo cuando la aplicación se está ejecutando en primer plano o en segundo plano. También recibimos información si etiquetas ciertas ubicaciones, como "casa" y "trabajo", esta es necesaria para poder conectar a un pasajero con un conductor, además para saber el punto exacto de recogida y destino del viaje, también se utilizan estos datos para calcular el valor de un viaje, si nos da permiso a través de la configuración de su dispositivo o de la aplicación Bogui, podemos recopilar su\r\nubicación mientras la aplicación está desactivada para identificar promociones o actualizaciones de servicio en su área.\r\nContactos de la Libreta de direcciones.\r\nBogui solicita permiso para acceder a su libreta de direcciones de su dispositivo a través de la plataforma móvil, Bogui podrá acceder y almacenar nombres e información de contacto para facilitar invitaciones y las interacciones sociales que inicie a través de nuestra plataforma y para otros. Los fines descritos en esta política de privacidad o en el momento del consentimiento o de la recaudación.\r\nInformación general sobre los conductores.\r\nBogui lleva a cabo el registro de conducción y verificación de antecedentes penales en los conductores, y recibimos información de ellos, tales como información disponible públicamente acerca de un registro de conducción o historial criminal. Así también recopilamos información de los pasajeros para de cierta medida en algo garantizar que los titulares de las cuentas de usuario tanto de conductor como de pasajero son personas reales y fiables para enlazarse entre sí.\r\nLos pasajeros y conductores que han sido enlazados para un viaje, pueden ver mutuamente la información básica como fotografía, nombres, número de teléfono, calificaciones y cualquier otra información que hayan agregado a sus perfiles, Los conductores pueden ver la ubicación de recogida que el pasajero ha proporcionado. Los pasajeros ven la información del vehículo del conductor y la localización en tiempo real mientras que el conductor se acerca a la localización de la recogida. Aunque ayudamos a pasajeros y conductores a enlazarse entre sí para organizar una recogida o un viaje, no compartimos su número de teléfono real u otra información con otros usuarios. Si usted reporta un artículo perdido o encontrado, trataremos de conectarlo con el conductor o el controlador correspondiente, incluyendo el intercambio de información de contacto real con su permiso.\r\nBogui puede compartir su información con terceros en los siguientes casos:\r\nDurante la negociación o en relación con un cambio de control corporativo como una reestructuración, fusión o venta de nuestros activos;\r\nSi una autoridad gubernamental solicita información y creemos que la divulgación es necesaria o apropiada para cumplir con leyes, reglamentos o un proceso legal;\r\nCon funcionarios de la ley, autoridades gubernamentales o terceros, si creemos que hacerlo es necesario para proteger los derechos, la propiedad o la seguridad de la comunidad de Bogui, de Bogui o del público.\r\nSi se ha inscrito en una promoción con el código de referencia o promoción de otro usuario, comuníqueselo a su referente para informarle sobre su reembolso o calificación para la promoción;\r\nCon nuestros socios de seguros para ayudar a determinar y proporcionar cobertura relevante en caso de un incidente;\r\nProporcionar información sobre el uso de la Plataforma Bogui a potenciales socios comerciales en forma agregada o des-identificada que no pueda utilizarse razonablemente para identificarlo; y siempre que usted consiente a compartir.\r\nBogui está comprometido en proteger los datos de la comunidad Bogui. Aunque tomamos las precauciones razonables para proteger sus datos, ninguna medida de seguridad puede ser 100% segura, y no podemos garantizar la seguridad de sus datos.\r\nCambios en nuestra política de privacidad. Podemos realizar cambios en esta Política de privacidad de vez en cuando. Si realizamos cambios materiales, le informaremos a través de la Plataforma Bogui, por correo electrónico u otro medio de comunicación. Le recomendamos que lea esta Política de privacidad periódicamente para mantenerse al día sobre nuestras prácticas de privacidad. Mientras utilice la Plataforma Bogui, está aceptando esta Política de Privacidad y cualquier actualización que le hagamos en el futuro\r\nUsted puede comunicarse con nosotros a través de nuestro centro de ayuda en cualquier momento si tiene alguna duda o pregunta referente a estas políticas de privacidad.', 'Bogui es una empresa de tecnología y lo que buscamos es conectar a las comunidades a través del transporte. Con estas políticas de privacidad pasamos a informar cual es el tipo de información que recibimos tanto de pasajeros como de conductores de Bogui y como la usamos para que estos dos entes se puedan conectar. A continuación pasamos a explicar cómo es la manera de conectar un pasajero con un conductor con la idea de ir mejorando la integración de las comunidades con un fin en común, mejorar la calidad de vida del día a día referente a la manera de moverse por tu ciudad.\r\nInformación que nos proporciona.\r\nRegistro.\r\nCuando usted se inscribe en una cuenta de Bogui, usted nos da su nombre, dirección de correo electrónico y número de teléfono. Si se inscribe a Bogui usando su cuenta de Facebook, obtendremos información básica de su perfil de Facebook como su nombre, foto de perfil, sexo, y amigos de Facebook.\r\nForma de pago.\r\nUsted como usuario cuando agrega una tarjeta de crédito a su cuenta personal de Bogui, es un tercero el que recepción y almacena los datos de su tarjeta con el propósito de que sus datos se encuentren seguros y bajo resguardo, Bogui no almacena sus datos financieros en nuestros servidores.\r\nComunicación.\r\nSi por alguna razón nos contacta directamente por medio de nuestro servicio de atención al cliente es probable que le solicitemos información adicional como sus datos personales para poder identificar si usted es el titular de alguna cuenta en particular, estos datos pueden ser (su nombre completo, su número personal de identificación, su número telefónico, su dirección de correo electrónico o algún contenido enviado como archivo adjunto o cualquier otro tipo de información que nos desee proporcionar con fines meramente para identificación.\r\nInformación para la aplicación de conductor.\r\nEn el momento que usted decide formar parte de la comunidad de conductores de Bogui, además de la información básica que se requiere para el registro, le pedimos proporcionar información adicional personal como una fotografía personal actualizada, un certificado de antecedentes personales, un certificado de conductor, datos de su carnet de conducir, una dirección física, datos financieros como un número de cuenta bancaria, tipo de cuenta bancaria, además se le solicitara información del vehículo que usara para prestar los servicios como, padrón del vehículo, revisión técnica del vehículo al día, certificado de gases vigente, permiso de circulación vigente, seguro obligatorio vigente, además de un seguro adicional por asiento y una fotografía del vehículo actualizada, toda esta información se almacena en nuestros servidores para mantener una verificación constante y actualizada para ayudar a mantener el orden y proteger a toda nuestra comunidad.\r\nInformación para la aplicación de pasajero.\r\nEn el momento que usted decide formar parte de la comunidad de pasajeros de Bogui, además de la información básica que se requiere para el registro, le pedimos proporcionar información adicional personal como una fotografía personal actualizada, una fotografía de su documento de identificación o de pasaporte (solamente para cruzar información y garantizar que su cuenta es fidedigna legal y transparente), una dirección física.\r\nPagos.\r\nEn Bogui queremos asegurarnos que todos nuestros conductores reciban sus pagos es por eso que mantenemos en nuestros servidores información bancaria proporcionada por usted al momento de registrarse como conductor de Bogui.\r\nInformación del terminal.\r\nBogui recibe información de los terminales de los usuarios, incluyendo la dirección IP, el tipo de navegador web, la versión del sistema operativo móvil, el operador y el fabricante del teléfono, las instalaciones de la aplicación, los identificadores de dispositivos, los identificadores de publicidad móvil, los tokens de notificación push y, Su identificador de Facebook. Recopilamos datos de sensores móviles de los\r\ndispositivos de los controladores (como velocidad, dirección, altura, aceleración o desaceleración) para mejorar la precisión de la ubicación y analizar los patrones de uso.\r\nInformación de llamada y texto.\r\nBogui trabaja con empresas externas que proveen el servicio de llamadas telefónicas y mensajes de texto entre conductores y pasajeros que se han enlazado para un viaje, Bogui recibe información de estas comunicaciones, incluyendo la fecha y hora de la llamada o mensaje SMS, los números de teléfono de las partes y el contenido de cualquier mensaje SMS.\r\nCalificaciones.\r\nEn Bogui queremos garantizar que los viajes que realicen los disfruten al máximo, para eso nuestro estricto control de calificaciones entre pasajeros y conductores es súper importante, esto nos ayuda a mantener la plataforma con conductores y pasajeros con buenas calificaciones ya que un conductor o pasajero mal evaluado no califica para formar parte de la comunidad Bogui.\r\nInformación de uso.\r\nPara ayudarnos a mejorar la plataforma Bogui, recibimos automáticamente toda la información de las interacciones que usted tiene con la plataforma, es decir hora y fecha de sus visitas, las acciones que ejecuta.\r\nInformación sobre la ubicación.\r\nBogui y sus aplicaciones tecnológicas se encargar de conectar un conductor con un pasajero, para esto recibimos su ubicación cuando usted abre la aplicación en su dispositivo móvil, También podemos recopilar la ubicación precisa de su dispositivo cuando la aplicación se está ejecutando en primer plano o en segundo plano. También recibimos información si etiquetas ciertas ubicaciones, como "casa" y "trabajo", esta es necesaria para poder conectar a un pasajero con un conductor, además para saber el punto exacto de recogida y destino del viaje, también se utilizan estos datos para calcular el valor de un viaje, si nos da permiso a través de la configuración de su dispositivo o de la aplicación Bogui, podemos recopilar su\r\nubicación mientras la aplicación está desactivada para identificar promociones o actualizaciones de servicio en su área.\r\nContactos de la Libreta de direcciones.\r\nBogui solicita permiso para acceder a su libreta de direcciones de su dispositivo a través de la plataforma móvil, Bogui podrá acceder y almacenar nombres e información de contacto para facilitar invitaciones y las interacciones sociales que inicie a través de nuestra plataforma y para otros. Los fines descritos en esta política de privacidad o en el momento del consentimiento o de la recaudación.\r\nInformación general sobre los conductores.\r\nBogui lleva a cabo el registro de conducción y verificación de antecedentes penales en los conductores, y recibimos información de ellos, tales como información disponible públicamente acerca de un registro de conducción o historial criminal. Así también recopilamos información de los pasajeros para de cierta medida en algo garantizar que los titulares de las cuentas de usuario tanto de conductor como de pasajero son personas reales y fiables para enlazarse entre sí.\r\nLos pasajeros y conductores que han sido enlazados para un viaje, pueden ver mutuamente la información básica como fotografía, nombres, número de teléfono, calificaciones y cualquier otra información que hayan agregado a sus perfiles, Los conductores pueden ver la ubicación de recogida que el pasajero ha proporcionado. Los pasajeros ven la información del vehículo del conductor y la localización en tiempo real mientras que el conductor se acerca a la localización de la recogida. Aunque ayudamos a pasajeros y conductores a enlazarse entre sí para organizar una recogida o un viaje, no compartimos su número de teléfono real u otra información con otros usuarios. Si usted reporta un artículo perdido o encontrado, trataremos de conectarlo con el conductor o el controlador correspondiente, incluyendo el intercambio de información de contacto real con su permiso.\r\nBogui puede compartir su información con terceros en los siguientes casos:\r\nDurante la negociación o en relación con un cambio de control corporativo como una reestructuración, fusión o venta de nuestros activos;\r\nSi una autoridad gubernamental solicita información y creemos que la divulgación es necesaria o apropiada para cumplir con leyes, reglamentos o un proceso legal;\r\nCon funcionarios de la ley, autoridades gubernamentales o terceros, si creemos que hacerlo es necesario para proteger los derechos, la propiedad o la seguridad de la comunidad de Bogui, de Bogui o del público.\r\nSi se ha inscrito en una promoción con el código de referencia o promoción de otro usuario, comuníqueselo a su referente para informarle sobre su reembolso o calificación para la promoción;\r\nCon nuestros socios de seguros para ayudar a determinar y proporcionar cobertura relevante en caso de un incidente;\r\nProporcionar información sobre el uso de la Plataforma Bogui a potenciales socios comerciales en forma agregada o des-identificada que no pueda utilizarse razonablemente para identificarlo; y siempre que usted consiente a compartir.\r\nBogui está comprometido en proteger los datos de la comunidad Bogui. Aunque tomamos las precauciones razonables para proteger sus datos, ninguna medida de seguridad puede ser 100% segura, y no podemos garantizar la seguridad de sus datos.\r\nCambios en nuestra política de privacidad. Podemos realizar cambios en esta Política de privacidad de vez en cuando. Si realizamos cambios materiales, le informaremos a través de la Plataforma Bogui, por correo electrónico u otro medio de comunicación. Le recomendamos que lea esta Política de privacidad periódicamente para mantenerse al día sobre nuestras prácticas de privacidad. Mientras utilice la Plataforma Bogui, está aceptando esta Política de Privacidad y cualquier actualización que le hagamos en el futuro\r\nUsted puede comunicarse con nosotros a través de nuestro centro de ayuda en cualquier momento si tiene alguna duda o pregunta referente a estas políticas de privacidad.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions`
--

CREATE TABLE `promotions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Codigo de la promocion',
  `num_uses` int(11) NOT NULL COMMENT 'Numero de veces que se puede vender la promocion',
  `expire_date` date NOT NULL,
  `amount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotion_user`
--

CREATE TABLE `promotion_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `promotion_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: no pagado, 1: pagado',
  `accepted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: no pagado, 1: pagado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `reviewed_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Calificado',
  `reviewer_id` int(10) UNSIGNED NOT NULL COMMENT 'Calificador',
  `route_id` int(10) UNSIGNED NOT NULL,
  `bogui` int(11) NOT NULL DEFAULT '0' COMMENT 'Es calificación a Bogui',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `points` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `refresh_tokens`
--

CREATE TABLE `refresh_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `refresh_tokens`
--

INSERT INTO `refresh_tokens` (`id`, `token`, `user_id`, `device_id`, `created_at`, `updated_at`) VALUES
(110, 'fDQ9975yLI4:APA91bHiYWKJZEqszYFkohAh0BcGPuczKz7njiaLyqoZwMAfTR-H5kOmvuOsXgViLjxWrCcYU-kXQL1EZeqWfdSTee1F27P2efRgMjNuORZUuh5bPU-JmaGiWpPg88DQPzrn8pzDIBNI', 2746, '51d0983d98a029f3', '2019-12-28 15:04:58', '2019-12-28 17:10:56'),
(111, 'eO3ps_Avsd0:APA91bHBRPI_YdzyV3UzSx5PpgHDUiOBZBh2Y16shRM2wqsH2Tu3QVwlSu7zgGXEgvzpuGfMLGTucQ6lxNpHELNjU3IE1NFGNSs1ojrw31uddXDYoHMzMa9gOffcmecnvwHxUFwuEiBh', 2748, 'c599d81fa181cb74', '2019-12-28 15:37:42', '2019-12-28 17:11:25'),
(112, 'diwK5P0uxh4:APA91bEDY5Ua-I-irMD-txry369SkltKEEPvgUteC6XnSRo57xW_TooU4hHw-DoUWD8LnW0Dh69im6juecLsWd5n6Ma7rbdkA3LtEAAhp1rec_4GsaELJiRmg09hl3OLTeXTgnS_ieVh', 2747, 'e6f03cec1c505cb3', '2019-12-28 15:48:34', '2019-12-28 17:05:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'Super Admin', 'Super administrador del sistema', '2018-09-18 13:23:55', '2018-09-18 13:23:55'),
(2, 'admin', 'Administrador', 'Usuario con todos los permisos del sistema', '2018-09-18 13:23:55', '2018-09-18 13:23:55'),
(3, 'moderators', 'Moderadores', 'Moderadores con permisos limitados', '2018-09-18 13:23:55', '2018-09-18 13:23:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `routes`
--

CREATE TABLE `routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'conductor de la ruta',
  `latitud_origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitud_origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0: Suspendida, 1: Activa, 2: Culminada, 3: Cancelada',
  `status_route` int(11) NOT NULL DEFAULT '0' COMMENT '0: Pendiente, 1: Pasajero a Bordo, 2: Iniciada',
  `route_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_driver` datetime DEFAULT NULL COMMENT 'Fecha y Hora en que el conductor acepto la carrera. Se usa para el contador de 5 minutos de la app',
  `time` int(10) UNSIGNED NOT NULL COMMENT 'En segundos',
  `cost` double(10,2) NOT NULL,
  `distance` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT 'distancia en km',
  `payment_id` int(10) UNSIGNED NOT NULL,
  `payment` int(11) NOT NULL DEFAULT '1' COMMENT '1: Tarjeta, 2: Efectivo',
  `vehicle_type_id` int(10) UNSIGNED NOT NULL,
  `tariff_id` int(10) UNSIGNED NOT NULL,
  `airport` int(11) NOT NULL DEFAULT '0' COMMENT 'n° de puntos cerca del aeropuerto',
  `special_tariff` int(11) NOT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  `canceled_reason` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `time_stop` int(11) DEFAULT NULL COMMENT 'Tiempo Adicional en Parada (En Segundos)',
  `paid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: no pagado, 1: pagado',
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `routes`
--

INSERT INTO `routes` (`id`, `user_id`, `driver_id`, `latitud_origin`, `longitud_origin`, `status`, `status_route`, `route_photo`, `date_driver`, `time`, `cost`, `distance`, `payment_id`, `payment`, `vehicle_type_id`, `tariff_id`, `airport`, `special_tariff`, `canceled_by`, `canceled_reason`, `deleted_at`, `created_at`, `updated_at`, `time_stop`, `paid`, `description`) VALUES
(1, 2748, NULL, '10.306448563830868', '-67.62887226417661', 3, 0, NULL, NULL, 382, 1150.00, '2.48', 1, 2, 1, 1, 0, 0, 2748, 'Cancelación por el Cliente', NULL, '2019-12-28 14:47:36', '2019-12-28 14:48:10', NULL, 0, '21 Calle José Gregorio Hernandez, El Limón 2105, Carabobo, Venezuela'),
(2, 2748, NULL, '10.297119803861765', '-67.63158163055778', 3, 0, NULL, NULL, 211, 1100.00, '1.88', 1, 2, 1, 1, 0, 0, 2748, 'Cancelación por el Cliente', NULL, '2019-12-28 14:48:32', '2019-12-28 14:49:00', NULL, 0, 'Av. Universidad, CC El Limon, nivel 2, local 37, El Limón 2105, Aragua, Venezuela'),
(3, 2748, NULL, '10.29898887668198', '-67.63323990628123', 3, 0, NULL, NULL, 141, 1100.00, '1.11', 1, 2, 1, 1, 0, 0, 2748, 'Cancelación por el Cliente', NULL, '2019-12-28 16:01:44', '2019-12-28 16:03:43', NULL, 0, 'AV. Universidad Local 18-3, El Progreso, Sector Arias Blanco, El Limón 2105, Aragua, Venezuela'),
(4, 2748, 2746, '10.30017675147901', '-67.63402679935098', 1, 0, NULL, NULL, 189, 1100.00, '1.05', 1, 2, 1, 1, 0, 0, NULL, NULL, NULL, '2019-12-28 16:10:46', '2019-12-28 16:11:24', NULL, 0, 'Avenida Universidad, El Limón 2105, Aragua, Venezuela');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `route_user`
--

CREATE TABLE `route_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `route_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `search_radius`
--

CREATE TABLE `search_radius` (
  `id` int(10) UNSIGNED NOT NULL,
  `radius` int(11) NOT NULL COMMENT 'En Kilometros',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `search_radius`
--

INSERT INTO `search_radius` (`id`, `radius`, `created_at`, `updated_at`) VALUES
(1, 2, '2018-10-26 19:14:51', '2019-12-17 11:12:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `states`
--

INSERT INTO `states` (`id`, `name`, `country_id`) VALUES
(1, 'Andaman and Nicobar Islands', 101),
(2, 'Andhra Pradesh', 101),
(3, 'Arunachal Pradesh', 101),
(4, 'Assam', 101),
(5, 'Bihar', 101),
(6, 'Chandigarh', 101),
(7, 'Chhattisgarh', 101),
(8, 'Dadra and Nagar Haveli', 101),
(9, 'Daman and Diu', 101),
(10, 'Delhi', 101),
(11, 'Goa', 101),
(12, 'Gujarat', 101),
(13, 'Haryana', 101),
(14, 'Himachal Pradesh', 101),
(15, 'Jammu and Kashmir', 101),
(16, 'Jharkhand', 101),
(17, 'Karnataka', 101),
(18, 'Kenmore', 101),
(19, 'Kerala', 101),
(20, 'Lakshadweep', 101),
(21, 'Madhya Pradesh', 101),
(22, 'Maharashtra', 101),
(23, 'Manipur', 101),
(24, 'Meghalaya', 101),
(25, 'Mizoram', 101),
(26, 'Nagaland', 101),
(27, 'Narora', 101),
(28, 'Natwar', 101),
(29, 'Odisha', 101),
(30, 'Paschim Medinipur', 101),
(31, 'Pondicherry', 101),
(32, 'Punjab', 101),
(33, 'Rajasthan', 101),
(34, 'Sikkim', 101),
(35, 'Tamil Nadu', 101),
(36, 'Telangana', 101),
(37, 'Tripura', 101),
(38, 'Uttar Pradesh', 101),
(39, 'Uttarakhand', 101),
(40, 'Vaishali', 101),
(41, 'West Bengal', 101),
(42, 'Badakhshan', 1),
(43, 'Badgis', 1),
(44, 'Baglan', 1),
(45, 'Balkh', 1),
(46, 'Bamiyan', 1),
(47, 'Farah', 1),
(48, 'Faryab', 1),
(49, 'Gawr', 1),
(50, 'Gazni', 1),
(51, 'Herat', 1),
(52, 'Hilmand', 1),
(53, 'Jawzjan', 1),
(54, 'Kabul', 1),
(55, 'Kapisa', 1),
(56, 'Khawst', 1),
(57, 'Kunar', 1),
(58, 'Lagman', 1),
(59, 'Lawghar', 1),
(60, 'Nangarhar', 1),
(61, 'Nimruz', 1),
(62, 'Nuristan', 1),
(63, 'Paktika', 1),
(64, 'Paktiya', 1),
(65, 'Parwan', 1),
(66, 'Qandahar', 1),
(67, 'Qunduz', 1),
(68, 'Samangan', 1),
(69, 'Sar-e Pul', 1),
(70, 'Takhar', 1),
(71, 'Uruzgan', 1),
(72, 'Wardag', 1),
(73, 'Zabul', 1),
(74, 'Berat', 2),
(75, 'Bulqize', 2),
(76, 'Delvine', 2),
(77, 'Devoll', 2),
(78, 'Dibre', 2),
(79, 'Durres', 2),
(80, 'Elbasan', 2),
(81, 'Fier', 2),
(82, 'Gjirokaster', 2),
(83, 'Gramsh', 2),
(84, 'Has', 2),
(85, 'Kavaje', 2),
(86, 'Kolonje', 2),
(87, 'Korce', 2),
(88, 'Kruje', 2),
(89, 'Kucove', 2),
(90, 'Kukes', 2),
(91, 'Kurbin', 2),
(92, 'Lezhe', 2),
(93, 'Librazhd', 2),
(94, 'Lushnje', 2),
(95, 'Mallakaster', 2),
(96, 'Malsi e Madhe', 2),
(97, 'Mat', 2),
(98, 'Mirdite', 2),
(99, 'Peqin', 2),
(100, 'Permet', 2),
(101, 'Pogradec', 2),
(102, 'Puke', 2),
(103, 'Sarande', 2),
(104, 'Shkoder', 2),
(105, 'Skrapar', 2),
(106, 'Tepelene', 2),
(107, 'Tirane', 2),
(108, 'Tropoje', 2),
(109, 'Vlore', 2),
(110, '\'Ayn Daflah', 3),
(111, '\'Ayn Tamushanat', 3),
(112, 'Adrar', 3),
(113, 'Algiers', 3),
(114, 'Annabah', 3),
(115, 'Bashshar', 3),
(116, 'Batnah', 3),
(117, 'Bijayah', 3),
(118, 'Biskrah', 3),
(119, 'Blidah', 3),
(120, 'Buirah', 3),
(121, 'Bumardas', 3),
(122, 'Burj Bu Arririj', 3),
(123, 'Ghalizan', 3),
(124, 'Ghardayah', 3),
(125, 'Ilizi', 3),
(126, 'Jijili', 3),
(127, 'Jilfah', 3),
(128, 'Khanshalah', 3),
(129, 'Masilah', 3),
(130, 'Midyah', 3),
(131, 'Milah', 3),
(132, 'Muaskar', 3),
(133, 'Mustaghanam', 3),
(134, 'Naama', 3),
(135, 'Oran', 3),
(136, 'Ouargla', 3),
(137, 'Qalmah', 3),
(138, 'Qustantinah', 3),
(139, 'Sakikdah', 3),
(140, 'Satif', 3),
(141, 'Sayda\'', 3),
(142, 'Sidi ban-al-\'Abbas', 3),
(143, 'Suq Ahras', 3),
(144, 'Tamanghasat', 3),
(145, 'Tibazah', 3),
(146, 'Tibissah', 3),
(147, 'Tilimsan', 3),
(148, 'Tinduf', 3),
(149, 'Tisamsilt', 3),
(150, 'Tiyarat', 3),
(151, 'Tizi Wazu', 3),
(152, 'Umm-al-Bawaghi', 3),
(153, 'Wahran', 3),
(154, 'Warqla', 3),
(155, 'Wilaya d Alger', 3),
(156, 'Wilaya de Bejaia', 3),
(157, 'Wilaya de Constantine', 3),
(158, 'al-Aghwat', 3),
(159, 'al-Bayadh', 3),
(160, 'al-Jaza\'ir', 3),
(161, 'al-Wad', 3),
(162, 'ash-Shalif', 3),
(163, 'at-Tarif', 3),
(164, 'Eastern', 4),
(165, 'Manu\'a', 4),
(166, 'Swains Island', 4),
(167, 'Western', 4),
(168, 'Andorra la Vella', 5),
(169, 'Canillo', 5),
(170, 'Encamp', 5),
(171, 'La Massana', 5),
(172, 'Les Escaldes', 5),
(173, 'Ordino', 5),
(174, 'Sant Julia de Loria', 5),
(175, 'Bengo', 6),
(176, 'Benguela', 6),
(177, 'Bie', 6),
(178, 'Cabinda', 6),
(179, 'Cunene', 6),
(180, 'Huambo', 6),
(181, 'Huila', 6),
(182, 'Kuando-Kubango', 6),
(183, 'Kwanza Norte', 6),
(184, 'Kwanza Sul', 6),
(185, 'Luanda', 6),
(186, 'Lunda Norte', 6),
(187, 'Lunda Sul', 6),
(188, 'Malanje', 6),
(189, 'Moxico', 6),
(190, 'Namibe', 6),
(191, 'Uige', 6),
(192, 'Zaire', 6),
(193, 'Other Provinces', 7),
(194, 'Sector claimed by Argentina/Ch', 8),
(195, 'Sector claimed by Argentina/UK', 8),
(196, 'Sector claimed by Australia', 8),
(197, 'Sector claimed by France', 8),
(198, 'Sector claimed by New Zealand', 8),
(199, 'Sector claimed by Norway', 8),
(200, 'Unclaimed Sector', 8),
(201, 'Barbuda', 9),
(202, 'Saint George', 9),
(203, 'Saint John', 9),
(204, 'Saint Mary', 9),
(205, 'Saint Paul', 9),
(206, 'Saint Peter', 9),
(207, 'Saint Philip', 9),
(208, 'Buenos Aires', 10),
(209, 'Catamarca', 10),
(210, 'Chaco', 10),
(211, 'Chubut', 10),
(212, 'Cordoba', 10),
(213, 'Corrientes', 10),
(214, 'Distrito Federal', 10),
(215, 'Entre Rios', 10),
(216, 'Formosa', 10),
(217, 'Jujuy', 10),
(218, 'La Pampa', 10),
(219, 'La Rioja', 10),
(220, 'Mendoza', 10),
(221, 'Misiones', 10),
(222, 'Neuquen', 10),
(223, 'Rio Negro', 10),
(224, 'Salta', 10),
(225, 'San Juan', 10),
(226, 'San Luis', 10),
(227, 'Santa Cruz', 10),
(228, 'Santa Fe', 10),
(229, 'Santiago del Estero', 10),
(230, 'Tierra del Fuego', 10),
(231, 'Tucuman', 10),
(232, 'Aragatsotn', 11),
(233, 'Ararat', 11),
(234, 'Armavir', 11),
(235, 'Gegharkunik', 11),
(236, 'Kotaik', 11),
(237, 'Lori', 11),
(238, 'Shirak', 11),
(239, 'Stepanakert', 11),
(240, 'Syunik', 11),
(241, 'Tavush', 11),
(242, 'Vayots Dzor', 11),
(243, 'Yerevan', 11),
(244, 'Aruba', 12),
(245, 'Auckland', 13),
(246, 'Australian Capital Territory', 13),
(247, 'Balgowlah', 13),
(248, 'Balmain', 13),
(249, 'Bankstown', 13),
(250, 'Baulkham Hills', 13),
(251, 'Bonnet Bay', 13),
(252, 'Camberwell', 13),
(253, 'Carole Park', 13),
(254, 'Castle Hill', 13),
(255, 'Caulfield', 13),
(256, 'Chatswood', 13),
(257, 'Cheltenham', 13),
(258, 'Cherrybrook', 13),
(259, 'Clayton', 13),
(260, 'Collingwood', 13),
(261, 'Frenchs Forest', 13),
(262, 'Hawthorn', 13),
(263, 'Jannnali', 13),
(264, 'Knoxfield', 13),
(265, 'Melbourne', 13),
(266, 'New South Wales', 13),
(267, 'Northern Territory', 13),
(268, 'Perth', 13),
(269, 'Queensland', 13),
(270, 'South Australia', 13),
(271, 'Tasmania', 13),
(272, 'Templestowe', 13),
(273, 'Victoria', 13),
(274, 'Werribee south', 13),
(275, 'Western Australia', 13),
(276, 'Wheeler', 13),
(277, 'Bundesland Salzburg', 14),
(278, 'Bundesland Steiermark', 14),
(279, 'Bundesland Tirol', 14),
(280, 'Burgenland', 14),
(281, 'Carinthia', 14),
(282, 'Karnten', 14),
(283, 'Liezen', 14),
(284, 'Lower Austria', 14),
(285, 'Niederosterreich', 14),
(286, 'Oberosterreich', 14),
(287, 'Salzburg', 14),
(288, 'Schleswig-Holstein', 14),
(289, 'Steiermark', 14),
(290, 'Styria', 14),
(291, 'Tirol', 14),
(292, 'Upper Austria', 14),
(293, 'Vorarlberg', 14),
(294, 'Wien', 14),
(295, 'Abseron', 15),
(296, 'Baki Sahari', 15),
(297, 'Ganca', 15),
(298, 'Ganja', 15),
(299, 'Kalbacar', 15),
(300, 'Lankaran', 15),
(301, 'Mil-Qarabax', 15),
(302, 'Mugan-Salyan', 15),
(303, 'Nagorni-Qarabax', 15),
(304, 'Naxcivan', 15),
(305, 'Priaraks', 15),
(306, 'Qazax', 15),
(307, 'Saki', 15),
(308, 'Sirvan', 15),
(309, 'Xacmaz', 15),
(310, 'Abaco', 16),
(311, 'Acklins Island', 16),
(312, 'Andros', 16),
(313, 'Berry Islands', 16),
(314, 'Biminis', 16),
(315, 'Cat Island', 16),
(316, 'Crooked Island', 16),
(317, 'Eleuthera', 16),
(318, 'Exuma and Cays', 16),
(319, 'Grand Bahama', 16),
(320, 'Inagua Islands', 16),
(321, 'Long Island', 16),
(322, 'Mayaguana', 16),
(323, 'New Providence', 16),
(324, 'Ragged Island', 16),
(325, 'Rum Cay', 16),
(326, 'San Salvador', 16),
(327, '\'Isa', 17),
(328, 'Badiyah', 17),
(329, 'Hidd', 17),
(330, 'Jidd Hafs', 17),
(331, 'Mahama', 17),
(332, 'Manama', 17),
(333, 'Sitrah', 17),
(334, 'al-Manamah', 17),
(335, 'al-Muharraq', 17),
(336, 'ar-Rifa\'a', 17),
(337, 'Bagar Hat', 18),
(338, 'Bandarban', 18),
(339, 'Barguna', 18),
(340, 'Barisal', 18),
(341, 'Bhola', 18),
(342, 'Bogora', 18),
(343, 'Brahman Bariya', 18),
(344, 'Chandpur', 18),
(345, 'Chattagam', 18),
(346, 'Chittagong Division', 18),
(347, 'Chuadanga', 18),
(348, 'Dhaka', 18),
(349, 'Dinajpur', 18),
(350, 'Faridpur', 18),
(351, 'Feni', 18),
(352, 'Gaybanda', 18),
(353, 'Gazipur', 18),
(354, 'Gopalganj', 18),
(355, 'Habiganj', 18),
(356, 'Jaipur Hat', 18),
(357, 'Jamalpur', 18),
(358, 'Jessor', 18),
(359, 'Jhalakati', 18),
(360, 'Jhanaydah', 18),
(361, 'Khagrachhari', 18),
(362, 'Khulna', 18),
(363, 'Kishorganj', 18),
(364, 'Koks Bazar', 18),
(365, 'Komilla', 18),
(366, 'Kurigram', 18),
(367, 'Kushtiya', 18),
(368, 'Lakshmipur', 18),
(369, 'Lalmanir Hat', 18),
(370, 'Madaripur', 18),
(371, 'Magura', 18),
(372, 'Maimansingh', 18),
(373, 'Manikganj', 18),
(374, 'Maulvi Bazar', 18),
(375, 'Meherpur', 18),
(376, 'Munshiganj', 18),
(377, 'Naral', 18),
(378, 'Narayanganj', 18),
(379, 'Narsingdi', 18),
(380, 'Nator', 18),
(381, 'Naugaon', 18),
(382, 'Nawabganj', 18),
(383, 'Netrakona', 18),
(384, 'Nilphamari', 18),
(385, 'Noakhali', 18),
(386, 'Pabna', 18),
(387, 'Panchagarh', 18),
(388, 'Patuakhali', 18),
(389, 'Pirojpur', 18),
(390, 'Rajbari', 18),
(391, 'Rajshahi', 18),
(392, 'Rangamati', 18),
(393, 'Rangpur', 18),
(394, 'Satkhira', 18),
(395, 'Shariatpur', 18),
(396, 'Sherpur', 18),
(397, 'Silhat', 18),
(398, 'Sirajganj', 18),
(399, 'Sunamganj', 18),
(400, 'Tangayal', 18),
(401, 'Thakurgaon', 18),
(402, 'Christ Church', 19),
(403, 'Saint Andrew', 19),
(404, 'Saint George', 19),
(405, 'Saint James', 19),
(406, 'Saint John', 19),
(407, 'Saint Joseph', 19),
(408, 'Saint Lucy', 19),
(409, 'Saint Michael', 19),
(410, 'Saint Peter', 19),
(411, 'Saint Philip', 19),
(412, 'Saint Thomas', 19),
(413, 'Brest', 20),
(414, 'Homjel\'', 20),
(415, 'Hrodna', 20),
(416, 'Mahiljow', 20),
(417, 'Mahilyowskaya Voblasts', 20),
(418, 'Minsk', 20),
(419, 'Minskaja Voblasts\'', 20),
(420, 'Petrik', 20),
(421, 'Vicebsk', 20),
(422, 'Antwerpen', 21),
(423, 'Berchem', 21),
(424, 'Brabant', 21),
(425, 'Brabant Wallon', 21),
(426, 'Brussel', 21),
(427, 'East Flanders', 21),
(428, 'Hainaut', 21),
(429, 'Liege', 21),
(430, 'Limburg', 21),
(431, 'Luxembourg', 21),
(432, 'Namur', 21),
(433, 'Ontario', 21),
(434, 'Oost-Vlaanderen', 21),
(435, 'Provincie Brabant', 21),
(436, 'Vlaams-Brabant', 21),
(437, 'Wallonne', 21),
(438, 'West-Vlaanderen', 21),
(439, 'Belize', 22),
(440, 'Cayo', 22),
(441, 'Corozal', 22),
(442, 'Orange Walk', 22),
(443, 'Stann Creek', 22),
(444, 'Toledo', 22),
(445, 'Alibori', 23),
(446, 'Atacora', 23),
(447, 'Atlantique', 23),
(448, 'Borgou', 23),
(449, 'Collines', 23),
(450, 'Couffo', 23),
(451, 'Donga', 23),
(452, 'Littoral', 23),
(453, 'Mono', 23),
(454, 'Oueme', 23),
(455, 'Plateau', 23),
(456, 'Zou', 23),
(457, 'Hamilton', 24),
(458, 'Saint George', 24),
(459, 'Bumthang', 25),
(460, 'Chhukha', 25),
(461, 'Chirang', 25),
(462, 'Daga', 25),
(463, 'Geylegphug', 25),
(464, 'Ha', 25),
(465, 'Lhuntshi', 25),
(466, 'Mongar', 25),
(467, 'Pemagatsel', 25),
(468, 'Punakha', 25),
(469, 'Rinpung', 25),
(470, 'Samchi', 25),
(471, 'Samdrup Jongkhar', 25),
(472, 'Shemgang', 25),
(473, 'Tashigang', 25),
(474, 'Timphu', 25),
(475, 'Tongsa', 25),
(476, 'Wangdiphodrang', 25),
(477, 'Beni', 26),
(478, 'Chuquisaca', 26),
(479, 'Cochabamba', 26),
(480, 'La Paz', 26),
(481, 'Oruro', 26),
(482, 'Pando', 26),
(483, 'Potosi', 26),
(484, 'Santa Cruz', 26),
(485, 'Tarija', 26),
(486, 'Federacija Bosna i Hercegovina', 27),
(487, 'Republika Srpska', 27),
(488, 'Central Bobonong', 28),
(489, 'Central Boteti', 28),
(490, 'Central Mahalapye', 28),
(491, 'Central Serowe-Palapye', 28),
(492, 'Central Tutume', 28),
(493, 'Chobe', 28),
(494, 'Francistown', 28),
(495, 'Gaborone', 28),
(496, 'Ghanzi', 28),
(497, 'Jwaneng', 28),
(498, 'Kgalagadi North', 28),
(499, 'Kgalagadi South', 28),
(500, 'Kgatleng', 28),
(501, 'Kweneng', 28),
(502, 'Lobatse', 28),
(503, 'Ngamiland', 28),
(504, 'Ngwaketse', 28),
(505, 'North East', 28),
(506, 'Okavango', 28),
(507, 'Orapa', 28),
(508, 'Selibe Phikwe', 28),
(509, 'South East', 28),
(510, 'Sowa', 28),
(511, 'Bouvet Island', 29),
(512, 'Acre', 30),
(513, 'Alagoas', 30),
(514, 'Amapa', 30),
(515, 'Amazonas', 30),
(516, 'Bahia', 30),
(517, 'Ceara', 30),
(518, 'Distrito Federal', 30),
(519, 'Espirito Santo', 30),
(520, 'Estado de Sao Paulo', 30),
(521, 'Goias', 30),
(522, 'Maranhao', 30),
(523, 'Mato Grosso', 30),
(524, 'Mato Grosso do Sul', 30),
(525, 'Minas Gerais', 30),
(526, 'Para', 30),
(527, 'Paraiba', 30),
(528, 'Parana', 30),
(529, 'Pernambuco', 30),
(530, 'Piaui', 30),
(531, 'Rio Grande do Norte', 30),
(532, 'Rio Grande do Sul', 30),
(533, 'Rio de Janeiro', 30),
(534, 'Rondonia', 30),
(535, 'Roraima', 30),
(536, 'Santa Catarina', 30),
(537, 'Sao Paulo', 30),
(538, 'Sergipe', 30),
(539, 'Tocantins', 30),
(540, 'British Indian Ocean Territory', 31),
(541, 'Belait', 32),
(542, 'Brunei-Muara', 32),
(543, 'Temburong', 32),
(544, 'Tutong', 32),
(545, 'Blagoevgrad', 33),
(546, 'Burgas', 33),
(547, 'Dobrich', 33),
(548, 'Gabrovo', 33),
(549, 'Haskovo', 33),
(550, 'Jambol', 33),
(551, 'Kardzhali', 33),
(552, 'Kjustendil', 33),
(553, 'Lovech', 33),
(554, 'Montana', 33),
(555, 'Oblast Sofiya-Grad', 33),
(556, 'Pazardzhik', 33),
(557, 'Pernik', 33),
(558, 'Pleven', 33),
(559, 'Plovdiv', 33),
(560, 'Razgrad', 33),
(561, 'Ruse', 33),
(562, 'Shumen', 33),
(563, 'Silistra', 33),
(564, 'Sliven', 33),
(565, 'Smoljan', 33),
(566, 'Sofija grad', 33),
(567, 'Sofijska oblast', 33),
(568, 'Stara Zagora', 33),
(569, 'Targovishte', 33),
(570, 'Varna', 33),
(571, 'Veliko Tarnovo', 33),
(572, 'Vidin', 33),
(573, 'Vraca', 33),
(574, 'Yablaniza', 33),
(575, 'Bale', 34),
(576, 'Bam', 34),
(577, 'Bazega', 34),
(578, 'Bougouriba', 34),
(579, 'Boulgou', 34),
(580, 'Boulkiemde', 34),
(581, 'Comoe', 34),
(582, 'Ganzourgou', 34),
(583, 'Gnagna', 34),
(584, 'Gourma', 34),
(585, 'Houet', 34),
(586, 'Ioba', 34),
(587, 'Kadiogo', 34),
(588, 'Kenedougou', 34),
(589, 'Komandjari', 34),
(590, 'Kompienga', 34),
(591, 'Kossi', 34),
(592, 'Kouritenga', 34),
(593, 'Kourweogo', 34),
(594, 'Leraba', 34),
(595, 'Mouhoun', 34),
(596, 'Nahouri', 34),
(597, 'Namentenga', 34),
(598, 'Noumbiel', 34),
(599, 'Oubritenga', 34),
(600, 'Oudalan', 34),
(601, 'Passore', 34),
(602, 'Poni', 34),
(603, 'Sanguie', 34),
(604, 'Sanmatenga', 34),
(605, 'Seno', 34),
(606, 'Sissili', 34),
(607, 'Soum', 34),
(608, 'Sourou', 34),
(609, 'Tapoa', 34),
(610, 'Tuy', 34),
(611, 'Yatenga', 34),
(612, 'Zondoma', 34),
(613, 'Zoundweogo', 34),
(614, 'Bubanza', 35),
(615, 'Bujumbura', 35),
(616, 'Bururi', 35),
(617, 'Cankuzo', 35),
(618, 'Cibitoke', 35),
(619, 'Gitega', 35),
(620, 'Karuzi', 35),
(621, 'Kayanza', 35),
(622, 'Kirundo', 35),
(623, 'Makamba', 35),
(624, 'Muramvya', 35),
(625, 'Muyinga', 35),
(626, 'Ngozi', 35),
(627, 'Rutana', 35),
(628, 'Ruyigi', 35),
(629, 'Banteay Mean Chey', 36),
(630, 'Bat Dambang', 36),
(631, 'Kampong Cham', 36),
(632, 'Kampong Chhnang', 36),
(633, 'Kampong Spoeu', 36),
(634, 'Kampong Thum', 36),
(635, 'Kampot', 36),
(636, 'Kandal', 36),
(637, 'Kaoh Kong', 36),
(638, 'Kracheh', 36),
(639, 'Krong Kaeb', 36),
(640, 'Krong Pailin', 36),
(641, 'Krong Preah Sihanouk', 36),
(642, 'Mondol Kiri', 36),
(643, 'Otdar Mean Chey', 36),
(644, 'Phnum Penh', 36),
(645, 'Pousat', 36),
(646, 'Preah Vihear', 36),
(647, 'Prey Veaeng', 36),
(648, 'Rotanak Kiri', 36),
(649, 'Siem Reab', 36),
(650, 'Stueng Traeng', 36),
(651, 'Svay Rieng', 36),
(652, 'Takaev', 36),
(653, 'Adamaoua', 37),
(654, 'Centre', 37),
(655, 'Est', 37),
(656, 'Littoral', 37),
(657, 'Nord', 37),
(658, 'Nord Extreme', 37),
(659, 'Nordouest', 37),
(660, 'Ouest', 37),
(661, 'Sud', 37),
(662, 'Sudouest', 37),
(663, 'Alberta', 38),
(664, 'British Columbia', 38),
(665, 'Manitoba', 38),
(666, 'New Brunswick', 38),
(667, 'Newfoundland and Labrador', 38),
(668, 'Northwest Territories', 38),
(669, 'Nova Scotia', 38),
(670, 'Nunavut', 38),
(671, 'Ontario', 38),
(672, 'Prince Edward Island', 38),
(673, 'Quebec', 38),
(674, 'Saskatchewan', 38),
(675, 'Yukon', 38),
(676, 'Boavista', 39),
(677, 'Brava', 39),
(678, 'Fogo', 39),
(679, 'Maio', 39),
(680, 'Sal', 39),
(681, 'Santo Antao', 39),
(682, 'Sao Nicolau', 39),
(683, 'Sao Tiago', 39),
(684, 'Sao Vicente', 39),
(685, 'Grand Cayman', 40),
(686, 'Bamingui-Bangoran', 41),
(687, 'Bangui', 41),
(688, 'Basse-Kotto', 41),
(689, 'Haut-Mbomou', 41),
(690, 'Haute-Kotto', 41),
(691, 'Kemo', 41),
(692, 'Lobaye', 41),
(693, 'Mambere-Kadei', 41),
(694, 'Mbomou', 41),
(695, 'Nana-Gribizi', 41),
(696, 'Nana-Mambere', 41),
(697, 'Ombella Mpoko', 41),
(698, 'Ouaka', 41),
(699, 'Ouham', 41),
(700, 'Ouham-Pende', 41),
(701, 'Sangha-Mbaere', 41),
(702, 'Vakaga', 41),
(703, 'Batha', 42),
(704, 'Biltine', 42),
(705, 'Bourkou-Ennedi-Tibesti', 42),
(706, 'Chari-Baguirmi', 42),
(707, 'Guera', 42),
(708, 'Kanem', 42),
(709, 'Lac', 42),
(710, 'Logone Occidental', 42),
(711, 'Logone Oriental', 42),
(712, 'Mayo-Kebbi', 42),
(713, 'Moyen-Chari', 42),
(714, 'Ouaddai', 42),
(715, 'Salamat', 42),
(716, 'Tandjile', 42),
(717, 'Aisen', 43),
(718, 'Antofagasta', 43),
(719, 'Araucania', 43),
(720, 'Atacama', 43),
(721, 'Bio Bio', 43),
(722, 'Coquimbo', 43),
(723, 'Libertador General Bernardo O\'', 43),
(724, 'Los Lagos', 43),
(725, 'Magellanes', 43),
(726, 'Maule', 43),
(727, 'Metropolitana', 43),
(728, 'Metropolitana de Santiago', 43),
(729, 'Tarapaca', 43),
(730, 'Valparaiso', 43),
(731, 'Anhui', 44),
(732, 'Anhui Province', 44),
(733, 'Anhui Sheng', 44),
(734, 'Aomen', 44),
(735, 'Beijing', 44),
(736, 'Beijing Shi', 44),
(737, 'Chongqing', 44),
(738, 'Fujian', 44),
(739, 'Fujian Sheng', 44),
(740, 'Gansu', 44),
(741, 'Guangdong', 44),
(742, 'Guangdong Sheng', 44),
(743, 'Guangxi', 44),
(744, 'Guizhou', 44),
(745, 'Hainan', 44),
(746, 'Hebei', 44),
(747, 'Heilongjiang', 44),
(748, 'Henan', 44),
(749, 'Hubei', 44),
(750, 'Hunan', 44),
(751, 'Jiangsu', 44),
(752, 'Jiangsu Sheng', 44),
(753, 'Jiangxi', 44),
(754, 'Jilin', 44),
(755, 'Liaoning', 44),
(756, 'Liaoning Sheng', 44),
(757, 'Nei Monggol', 44),
(758, 'Ningxia Hui', 44),
(759, 'Qinghai', 44),
(760, 'Shaanxi', 44),
(761, 'Shandong', 44),
(762, 'Shandong Sheng', 44),
(763, 'Shanghai', 44),
(764, 'Shanxi', 44),
(765, 'Sichuan', 44),
(766, 'Tianjin', 44),
(767, 'Xianggang', 44),
(768, 'Xinjiang', 44),
(769, 'Xizang', 44),
(770, 'Yunnan', 44),
(771, 'Zhejiang', 44),
(772, 'Zhejiang Sheng', 44),
(773, 'Christmas Island', 45),
(774, 'Cocos (Keeling) Islands', 46),
(775, 'Amazonas', 47),
(776, 'Antioquia', 47),
(777, 'Arauca', 47),
(778, 'Atlantico', 47),
(779, 'Bogota', 47),
(780, 'Bolivar', 47),
(781, 'Boyaca', 47),
(782, 'Caldas', 47),
(783, 'Caqueta', 47),
(784, 'Casanare', 47),
(785, 'Cauca', 47),
(786, 'Cesar', 47),
(787, 'Choco', 47),
(788, 'Cordoba', 47),
(789, 'Cundinamarca', 47),
(790, 'Guainia', 47),
(791, 'Guaviare', 47),
(792, 'Huila', 47),
(793, 'La Guajira', 47),
(794, 'Magdalena', 47),
(795, 'Meta', 47),
(796, 'Narino', 47),
(797, 'Norte de Santander', 47),
(798, 'Putumayo', 47),
(799, 'Quindio', 47),
(800, 'Risaralda', 47),
(801, 'San Andres y Providencia', 47),
(802, 'Santander', 47),
(803, 'Sucre', 47),
(804, 'Tolima', 47),
(805, 'Valle del Cauca', 47),
(806, 'Vaupes', 47),
(807, 'Vichada', 47),
(808, 'Mwali', 48),
(809, 'Njazidja', 48),
(810, 'Nzwani', 48),
(811, 'Bouenza', 49),
(812, 'Brazzaville', 49),
(813, 'Cuvette', 49),
(814, 'Kouilou', 49),
(815, 'Lekoumou', 49),
(816, 'Likouala', 49),
(817, 'Niari', 49),
(818, 'Plateaux', 49),
(819, 'Pool', 49),
(820, 'Sangha', 49),
(821, 'Bandundu', 50),
(822, 'Bas-Congo', 50),
(823, 'Equateur', 50),
(824, 'Haut-Congo', 50),
(825, 'Kasai-Occidental', 50),
(826, 'Kasai-Oriental', 50),
(827, 'Katanga', 50),
(828, 'Kinshasa', 50),
(829, 'Maniema', 50),
(830, 'Nord-Kivu', 50),
(831, 'Sud-Kivu', 50),
(832, 'Aitutaki', 51),
(833, 'Atiu', 51),
(834, 'Mangaia', 51),
(835, 'Manihiki', 51),
(836, 'Mauke', 51),
(837, 'Mitiaro', 51),
(838, 'Nassau', 51),
(839, 'Pukapuka', 51),
(840, 'Rakahanga', 51),
(841, 'Rarotonga', 51),
(842, 'Tongareva', 51),
(843, 'Alajuela', 52),
(844, 'Cartago', 52),
(845, 'Guanacaste', 52),
(846, 'Heredia', 52),
(847, 'Limon', 52),
(848, 'Puntarenas', 52),
(849, 'San Jose', 52),
(850, 'Abidjan', 53),
(851, 'Agneby', 53),
(852, 'Bafing', 53),
(853, 'Denguele', 53),
(854, 'Dix-huit Montagnes', 53),
(855, 'Fromager', 53),
(856, 'Haut-Sassandra', 53),
(857, 'Lacs', 53),
(858, 'Lagunes', 53),
(859, 'Marahoue', 53),
(860, 'Moyen-Cavally', 53),
(861, 'Moyen-Comoe', 53),
(862, 'N\'zi-Comoe', 53),
(863, 'Sassandra', 53),
(864, 'Savanes', 53),
(865, 'Sud-Bandama', 53),
(866, 'Sud-Comoe', 53),
(867, 'Vallee du Bandama', 53),
(868, 'Worodougou', 53),
(869, 'Zanzan', 53),
(870, 'Bjelovar-Bilogora', 54),
(871, 'Dubrovnik-Neretva', 54),
(872, 'Grad Zagreb', 54),
(873, 'Istra', 54),
(874, 'Karlovac', 54),
(875, 'Koprivnica-Krizhevci', 54),
(876, 'Krapina-Zagorje', 54),
(877, 'Lika-Senj', 54),
(878, 'Medhimurje', 54),
(879, 'Medimurska Zupanija', 54),
(880, 'Osijek-Baranja', 54),
(881, 'Osjecko-Baranjska Zupanija', 54),
(882, 'Pozhega-Slavonija', 54),
(883, 'Primorje-Gorski Kotar', 54),
(884, 'Shibenik-Knin', 54),
(885, 'Sisak-Moslavina', 54),
(886, 'Slavonski Brod-Posavina', 54),
(887, 'Split-Dalmacija', 54),
(888, 'Varazhdin', 54),
(889, 'Virovitica-Podravina', 54),
(890, 'Vukovar-Srijem', 54),
(891, 'Zadar', 54),
(892, 'Zagreb', 54),
(893, 'Camaguey', 55),
(894, 'Ciego de Avila', 55),
(895, 'Cienfuegos', 55),
(896, 'Ciudad de la Habana', 55),
(897, 'Granma', 55),
(898, 'Guantanamo', 55),
(899, 'Habana', 55),
(900, 'Holguin', 55),
(901, 'Isla de la Juventud', 55),
(902, 'La Habana', 55),
(903, 'Las Tunas', 55),
(904, 'Matanzas', 55),
(905, 'Pinar del Rio', 55),
(906, 'Sancti Spiritus', 55),
(907, 'Santiago de Cuba', 55),
(908, 'Villa Clara', 55),
(909, 'Government controlled area', 56),
(910, 'Limassol', 56),
(911, 'Nicosia District', 56),
(912, 'Paphos', 56),
(913, 'Turkish controlled area', 56),
(914, 'Central Bohemian', 57),
(915, 'Frycovice', 57),
(916, 'Jihocesky Kraj', 57),
(917, 'Jihochesky', 57),
(918, 'Jihomoravsky', 57),
(919, 'Karlovarsky', 57),
(920, 'Klecany', 57),
(921, 'Kralovehradecky', 57),
(922, 'Liberecky', 57),
(923, 'Lipov', 57),
(924, 'Moravskoslezsky', 57),
(925, 'Olomoucky', 57),
(926, 'Olomoucky Kraj', 57),
(927, 'Pardubicky', 57),
(928, 'Plzensky', 57),
(929, 'Praha', 57),
(930, 'Rajhrad', 57),
(931, 'Smirice', 57),
(932, 'South Moravian', 57),
(933, 'Straz nad Nisou', 57),
(934, 'Stredochesky', 57),
(935, 'Unicov', 57),
(936, 'Ustecky', 57),
(937, 'Valletta', 57),
(938, 'Velesin', 57),
(939, 'Vysochina', 57),
(940, 'Zlinsky', 57),
(941, 'Arhus', 58),
(942, 'Bornholm', 58),
(943, 'Frederiksborg', 58),
(944, 'Fyn', 58),
(945, 'Hovedstaden', 58),
(946, 'Kobenhavn', 58),
(947, 'Kobenhavns Amt', 58),
(948, 'Kobenhavns Kommune', 58),
(949, 'Nordjylland', 58),
(950, 'Ribe', 58),
(951, 'Ringkobing', 58),
(952, 'Roervig', 58),
(953, 'Roskilde', 58),
(954, 'Roslev', 58),
(955, 'Sjaelland', 58),
(956, 'Soeborg', 58),
(957, 'Sonderjylland', 58),
(958, 'Storstrom', 58),
(959, 'Syddanmark', 58),
(960, 'Toelloese', 58),
(961, 'Vejle', 58),
(962, 'Vestsjalland', 58),
(963, 'Viborg', 58),
(964, '\'Ali Sabih', 59),
(965, 'Dikhil', 59),
(966, 'Jibuti', 59),
(967, 'Tajurah', 59),
(968, 'Ubuk', 59),
(969, 'Saint Andrew', 60),
(970, 'Saint David', 60),
(971, 'Saint George', 60),
(972, 'Saint John', 60),
(973, 'Saint Joseph', 60),
(974, 'Saint Luke', 60),
(975, 'Saint Mark', 60),
(976, 'Saint Patrick', 60),
(977, 'Saint Paul', 60),
(978, 'Saint Peter', 60),
(979, 'Azua', 61),
(980, 'Bahoruco', 61),
(981, 'Barahona', 61),
(982, 'Dajabon', 61),
(983, 'Distrito Nacional', 61),
(984, 'Duarte', 61),
(985, 'El Seybo', 61),
(986, 'Elias Pina', 61),
(987, 'Espaillat', 61),
(988, 'Hato Mayor', 61),
(989, 'Independencia', 61),
(990, 'La Altagracia', 61),
(991, 'La Romana', 61),
(992, 'La Vega', 61),
(993, 'Maria Trinidad Sanchez', 61),
(994, 'Monsenor Nouel', 61),
(995, 'Monte Cristi', 61),
(996, 'Monte Plata', 61),
(997, 'Pedernales', 61),
(998, 'Peravia', 61),
(999, 'Puerto Plata', 61),
(1000, 'Salcedo', 61),
(1001, 'Samana', 61),
(1002, 'San Cristobal', 61),
(1003, 'San Juan', 61),
(1004, 'San Pedro de Macoris', 61),
(1005, 'Sanchez Ramirez', 61),
(1006, 'Santiago', 61),
(1007, 'Santiago Rodriguez', 61),
(1008, 'Valverde', 61),
(1009, 'Aileu', 62),
(1010, 'Ainaro', 62),
(1011, 'Ambeno', 62),
(1012, 'Baucau', 62),
(1013, 'Bobonaro', 62),
(1014, 'Cova Lima', 62),
(1015, 'Dili', 62),
(1016, 'Ermera', 62),
(1017, 'Lautem', 62),
(1018, 'Liquica', 62),
(1019, 'Manatuto', 62),
(1020, 'Manufahi', 62),
(1021, 'Viqueque', 62),
(1022, 'Azuay', 63),
(1023, 'Bolivar', 63),
(1024, 'Canar', 63),
(1025, 'Carchi', 63),
(1026, 'Chimborazo', 63),
(1027, 'Cotopaxi', 63),
(1028, 'El Oro', 63),
(1029, 'Esmeraldas', 63),
(1030, 'Galapagos', 63),
(1031, 'Guayas', 63),
(1032, 'Imbabura', 63),
(1033, 'Loja', 63),
(1034, 'Los Rios', 63),
(1035, 'Manabi', 63),
(1036, 'Morona Santiago', 63),
(1037, 'Napo', 63),
(1038, 'Orellana', 63),
(1039, 'Pastaza', 63),
(1040, 'Pichincha', 63),
(1041, 'Sucumbios', 63),
(1042, 'Tungurahua', 63),
(1043, 'Zamora Chinchipe', 63),
(1044, 'Aswan', 64),
(1045, 'Asyut', 64),
(1046, 'Bani Suwayf', 64),
(1047, 'Bur Sa\'id', 64),
(1048, 'Cairo', 64),
(1049, 'Dumyat', 64),
(1050, 'Kafr-ash-Shaykh', 64),
(1051, 'Matruh', 64),
(1052, 'Muhafazat ad Daqahliyah', 64),
(1053, 'Muhafazat al Fayyum', 64),
(1054, 'Muhafazat al Gharbiyah', 64),
(1055, 'Muhafazat al Iskandariyah', 64),
(1056, 'Muhafazat al Qahirah', 64),
(1057, 'Qina', 64),
(1058, 'Sawhaj', 64),
(1059, 'Sina al-Janubiyah', 64),
(1060, 'Sina ash-Shamaliyah', 64),
(1061, 'ad-Daqahliyah', 64),
(1062, 'al-Bahr-al-Ahmar', 64),
(1063, 'al-Buhayrah', 64),
(1064, 'al-Fayyum', 64),
(1065, 'al-Gharbiyah', 64),
(1066, 'al-Iskandariyah', 64),
(1067, 'al-Ismailiyah', 64),
(1068, 'al-Jizah', 64),
(1069, 'al-Minufiyah', 64),
(1070, 'al-Minya', 64),
(1071, 'al-Qahira', 64),
(1072, 'al-Qalyubiyah', 64),
(1073, 'al-Uqsur', 64),
(1074, 'al-Wadi al-Jadid', 64),
(1075, 'as-Suways', 64),
(1076, 'ash-Sharqiyah', 64),
(1077, 'Ahuachapan', 65),
(1078, 'Cabanas', 65),
(1079, 'Chalatenango', 65),
(1080, 'Cuscatlan', 65),
(1081, 'La Libertad', 65),
(1082, 'La Paz', 65),
(1083, 'La Union', 65),
(1084, 'Morazan', 65),
(1085, 'San Miguel', 65),
(1086, 'San Salvador', 65),
(1087, 'San Vicente', 65),
(1088, 'Santa Ana', 65),
(1089, 'Sonsonate', 65),
(1090, 'Usulutan', 65),
(1091, 'Annobon', 66),
(1092, 'Bioko Norte', 66),
(1093, 'Bioko Sur', 66),
(1094, 'Centro Sur', 66),
(1095, 'Kie-Ntem', 66),
(1096, 'Litoral', 66),
(1097, 'Wele-Nzas', 66),
(1098, 'Anseba', 67),
(1099, 'Debub', 67),
(1100, 'Debub-Keih-Bahri', 67),
(1101, 'Gash-Barka', 67),
(1102, 'Maekel', 67),
(1103, 'Semien-Keih-Bahri', 67),
(1104, 'Harju', 68),
(1105, 'Hiiu', 68),
(1106, 'Ida-Viru', 68),
(1107, 'Jarva', 68),
(1108, 'Jogeva', 68),
(1109, 'Laane', 68),
(1110, 'Laane-Viru', 68),
(1111, 'Parnu', 68),
(1112, 'Polva', 68),
(1113, 'Rapla', 68),
(1114, 'Saare', 68),
(1115, 'Tartu', 68),
(1116, 'Valga', 68),
(1117, 'Viljandi', 68),
(1118, 'Voru', 68),
(1119, 'Addis Abeba', 69),
(1120, 'Afar', 69),
(1121, 'Amhara', 69),
(1122, 'Benishangul', 69),
(1123, 'Diredawa', 69),
(1124, 'Gambella', 69),
(1125, 'Harar', 69),
(1126, 'Jigjiga', 69),
(1127, 'Mekele', 69),
(1128, 'Oromia', 69),
(1129, 'Somali', 69),
(1130, 'Southern', 69),
(1131, 'Tigray', 69),
(1132, 'Christmas Island', 70),
(1133, 'Cocos Islands', 70),
(1134, 'Coral Sea Islands', 70),
(1135, 'Falkland Islands', 71),
(1136, 'South Georgia', 71),
(1137, 'Klaksvik', 72),
(1138, 'Nor ara Eysturoy', 72),
(1139, 'Nor oy', 72),
(1140, 'Sandoy', 72),
(1141, 'Streymoy', 72),
(1142, 'Su uroy', 72),
(1143, 'Sy ra Eysturoy', 72),
(1144, 'Torshavn', 72),
(1145, 'Vaga', 72),
(1146, 'Central', 73),
(1147, 'Eastern', 73),
(1148, 'Northern', 73),
(1149, 'South Pacific', 73),
(1150, 'Western', 73),
(1151, 'Ahvenanmaa', 74),
(1152, 'Etela-Karjala', 74),
(1153, 'Etela-Pohjanmaa', 74),
(1154, 'Etela-Savo', 74),
(1155, 'Etela-Suomen Laani', 74),
(1156, 'Ita-Suomen Laani', 74),
(1157, 'Ita-Uusimaa', 74),
(1158, 'Kainuu', 74),
(1159, 'Kanta-Hame', 74),
(1160, 'Keski-Pohjanmaa', 74),
(1161, 'Keski-Suomi', 74),
(1162, 'Kymenlaakso', 74),
(1163, 'Lansi-Suomen Laani', 74),
(1164, 'Lappi', 74),
(1165, 'Northern Savonia', 74),
(1166, 'Ostrobothnia', 74),
(1167, 'Oulun Laani', 74),
(1168, 'Paijat-Hame', 74),
(1169, 'Pirkanmaa', 74),
(1170, 'Pohjanmaa', 74),
(1171, 'Pohjois-Karjala', 74),
(1172, 'Pohjois-Pohjanmaa', 74),
(1173, 'Pohjois-Savo', 74),
(1174, 'Saarijarvi', 74),
(1175, 'Satakunta', 74),
(1176, 'Southern Savonia', 74),
(1177, 'Tavastia Proper', 74),
(1178, 'Uleaborgs Lan', 74),
(1179, 'Uusimaa', 74),
(1180, 'Varsinais-Suomi', 74),
(1181, 'Ain', 75),
(1182, 'Aisne', 75),
(1183, 'Albi Le Sequestre', 75),
(1184, 'Allier', 75),
(1185, 'Alpes-Cote dAzur', 75),
(1186, 'Alpes-Maritimes', 75),
(1187, 'Alpes-de-Haute-Provence', 75),
(1188, 'Alsace', 75),
(1189, 'Aquitaine', 75),
(1190, 'Ardeche', 75),
(1191, 'Ardennes', 75),
(1192, 'Ariege', 75),
(1193, 'Aube', 75),
(1194, 'Aude', 75),
(1195, 'Auvergne', 75),
(1196, 'Aveyron', 75),
(1197, 'Bas-Rhin', 75),
(1198, 'Basse-Normandie', 75),
(1199, 'Bouches-du-Rhone', 75),
(1200, 'Bourgogne', 75),
(1201, 'Bretagne', 75),
(1202, 'Brittany', 75),
(1203, 'Burgundy', 75),
(1204, 'Calvados', 75),
(1205, 'Cantal', 75),
(1206, 'Cedex', 75),
(1207, 'Centre', 75),
(1208, 'Charente', 75),
(1209, 'Charente-Maritime', 75),
(1210, 'Cher', 75),
(1211, 'Correze', 75),
(1212, 'Corse-du-Sud', 75),
(1213, 'Cote-d\'Or', 75),
(1214, 'Cotes-d\'Armor', 75),
(1215, 'Creuse', 75),
(1216, 'Crolles', 75),
(1217, 'Deux-Sevres', 75),
(1218, 'Dordogne', 75),
(1219, 'Doubs', 75),
(1220, 'Drome', 75),
(1221, 'Essonne', 75),
(1222, 'Eure', 75),
(1223, 'Eure-et-Loir', 75),
(1224, 'Feucherolles', 75),
(1225, 'Finistere', 75),
(1226, 'Franche-Comte', 75),
(1227, 'Gard', 75),
(1228, 'Gers', 75),
(1229, 'Gironde', 75),
(1230, 'Haut-Rhin', 75),
(1231, 'Haute-Corse', 75),
(1232, 'Haute-Garonne', 75),
(1233, 'Haute-Loire', 75),
(1234, 'Haute-Marne', 75),
(1235, 'Haute-Saone', 75),
(1236, 'Haute-Savoie', 75),
(1237, 'Haute-Vienne', 75),
(1238, 'Hautes-Alpes', 75),
(1239, 'Hautes-Pyrenees', 75),
(1240, 'Hauts-de-Seine', 75),
(1241, 'Herault', 75),
(1242, 'Ile-de-France', 75),
(1243, 'Ille-et-Vilaine', 75),
(1244, 'Indre', 75),
(1245, 'Indre-et-Loire', 75),
(1246, 'Isere', 75),
(1247, 'Jura', 75),
(1248, 'Klagenfurt', 75),
(1249, 'Landes', 75),
(1250, 'Languedoc-Roussillon', 75),
(1251, 'Larcay', 75),
(1252, 'Le Castellet', 75),
(1253, 'Le Creusot', 75),
(1254, 'Limousin', 75),
(1255, 'Loir-et-Cher', 75),
(1256, 'Loire', 75),
(1257, 'Loire-Atlantique', 75),
(1258, 'Loiret', 75),
(1259, 'Lorraine', 75),
(1260, 'Lot', 75),
(1261, 'Lot-et-Garonne', 75),
(1262, 'Lower Normandy', 75),
(1263, 'Lozere', 75),
(1264, 'Maine-et-Loire', 75),
(1265, 'Manche', 75),
(1266, 'Marne', 75),
(1267, 'Mayenne', 75),
(1268, 'Meurthe-et-Moselle', 75),
(1269, 'Meuse', 75),
(1270, 'Midi-Pyrenees', 75),
(1271, 'Morbihan', 75),
(1272, 'Moselle', 75),
(1273, 'Nievre', 75),
(1274, 'Nord', 75),
(1275, 'Nord-Pas-de-Calais', 75),
(1276, 'Oise', 75),
(1277, 'Orne', 75),
(1278, 'Paris', 75),
(1279, 'Pas-de-Calais', 75),
(1280, 'Pays de la Loire', 75),
(1281, 'Pays-de-la-Loire', 75),
(1282, 'Picardy', 75),
(1283, 'Puy-de-Dome', 75),
(1284, 'Pyrenees-Atlantiques', 75),
(1285, 'Pyrenees-Orientales', 75),
(1286, 'Quelmes', 75),
(1287, 'Rhone', 75),
(1288, 'Rhone-Alpes', 75),
(1289, 'Saint Ouen', 75),
(1290, 'Saint Viatre', 75),
(1291, 'Saone-et-Loire', 75),
(1292, 'Sarthe', 75),
(1293, 'Savoie', 75),
(1294, 'Seine-Maritime', 75),
(1295, 'Seine-Saint-Denis', 75),
(1296, 'Seine-et-Marne', 75),
(1297, 'Somme', 75),
(1298, 'Sophia Antipolis', 75),
(1299, 'Souvans', 75),
(1300, 'Tarn', 75),
(1301, 'Tarn-et-Garonne', 75),
(1302, 'Territoire de Belfort', 75),
(1303, 'Treignac', 75),
(1304, 'Upper Normandy', 75),
(1305, 'Val-d\'Oise', 75),
(1306, 'Val-de-Marne', 75),
(1307, 'Var', 75),
(1308, 'Vaucluse', 75),
(1309, 'Vellise', 75),
(1310, 'Vendee', 75),
(1311, 'Vienne', 75),
(1312, 'Vosges', 75),
(1313, 'Yonne', 75),
(1314, 'Yvelines', 75),
(1315, 'Cayenne', 76),
(1316, 'Saint-Laurent-du-Maroni', 76),
(1317, 'Iles du Vent', 77),
(1318, 'Iles sous le Vent', 77),
(1319, 'Marquesas', 77),
(1320, 'Tuamotu', 77),
(1321, 'Tubuai', 77),
(1322, 'Amsterdam', 78),
(1323, 'Crozet Islands', 78),
(1324, 'Kerguelen', 78),
(1325, 'Estuaire', 79),
(1326, 'Haut-Ogooue', 79),
(1327, 'Moyen-Ogooue', 79),
(1328, 'Ngounie', 79),
(1329, 'Nyanga', 79),
(1330, 'Ogooue-Ivindo', 79),
(1331, 'Ogooue-Lolo', 79),
(1332, 'Ogooue-Maritime', 79),
(1333, 'Woleu-Ntem', 79),
(1334, 'Banjul', 80),
(1335, 'Basse', 80),
(1336, 'Brikama', 80),
(1337, 'Janjanbureh', 80),
(1338, 'Kanifing', 80),
(1339, 'Kerewan', 80),
(1340, 'Kuntaur', 80),
(1341, 'Mansakonko', 80),
(1342, 'Abhasia', 81),
(1343, 'Ajaria', 81),
(1344, 'Guria', 81),
(1345, 'Imereti', 81),
(1346, 'Kaheti', 81),
(1347, 'Kvemo Kartli', 81),
(1348, 'Mcheta-Mtianeti', 81),
(1349, 'Racha', 81),
(1350, 'Samagrelo-Zemo Svaneti', 81),
(1351, 'Samche-Zhavaheti', 81),
(1352, 'Shida Kartli', 81),
(1353, 'Tbilisi', 81),
(1354, 'Auvergne', 82),
(1355, 'Baden-Wurttemberg', 82),
(1356, 'Bavaria', 82),
(1357, 'Bayern', 82),
(1358, 'Beilstein Wurtt', 82),
(1359, 'Berlin', 82),
(1360, 'Brandenburg', 82),
(1361, 'Bremen', 82),
(1362, 'Dreisbach', 82),
(1363, 'Freistaat Bayern', 82),
(1364, 'Hamburg', 82),
(1365, 'Hannover', 82),
(1366, 'Heroldstatt', 82),
(1367, 'Hessen', 82),
(1368, 'Kortenberg', 82),
(1369, 'Laasdorf', 82),
(1370, 'Land Baden-Wurttemberg', 82),
(1371, 'Land Bayern', 82),
(1372, 'Land Brandenburg', 82),
(1373, 'Land Hessen', 82),
(1374, 'Land Mecklenburg-Vorpommern', 82),
(1375, 'Land Nordrhein-Westfalen', 82),
(1376, 'Land Rheinland-Pfalz', 82),
(1377, 'Land Sachsen', 82),
(1378, 'Land Sachsen-Anhalt', 82),
(1379, 'Land Thuringen', 82),
(1380, 'Lower Saxony', 82),
(1381, 'Mecklenburg-Vorpommern', 82),
(1382, 'Mulfingen', 82),
(1383, 'Munich', 82),
(1384, 'Neubeuern', 82),
(1385, 'Niedersachsen', 82),
(1386, 'Noord-Holland', 82),
(1387, 'Nordrhein-Westfalen', 82),
(1388, 'North Rhine-Westphalia', 82),
(1389, 'Osterode', 82),
(1390, 'Rheinland-Pfalz', 82),
(1391, 'Rhineland-Palatinate', 82),
(1392, 'Saarland', 82),
(1393, 'Sachsen', 82),
(1394, 'Sachsen-Anhalt', 82),
(1395, 'Saxony', 82),
(1396, 'Schleswig-Holstein', 82),
(1397, 'Thuringia', 82),
(1398, 'Webling', 82),
(1399, 'Weinstrabe', 82),
(1400, 'schlobborn', 82),
(1401, 'Ashanti', 83),
(1402, 'Brong-Ahafo', 83),
(1403, 'Central', 83),
(1404, 'Eastern', 83),
(1405, 'Greater Accra', 83),
(1406, 'Northern', 83),
(1407, 'Upper East', 83),
(1408, 'Upper West', 83),
(1409, 'Volta', 83),
(1410, 'Western', 83),
(1411, 'Gibraltar', 84),
(1412, 'Acharnes', 85),
(1413, 'Ahaia', 85),
(1414, 'Aitolia kai Akarnania', 85),
(1415, 'Argolis', 85),
(1416, 'Arkadia', 85),
(1417, 'Arta', 85),
(1418, 'Attica', 85),
(1419, 'Attiki', 85),
(1420, 'Ayion Oros', 85),
(1421, 'Crete', 85),
(1422, 'Dodekanisos', 85),
(1423, 'Drama', 85),
(1424, 'Evia', 85),
(1425, 'Evritania', 85),
(1426, 'Evros', 85),
(1427, 'Evvoia', 85),
(1428, 'Florina', 85),
(1429, 'Fokis', 85),
(1430, 'Fthiotis', 85),
(1431, 'Grevena', 85),
(1432, 'Halandri', 85),
(1433, 'Halkidiki', 85),
(1434, 'Hania', 85),
(1435, 'Heraklion', 85),
(1436, 'Hios', 85),
(1437, 'Ilia', 85),
(1438, 'Imathia', 85),
(1439, 'Ioannina', 85),
(1440, 'Iraklion', 85),
(1441, 'Karditsa', 85),
(1442, 'Kastoria', 85),
(1443, 'Kavala', 85),
(1444, 'Kefallinia', 85),
(1445, 'Kerkira', 85),
(1446, 'Kiklades', 85),
(1447, 'Kilkis', 85),
(1448, 'Korinthia', 85),
(1449, 'Kozani', 85),
(1450, 'Lakonia', 85),
(1451, 'Larisa', 85),
(1452, 'Lasithi', 85),
(1453, 'Lesvos', 85),
(1454, 'Levkas', 85),
(1455, 'Magnisia', 85),
(1456, 'Messinia', 85),
(1457, 'Nomos Attikis', 85),
(1458, 'Nomos Zakynthou', 85),
(1459, 'Pella', 85),
(1460, 'Pieria', 85),
(1461, 'Piraios', 85),
(1462, 'Preveza', 85),
(1463, 'Rethimni', 85),
(1464, 'Rodopi', 85),
(1465, 'Samos', 85),
(1466, 'Serrai', 85),
(1467, 'Thesprotia', 85),
(1468, 'Thessaloniki', 85),
(1469, 'Trikala', 85),
(1470, 'Voiotia', 85),
(1471, 'West Greece', 85),
(1472, 'Xanthi', 85),
(1473, 'Zakinthos', 85),
(1474, 'Aasiaat', 86),
(1475, 'Ammassalik', 86),
(1476, 'Illoqqortoormiut', 86),
(1477, 'Ilulissat', 86),
(1478, 'Ivittuut', 86),
(1479, 'Kangaatsiaq', 86),
(1480, 'Maniitsoq', 86),
(1481, 'Nanortalik', 86),
(1482, 'Narsaq', 86),
(1483, 'Nuuk', 86),
(1484, 'Paamiut', 86),
(1485, 'Qaanaaq', 86),
(1486, 'Qaqortoq', 86),
(1487, 'Qasigiannguit', 86),
(1488, 'Qeqertarsuaq', 86),
(1489, 'Sisimiut', 86),
(1490, 'Udenfor kommunal inddeling', 86),
(1491, 'Upernavik', 86),
(1492, 'Uummannaq', 86),
(1493, 'Carriacou-Petite Martinique', 87),
(1494, 'Saint Andrew', 87),
(1495, 'Saint Davids', 87),
(1496, 'Saint George\'s', 87),
(1497, 'Saint John', 87),
(1498, 'Saint Mark', 87),
(1499, 'Saint Patrick', 87),
(1500, 'Basse-Terre', 88),
(1501, 'Grande-Terre', 88),
(1502, 'Iles des Saintes', 88),
(1503, 'La Desirade', 88),
(1504, 'Marie-Galante', 88),
(1505, 'Saint Barthelemy', 88),
(1506, 'Saint Martin', 88),
(1507, 'Agana Heights', 89),
(1508, 'Agat', 89),
(1509, 'Barrigada', 89),
(1510, 'Chalan-Pago-Ordot', 89),
(1511, 'Dededo', 89),
(1512, 'Hagatna', 89),
(1513, 'Inarajan', 89),
(1514, 'Mangilao', 89),
(1515, 'Merizo', 89),
(1516, 'Mongmong-Toto-Maite', 89),
(1517, 'Santa Rita', 89),
(1518, 'Sinajana', 89),
(1519, 'Talofofo', 89),
(1520, 'Tamuning', 89),
(1521, 'Yigo', 89),
(1522, 'Yona', 89),
(1523, 'Alta Verapaz', 90),
(1524, 'Baja Verapaz', 90),
(1525, 'Chimaltenango', 90),
(1526, 'Chiquimula', 90),
(1527, 'El Progreso', 90),
(1528, 'Escuintla', 90),
(1529, 'Guatemala', 90),
(1530, 'Huehuetenango', 90),
(1531, 'Izabal', 90),
(1532, 'Jalapa', 90),
(1533, 'Jutiapa', 90),
(1534, 'Peten', 90),
(1535, 'Quezaltenango', 90),
(1536, 'Quiche', 90),
(1537, 'Retalhuleu', 90),
(1538, 'Sacatepequez', 90),
(1539, 'San Marcos', 90),
(1540, 'Santa Rosa', 90),
(1541, 'Solola', 90),
(1542, 'Suchitepequez', 90),
(1543, 'Totonicapan', 90),
(1544, 'Zacapa', 90),
(1545, 'Alderney', 91),
(1546, 'Castel', 91),
(1547, 'Forest', 91),
(1548, 'Saint Andrew', 91),
(1549, 'Saint Martin', 91),
(1550, 'Saint Peter Port', 91),
(1551, 'Saint Pierre du Bois', 91),
(1552, 'Saint Sampson', 91),
(1553, 'Saint Saviour', 91),
(1554, 'Sark', 91),
(1555, 'Torteval', 91),
(1556, 'Vale', 91),
(1557, 'Beyla', 92),
(1558, 'Boffa', 92),
(1559, 'Boke', 92),
(1560, 'Conakry', 92),
(1561, 'Coyah', 92),
(1562, 'Dabola', 92),
(1563, 'Dalaba', 92),
(1564, 'Dinguiraye', 92),
(1565, 'Faranah', 92),
(1566, 'Forecariah', 92),
(1567, 'Fria', 92),
(1568, 'Gaoual', 92),
(1569, 'Gueckedou', 92),
(1570, 'Kankan', 92),
(1571, 'Kerouane', 92),
(1572, 'Kindia', 92),
(1573, 'Kissidougou', 92),
(1574, 'Koubia', 92),
(1575, 'Koundara', 92),
(1576, 'Kouroussa', 92),
(1577, 'Labe', 92),
(1578, 'Lola', 92),
(1579, 'Macenta', 92),
(1580, 'Mali', 92),
(1581, 'Mamou', 92),
(1582, 'Mandiana', 92),
(1583, 'Nzerekore', 92),
(1584, 'Pita', 92),
(1585, 'Siguiri', 92),
(1586, 'Telimele', 92),
(1587, 'Tougue', 92),
(1588, 'Yomou', 92),
(1589, 'Bafata', 93),
(1590, 'Bissau', 93),
(1591, 'Bolama', 93),
(1592, 'Cacheu', 93),
(1593, 'Gabu', 93),
(1594, 'Oio', 93),
(1595, 'Quinara', 93),
(1596, 'Tombali', 93),
(1597, 'Barima-Waini', 94),
(1598, 'Cuyuni-Mazaruni', 94),
(1599, 'Demerara-Mahaica', 94),
(1600, 'East Berbice-Corentyne', 94),
(1601, 'Essequibo Islands-West Demerar', 94),
(1602, 'Mahaica-Berbice', 94),
(1603, 'Pomeroon-Supenaam', 94),
(1604, 'Potaro-Siparuni', 94),
(1605, 'Upper Demerara-Berbice', 94),
(1606, 'Upper Takutu-Upper Essequibo', 94),
(1607, 'Artibonite', 95),
(1608, 'Centre', 95),
(1609, 'Grand\'Anse', 95),
(1610, 'Nord', 95),
(1611, 'Nord-Est', 95),
(1612, 'Nord-Ouest', 95),
(1613, 'Ouest', 95),
(1614, 'Sud', 95),
(1615, 'Sud-Est', 95),
(1616, 'Heard and McDonald Islands', 96),
(1617, 'Atlantida', 97),
(1618, 'Choluteca', 97),
(1619, 'Colon', 97),
(1620, 'Comayagua', 97),
(1621, 'Copan', 97),
(1622, 'Cortes', 97),
(1623, 'Distrito Central', 97),
(1624, 'El Paraiso', 97),
(1625, 'Francisco Morazan', 97),
(1626, 'Gracias a Dios', 97),
(1627, 'Intibuca', 97),
(1628, 'Islas de la Bahia', 97),
(1629, 'La Paz', 97),
(1630, 'Lempira', 97),
(1631, 'Ocotepeque', 97),
(1632, 'Olancho', 97),
(1633, 'Santa Barbara', 97),
(1634, 'Valle', 97),
(1635, 'Yoro', 97),
(1636, 'Hong Kong', 98),
(1637, 'Bacs-Kiskun', 99),
(1638, 'Baranya', 99),
(1639, 'Bekes', 99),
(1640, 'Borsod-Abauj-Zemplen', 99),
(1641, 'Budapest', 99),
(1642, 'Csongrad', 99),
(1643, 'Fejer', 99),
(1644, 'Gyor-Moson-Sopron', 99),
(1645, 'Hajdu-Bihar', 99),
(1646, 'Heves', 99),
(1647, 'Jasz-Nagykun-Szolnok', 99),
(1648, 'Komarom-Esztergom', 99),
(1649, 'Nograd', 99),
(1650, 'Pest', 99),
(1651, 'Somogy', 99),
(1652, 'Szabolcs-Szatmar-Bereg', 99),
(1653, 'Tolna', 99),
(1654, 'Vas', 99),
(1655, 'Veszprem', 99),
(1656, 'Zala', 99),
(1657, 'Austurland', 100),
(1658, 'Gullbringusysla', 100),
(1659, 'Hofu borgarsva i', 100),
(1660, 'Nor urland eystra', 100),
(1661, 'Nor urland vestra', 100),
(1662, 'Su urland', 100),
(1663, 'Su urnes', 100),
(1664, 'Vestfir ir', 100),
(1665, 'Vesturland', 100),
(1666, 'Aceh', 102),
(1667, 'Bali', 102),
(1668, 'Bangka-Belitung', 102),
(1669, 'Banten', 102),
(1670, 'Bengkulu', 102),
(1671, 'Gandaria', 102),
(1672, 'Gorontalo', 102),
(1673, 'Jakarta', 102),
(1674, 'Jambi', 102),
(1675, 'Jawa Barat', 102),
(1676, 'Jawa Tengah', 102),
(1677, 'Jawa Timur', 102),
(1678, 'Kalimantan Barat', 102),
(1679, 'Kalimantan Selatan', 102),
(1680, 'Kalimantan Tengah', 102),
(1681, 'Kalimantan Timur', 102),
(1682, 'Kendal', 102),
(1683, 'Lampung', 102),
(1684, 'Maluku', 102),
(1685, 'Maluku Utara', 102),
(1686, 'Nusa Tenggara Barat', 102),
(1687, 'Nusa Tenggara Timur', 102),
(1688, 'Papua', 102),
(1689, 'Riau', 102),
(1690, 'Riau Kepulauan', 102),
(1691, 'Solo', 102),
(1692, 'Sulawesi Selatan', 102),
(1693, 'Sulawesi Tengah', 102),
(1694, 'Sulawesi Tenggara', 102),
(1695, 'Sulawesi Utara', 102),
(1696, 'Sumatera Barat', 102),
(1697, 'Sumatera Selatan', 102),
(1698, 'Sumatera Utara', 102),
(1699, 'Yogyakarta', 102),
(1700, 'Ardabil', 103),
(1701, 'Azarbayjan-e Bakhtari', 103),
(1702, 'Azarbayjan-e Khavari', 103),
(1703, 'Bushehr', 103),
(1704, 'Chahar Mahal-e Bakhtiari', 103),
(1705, 'Esfahan', 103),
(1706, 'Fars', 103),
(1707, 'Gilan', 103),
(1708, 'Golestan', 103),
(1709, 'Hamadan', 103),
(1710, 'Hormozgan', 103),
(1711, 'Ilam', 103),
(1712, 'Kerman', 103),
(1713, 'Kermanshah', 103),
(1714, 'Khorasan', 103),
(1715, 'Khuzestan', 103),
(1716, 'Kohgiluyeh-e Boyerahmad', 103),
(1717, 'Kordestan', 103),
(1718, 'Lorestan', 103),
(1719, 'Markazi', 103),
(1720, 'Mazandaran', 103),
(1721, 'Ostan-e Esfahan', 103),
(1722, 'Qazvin', 103),
(1723, 'Qom', 103),
(1724, 'Semnan', 103),
(1725, 'Sistan-e Baluchestan', 103),
(1726, 'Tehran', 103),
(1727, 'Yazd', 103),
(1728, 'Zanjan', 103),
(1729, 'Babil', 104),
(1730, 'Baghdad', 104),
(1731, 'Dahuk', 104),
(1732, 'Dhi Qar', 104),
(1733, 'Diyala', 104),
(1734, 'Erbil', 104),
(1735, 'Irbil', 104),
(1736, 'Karbala', 104),
(1737, 'Kurdistan', 104),
(1738, 'Maysan', 104),
(1739, 'Ninawa', 104),
(1740, 'Salah-ad-Din', 104),
(1741, 'Wasit', 104),
(1742, 'al-Anbar', 104),
(1743, 'al-Basrah', 104),
(1744, 'al-Muthanna', 104),
(1745, 'al-Qadisiyah', 104),
(1746, 'an-Najaf', 104),
(1747, 'as-Sulaymaniyah', 104),
(1748, 'at-Ta\'mim', 104),
(1749, 'Armagh', 105),
(1750, 'Carlow', 105),
(1751, 'Cavan', 105),
(1752, 'Clare', 105),
(1753, 'Cork', 105),
(1754, 'Donegal', 105),
(1755, 'Dublin', 105),
(1756, 'Galway', 105),
(1757, 'Kerry', 105),
(1758, 'Kildare', 105),
(1759, 'Kilkenny', 105),
(1760, 'Laois', 105),
(1761, 'Leinster', 105),
(1762, 'Leitrim', 105),
(1763, 'Limerick', 105),
(1764, 'Loch Garman', 105),
(1765, 'Longford', 105),
(1766, 'Louth', 105),
(1767, 'Mayo', 105),
(1768, 'Meath', 105),
(1769, 'Monaghan', 105),
(1770, 'Offaly', 105),
(1771, 'Roscommon', 105),
(1772, 'Sligo', 105),
(1773, 'Tipperary North Riding', 105),
(1774, 'Tipperary South Riding', 105),
(1775, 'Ulster', 105),
(1776, 'Waterford', 105),
(1777, 'Westmeath', 105),
(1778, 'Wexford', 105),
(1779, 'Wicklow', 105),
(1780, 'Beit Hanania', 106),
(1781, 'Ben Gurion Airport', 106),
(1782, 'Bethlehem', 106),
(1783, 'Caesarea', 106),
(1784, 'Centre', 106),
(1785, 'Gaza', 106),
(1786, 'Hadaron', 106),
(1787, 'Haifa District', 106),
(1788, 'Hamerkaz', 106),
(1789, 'Hazafon', 106),
(1790, 'Hebron', 106),
(1791, 'Jaffa', 106),
(1792, 'Jerusalem', 106),
(1793, 'Khefa', 106),
(1794, 'Kiryat Yam', 106),
(1795, 'Lower Galilee', 106),
(1796, 'Qalqilya', 106),
(1797, 'Talme Elazar', 106),
(1798, 'Tel Aviv', 106),
(1799, 'Tsafon', 106),
(1800, 'Umm El Fahem', 106),
(1801, 'Yerushalayim', 106),
(1802, 'Abruzzi', 107),
(1803, 'Abruzzo', 107),
(1804, 'Agrigento', 107),
(1805, 'Alessandria', 107),
(1806, 'Ancona', 107),
(1807, 'Arezzo', 107),
(1808, 'Ascoli Piceno', 107),
(1809, 'Asti', 107),
(1810, 'Avellino', 107),
(1811, 'Bari', 107),
(1812, 'Basilicata', 107),
(1813, 'Belluno', 107),
(1814, 'Benevento', 107),
(1815, 'Bergamo', 107),
(1816, 'Biella', 107),
(1817, 'Bologna', 107),
(1818, 'Bolzano', 107),
(1819, 'Brescia', 107),
(1820, 'Brindisi', 107),
(1821, 'Calabria', 107),
(1822, 'Campania', 107),
(1823, 'Cartoceto', 107),
(1824, 'Caserta', 107),
(1825, 'Catania', 107),
(1826, 'Chieti', 107),
(1827, 'Como', 107),
(1828, 'Cosenza', 107),
(1829, 'Cremona', 107),
(1830, 'Cuneo', 107),
(1831, 'Emilia-Romagna', 107),
(1832, 'Ferrara', 107),
(1833, 'Firenze', 107),
(1834, 'Florence', 107),
(1835, 'Forli-Cesena ', 107),
(1836, 'Friuli-Venezia Giulia', 107),
(1837, 'Frosinone', 107),
(1838, 'Genoa', 107),
(1839, 'Gorizia', 107),
(1840, 'L\'Aquila', 107),
(1841, 'Lazio', 107),
(1842, 'Lecce', 107),
(1843, 'Lecco', 107),
(1844, 'Lecco Province', 107),
(1845, 'Liguria', 107),
(1846, 'Lodi', 107),
(1847, 'Lombardia', 107),
(1848, 'Lombardy', 107),
(1849, 'Macerata', 107),
(1850, 'Mantova', 107),
(1851, 'Marche', 107),
(1852, 'Messina', 107),
(1853, 'Milan', 107),
(1854, 'Modena', 107),
(1855, 'Molise', 107),
(1856, 'Molteno', 107),
(1857, 'Montenegro', 107),
(1858, 'Monza and Brianza', 107),
(1859, 'Naples', 107),
(1860, 'Novara', 107),
(1861, 'Padova', 107),
(1862, 'Parma', 107),
(1863, 'Pavia', 107),
(1864, 'Perugia', 107),
(1865, 'Pesaro-Urbino', 107),
(1866, 'Piacenza', 107),
(1867, 'Piedmont', 107),
(1868, 'Piemonte', 107),
(1869, 'Pisa', 107),
(1870, 'Pordenone', 107),
(1871, 'Potenza', 107),
(1872, 'Puglia', 107),
(1873, 'Reggio Emilia', 107),
(1874, 'Rimini', 107),
(1875, 'Roma', 107),
(1876, 'Salerno', 107),
(1877, 'Sardegna', 107),
(1878, 'Sassari', 107),
(1879, 'Savona', 107),
(1880, 'Sicilia', 107),
(1881, 'Siena', 107),
(1882, 'Sondrio', 107),
(1883, 'South Tyrol', 107),
(1884, 'Taranto', 107),
(1885, 'Teramo', 107),
(1886, 'Torino', 107),
(1887, 'Toscana', 107),
(1888, 'Trapani', 107),
(1889, 'Trentino-Alto Adige', 107),
(1890, 'Trento', 107),
(1891, 'Treviso', 107),
(1892, 'Udine', 107),
(1893, 'Umbria', 107),
(1894, 'Valle d\'Aosta', 107),
(1895, 'Varese', 107),
(1896, 'Veneto', 107),
(1897, 'Venezia', 107),
(1898, 'Verbano-Cusio-Ossola', 107),
(1899, 'Vercelli', 107),
(1900, 'Verona', 107),
(1901, 'Vicenza', 107),
(1902, 'Viterbo', 107),
(1903, 'Buxoro Viloyati', 108),
(1904, 'Clarendon', 108),
(1905, 'Hanover', 108),
(1906, 'Kingston', 108),
(1907, 'Manchester', 108),
(1908, 'Portland', 108),
(1909, 'Saint Andrews', 108),
(1910, 'Saint Ann', 108),
(1911, 'Saint Catherine', 108),
(1912, 'Saint Elizabeth', 108),
(1913, 'Saint James', 108),
(1914, 'Saint Mary', 108),
(1915, 'Saint Thomas', 108),
(1916, 'Trelawney', 108),
(1917, 'Westmoreland', 108),
(1918, 'Aichi', 109),
(1919, 'Akita', 109),
(1920, 'Aomori', 109),
(1921, 'Chiba', 109),
(1922, 'Ehime', 109),
(1923, 'Fukui', 109),
(1924, 'Fukuoka', 109),
(1925, 'Fukushima', 109),
(1926, 'Gifu', 109),
(1927, 'Gumma', 109),
(1928, 'Hiroshima', 109),
(1929, 'Hokkaido', 109),
(1930, 'Hyogo', 109),
(1931, 'Ibaraki', 109),
(1932, 'Ishikawa', 109),
(1933, 'Iwate', 109),
(1934, 'Kagawa', 109),
(1935, 'Kagoshima', 109),
(1936, 'Kanagawa', 109),
(1937, 'Kanto', 109),
(1938, 'Kochi', 109),
(1939, 'Kumamoto', 109),
(1940, 'Kyoto', 109),
(1941, 'Mie', 109),
(1942, 'Miyagi', 109),
(1943, 'Miyazaki', 109),
(1944, 'Nagano', 109),
(1945, 'Nagasaki', 109),
(1946, 'Nara', 109),
(1947, 'Niigata', 109),
(1948, 'Oita', 109),
(1949, 'Okayama', 109),
(1950, 'Okinawa', 109),
(1951, 'Osaka', 109),
(1952, 'Saga', 109),
(1953, 'Saitama', 109),
(1954, 'Shiga', 109),
(1955, 'Shimane', 109),
(1956, 'Shizuoka', 109),
(1957, 'Tochigi', 109),
(1958, 'Tokushima', 109),
(1959, 'Tokyo', 109),
(1960, 'Tottori', 109),
(1961, 'Toyama', 109),
(1962, 'Wakayama', 109),
(1963, 'Yamagata', 109),
(1964, 'Yamaguchi', 109),
(1965, 'Yamanashi', 109),
(1966, 'Grouville', 110),
(1967, 'Saint Brelade', 110),
(1968, 'Saint Clement', 110),
(1969, 'Saint Helier', 110),
(1970, 'Saint John', 110),
(1971, 'Saint Lawrence', 110),
(1972, 'Saint Martin', 110),
(1973, 'Saint Mary', 110),
(1974, 'Saint Peter', 110),
(1975, 'Saint Saviour', 110),
(1976, 'Trinity', 110),
(1977, '\'Ajlun', 111),
(1978, 'Amman', 111),
(1979, 'Irbid', 111),
(1980, 'Jarash', 111),
(1981, 'Ma\'an', 111),
(1982, 'Madaba', 111),
(1983, 'al-\'Aqabah', 111),
(1984, 'al-Balqa\'', 111),
(1985, 'al-Karak', 111),
(1986, 'al-Mafraq', 111),
(1987, 'at-Tafilah', 111),
(1988, 'az-Zarqa\'', 111),
(1989, 'Akmecet', 112),
(1990, 'Akmola', 112),
(1991, 'Aktobe', 112),
(1992, 'Almati', 112),
(1993, 'Atirau', 112),
(1994, 'Batis Kazakstan', 112),
(1995, 'Burlinsky Region', 112),
(1996, 'Karagandi', 112),
(1997, 'Kostanay', 112),
(1998, 'Mankistau', 112),
(1999, 'Ontustik Kazakstan', 112),
(2000, 'Pavlodar', 112),
(2001, 'Sigis Kazakstan', 112),
(2002, 'Soltustik Kazakstan', 112),
(2003, 'Taraz', 112),
(2004, 'Central', 113),
(2005, 'Coast', 113),
(2006, 'Eastern', 113),
(2007, 'Nairobi', 113),
(2008, 'North Eastern', 113),
(2009, 'Nyanza', 113),
(2010, 'Rift Valley', 113),
(2011, 'Western', 113),
(2012, 'Abaiang', 114),
(2013, 'Abemana', 114),
(2014, 'Aranuka', 114),
(2015, 'Arorae', 114),
(2016, 'Banaba', 114),
(2017, 'Beru', 114),
(2018, 'Butaritari', 114),
(2019, 'Kiritimati', 114),
(2020, 'Kuria', 114),
(2021, 'Maiana', 114),
(2022, 'Makin', 114),
(2023, 'Marakei', 114),
(2024, 'Nikunau', 114),
(2025, 'Nonouti', 114),
(2026, 'Onotoa', 114),
(2027, 'Phoenix Islands', 114),
(2028, 'Tabiteuea North', 114),
(2029, 'Tabiteuea South', 114),
(2030, 'Tabuaeran', 114),
(2031, 'Tamana', 114),
(2032, 'Tarawa North', 114),
(2033, 'Tarawa South', 114),
(2034, 'Teraina', 114),
(2035, 'Chagangdo', 115),
(2036, 'Hamgyeongbukto', 115),
(2037, 'Hamgyeongnamdo', 115),
(2038, 'Hwanghaebukto', 115),
(2039, 'Hwanghaenamdo', 115),
(2040, 'Kaeseong', 115),
(2041, 'Kangweon', 115),
(2042, 'Nampo', 115),
(2043, 'Pyeonganbukto', 115),
(2044, 'Pyeongannamdo', 115),
(2045, 'Pyeongyang', 115),
(2046, 'Yanggang', 115),
(2047, 'Busan', 116),
(2048, 'Cheju', 116),
(2049, 'Chollabuk', 116),
(2050, 'Chollanam', 116),
(2051, 'Chungbuk', 116),
(2052, 'Chungcheongbuk', 116),
(2053, 'Chungcheongnam', 116),
(2054, 'Chungnam', 116),
(2055, 'Daegu', 116),
(2056, 'Gangwon-do', 116),
(2057, 'Goyang-si', 116),
(2058, 'Gyeonggi-do', 116),
(2059, 'Gyeongsang ', 116),
(2060, 'Gyeongsangnam-do', 116),
(2061, 'Incheon', 116),
(2062, 'Jeju-Si', 116),
(2063, 'Jeonbuk', 116),
(2064, 'Kangweon', 116),
(2065, 'Kwangju', 116),
(2066, 'Kyeonggi', 116),
(2067, 'Kyeongsangbuk', 116),
(2068, 'Kyeongsangnam', 116),
(2069, 'Kyonggi-do', 116),
(2070, 'Kyungbuk-Do', 116),
(2071, 'Kyunggi-Do', 116),
(2072, 'Kyunggi-do', 116),
(2073, 'Pusan', 116),
(2074, 'Seoul', 116),
(2075, 'Sudogwon', 116),
(2076, 'Taegu', 116),
(2077, 'Taejeon', 116),
(2078, 'Taejon-gwangyoksi', 116),
(2079, 'Ulsan', 116),
(2080, 'Wonju', 116),
(2081, 'gwangyoksi', 116),
(2082, 'Al Asimah', 117),
(2083, 'Hawalli', 117),
(2084, 'Mishref', 117),
(2085, 'Qadesiya', 117),
(2086, 'Safat', 117),
(2087, 'Salmiya', 117),
(2088, 'al-Ahmadi', 117),
(2089, 'al-Farwaniyah', 117),
(2090, 'al-Jahra', 117),
(2091, 'al-Kuwayt', 117),
(2092, 'Batken', 118),
(2093, 'Bishkek', 118),
(2094, 'Chui', 118),
(2095, 'Issyk-Kul', 118),
(2096, 'Jalal-Abad', 118),
(2097, 'Naryn', 118),
(2098, 'Osh', 118),
(2099, 'Talas', 118),
(2100, 'Attopu', 119),
(2101, 'Bokeo', 119),
(2102, 'Bolikhamsay', 119),
(2103, 'Champasak', 119),
(2104, 'Houaphanh', 119),
(2105, 'Khammouane', 119),
(2106, 'Luang Nam Tha', 119),
(2107, 'Luang Prabang', 119),
(2108, 'Oudomxay', 119),
(2109, 'Phongsaly', 119),
(2110, 'Saravan', 119),
(2111, 'Savannakhet', 119),
(2112, 'Sekong', 119),
(2113, 'Viangchan Prefecture', 119),
(2114, 'Viangchan Province', 119),
(2115, 'Xaignabury', 119),
(2116, 'Xiang Khuang', 119),
(2117, 'Aizkraukles', 120),
(2118, 'Aluksnes', 120),
(2119, 'Balvu', 120),
(2120, 'Bauskas', 120),
(2121, 'Cesu', 120),
(2122, 'Daugavpils', 120),
(2123, 'Daugavpils City', 120),
(2124, 'Dobeles', 120),
(2125, 'Gulbenes', 120),
(2126, 'Jekabspils', 120),
(2127, 'Jelgava', 120),
(2128, 'Jelgavas', 120),
(2129, 'Jurmala City', 120),
(2130, 'Kraslavas', 120),
(2131, 'Kuldigas', 120),
(2132, 'Liepaja', 120),
(2133, 'Liepajas', 120),
(2134, 'Limbazhu', 120),
(2135, 'Ludzas', 120),
(2136, 'Madonas', 120),
(2137, 'Ogres', 120),
(2138, 'Preilu', 120),
(2139, 'Rezekne', 120),
(2140, 'Rezeknes', 120),
(2141, 'Riga', 120),
(2142, 'Rigas', 120),
(2143, 'Saldus', 120),
(2144, 'Talsu', 120),
(2145, 'Tukuma', 120),
(2146, 'Valkas', 120),
(2147, 'Valmieras', 120),
(2148, 'Ventspils', 120),
(2149, 'Ventspils City', 120),
(2150, 'Beirut', 121),
(2151, 'Jabal Lubnan', 121),
(2152, 'Mohafazat Liban-Nord', 121),
(2153, 'Mohafazat Mont-Liban', 121),
(2154, 'Sidon', 121),
(2155, 'al-Biqa', 121),
(2156, 'al-Janub', 121),
(2157, 'an-Nabatiyah', 121),
(2158, 'ash-Shamal', 121),
(2159, 'Berea', 122),
(2160, 'Butha-Buthe', 122),
(2161, 'Leribe', 122),
(2162, 'Mafeteng', 122),
(2163, 'Maseru', 122),
(2164, 'Mohale\'s Hoek', 122),
(2165, 'Mokhotlong', 122),
(2166, 'Qacha\'s Nek', 122),
(2167, 'Quthing', 122),
(2168, 'Thaba-Tseka', 122),
(2169, 'Bomi', 123),
(2170, 'Bong', 123),
(2171, 'Grand Bassa', 123),
(2172, 'Grand Cape Mount', 123),
(2173, 'Grand Gedeh', 123),
(2174, 'Loffa', 123),
(2175, 'Margibi', 123),
(2176, 'Maryland and Grand Kru', 123),
(2177, 'Montserrado', 123),
(2178, 'Nimba', 123),
(2179, 'Rivercess', 123),
(2180, 'Sinoe', 123),
(2181, 'Ajdabiya', 124);
INSERT INTO `states` (`id`, `name`, `country_id`) VALUES
(2182, 'Fezzan', 124),
(2183, 'Banghazi', 124),
(2184, 'Darnah', 124),
(2185, 'Ghadamis', 124),
(2186, 'Gharyan', 124),
(2187, 'Misratah', 124),
(2188, 'Murzuq', 124),
(2189, 'Sabha', 124),
(2190, 'Sawfajjin', 124),
(2191, 'Surt', 124),
(2192, 'Tarabulus', 124),
(2193, 'Tarhunah', 124),
(2194, 'Tripolitania', 124),
(2195, 'Tubruq', 124),
(2196, 'Yafran', 124),
(2197, 'Zlitan', 124),
(2198, 'al-\'Aziziyah', 124),
(2199, 'al-Fatih', 124),
(2200, 'al-Jabal al Akhdar', 124),
(2201, 'al-Jufrah', 124),
(2202, 'al-Khums', 124),
(2203, 'al-Kufrah', 124),
(2204, 'an-Nuqat al-Khams', 124),
(2205, 'ash-Shati\'', 124),
(2206, 'az-Zawiyah', 124),
(2207, 'Balzers', 125),
(2208, 'Eschen', 125),
(2209, 'Gamprin', 125),
(2210, 'Mauren', 125),
(2211, 'Planken', 125),
(2212, 'Ruggell', 125),
(2213, 'Schaan', 125),
(2214, 'Schellenberg', 125),
(2215, 'Triesen', 125),
(2216, 'Triesenberg', 125),
(2217, 'Vaduz', 125),
(2218, 'Alytaus', 126),
(2219, 'Anyksciai', 126),
(2220, 'Kauno', 126),
(2221, 'Klaipedos', 126),
(2222, 'Marijampoles', 126),
(2223, 'Panevezhio', 126),
(2224, 'Panevezys', 126),
(2225, 'Shiauliu', 126),
(2226, 'Taurages', 126),
(2227, 'Telshiu', 126),
(2228, 'Telsiai', 126),
(2229, 'Utenos', 126),
(2230, 'Vilniaus', 126),
(2231, 'Capellen', 127),
(2232, 'Clervaux', 127),
(2233, 'Diekirch', 127),
(2234, 'Echternach', 127),
(2235, 'Esch-sur-Alzette', 127),
(2236, 'Grevenmacher', 127),
(2237, 'Luxembourg', 127),
(2238, 'Mersch', 127),
(2239, 'Redange', 127),
(2240, 'Remich', 127),
(2241, 'Vianden', 127),
(2242, 'Wiltz', 127),
(2243, 'Macau', 128),
(2244, 'Berovo', 129),
(2245, 'Bitola', 129),
(2246, 'Brod', 129),
(2247, 'Debar', 129),
(2248, 'Delchevo', 129),
(2249, 'Demir Hisar', 129),
(2250, 'Gevgelija', 129),
(2251, 'Gostivar', 129),
(2252, 'Kavadarci', 129),
(2253, 'Kichevo', 129),
(2254, 'Kochani', 129),
(2255, 'Kratovo', 129),
(2256, 'Kriva Palanka', 129),
(2257, 'Krushevo', 129),
(2258, 'Kumanovo', 129),
(2259, 'Negotino', 129),
(2260, 'Ohrid', 129),
(2261, 'Prilep', 129),
(2262, 'Probishtip', 129),
(2263, 'Radovish', 129),
(2264, 'Resen', 129),
(2265, 'Shtip', 129),
(2266, 'Skopje', 129),
(2267, 'Struga', 129),
(2268, 'Strumica', 129),
(2269, 'Sveti Nikole', 129),
(2270, 'Tetovo', 129),
(2271, 'Valandovo', 129),
(2272, 'Veles', 129),
(2273, 'Vinica', 129),
(2274, 'Antananarivo', 130),
(2275, 'Antsiranana', 130),
(2276, 'Fianarantsoa', 130),
(2277, 'Mahajanga', 130),
(2278, 'Toamasina', 130),
(2279, 'Toliary', 130),
(2280, 'Balaka', 131),
(2281, 'Blantyre City', 131),
(2282, 'Chikwawa', 131),
(2283, 'Chiradzulu', 131),
(2284, 'Chitipa', 131),
(2285, 'Dedza', 131),
(2286, 'Dowa', 131),
(2287, 'Karonga', 131),
(2288, 'Kasungu', 131),
(2289, 'Lilongwe City', 131),
(2290, 'Machinga', 131),
(2291, 'Mangochi', 131),
(2292, 'Mchinji', 131),
(2293, 'Mulanje', 131),
(2294, 'Mwanza', 131),
(2295, 'Mzimba', 131),
(2296, 'Mzuzu City', 131),
(2297, 'Nkhata Bay', 131),
(2298, 'Nkhotakota', 131),
(2299, 'Nsanje', 131),
(2300, 'Ntcheu', 131),
(2301, 'Ntchisi', 131),
(2302, 'Phalombe', 131),
(2303, 'Rumphi', 131),
(2304, 'Salima', 131),
(2305, 'Thyolo', 131),
(2306, 'Zomba Municipality', 131),
(2307, 'Johor', 132),
(2308, 'Kedah', 132),
(2309, 'Kelantan', 132),
(2310, 'Kuala Lumpur', 132),
(2311, 'Labuan', 132),
(2312, 'Melaka', 132),
(2313, 'Negeri Johor', 132),
(2314, 'Negeri Sembilan', 132),
(2315, 'Pahang', 132),
(2316, 'Penang', 132),
(2317, 'Perak', 132),
(2318, 'Perlis', 132),
(2319, 'Pulau Pinang', 132),
(2320, 'Sabah', 132),
(2321, 'Sarawak', 132),
(2322, 'Selangor', 132),
(2323, 'Sembilan', 132),
(2324, 'Terengganu', 132),
(2325, 'Alif Alif', 133),
(2326, 'Alif Dhaal', 133),
(2327, 'Baa', 133),
(2328, 'Dhaal', 133),
(2329, 'Faaf', 133),
(2330, 'Gaaf Alif', 133),
(2331, 'Gaaf Dhaal', 133),
(2332, 'Ghaviyani', 133),
(2333, 'Haa Alif', 133),
(2334, 'Haa Dhaal', 133),
(2335, 'Kaaf', 133),
(2336, 'Laam', 133),
(2337, 'Lhaviyani', 133),
(2338, 'Male', 133),
(2339, 'Miim', 133),
(2340, 'Nuun', 133),
(2341, 'Raa', 133),
(2342, 'Shaviyani', 133),
(2343, 'Siin', 133),
(2344, 'Thaa', 133),
(2345, 'Vaav', 133),
(2346, 'Bamako', 134),
(2347, 'Gao', 134),
(2348, 'Kayes', 134),
(2349, 'Kidal', 134),
(2350, 'Koulikoro', 134),
(2351, 'Mopti', 134),
(2352, 'Segou', 134),
(2353, 'Sikasso', 134),
(2354, 'Tombouctou', 134),
(2355, 'Gozo and Comino', 135),
(2356, 'Inner Harbour', 135),
(2357, 'Northern', 135),
(2358, 'Outer Harbour', 135),
(2359, 'South Eastern', 135),
(2360, 'Valletta', 135),
(2361, 'Western', 135),
(2362, 'Castletown', 136),
(2363, 'Douglas', 136),
(2364, 'Laxey', 136),
(2365, 'Onchan', 136),
(2366, 'Peel', 136),
(2367, 'Port Erin', 136),
(2368, 'Port Saint Mary', 136),
(2369, 'Ramsey', 136),
(2370, 'Ailinlaplap', 137),
(2371, 'Ailuk', 137),
(2372, 'Arno', 137),
(2373, 'Aur', 137),
(2374, 'Bikini', 137),
(2375, 'Ebon', 137),
(2376, 'Enewetak', 137),
(2377, 'Jabat', 137),
(2378, 'Jaluit', 137),
(2379, 'Kili', 137),
(2380, 'Kwajalein', 137),
(2381, 'Lae', 137),
(2382, 'Lib', 137),
(2383, 'Likiep', 137),
(2384, 'Majuro', 137),
(2385, 'Maloelap', 137),
(2386, 'Mejit', 137),
(2387, 'Mili', 137),
(2388, 'Namorik', 137),
(2389, 'Namu', 137),
(2390, 'Rongelap', 137),
(2391, 'Ujae', 137),
(2392, 'Utrik', 137),
(2393, 'Wotho', 137),
(2394, 'Wotje', 137),
(2395, 'Fort-de-France', 138),
(2396, 'La Trinite', 138),
(2397, 'Le Marin', 138),
(2398, 'Saint-Pierre', 138),
(2399, 'Adrar', 139),
(2400, 'Assaba', 139),
(2401, 'Brakna', 139),
(2402, 'Dhakhlat Nawadibu', 139),
(2403, 'Hudh-al-Gharbi', 139),
(2404, 'Hudh-ash-Sharqi', 139),
(2405, 'Inshiri', 139),
(2406, 'Nawakshut', 139),
(2407, 'Qidimagha', 139),
(2408, 'Qurqul', 139),
(2409, 'Taqant', 139),
(2410, 'Tiris Zammur', 139),
(2411, 'Trarza', 139),
(2412, 'Black River', 140),
(2413, 'Eau Coulee', 140),
(2414, 'Flacq', 140),
(2415, 'Floreal', 140),
(2416, 'Grand Port', 140),
(2417, 'Moka', 140),
(2418, 'Pamplempousses', 140),
(2419, 'Plaines Wilhelm', 140),
(2420, 'Port Louis', 140),
(2421, 'Riviere du Rempart', 140),
(2422, 'Rodrigues', 140),
(2423, 'Rose Hill', 140),
(2424, 'Savanne', 140),
(2425, 'Mayotte', 141),
(2426, 'Pamanzi', 141),
(2427, 'Aguascalientes', 142),
(2428, 'Baja California', 142),
(2429, 'Baja California Sur', 142),
(2430, 'Campeche', 142),
(2431, 'Chiapas', 142),
(2432, 'Chihuahua', 142),
(2433, 'Coahuila', 142),
(2434, 'Colima', 142),
(2435, 'Distrito Federal', 142),
(2436, 'Durango', 142),
(2437, 'Estado de Mexico', 142),
(2438, 'Guanajuato', 142),
(2439, 'Guerrero', 142),
(2440, 'Hidalgo', 142),
(2441, 'Jalisco', 142),
(2442, 'Mexico', 142),
(2443, 'Michoacan', 142),
(2444, 'Morelos', 142),
(2445, 'Nayarit', 142),
(2446, 'Nuevo Leon', 142),
(2447, 'Oaxaca', 142),
(2448, 'Puebla', 142),
(2449, 'Queretaro', 142),
(2450, 'Quintana Roo', 142),
(2451, 'San Luis Potosi', 142),
(2452, 'Sinaloa', 142),
(2453, 'Sonora', 142),
(2454, 'Tabasco', 142),
(2455, 'Tamaulipas', 142),
(2456, 'Tlaxcala', 142),
(2457, 'Veracruz', 142),
(2458, 'Yucatan', 142),
(2459, 'Zacatecas', 142),
(2460, 'Chuuk', 143),
(2461, 'Kusaie', 143),
(2462, 'Pohnpei', 143),
(2463, 'Yap', 143),
(2464, 'Balti', 144),
(2465, 'Cahul', 144),
(2466, 'Chisinau', 144),
(2467, 'Chisinau Oras', 144),
(2468, 'Edinet', 144),
(2469, 'Gagauzia', 144),
(2470, 'Lapusna', 144),
(2471, 'Orhei', 144),
(2472, 'Soroca', 144),
(2473, 'Taraclia', 144),
(2474, 'Tighina', 144),
(2475, 'Transnistria', 144),
(2476, 'Ungheni', 144),
(2477, 'Fontvieille', 145),
(2478, 'La Condamine', 145),
(2479, 'Monaco-Ville', 145),
(2480, 'Monte Carlo', 145),
(2481, 'Arhangaj', 146),
(2482, 'Bajan-Olgij', 146),
(2483, 'Bajanhongor', 146),
(2484, 'Bulgan', 146),
(2485, 'Darhan-Uul', 146),
(2486, 'Dornod', 146),
(2487, 'Dornogovi', 146),
(2488, 'Dundgovi', 146),
(2489, 'Govi-Altaj', 146),
(2490, 'Govisumber', 146),
(2491, 'Hentij', 146),
(2492, 'Hovd', 146),
(2493, 'Hovsgol', 146),
(2494, 'Omnogovi', 146),
(2495, 'Orhon', 146),
(2496, 'Ovorhangaj', 146),
(2497, 'Selenge', 146),
(2498, 'Suhbaatar', 146),
(2499, 'Tov', 146),
(2500, 'Ulaanbaatar', 146),
(2501, 'Uvs', 146),
(2502, 'Zavhan', 146),
(2503, 'Montserrat', 147),
(2504, 'Agadir', 148),
(2505, 'Casablanca', 148),
(2506, 'Chaouia-Ouardigha', 148),
(2507, 'Doukkala-Abda', 148),
(2508, 'Fes-Boulemane', 148),
(2509, 'Gharb-Chrarda-Beni Hssen', 148),
(2510, 'Guelmim', 148),
(2511, 'Kenitra', 148),
(2512, 'Marrakech-Tensift-Al Haouz', 148),
(2513, 'Meknes-Tafilalet', 148),
(2514, 'Oriental', 148),
(2515, 'Oujda', 148),
(2516, 'Province de Tanger', 148),
(2517, 'Rabat-Sale-Zammour-Zaer', 148),
(2518, 'Sala Al Jadida', 148),
(2519, 'Settat', 148),
(2520, 'Souss Massa-Draa', 148),
(2521, 'Tadla-Azilal', 148),
(2522, 'Tangier-Tetouan', 148),
(2523, 'Taza-Al Hoceima-Taounate', 148),
(2524, 'Wilaya de Casablanca', 148),
(2525, 'Wilaya de Rabat-Sale', 148),
(2526, 'Cabo Delgado', 149),
(2527, 'Gaza', 149),
(2528, 'Inhambane', 149),
(2529, 'Manica', 149),
(2530, 'Maputo', 149),
(2531, 'Maputo Provincia', 149),
(2532, 'Nampula', 149),
(2533, 'Niassa', 149),
(2534, 'Sofala', 149),
(2535, 'Tete', 149),
(2536, 'Zambezia', 149),
(2537, 'Ayeyarwady', 150),
(2538, 'Bago', 150),
(2539, 'Chin', 150),
(2540, 'Kachin', 150),
(2541, 'Kayah', 150),
(2542, 'Kayin', 150),
(2543, 'Magway', 150),
(2544, 'Mandalay', 150),
(2545, 'Mon', 150),
(2546, 'Nay Pyi Taw', 150),
(2547, 'Rakhine', 150),
(2548, 'Sagaing', 150),
(2549, 'Shan', 150),
(2550, 'Tanintharyi', 150),
(2551, 'Yangon', 150),
(2552, 'Caprivi', 151),
(2553, 'Erongo', 151),
(2554, 'Hardap', 151),
(2555, 'Karas', 151),
(2556, 'Kavango', 151),
(2557, 'Khomas', 151),
(2558, 'Kunene', 151),
(2559, 'Ohangwena', 151),
(2560, 'Omaheke', 151),
(2561, 'Omusati', 151),
(2562, 'Oshana', 151),
(2563, 'Oshikoto', 151),
(2564, 'Otjozondjupa', 151),
(2565, 'Yaren', 152),
(2566, 'Bagmati', 153),
(2567, 'Bheri', 153),
(2568, 'Dhawalagiri', 153),
(2569, 'Gandaki', 153),
(2570, 'Janakpur', 153),
(2571, 'Karnali', 153),
(2572, 'Koshi', 153),
(2573, 'Lumbini', 153),
(2574, 'Mahakali', 153),
(2575, 'Mechi', 153),
(2576, 'Narayani', 153),
(2577, 'Rapti', 153),
(2578, 'Sagarmatha', 153),
(2579, 'Seti', 153),
(2580, 'Bonaire', 154),
(2581, 'Curacao', 154),
(2582, 'Saba', 154),
(2583, 'Sint Eustatius', 154),
(2584, 'Sint Maarten', 154),
(2585, 'Amsterdam', 155),
(2586, 'Benelux', 155),
(2587, 'Drenthe', 155),
(2588, 'Flevoland', 155),
(2589, 'Friesland', 155),
(2590, 'Gelderland', 155),
(2591, 'Groningen', 155),
(2592, 'Limburg', 155),
(2593, 'Noord-Brabant', 155),
(2594, 'Noord-Holland', 155),
(2595, 'Overijssel', 155),
(2596, 'South Holland', 155),
(2597, 'Utrecht', 155),
(2598, 'Zeeland', 155),
(2599, 'Zuid-Holland', 155),
(2600, 'Iles', 156),
(2601, 'Nord', 156),
(2602, 'Sud', 156),
(2603, 'Area Outside Region', 157),
(2604, 'Auckland', 157),
(2605, 'Bay of Plenty', 157),
(2606, 'Canterbury', 157),
(2607, 'Christchurch', 157),
(2608, 'Gisborne', 157),
(2609, 'Hawke\'s Bay', 157),
(2610, 'Manawatu-Wanganui', 157),
(2611, 'Marlborough', 157),
(2612, 'Nelson', 157),
(2613, 'Northland', 157),
(2614, 'Otago', 157),
(2615, 'Rodney', 157),
(2616, 'Southland', 157),
(2617, 'Taranaki', 157),
(2618, 'Tasman', 157),
(2619, 'Waikato', 157),
(2620, 'Wellington', 157),
(2621, 'West Coast', 157),
(2622, 'Atlantico Norte', 158),
(2623, 'Atlantico Sur', 158),
(2624, 'Boaco', 158),
(2625, 'Carazo', 158),
(2626, 'Chinandega', 158),
(2627, 'Chontales', 158),
(2628, 'Esteli', 158),
(2629, 'Granada', 158),
(2630, 'Jinotega', 158),
(2631, 'Leon', 158),
(2632, 'Madriz', 158),
(2633, 'Managua', 158),
(2634, 'Masaya', 158),
(2635, 'Matagalpa', 158),
(2636, 'Nueva Segovia', 158),
(2637, 'Rio San Juan', 158),
(2638, 'Rivas', 158),
(2639, 'Agadez', 159),
(2640, 'Diffa', 159),
(2641, 'Dosso', 159),
(2642, 'Maradi', 159),
(2643, 'Niamey', 159),
(2644, 'Tahoua', 159),
(2645, 'Tillabery', 159),
(2646, 'Zinder', 159),
(2647, 'Abia', 160),
(2648, 'Abuja Federal Capital Territor', 160),
(2649, 'Adamawa', 160),
(2650, 'Akwa Ibom', 160),
(2651, 'Anambra', 160),
(2652, 'Bauchi', 160),
(2653, 'Bayelsa', 160),
(2654, 'Benue', 160),
(2655, 'Borno', 160),
(2656, 'Cross River', 160),
(2657, 'Delta', 160),
(2658, 'Ebonyi', 160),
(2659, 'Edo', 160),
(2660, 'Ekiti', 160),
(2661, 'Enugu', 160),
(2662, 'Gombe', 160),
(2663, 'Imo', 160),
(2664, 'Jigawa', 160),
(2665, 'Kaduna', 160),
(2666, 'Kano', 160),
(2667, 'Katsina', 160),
(2668, 'Kebbi', 160),
(2669, 'Kogi', 160),
(2670, 'Kwara', 160),
(2671, 'Lagos', 160),
(2672, 'Nassarawa', 160),
(2673, 'Niger', 160),
(2674, 'Ogun', 160),
(2675, 'Ondo', 160),
(2676, 'Osun', 160),
(2677, 'Oyo', 160),
(2678, 'Plateau', 160),
(2679, 'Rivers', 160),
(2680, 'Sokoto', 160),
(2681, 'Taraba', 160),
(2682, 'Yobe', 160),
(2683, 'Zamfara', 160),
(2684, 'Niue', 161),
(2685, 'Norfolk Island', 162),
(2686, 'Northern Islands', 163),
(2687, 'Rota', 163),
(2688, 'Saipan', 163),
(2689, 'Tinian', 163),
(2690, 'Akershus', 164),
(2691, 'Aust Agder', 164),
(2692, 'Bergen', 164),
(2693, 'Buskerud', 164),
(2694, 'Finnmark', 164),
(2695, 'Hedmark', 164),
(2696, 'Hordaland', 164),
(2697, 'Moere og Romsdal', 164),
(2698, 'Nord Trondelag', 164),
(2699, 'Nordland', 164),
(2700, 'Oestfold', 164),
(2701, 'Oppland', 164),
(2702, 'Oslo', 164),
(2703, 'Rogaland', 164),
(2704, 'Soer Troendelag', 164),
(2705, 'Sogn og Fjordane', 164),
(2706, 'Stavern', 164),
(2707, 'Sykkylven', 164),
(2708, 'Telemark', 164),
(2709, 'Troms', 164),
(2710, 'Vest Agder', 164),
(2711, 'Vestfold', 164),
(2712, 'ÃƒÆ’Ã‚Ëœstfold', 164),
(2713, 'Al Buraimi', 165),
(2714, 'Dhufar', 165),
(2715, 'Masqat', 165),
(2716, 'Musandam', 165),
(2717, 'Rusayl', 165),
(2718, 'Wadi Kabir', 165),
(2719, 'ad-Dakhiliyah', 165),
(2720, 'adh-Dhahirah', 165),
(2721, 'al-Batinah', 165),
(2722, 'ash-Sharqiyah', 165),
(2723, 'Baluchistan', 166),
(2724, 'Federal Capital Area', 166),
(2725, 'Federally administered Tribal ', 166),
(2726, 'North-West Frontier', 166),
(2727, 'Northern Areas', 166),
(2728, 'Punjab', 166),
(2729, 'Sind', 166),
(2730, 'Aimeliik', 167),
(2731, 'Airai', 167),
(2732, 'Angaur', 167),
(2733, 'Hatobohei', 167),
(2734, 'Kayangel', 167),
(2735, 'Koror', 167),
(2736, 'Melekeok', 167),
(2737, 'Ngaraard', 167),
(2738, 'Ngardmau', 167),
(2739, 'Ngaremlengui', 167),
(2740, 'Ngatpang', 167),
(2741, 'Ngchesar', 167),
(2742, 'Ngerchelong', 167),
(2743, 'Ngiwal', 167),
(2744, 'Peleliu', 167),
(2745, 'Sonsorol', 167),
(2746, 'Ariha', 168),
(2747, 'Bayt Lahm', 168),
(2748, 'Bethlehem', 168),
(2749, 'Dayr-al-Balah', 168),
(2750, 'Ghazzah', 168),
(2751, 'Ghazzah ash-Shamaliyah', 168),
(2752, 'Janin', 168),
(2753, 'Khan Yunis', 168),
(2754, 'Nabulus', 168),
(2755, 'Qalqilyah', 168),
(2756, 'Rafah', 168),
(2757, 'Ram Allah wal-Birah', 168),
(2758, 'Salfit', 168),
(2759, 'Tubas', 168),
(2760, 'Tulkarm', 168),
(2761, 'al-Khalil', 168),
(2762, 'al-Quds', 168),
(2763, 'Bocas del Toro', 169),
(2764, 'Chiriqui', 169),
(2765, 'Cocle', 169),
(2766, 'Colon', 169),
(2767, 'Darien', 169),
(2768, 'Embera', 169),
(2769, 'Herrera', 169),
(2770, 'Kuna Yala', 169),
(2771, 'Los Santos', 169),
(2772, 'Ngobe Bugle', 169),
(2773, 'Panama', 169),
(2774, 'Veraguas', 169),
(2775, 'East New Britain', 170),
(2776, 'East Sepik', 170),
(2777, 'Eastern Highlands', 170),
(2778, 'Enga', 170),
(2779, 'Fly River', 170),
(2780, 'Gulf', 170),
(2781, 'Madang', 170),
(2782, 'Manus', 170),
(2783, 'Milne Bay', 170),
(2784, 'Morobe', 170),
(2785, 'National Capital District', 170),
(2786, 'New Ireland', 170),
(2787, 'North Solomons', 170),
(2788, 'Oro', 170),
(2789, 'Sandaun', 170),
(2790, 'Simbu', 170),
(2791, 'Southern Highlands', 170),
(2792, 'West New Britain', 170),
(2793, 'Western Highlands', 170),
(2794, 'Alto Paraguay', 171),
(2795, 'Alto Parana', 171),
(2796, 'Amambay', 171),
(2797, 'Asuncion', 171),
(2798, 'Boqueron', 171),
(2799, 'Caaguazu', 171),
(2800, 'Caazapa', 171),
(2801, 'Canendiyu', 171),
(2802, 'Central', 171),
(2803, 'Concepcion', 171),
(2804, 'Cordillera', 171),
(2805, 'Guaira', 171),
(2806, 'Itapua', 171),
(2807, 'Misiones', 171),
(2808, 'Neembucu', 171),
(2809, 'Paraguari', 171),
(2810, 'Presidente Hayes', 171),
(2811, 'San Pedro', 171),
(2812, 'Amazonas', 172),
(2813, 'Ancash', 172),
(2814, 'Apurimac', 172),
(2815, 'Arequipa', 172),
(2816, 'Ayacucho', 172),
(2817, 'Cajamarca', 172),
(2818, 'Cusco', 172),
(2819, 'Huancavelica', 172),
(2820, 'Huanuco', 172),
(2821, 'Ica', 172),
(2822, 'Junin', 172),
(2823, 'La Libertad', 172),
(2824, 'Lambayeque', 172),
(2825, 'Lima y Callao', 172),
(2826, 'Loreto', 172),
(2827, 'Madre de Dios', 172),
(2828, 'Moquegua', 172),
(2829, 'Pasco', 172),
(2830, 'Piura', 172),
(2831, 'Puno', 172),
(2832, 'San Martin', 172),
(2833, 'Tacna', 172),
(2834, 'Tumbes', 172),
(2835, 'Ucayali', 172),
(2836, 'Batangas', 173),
(2837, 'Bicol', 173),
(2838, 'Bulacan', 173),
(2839, 'Cagayan', 173),
(2840, 'Caraga', 173),
(2841, 'Central Luzon', 173),
(2842, 'Central Mindanao', 173),
(2843, 'Central Visayas', 173),
(2844, 'Cordillera', 173),
(2845, 'Davao', 173),
(2846, 'Eastern Visayas', 173),
(2847, 'Greater Metropolitan Area', 173),
(2848, 'Ilocos', 173),
(2849, 'Laguna', 173),
(2850, 'Luzon', 173),
(2851, 'Mactan', 173),
(2852, 'Metropolitan Manila Area', 173),
(2853, 'Muslim Mindanao', 173),
(2854, 'Northern Mindanao', 173),
(2855, 'Southern Mindanao', 173),
(2856, 'Southern Tagalog', 173),
(2857, 'Western Mindanao', 173),
(2858, 'Western Visayas', 173),
(2859, 'Pitcairn Island', 174),
(2860, 'Biale Blota', 175),
(2861, 'Dobroszyce', 175),
(2862, 'Dolnoslaskie', 175),
(2863, 'Dziekanow Lesny', 175),
(2864, 'Hopowo', 175),
(2865, 'Kartuzy', 175),
(2866, 'Koscian', 175),
(2867, 'Krakow', 175),
(2868, 'Kujawsko-Pomorskie', 175),
(2869, 'Lodzkie', 175),
(2870, 'Lubelskie', 175),
(2871, 'Lubuskie', 175),
(2872, 'Malomice', 175),
(2873, 'Malopolskie', 175),
(2874, 'Mazowieckie', 175),
(2875, 'Mirkow', 175),
(2876, 'Opolskie', 175),
(2877, 'Ostrowiec', 175),
(2878, 'Podkarpackie', 175),
(2879, 'Podlaskie', 175),
(2880, 'Polska', 175),
(2881, 'Pomorskie', 175),
(2882, 'Poznan', 175),
(2883, 'Pruszkow', 175),
(2884, 'Rymanowska', 175),
(2885, 'Rzeszow', 175),
(2886, 'Slaskie', 175),
(2887, 'Stare Pole', 175),
(2888, 'Swietokrzyskie', 175),
(2889, 'Warminsko-Mazurskie', 175),
(2890, 'Warsaw', 175),
(2891, 'Wejherowo', 175),
(2892, 'Wielkopolskie', 175),
(2893, 'Wroclaw', 175),
(2894, 'Zachodnio-Pomorskie', 175),
(2895, 'Zukowo', 175),
(2896, 'Abrantes', 176),
(2897, 'Acores', 176),
(2898, 'Alentejo', 176),
(2899, 'Algarve', 176),
(2900, 'Braga', 176),
(2901, 'Centro', 176),
(2902, 'Distrito de Leiria', 176),
(2903, 'Distrito de Viana do Castelo', 176),
(2904, 'Distrito de Vila Real', 176),
(2905, 'Distrito do Porto', 176),
(2906, 'Lisboa e Vale do Tejo', 176),
(2907, 'Madeira', 176),
(2908, 'Norte', 176),
(2909, 'Paivas', 176),
(2910, 'Arecibo', 177),
(2911, 'Bayamon', 177),
(2912, 'Carolina', 177),
(2913, 'Florida', 177),
(2914, 'Guayama', 177),
(2915, 'Humacao', 177),
(2916, 'Mayaguez-Aguadilla', 177),
(2917, 'Ponce', 177),
(2918, 'Salinas', 177),
(2919, 'San Juan', 177),
(2920, 'Doha', 178),
(2921, 'Jarian-al-Batnah', 178),
(2922, 'Umm Salal', 178),
(2923, 'ad-Dawhah', 178),
(2924, 'al-Ghuwayriyah', 178),
(2925, 'al-Jumayliyah', 178),
(2926, 'al-Khawr', 178),
(2927, 'al-Wakrah', 178),
(2928, 'ar-Rayyan', 178),
(2929, 'ash-Shamal', 178),
(2930, 'Saint-Benoit', 179),
(2931, 'Saint-Denis', 179),
(2932, 'Saint-Paul', 179),
(2933, 'Saint-Pierre', 179),
(2934, 'Alba', 180),
(2935, 'Arad', 180),
(2936, 'Arges', 180),
(2937, 'Bacau', 180),
(2938, 'Bihor', 180),
(2939, 'Bistrita-Nasaud', 180),
(2940, 'Botosani', 180),
(2941, 'Braila', 180),
(2942, 'Brasov', 180),
(2943, 'Bucuresti', 180),
(2944, 'Buzau', 180),
(2945, 'Calarasi', 180),
(2946, 'Caras-Severin', 180),
(2947, 'Cluj', 180),
(2948, 'Constanta', 180),
(2949, 'Covasna', 180),
(2950, 'Dambovita', 180),
(2951, 'Dolj', 180),
(2952, 'Galati', 180),
(2953, 'Giurgiu', 180),
(2954, 'Gorj', 180),
(2955, 'Harghita', 180),
(2956, 'Hunedoara', 180),
(2957, 'Ialomita', 180),
(2958, 'Iasi', 180),
(2959, 'Ilfov', 180),
(2960, 'Maramures', 180),
(2961, 'Mehedinti', 180),
(2962, 'Mures', 180),
(2963, 'Neamt', 180),
(2964, 'Olt', 180),
(2965, 'Prahova', 180),
(2966, 'Salaj', 180),
(2967, 'Satu Mare', 180),
(2968, 'Sibiu', 180),
(2969, 'Sondelor', 180),
(2970, 'Suceava', 180),
(2971, 'Teleorman', 180),
(2972, 'Timis', 180),
(2973, 'Tulcea', 180),
(2974, 'Valcea', 180),
(2975, 'Vaslui', 180),
(2976, 'Vrancea', 180),
(2977, 'Adygeja', 181),
(2978, 'Aga', 181),
(2979, 'Alanija', 181),
(2980, 'Altaj', 181),
(2981, 'Amur', 181),
(2982, 'Arhangelsk', 181),
(2983, 'Astrahan', 181),
(2984, 'Bashkortostan', 181),
(2985, 'Belgorod', 181),
(2986, 'Brjansk', 181),
(2987, 'Burjatija', 181),
(2988, 'Chechenija', 181),
(2989, 'Cheljabinsk', 181),
(2990, 'Chita', 181),
(2991, 'Chukotka', 181),
(2992, 'Chuvashija', 181),
(2993, 'Dagestan', 181),
(2994, 'Evenkija', 181),
(2995, 'Gorno-Altaj', 181),
(2996, 'Habarovsk', 181),
(2997, 'Hakasija', 181),
(2998, 'Hanty-Mansija', 181),
(2999, 'Ingusetija', 181),
(3000, 'Irkutsk', 181),
(3001, 'Ivanovo', 181),
(3002, 'Jamalo-Nenets', 181),
(3003, 'Jaroslavl', 181),
(3004, 'Jevrej', 181),
(3005, 'Kabardino-Balkarija', 181),
(3006, 'Kaliningrad', 181),
(3007, 'Kalmykija', 181),
(3008, 'Kaluga', 181),
(3009, 'Kamchatka', 181),
(3010, 'Karachaj-Cherkessija', 181),
(3011, 'Karelija', 181),
(3012, 'Kemerovo', 181),
(3013, 'Khabarovskiy Kray', 181),
(3014, 'Kirov', 181),
(3015, 'Komi', 181),
(3016, 'Komi-Permjakija', 181),
(3017, 'Korjakija', 181),
(3018, 'Kostroma', 181),
(3019, 'Krasnodar', 181),
(3020, 'Krasnojarsk', 181),
(3021, 'Krasnoyarskiy Kray', 181),
(3022, 'Kurgan', 181),
(3023, 'Kursk', 181),
(3024, 'Leningrad', 181),
(3025, 'Lipeck', 181),
(3026, 'Magadan', 181),
(3027, 'Marij El', 181),
(3028, 'Mordovija', 181),
(3029, 'Moscow', 181),
(3030, 'Moskovskaja Oblast', 181),
(3031, 'Moskovskaya Oblast', 181),
(3032, 'Moskva', 181),
(3033, 'Murmansk', 181),
(3034, 'Nenets', 181),
(3035, 'Nizhnij Novgorod', 181),
(3036, 'Novgorod', 181),
(3037, 'Novokusnezk', 181),
(3038, 'Novosibirsk', 181),
(3039, 'Omsk', 181),
(3040, 'Orenburg', 181),
(3041, 'Orjol', 181),
(3042, 'Penza', 181),
(3043, 'Perm', 181),
(3044, 'Primorje', 181),
(3045, 'Pskov', 181),
(3046, 'Pskovskaya Oblast', 181),
(3047, 'Rjazan', 181),
(3048, 'Rostov', 181),
(3049, 'Saha', 181),
(3050, 'Sahalin', 181),
(3051, 'Samara', 181),
(3052, 'Samarskaya', 181),
(3053, 'Sankt-Peterburg', 181),
(3054, 'Saratov', 181),
(3055, 'Smolensk', 181),
(3056, 'Stavropol', 181),
(3057, 'Sverdlovsk', 181),
(3058, 'Tajmyrija', 181),
(3059, 'Tambov', 181),
(3060, 'Tatarstan', 181),
(3061, 'Tjumen', 181),
(3062, 'Tomsk', 181),
(3063, 'Tula', 181),
(3064, 'Tver', 181),
(3065, 'Tyva', 181),
(3066, 'Udmurtija', 181),
(3067, 'Uljanovsk', 181),
(3068, 'Ulyanovskaya Oblast', 181),
(3069, 'Ust-Orda', 181),
(3070, 'Vladimir', 181),
(3071, 'Volgograd', 181),
(3072, 'Vologda', 181),
(3073, 'Voronezh', 181),
(3074, 'Butare', 182),
(3075, 'Byumba', 182),
(3076, 'Cyangugu', 182),
(3077, 'Gikongoro', 182),
(3078, 'Gisenyi', 182),
(3079, 'Gitarama', 182),
(3080, 'Kibungo', 182),
(3081, 'Kibuye', 182),
(3082, 'Kigali-ngali', 182),
(3083, 'Ruhengeri', 182),
(3084, 'Ascension', 183),
(3085, 'Gough Island', 183),
(3086, 'Saint Helena', 183),
(3087, 'Tristan da Cunha', 183),
(3088, 'Christ Church Nichola Town', 184),
(3089, 'Saint Anne Sandy Point', 184),
(3090, 'Saint George Basseterre', 184),
(3091, 'Saint George Gingerland', 184),
(3092, 'Saint James Windward', 184),
(3093, 'Saint John Capesterre', 184),
(3094, 'Saint John Figtree', 184),
(3095, 'Saint Mary Cayon', 184),
(3096, 'Saint Paul Capesterre', 184),
(3097, 'Saint Paul Charlestown', 184),
(3098, 'Saint Peter Basseterre', 184),
(3099, 'Saint Thomas Lowland', 184),
(3100, 'Saint Thomas Middle Island', 184),
(3101, 'Trinity Palmetto Point', 184),
(3102, 'Anse-la-Raye', 185),
(3103, 'Canaries', 185),
(3104, 'Castries', 185),
(3105, 'Choiseul', 185),
(3106, 'Dennery', 185),
(3107, 'Gros Inlet', 185),
(3108, 'Laborie', 185),
(3109, 'Micoud', 185),
(3110, 'Soufriere', 185),
(3111, 'Vieux Fort', 185),
(3112, 'Miquelon-Langlade', 186),
(3113, 'Saint-Pierre', 186),
(3114, 'Charlotte', 187),
(3115, 'Grenadines', 187),
(3116, 'Saint Andrew', 187),
(3117, 'Saint David', 187),
(3118, 'Saint George', 187),
(3119, 'Saint Patrick', 187),
(3120, 'A\'ana', 188),
(3121, 'Aiga-i-le-Tai', 188),
(3122, 'Atua', 188),
(3123, 'Fa\'asaleleaga', 188),
(3124, 'Gaga\'emauga', 188),
(3125, 'Gagaifomauga', 188),
(3126, 'Palauli', 188),
(3127, 'Satupa\'itea', 188),
(3128, 'Tuamasaga', 188),
(3129, 'Va\'a-o-Fonoti', 188),
(3130, 'Vaisigano', 188),
(3131, 'Acquaviva', 189),
(3132, 'Borgo Maggiore', 189),
(3133, 'Chiesanuova', 189),
(3134, 'Domagnano', 189),
(3135, 'Faetano', 189),
(3136, 'Fiorentino', 189),
(3137, 'Montegiardino', 189),
(3138, 'San Marino', 189),
(3139, 'Serravalle', 189),
(3140, 'Agua Grande', 190),
(3141, 'Cantagalo', 190),
(3142, 'Lemba', 190),
(3143, 'Lobata', 190),
(3144, 'Me-Zochi', 190),
(3145, 'Pague', 190),
(3146, 'Al Khobar', 191),
(3147, 'Aseer', 191),
(3148, 'Ash Sharqiyah', 191),
(3149, 'Asir', 191),
(3150, 'Central Province', 191),
(3151, 'Eastern Province', 191),
(3152, 'Ha\'il', 191),
(3153, 'Jawf', 191),
(3154, 'Jizan', 191),
(3155, 'Makkah', 191),
(3156, 'Najran', 191),
(3157, 'Qasim', 191),
(3158, 'Tabuk', 191),
(3159, 'Western Province', 191),
(3160, 'al-Bahah', 191),
(3161, 'al-Hudud-ash-Shamaliyah', 191),
(3162, 'al-Madinah', 191),
(3163, 'ar-Riyad', 191),
(3164, 'Dakar', 192),
(3165, 'Diourbel', 192),
(3166, 'Fatick', 192),
(3167, 'Kaolack', 192),
(3168, 'Kolda', 192),
(3169, 'Louga', 192),
(3170, 'Saint-Louis', 192),
(3171, 'Tambacounda', 192),
(3172, 'Thies', 192),
(3173, 'Ziguinchor', 192),
(3174, 'Central Serbia', 193),
(3175, 'Kosovo and Metohija', 193),
(3176, 'Vojvodina', 193),
(3177, 'Anse Boileau', 194),
(3178, 'Anse Royale', 194),
(3179, 'Cascade', 194),
(3180, 'Takamaka', 194),
(3181, 'Victoria', 194),
(3182, 'Eastern', 195),
(3183, 'Northern', 195),
(3184, 'Southern', 195),
(3185, 'Western', 195),
(3186, 'Singapore', 196),
(3187, 'Banskobystricky', 197),
(3188, 'Bratislavsky', 197),
(3189, 'Kosicky', 197),
(3190, 'Nitriansky', 197),
(3191, 'Presovsky', 197),
(3192, 'Trenciansky', 197),
(3193, 'Trnavsky', 197),
(3194, 'Zilinsky', 197),
(3195, 'Benedikt', 198),
(3196, 'Gorenjska', 198),
(3197, 'Gorishka', 198),
(3198, 'Jugovzhodna Slovenija', 198),
(3199, 'Koroshka', 198),
(3200, 'Notranjsko-krashka', 198),
(3201, 'Obalno-krashka', 198),
(3202, 'Obcina Domzale', 198),
(3203, 'Obcina Vitanje', 198),
(3204, 'Osrednjeslovenska', 198),
(3205, 'Podravska', 198),
(3206, 'Pomurska', 198),
(3207, 'Savinjska', 198),
(3208, 'Slovenian Littoral', 198),
(3209, 'Spodnjeposavska', 198),
(3210, 'Zasavska', 198),
(3211, 'Pitcairn', 199),
(3212, 'Central', 200),
(3213, 'Choiseul', 200),
(3214, 'Guadalcanal', 200),
(3215, 'Isabel', 200),
(3216, 'Makira and Ulawa', 200),
(3217, 'Malaita', 200),
(3218, 'Rennell and Bellona', 200),
(3219, 'Temotu', 200),
(3220, 'Western', 200),
(3221, 'Awdal', 201),
(3222, 'Bakol', 201),
(3223, 'Banadir', 201),
(3224, 'Bari', 201),
(3225, 'Bay', 201),
(3226, 'Galgudug', 201),
(3227, 'Gedo', 201),
(3228, 'Hiran', 201),
(3229, 'Jubbada Hose', 201),
(3230, 'Jubbadha Dexe', 201),
(3231, 'Mudug', 201),
(3232, 'Nugal', 201),
(3233, 'Sanag', 201),
(3234, 'Shabellaha Dhexe', 201),
(3235, 'Shabellaha Hose', 201),
(3236, 'Togdher', 201),
(3237, 'Woqoyi Galbed', 201),
(3238, 'Eastern Cape', 202),
(3239, 'Free State', 202),
(3240, 'Gauteng', 202),
(3241, 'Kempton Park', 202),
(3242, 'Kramerville', 202),
(3243, 'KwaZulu Natal', 202),
(3244, 'Limpopo', 202),
(3245, 'Mpumalanga', 202),
(3246, 'North West', 202),
(3247, 'Northern Cape', 202),
(3248, 'Parow', 202),
(3249, 'Table View', 202),
(3250, 'Umtentweni', 202),
(3251, 'Western Cape', 202),
(3252, 'South Georgia', 203),
(3253, 'Central Equatoria', 204),
(3254, 'A Coruna', 205),
(3255, 'Alacant', 205),
(3256, 'Alava', 205),
(3257, 'Albacete', 205),
(3258, 'Almeria', 205),
(3259, 'Andalucia', 205),
(3260, 'Asturias', 205),
(3261, 'Avila', 205),
(3262, 'Badajoz', 205),
(3263, 'Balears', 205),
(3264, 'Barcelona', 205),
(3265, 'Bertamirans', 205),
(3266, 'Biscay', 205),
(3267, 'Burgos', 205),
(3268, 'Caceres', 205),
(3269, 'Cadiz', 205),
(3270, 'Cantabria', 205),
(3271, 'Castello', 205),
(3272, 'Catalunya', 205),
(3273, 'Ceuta', 205),
(3274, 'Ciudad Real', 205),
(3275, 'Comunidad Autonoma de Canarias', 205),
(3276, 'Comunidad Autonoma de Cataluna', 205),
(3277, 'Comunidad Autonoma de Galicia', 205),
(3278, 'Comunidad Autonoma de las Isla', 205),
(3279, 'Comunidad Autonoma del Princip', 205),
(3280, 'Comunidad Valenciana', 205),
(3281, 'Cordoba', 205),
(3282, 'Cuenca', 205),
(3283, 'Gipuzkoa', 205),
(3284, 'Girona', 205),
(3285, 'Granada', 205),
(3286, 'Guadalajara', 205),
(3287, 'Guipuzcoa', 205),
(3288, 'Huelva', 205),
(3289, 'Huesca', 205),
(3290, 'Jaen', 205),
(3291, 'La Rioja', 205),
(3292, 'Las Palmas', 205),
(3293, 'Leon', 205),
(3294, 'Lerida', 205),
(3295, 'Lleida', 205),
(3296, 'Lugo', 205),
(3297, 'Madrid', 205),
(3298, 'Malaga', 205),
(3299, 'Melilla', 205),
(3300, 'Murcia', 205),
(3301, 'Navarra', 205),
(3302, 'Ourense', 205),
(3303, 'Pais Vasco', 205),
(3304, 'Palencia', 205),
(3305, 'Pontevedra', 205),
(3306, 'Salamanca', 205),
(3307, 'Santa Cruz de Tenerife', 205),
(3308, 'Segovia', 205),
(3309, 'Sevilla', 205),
(3310, 'Soria', 205),
(3311, 'Tarragona', 205),
(3312, 'Tenerife', 205),
(3313, 'Teruel', 205),
(3314, 'Toledo', 205),
(3315, 'Valencia', 205),
(3316, 'Valladolid', 205),
(3317, 'Vizcaya', 205),
(3318, 'Zamora', 205),
(3319, 'Zaragoza', 205),
(3320, 'Amparai', 206),
(3321, 'Anuradhapuraya', 206),
(3322, 'Badulla', 206),
(3323, 'Boralesgamuwa', 206),
(3324, 'Colombo', 206),
(3325, 'Galla', 206),
(3326, 'Gampaha', 206),
(3327, 'Hambantota', 206),
(3328, 'Kalatura', 206),
(3329, 'Kegalla', 206),
(3330, 'Kilinochchi', 206),
(3331, 'Kurunegala', 206),
(3332, 'Madakalpuwa', 206),
(3333, 'Maha Nuwara', 206),
(3334, 'Malwana', 206),
(3335, 'Mannarama', 206),
(3336, 'Matale', 206),
(3337, 'Matara', 206),
(3338, 'Monaragala', 206),
(3339, 'Mullaitivu', 206),
(3340, 'North Eastern Province', 206),
(3341, 'North Western Province', 206),
(3342, 'Nuwara Eliya', 206),
(3343, 'Polonnaruwa', 206),
(3344, 'Puttalama', 206),
(3345, 'Ratnapuraya', 206),
(3346, 'Southern Province', 206),
(3347, 'Tirikunamalaya', 206),
(3348, 'Tuscany', 206),
(3349, 'Vavuniyawa', 206),
(3350, 'Western Province', 206),
(3351, 'Yapanaya', 206),
(3352, 'kadawatha', 206),
(3353, 'A\'ali-an-Nil', 207),
(3354, 'Bahr-al-Jabal', 207),
(3355, 'Central Equatoria', 207),
(3356, 'Gharb Bahr-al-Ghazal', 207),
(3357, 'Gharb Darfur', 207),
(3358, 'Gharb Kurdufan', 207),
(3359, 'Gharb-al-Istiwa\'iyah', 207),
(3360, 'Janub Darfur', 207),
(3361, 'Janub Kurdufan', 207),
(3362, 'Junqali', 207),
(3363, 'Kassala', 207),
(3364, 'Nahr-an-Nil', 207),
(3365, 'Shamal Bahr-al-Ghazal', 207),
(3366, 'Shamal Darfur', 207),
(3367, 'Shamal Kurdufan', 207),
(3368, 'Sharq-al-Istiwa\'iyah', 207),
(3369, 'Sinnar', 207),
(3370, 'Warab', 207),
(3371, 'Wilayat al Khartum', 207),
(3372, 'al-Bahr-al-Ahmar', 207),
(3373, 'al-Buhayrat', 207),
(3374, 'al-Jazirah', 207),
(3375, 'al-Khartum', 207),
(3376, 'al-Qadarif', 207),
(3377, 'al-Wahdah', 207),
(3378, 'an-Nil-al-Abyad', 207),
(3379, 'an-Nil-al-Azraq', 207),
(3380, 'ash-Shamaliyah', 207),
(3381, 'Brokopondo', 208),
(3382, 'Commewijne', 208),
(3383, 'Coronie', 208),
(3384, 'Marowijne', 208),
(3385, 'Nickerie', 208),
(3386, 'Para', 208),
(3387, 'Paramaribo', 208),
(3388, 'Saramacca', 208),
(3389, 'Wanica', 208),
(3390, 'Svalbard', 209),
(3391, 'Hhohho', 210),
(3392, 'Lubombo', 210),
(3393, 'Manzini', 210),
(3394, 'Shiselweni', 210),
(3395, 'Alvsborgs Lan', 211),
(3396, 'Angermanland', 211),
(3397, 'Blekinge', 211),
(3398, 'Bohuslan', 211),
(3399, 'Dalarna', 211),
(3400, 'Gavleborg', 211),
(3401, 'Gaza', 211),
(3402, 'Gotland', 211),
(3403, 'Halland', 211),
(3404, 'Jamtland', 211),
(3405, 'Jonkoping', 211),
(3406, 'Kalmar', 211),
(3407, 'Kristianstads', 211),
(3408, 'Kronoberg', 211),
(3409, 'Norrbotten', 211),
(3410, 'Orebro', 211),
(3411, 'Ostergotland', 211),
(3412, 'Saltsjo-Boo', 211),
(3413, 'Skane', 211),
(3414, 'Smaland', 211),
(3415, 'Sodermanland', 211),
(3416, 'Stockholm', 211),
(3417, 'Uppsala', 211),
(3418, 'Varmland', 211),
(3419, 'Vasterbotten', 211),
(3420, 'Vastergotland', 211),
(3421, 'Vasternorrland', 211),
(3422, 'Vastmanland', 211),
(3423, 'Vastra Gotaland', 211),
(3424, 'Aargau', 212),
(3425, 'Appenzell Inner-Rhoden', 212),
(3426, 'Appenzell-Ausser Rhoden', 212),
(3427, 'Basel-Landschaft', 212),
(3428, 'Basel-Stadt', 212),
(3429, 'Bern', 212),
(3430, 'Canton Ticino', 212),
(3431, 'Fribourg', 212),
(3432, 'Geneve', 212),
(3433, 'Glarus', 212),
(3434, 'Graubunden', 212),
(3435, 'Heerbrugg', 212),
(3436, 'Jura', 212),
(3437, 'Kanton Aargau', 212),
(3438, 'Luzern', 212),
(3439, 'Morbio Inferiore', 212),
(3440, 'Muhen', 212),
(3441, 'Neuchatel', 212),
(3442, 'Nidwalden', 212),
(3443, 'Obwalden', 212),
(3444, 'Sankt Gallen', 212),
(3445, 'Schaffhausen', 212),
(3446, 'Schwyz', 212),
(3447, 'Solothurn', 212),
(3448, 'Thurgau', 212),
(3449, 'Ticino', 212),
(3450, 'Uri', 212),
(3451, 'Valais', 212),
(3452, 'Vaud', 212),
(3453, 'Vauffelin', 212),
(3454, 'Zug', 212),
(3455, 'Zurich', 212),
(3456, 'Aleppo', 213),
(3457, 'Dar\'a', 213),
(3458, 'Dayr-az-Zawr', 213),
(3459, 'Dimashq', 213),
(3460, 'Halab', 213),
(3461, 'Hamah', 213),
(3462, 'Hims', 213),
(3463, 'Idlib', 213),
(3464, 'Madinat Dimashq', 213),
(3465, 'Tartus', 213),
(3466, 'al-Hasakah', 213),
(3467, 'al-Ladhiqiyah', 213),
(3468, 'al-Qunaytirah', 213),
(3469, 'ar-Raqqah', 213),
(3470, 'as-Suwayda', 213),
(3471, 'Changhwa', 214),
(3472, 'Chiayi Hsien', 214),
(3473, 'Chiayi Shih', 214),
(3474, 'Eastern Taipei', 214),
(3475, 'Hsinchu Hsien', 214),
(3476, 'Hsinchu Shih', 214),
(3477, 'Hualien', 214),
(3478, 'Ilan', 214),
(3479, 'Kaohsiung Hsien', 214),
(3480, 'Kaohsiung Shih', 214),
(3481, 'Keelung Shih', 214),
(3482, 'Kinmen', 214),
(3483, 'Miaoli', 214),
(3484, 'Nantou', 214),
(3485, 'Northern Taiwan', 214),
(3486, 'Penghu', 214),
(3487, 'Pingtung', 214),
(3488, 'Taichung', 214),
(3489, 'Taichung Hsien', 214),
(3490, 'Taichung Shih', 214),
(3491, 'Tainan Hsien', 214),
(3492, 'Tainan Shih', 214),
(3493, 'Taipei Hsien', 214),
(3494, 'Taipei Shih / Taipei Hsien', 214),
(3495, 'Taitung', 214),
(3496, 'Taoyuan', 214),
(3497, 'Yilan', 214),
(3498, 'Yun-Lin Hsien', 214),
(3499, 'Yunlin', 214),
(3500, 'Dushanbe', 215),
(3501, 'Gorno-Badakhshan', 215),
(3502, 'Karotegin', 215),
(3503, 'Khatlon', 215),
(3504, 'Sughd', 215),
(3505, 'Arusha', 216),
(3506, 'Dar es Salaam', 216),
(3507, 'Dodoma', 216),
(3508, 'Iringa', 216),
(3509, 'Kagera', 216),
(3510, 'Kigoma', 216),
(3511, 'Kilimanjaro', 216),
(3512, 'Lindi', 216),
(3513, 'Mara', 216),
(3514, 'Mbeya', 216),
(3515, 'Morogoro', 216),
(3516, 'Mtwara', 216),
(3517, 'Mwanza', 216),
(3518, 'Pwani', 216),
(3519, 'Rukwa', 216),
(3520, 'Ruvuma', 216),
(3521, 'Shinyanga', 216),
(3522, 'Singida', 216),
(3523, 'Tabora', 216),
(3524, 'Tanga', 216),
(3525, 'Zanzibar and Pemba', 216),
(3526, 'Amnat Charoen', 217),
(3527, 'Ang Thong', 217),
(3528, 'Bangkok', 217),
(3529, 'Buri Ram', 217),
(3530, 'Chachoengsao', 217),
(3531, 'Chai Nat', 217),
(3532, 'Chaiyaphum', 217),
(3533, 'Changwat Chaiyaphum', 217),
(3534, 'Chanthaburi', 217),
(3535, 'Chiang Mai', 217),
(3536, 'Chiang Rai', 217),
(3537, 'Chon Buri', 217),
(3538, 'Chumphon', 217),
(3539, 'Kalasin', 217),
(3540, 'Kamphaeng Phet', 217),
(3541, 'Kanchanaburi', 217),
(3542, 'Khon Kaen', 217),
(3543, 'Krabi', 217),
(3544, 'Krung Thep', 217),
(3545, 'Lampang', 217),
(3546, 'Lamphun', 217),
(3547, 'Loei', 217),
(3548, 'Lop Buri', 217),
(3549, 'Mae Hong Son', 217),
(3550, 'Maha Sarakham', 217),
(3551, 'Mukdahan', 217),
(3552, 'Nakhon Nayok', 217),
(3553, 'Nakhon Pathom', 217),
(3554, 'Nakhon Phanom', 217),
(3555, 'Nakhon Ratchasima', 217),
(3556, 'Nakhon Sawan', 217),
(3557, 'Nakhon Si Thammarat', 217),
(3558, 'Nan', 217),
(3559, 'Narathiwat', 217),
(3560, 'Nong Bua Lam Phu', 217),
(3561, 'Nong Khai', 217),
(3562, 'Nonthaburi', 217),
(3563, 'Pathum Thani', 217),
(3564, 'Pattani', 217),
(3565, 'Phangnga', 217),
(3566, 'Phatthalung', 217),
(3567, 'Phayao', 217),
(3568, 'Phetchabun', 217),
(3569, 'Phetchaburi', 217),
(3570, 'Phichit', 217),
(3571, 'Phitsanulok', 217),
(3572, 'Phra Nakhon Si Ayutthaya', 217),
(3573, 'Phrae', 217),
(3574, 'Phuket', 217),
(3575, 'Prachin Buri', 217),
(3576, 'Prachuap Khiri Khan', 217),
(3577, 'Ranong', 217),
(3578, 'Ratchaburi', 217),
(3579, 'Rayong', 217),
(3580, 'Roi Et', 217),
(3581, 'Sa Kaeo', 217),
(3582, 'Sakon Nakhon', 217),
(3583, 'Samut Prakan', 217),
(3584, 'Samut Sakhon', 217),
(3585, 'Samut Songkhran', 217),
(3586, 'Saraburi', 217),
(3587, 'Satun', 217),
(3588, 'Si Sa Ket', 217),
(3589, 'Sing Buri', 217),
(3590, 'Songkhla', 217),
(3591, 'Sukhothai', 217),
(3592, 'Suphan Buri', 217),
(3593, 'Surat Thani', 217),
(3594, 'Surin', 217),
(3595, 'Tak', 217),
(3596, 'Trang', 217),
(3597, 'Trat', 217),
(3598, 'Ubon Ratchathani', 217),
(3599, 'Udon Thani', 217),
(3600, 'Uthai Thani', 217),
(3601, 'Uttaradit', 217),
(3602, 'Yala', 217),
(3603, 'Yasothon', 217),
(3604, 'Centre', 218),
(3605, 'Kara', 218),
(3606, 'Maritime', 218),
(3607, 'Plateaux', 218),
(3608, 'Savanes', 218),
(3609, 'Atafu', 219),
(3610, 'Fakaofo', 219),
(3611, 'Nukunonu', 219),
(3612, 'Eua', 220),
(3613, 'Ha\'apai', 220),
(3614, 'Niuas', 220),
(3615, 'Tongatapu', 220),
(3616, 'Vava\'u', 220),
(3617, 'Arima-Tunapuna-Piarco', 221),
(3618, 'Caroni', 221),
(3619, 'Chaguanas', 221),
(3620, 'Couva-Tabaquite-Talparo', 221),
(3621, 'Diego Martin', 221),
(3622, 'Glencoe', 221),
(3623, 'Penal Debe', 221),
(3624, 'Point Fortin', 221),
(3625, 'Port of Spain', 221),
(3626, 'Princes Town', 221),
(3627, 'Saint George', 221),
(3628, 'San Fernando', 221),
(3629, 'San Juan', 221),
(3630, 'Sangre Grande', 221),
(3631, 'Siparia', 221),
(3632, 'Tobago', 221),
(3633, 'Aryanah', 222),
(3634, 'Bajah', 222),
(3635, 'Bin \'Arus', 222),
(3636, 'Binzart', 222),
(3637, 'Gouvernorat de Ariana', 222),
(3638, 'Gouvernorat de Nabeul', 222),
(3639, 'Gouvernorat de Sousse', 222),
(3640, 'Hammamet Yasmine', 222),
(3641, 'Jundubah', 222),
(3642, 'Madaniyin', 222),
(3643, 'Manubah', 222),
(3644, 'Monastir', 222),
(3645, 'Nabul', 222),
(3646, 'Qabis', 222),
(3647, 'Qafsah', 222),
(3648, 'Qibili', 222),
(3649, 'Safaqis', 222),
(3650, 'Sfax', 222),
(3651, 'Sidi Bu Zayd', 222),
(3652, 'Silyanah', 222),
(3653, 'Susah', 222),
(3654, 'Tatawin', 222),
(3655, 'Tawzar', 222),
(3656, 'Tunis', 222),
(3657, 'Zaghwan', 222),
(3658, 'al-Kaf', 222),
(3659, 'al-Mahdiyah', 222),
(3660, 'al-Munastir', 222),
(3661, 'al-Qasrayn', 222),
(3662, 'al-Qayrawan', 222),
(3663, 'Adana', 223),
(3664, 'Adiyaman', 223),
(3665, 'Afyon', 223),
(3666, 'Agri', 223),
(3667, 'Aksaray', 223),
(3668, 'Amasya', 223),
(3669, 'Ankara', 223),
(3670, 'Antalya', 223),
(3671, 'Ardahan', 223),
(3672, 'Artvin', 223),
(3673, 'Aydin', 223),
(3674, 'Balikesir', 223),
(3675, 'Bartin', 223),
(3676, 'Batman', 223),
(3677, 'Bayburt', 223),
(3678, 'Bilecik', 223),
(3679, 'Bingol', 223),
(3680, 'Bitlis', 223),
(3681, 'Bolu', 223),
(3682, 'Burdur', 223),
(3683, 'Bursa', 223),
(3684, 'Canakkale', 223),
(3685, 'Cankiri', 223),
(3686, 'Corum', 223),
(3687, 'Denizli', 223),
(3688, 'Diyarbakir', 223),
(3689, 'Duzce', 223),
(3690, 'Edirne', 223),
(3691, 'Elazig', 223),
(3692, 'Erzincan', 223),
(3693, 'Erzurum', 223),
(3694, 'Eskisehir', 223),
(3695, 'Gaziantep', 223),
(3696, 'Giresun', 223),
(3697, 'Gumushane', 223),
(3698, 'Hakkari', 223),
(3699, 'Hatay', 223),
(3700, 'Icel', 223),
(3701, 'Igdir', 223),
(3702, 'Isparta', 223),
(3703, 'Istanbul', 223),
(3704, 'Izmir', 223),
(3705, 'Kahramanmaras', 223),
(3706, 'Karabuk', 223),
(3707, 'Karaman', 223),
(3708, 'Kars', 223),
(3709, 'Karsiyaka', 223),
(3710, 'Kastamonu', 223),
(3711, 'Kayseri', 223),
(3712, 'Kilis', 223),
(3713, 'Kirikkale', 223),
(3714, 'Kirklareli', 223),
(3715, 'Kirsehir', 223),
(3716, 'Kocaeli', 223),
(3717, 'Konya', 223),
(3718, 'Kutahya', 223),
(3719, 'Lefkosa', 223),
(3720, 'Malatya', 223),
(3721, 'Manisa', 223),
(3722, 'Mardin', 223),
(3723, 'Mugla', 223),
(3724, 'Mus', 223),
(3725, 'Nevsehir', 223),
(3726, 'Nigde', 223),
(3727, 'Ordu', 223),
(3728, 'Osmaniye', 223),
(3729, 'Rize', 223),
(3730, 'Sakarya', 223),
(3731, 'Samsun', 223),
(3732, 'Sanliurfa', 223),
(3733, 'Siirt', 223),
(3734, 'Sinop', 223),
(3735, 'Sirnak', 223),
(3736, 'Sivas', 223),
(3737, 'Tekirdag', 223),
(3738, 'Tokat', 223),
(3739, 'Trabzon', 223),
(3740, 'Tunceli', 223),
(3741, 'Usak', 223),
(3742, 'Van', 223),
(3743, 'Yalova', 223),
(3744, 'Yozgat', 223),
(3745, 'Zonguldak', 223),
(3746, 'Ahal', 224),
(3747, 'Asgabat', 224),
(3748, 'Balkan', 224),
(3749, 'Dasoguz', 224),
(3750, 'Lebap', 224),
(3751, 'Mari', 224),
(3752, 'Grand Turk', 225),
(3753, 'South Caicos and East Caicos', 225),
(3754, 'Funafuti', 226),
(3755, 'Nanumanga', 226),
(3756, 'Nanumea', 226),
(3757, 'Niutao', 226),
(3758, 'Nui', 226),
(3759, 'Nukufetau', 226),
(3760, 'Nukulaelae', 226),
(3761, 'Vaitupu', 226),
(3762, 'Central', 227),
(3763, 'Eastern', 227),
(3764, 'Northern', 227),
(3765, 'Western', 227),
(3766, 'Cherkas\'ka', 228),
(3767, 'Chernihivs\'ka', 228),
(3768, 'Chernivets\'ka', 228),
(3769, 'Crimea', 228),
(3770, 'Dnipropetrovska', 228),
(3771, 'Donets\'ka', 228),
(3772, 'Ivano-Frankivs\'ka', 228),
(3773, 'Kharkiv', 228),
(3774, 'Kharkov', 228),
(3775, 'Khersonska', 228),
(3776, 'Khmel\'nyts\'ka', 228),
(3777, 'Kirovohrad', 228),
(3778, 'Krym', 228),
(3779, 'Kyyiv', 228),
(3780, 'Kyyivs\'ka', 228),
(3781, 'L\'vivs\'ka', 228),
(3782, 'Luhans\'ka', 228),
(3783, 'Mykolayivs\'ka', 228),
(3784, 'Odes\'ka', 228),
(3785, 'Odessa', 228),
(3786, 'Poltavs\'ka', 228),
(3787, 'Rivnens\'ka', 228),
(3788, 'Sevastopol\'', 228),
(3789, 'Sums\'ka', 228),
(3790, 'Ternopil\'s\'ka', 228),
(3791, 'Volyns\'ka', 228),
(3792, 'Vynnyts\'ka', 228),
(3793, 'Zakarpats\'ka', 228),
(3794, 'Zaporizhia', 228),
(3795, 'Zhytomyrs\'ka', 228),
(3796, 'Abu Zabi', 229),
(3797, 'Ajman', 229),
(3798, 'Dubai', 229),
(3799, 'Ras al-Khaymah', 229),
(3800, 'Sharjah', 229),
(3801, 'Sharjha', 229),
(3802, 'Umm al Qaywayn', 229),
(3803, 'al-Fujayrah', 229),
(3804, 'ash-Shariqah', 229),
(3805, 'Aberdeen', 230),
(3806, 'Aberdeenshire', 230),
(3807, 'Argyll', 230),
(3808, 'Armagh', 230),
(3809, 'Bedfordshire', 230),
(3810, 'Belfast', 230),
(3811, 'Berkshire', 230),
(3812, 'Birmingham', 230),
(3813, 'Brechin', 230),
(3814, 'Bridgnorth', 230),
(3815, 'Bristol', 230),
(3816, 'Buckinghamshire', 230),
(3817, 'Cambridge', 230),
(3818, 'Cambridgeshire', 230),
(3819, 'Channel Islands', 230),
(3820, 'Cheshire', 230),
(3821, 'Cleveland', 230),
(3822, 'Co Fermanagh', 230),
(3823, 'Conwy', 230),
(3824, 'Cornwall', 230),
(3825, 'Coventry', 230),
(3826, 'Craven Arms', 230),
(3827, 'Cumbria', 230),
(3828, 'Denbighshire', 230),
(3829, 'Derby', 230),
(3830, 'Derbyshire', 230),
(3831, 'Devon', 230),
(3832, 'Dial Code Dungannon', 230),
(3833, 'Didcot', 230),
(3834, 'Dorset', 230),
(3835, 'Dunbartonshire', 230),
(3836, 'Durham', 230),
(3837, 'East Dunbartonshire', 230),
(3838, 'East Lothian', 230),
(3839, 'East Midlands', 230),
(3840, 'East Sussex', 230),
(3841, 'East Yorkshire', 230),
(3842, 'England', 230),
(3843, 'Essex', 230),
(3844, 'Fermanagh', 230),
(3845, 'Fife', 230),
(3846, 'Flintshire', 230),
(3847, 'Fulham', 230),
(3848, 'Gainsborough', 230),
(3849, 'Glocestershire', 230),
(3850, 'Gwent', 230),
(3851, 'Hampshire', 230),
(3852, 'Hants', 230),
(3853, 'Herefordshire', 230),
(3854, 'Hertfordshire', 230),
(3855, 'Ireland', 230),
(3856, 'Isle Of Man', 230),
(3857, 'Isle of Wight', 230),
(3858, 'Kenford', 230),
(3859, 'Kent', 230),
(3860, 'Kilmarnock', 230),
(3861, 'Lanarkshire', 230),
(3862, 'Lancashire', 230),
(3863, 'Leicestershire', 230),
(3864, 'Lincolnshire', 230),
(3865, 'Llanymynech', 230),
(3866, 'London', 230),
(3867, 'Ludlow', 230),
(3868, 'Manchester', 230),
(3869, 'Mayfair', 230),
(3870, 'Merseyside', 230),
(3871, 'Mid Glamorgan', 230),
(3872, 'Middlesex', 230),
(3873, 'Mildenhall', 230),
(3874, 'Monmouthshire', 230),
(3875, 'Newton Stewart', 230),
(3876, 'Norfolk', 230),
(3877, 'North Humberside', 230),
(3878, 'North Yorkshire', 230),
(3879, 'Northamptonshire', 230),
(3880, 'Northants', 230),
(3881, 'Northern Ireland', 230),
(3882, 'Northumberland', 230),
(3883, 'Nottinghamshire', 230),
(3884, 'Oxford', 230),
(3885, 'Powys', 230),
(3886, 'Roos-shire', 230),
(3887, 'SUSSEX', 230),
(3888, 'Sark', 230),
(3889, 'Scotland', 230),
(3890, 'Scottish Borders', 230),
(3891, 'Shropshire', 230),
(3892, 'Somerset', 230),
(3893, 'South Glamorgan', 230),
(3894, 'South Wales', 230),
(3895, 'South Yorkshire', 230),
(3896, 'Southwell', 230),
(3897, 'Staffordshire', 230),
(3898, 'Strabane', 230),
(3899, 'Suffolk', 230),
(3900, 'Surrey', 230),
(3901, 'Sussex', 230),
(3902, 'Twickenham', 230),
(3903, 'Tyne and Wear', 230),
(3904, 'Tyrone', 230),
(3905, 'Utah', 230),
(3906, 'Wales', 230),
(3907, 'Warwickshire', 230),
(3908, 'West Lothian', 230),
(3909, 'West Midlands', 230),
(3910, 'West Sussex', 230),
(3911, 'West Yorkshire', 230),
(3912, 'Whissendine', 230),
(3913, 'Wiltshire', 230),
(3914, 'Wokingham', 230),
(3915, 'Worcestershire', 230),
(3916, 'Wrexham', 230),
(3917, 'Wurttemberg', 230),
(3918, 'Yorkshire', 230),
(3919, 'Alabama', 231),
(3920, 'Alaska', 231),
(3921, 'Arizona', 231),
(3922, 'Arkansas', 231),
(3923, 'Byram', 231),
(3924, 'California', 231),
(3925, 'Cokato', 231),
(3926, 'Colorado', 231),
(3927, 'Connecticut', 231),
(3928, 'Delaware', 231),
(3929, 'District of Columbia', 231),
(3930, 'Florida', 231),
(3931, 'Georgia', 231),
(3932, 'Hawaii', 231),
(3933, 'Idaho', 231),
(3934, 'Illinois', 231),
(3935, 'Indiana', 231),
(3936, 'Iowa', 231),
(3937, 'Kansas', 231),
(3938, 'Kentucky', 231),
(3939, 'Louisiana', 231),
(3940, 'Lowa', 231),
(3941, 'Maine', 231),
(3942, 'Maryland', 231),
(3943, 'Massachusetts', 231),
(3944, 'Medfield', 231),
(3945, 'Michigan', 231),
(3946, 'Minnesota', 231),
(3947, 'Mississippi', 231),
(3948, 'Missouri', 231),
(3949, 'Montana', 231),
(3950, 'Nebraska', 231),
(3951, 'Nevada', 231),
(3952, 'New Hampshire', 231),
(3953, 'New Jersey', 231),
(3954, 'New Jersy', 231),
(3955, 'New Mexico', 231),
(3956, 'New York', 231),
(3957, 'North Carolina', 231),
(3958, 'North Dakota', 231),
(3959, 'Ohio', 231),
(3960, 'Oklahoma', 231),
(3961, 'Ontario', 231),
(3962, 'Oregon', 231),
(3963, 'Pennsylvania', 231),
(3964, 'Ramey', 231),
(3965, 'Rhode Island', 231),
(3966, 'South Carolina', 231),
(3967, 'South Dakota', 231),
(3968, 'Sublimity', 231),
(3969, 'Tennessee', 231),
(3970, 'Texas', 231),
(3971, 'Trimble', 231),
(3972, 'Utah', 231),
(3973, 'Vermont', 231),
(3974, 'Virginia', 231),
(3975, 'Washington', 231),
(3976, 'West Virginia', 231),
(3977, 'Wisconsin', 231),
(3978, 'Wyoming', 231),
(3979, 'United States Minor Outlying I', 232),
(3980, 'Artigas', 233),
(3981, 'Canelones', 233),
(3982, 'Cerro Largo', 233),
(3983, 'Colonia', 233),
(3984, 'Durazno', 233),
(3985, 'FLorida', 233),
(3986, 'Flores', 233),
(3987, 'Lavalleja', 233),
(3988, 'Maldonado', 233),
(3989, 'Montevideo', 233),
(3990, 'Paysandu', 233),
(3991, 'Rio Negro', 233),
(3992, 'Rivera', 233),
(3993, 'Rocha', 233),
(3994, 'Salto', 233),
(3995, 'San Jose', 233),
(3996, 'Soriano', 233),
(3997, 'Tacuarembo', 233),
(3998, 'Treinta y Tres', 233),
(3999, 'Andijon', 234),
(4000, 'Buhoro', 234),
(4001, 'Buxoro Viloyati', 234),
(4002, 'Cizah', 234),
(4003, 'Fargona', 234),
(4004, 'Horazm', 234),
(4005, 'Kaskadar', 234),
(4006, 'Korakalpogiston', 234),
(4007, 'Namangan', 234),
(4008, 'Navoi', 234),
(4009, 'Samarkand', 234),
(4010, 'Sirdare', 234),
(4011, 'Surhondar', 234),
(4012, 'Toskent', 234),
(4013, 'Malampa', 235),
(4014, 'Penama', 235),
(4015, 'Sanma', 235),
(4016, 'Shefa', 235),
(4017, 'Tafea', 235),
(4018, 'Torba', 235),
(4019, 'Vatican City State (Holy See)', 236),
(4020, 'Amazonas', 237),
(4021, 'Anzoategui', 237),
(4022, 'Apure', 237),
(4023, 'Aragua', 237),
(4024, 'Barinas', 237),
(4025, 'Bolivar', 237),
(4026, 'Carabobo', 237),
(4027, 'Cojedes', 237),
(4028, 'Delta Amacuro', 237),
(4029, 'Distrito Federal', 237),
(4030, 'Falcon', 237),
(4031, 'Guarico', 237),
(4032, 'Lara', 237),
(4033, 'Merida', 237),
(4034, 'Miranda', 237),
(4035, 'Monagas', 237),
(4036, 'Nueva Esparta', 237),
(4037, 'Portuguesa', 237),
(4038, 'Sucre', 237),
(4039, 'Tachira', 237),
(4040, 'Trujillo', 237),
(4041, 'Vargas', 237),
(4042, 'Yaracuy', 237),
(4043, 'Zulia', 237),
(4044, 'Bac Giang', 238),
(4045, 'Binh Dinh', 238),
(4046, 'Binh Duong', 238),
(4047, 'Da Nang', 238),
(4048, 'Dong Bang Song Cuu Long', 238),
(4049, 'Dong Bang Song Hong', 238),
(4050, 'Dong Nai', 238),
(4051, 'Dong Nam Bo', 238),
(4052, 'Duyen Hai Mien Trung', 238),
(4053, 'Hanoi', 238),
(4054, 'Hung Yen', 238),
(4055, 'Khu Bon Cu', 238),
(4056, 'Long An', 238),
(4057, 'Mien Nui Va Trung Du', 238),
(4058, 'Thai Nguyen', 238),
(4059, 'Thanh Pho Ho Chi Minh', 238),
(4060, 'Thu Do Ha Noi', 238),
(4061, 'Tinh Can Tho', 238),
(4062, 'Tinh Da Nang', 238),
(4063, 'Tinh Gia Lai', 238),
(4064, 'Anegada', 239),
(4065, 'Jost van Dyke', 239),
(4066, 'Tortola', 239),
(4067, 'Saint Croix', 240),
(4068, 'Saint John', 240),
(4069, 'Saint Thomas', 240),
(4070, 'Alo', 241),
(4071, 'Singave', 241),
(4072, 'Wallis', 241),
(4073, 'Bu Jaydur', 242),
(4074, 'Wad-adh-Dhahab', 242),
(4075, 'al-\'Ayun', 242),
(4076, 'as-Samarah', 242),
(4077, '\'Adan', 243),
(4078, 'Abyan', 243),
(4079, 'Dhamar', 243),
(4080, 'Hadramaut', 243),
(4081, 'Hajjah', 243),
(4082, 'Hudaydah', 243),
(4083, 'Ibb', 243),
(4084, 'Lahij', 243),
(4085, 'Ma\'rib', 243),
(4086, 'Madinat San\'a', 243),
(4087, 'Sa\'dah', 243),
(4088, 'Sana', 243),
(4089, 'Shabwah', 243),
(4090, 'Ta\'izz', 243),
(4091, 'al-Bayda', 243),
(4092, 'al-Hudaydah', 243),
(4093, 'al-Jawf', 243),
(4094, 'al-Mahrah', 243),
(4095, 'al-Mahwit', 243),
(4096, 'Central Serbia', 244),
(4097, 'Kosovo and Metohija', 244),
(4098, 'Montenegro', 244),
(4099, 'Republic of Serbia', 244),
(4100, 'Serbia', 244),
(4101, 'Vojvodina', 244),
(4102, 'Central', 245),
(4103, 'Copperbelt', 245),
(4104, 'Eastern', 245),
(4105, 'Luapala', 245),
(4106, 'Lusaka', 245),
(4107, 'North-Western', 245),
(4108, 'Northern', 245),
(4109, 'Southern', 245),
(4110, 'Western', 245),
(4111, 'Bulawayo', 246),
(4112, 'Harare', 246),
(4113, 'Manicaland', 246),
(4114, 'Mashonaland Central', 246),
(4115, 'Mashonaland East', 246),
(4116, 'Mashonaland West', 246),
(4117, 'Masvingo', 246),
(4118, 'Matabeleland North', 246),
(4119, 'Matabeleland South', 246),
(4120, 'Midlands', 246);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apple` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `stores`
--

INSERT INTO `stores` (`id`, `google`, `apple`, `type`, `created_at`, `updated_at`) VALUES
(1, 'https://play.google.com/store/apps/details?id=com.limonbyte.bogui', 'https://www.apple.com/la/ios/app-store/', 1, NULL, NULL),
(2, 'https://play.google.com/store/apps/details?id=com.limonbyte.boguiconductor', 'https://www.apple.com/la/ios/app-store/', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `support_motive`
--

CREATE TABLE `support_motive` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1: Soporte, 2: Ayuda',
  `type_user` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1: General, 2: Pasajero, 3: Conductor',
  `registered_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `support_motive`
--

INSERT INTO `support_motive` (`id`, `description`, `type`, `type_user`, `registered_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(6, 'Participé en un accidente', '1', '2', 1, NULL, '2018-10-01 14:48:27', '2018-12-21 14:01:32'),
(7, 'Articulos olvidados', '1', '1', 1, NULL, '2018-10-01 14:49:00', '2018-10-19 02:09:31'),
(8, 'Tarifas y pagos', '1', '1', 1, NULL, '2018-10-01 14:49:21', '2018-10-19 02:12:51'),
(9, 'Comentarios sobre un cliente', '1', '3', 1, NULL, '2018-10-01 14:49:46', '2018-10-01 14:49:46'),
(10, 'Activar mi vehiculo', '1', '3', 1, NULL, '2018-10-08 19:47:34', '2018-10-08 19:47:34'),
(11, 'No puedo ingresar a mi cuenta', '1', '1', 1, NULL, '2018-10-19 02:11:50', '2018-10-19 02:11:50'),
(12, 'Estuve en un accidente', '2', '2', 1, NULL, '2018-10-30 14:44:59', '2018-10-30 14:44:59'),
(13, 'Objetos olvidados en el vehículo', '2', '1', 1, NULL, '2018-10-30 14:45:37', '2018-11-26 22:20:56'),
(14, 'Revisión de Tarifa', '2', '1', 1, NULL, '2018-10-30 14:46:13', '2018-10-30 14:46:13'),
(15, 'Comentarios sobre el viaje', '2', '1', 1, NULL, '2018-10-30 14:46:39', '2018-11-26 22:27:35'),
(16, 'Otro tipo de problema', '2', '1', 1, NULL, '2018-10-30 14:47:21', '2018-11-26 22:31:41'),
(17, 'Recibi una multa de transito', '2', '3', 1, NULL, '2018-10-30 14:48:13', '2018-11-10 01:45:15'),
(18, 'Otros', '2', '1', 1, NULL, '2018-12-21 14:02:11', '2018-12-21 14:22:05'),
(19, 'Otro', '1', '1', 1, NULL, '2019-01-02 04:47:28', '2019-01-02 04:47:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tariffs`
--

CREATE TABLE `tariffs` (
  `id` int(10) UNSIGNED NOT NULL,
  `base_tariff` int(11) NOT NULL,
  `minimum_tariff` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `medium_tariff` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maximum_tariff` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_km_greater` double(10,2) NOT NULL,
  `value_km_less` double(10,2) NOT NULL,
  `registered_by` int(11) NOT NULL,
  `vehicle_type_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Si es 0, es una tarifa para todos los tipos de vehiculos',
  `other_tariff` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tariffs`
--

INSERT INTO `tariffs` (`id`, `base_tariff`, `minimum_tariff`, `medium_tariff`, `maximum_tariff`, `value_km_greater`, `value_km_less`, `registered_by`, `vehicle_type_id`, `other_tariff`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 460, '1100', '290', '500', 50.00, 2.00, 1, 1, NULL, NULL, '2018-09-13 17:30:48', '2019-04-10 03:41:04'),
(2, 1000, '1500', '500', '700', 50.00, 2.00, 1, 2, NULL, NULL, '2018-09-13 17:31:38', '2019-02-22 03:03:44'),
(3, 1600, '2700', '700', '800', 50.00, 2.00, 1, 3, NULL, '2019-10-14 16:38:00', '2018-09-13 17:31:38', '2019-02-22 03:06:14'),
(4, 6000, '0', '0', '0', 0.00, 0.00, 1, 0, 1, NULL, '2018-09-27 20:15:45', '2019-03-07 21:29:11'),
(5, 100, '0', '0', '0', 0.00, 0.00, 1, 0, 2, NULL, '2018-11-22 01:30:48', '2019-01-09 13:03:26'),
(6, 260, '800', '260', '310', 50.00, 2.00, 1, 4, NULL, NULL, '2019-10-14 03:00:00', '2019-12-10 02:42:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tariff_type`
--

CREATE TABLE `tariff_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `tariff_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `terms`
--

CREATE TABLE `terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `terms`
--

INSERT INTO `terms` (`id`, `content`, `english`, `created_at`, `updated_at`) VALUES
(1, 'BOGUI LTDA Es una Sociedad constituida en Chile con domicilio en la ciudad de Santiago, y se encuentra regulada por el estatuto que los socios han suscrito conforme a la Ley número tres mil novecientos dieciocho y sus modificaciones y demás leyes pertinentes. http://www.leychile.cl/Navegar?idNorma=24349&idParte=0\r\nLa Razón Social de la sociedad es “Consultores en Informática Bogui Limitada, y su Rol Único Tributario 76.664.261-6 con domicilio en la ciudad de Santiago.\r\nEl presente texto explica los términos y condiciones que regulan el acceso y uso que usted como persona o empresa haga del sitio web, la plataforma y de las aplicaciones móviles que Bogui Ltda pone a disposición.\r\nSolicitamos leer detenidamente los siguientes términos y condiciones antes del uso de cualquiera de nuestros servicios, si usted no está de acuerdo con los términos y condiciones, no puede acceder o usar la plataforma web y las aplicaciones Móviles.\r\nLas aplicaciones Bogui proporcionan tecnología para las personas que buscan movilizarse a ciertos destinos (usuarios), puedan ser enlazados con personas que están dispuestos a suplir aquella necesidad (conductores), estos se denominan colectivamente “usuarios”, y cada individuo deberá crear una cuenta de usuario que le permita el acceso a la plataforma de Bogui.\r\nPara efectos del presente acuerdo los servicios de conducción proporcionados de los conductores a pasajeros que se enlacen a través de la plataforma de Bogui se referirán colectivamente como los “servicios”.\r\nLa decisión de un usuario de ofrecer o aceptar servicios es una decisión de cada usuario a discreción de cada uno de ellos.\r\nEl servicio de transporte proporcionado por un conductor a un usuario constituirá un acuerdo separado entre dichas personas.\r\n*DE LAS MODIFICACIONES\r\nEn el fortuito caso que Bogui Ltda Modifique los presentes términos y condiciones de este acuerdo, solo serán vinculados cuando usted acepte el acuerdo modificado, Bogui Ltda se reserva el derecho de modificar cualquier información vinculada a este acuerdo, tales modificaciones entraran en vigor justo después de la publicación.\r\nEl uso de la plataforma y otros servicios de Bogui Ltda después de cualquier cambio en los términos y condiciones precisaran de su consentimiento para efectuar tales cambios, a menos que se hagan cambios significativos a las disposiciones de arbitraje de este documento, usted acepta que la modificación de este acuerdo no crea una oportunidad renovada de optar por no participar en el arbitraje (en caso de ser aplicable).\r\n*DE EL ACCESO Y USO\r\nLa plataforma Bogui solo podrá ser utilizada por personas mayores de 18 años y que puedan firmar y formalizar contratos legalmente vinculante bajo la Ley aplicable.\r\nLa plataforma Bogui no está autorizada para personas menores de 18 años o personas que han tenido su cuenta de usuario temporal o permanentemente desactivada.\r\nAl convertirse en un usuario activo de la plataforma web y las aplicaciones Bogui usted declara y garantiza que al menos tiene 18 años de edad y que tiene la capacidad, la autoridad y el derecho para entrar y cumplir con los términos y condiciones de este acuerdo.\r\nNo puede permitir que otras personas utilicen su cuenta de usuario y acepta que es la única persona autorizada para el uso de su cuenta.\r\n*DE LOS PAGOS\r\nUsted como persona acepta pagar las cantidades cobradas por su uso de la Plataforma y Servicios de Bogui Ltda "Cargos". Los cargos incluyen tarifas y otros cargos aplicables, peajes, recargos e impuestos según lo establecido en la página de Bogui Ltda (www.bogui.cl), además de cualquier sugerencia al conductor que elija pagar. Bogui tiene la autoridad y se reserva el derecho de determinar y modificar los precios publicando términos de precios aplicables a la página de Bogui.cl de su País. El precio puede variar en función del tipo de servicio que solicite (por ejemplo, Bogui Sedan, Bogui Suv, Bogui Van), tal como se describe en la página de Bogui.cl de su País. Usted es responsable de revisar la página correspondiente de Bogui.cl y será responsable de todos los cargos incurridos en su cuenta de usuario, independientemente de su conocimiento de dichos cargos o las cantidades de los mismos.\r\n*DE LAS TARIFAS\r\nTarifas variables.\r\nLas tarifas variables consisten en un cargo base y cargos incrementales basados en la duración y la distancia de su viaje. Para viajes particularmente cortos, pueden aplicarse tarifas mínimas. Tenga en cuenta que utilizamos los datos GPS del teléfono del conductor para calcular la distancia recorrida en su viaje. No podemos garantizar la disponibilidad o precisión de los datos GPS. Si perdemos la señal vamos a calcular el tiempo y la distancia utilizando los datos disponibles de su viaje.\r\nTarifas Fijas.\r\nEn algunos casos Bogui puede cotizar una tarifa en el momento de su solicitud. La cotización está sujeta a cambios hasta que se confirme la solicitud de viaje. Si durante su viaje cambia su destino, hace paradas múltiples o intenta abusar de la plataforma Bogui, podemos cancelar la tarifa y cobrarle una tarifa variable basada en el tiempo y la distancia de su viaje. Bogui no garantiza que el precio de la tarifa cotizada sea igual a una tarifa variable para el mismo viaje.\r\nTarifas en Hora Punta.\r\nLas tarifas pueden estar sujetas a un multiplicador en momentos de alta demanda de los Servicios ("Hora punta") según lo determine Bogui. Para todos los viajes con tarifa variable, usaremos cálculos razonables para informarle de cualquier multiplicador de Hora Punta en el momento de su solicitud. Para las tarifas cotizadas, podemos tener en cuenta el multiplicador de Hora Punta en el precio cotizado del viaje. Cualquier cargo de Hora punta se considerará parte de la tarifa.\r\nTarifas y otros cargos.\r\nCuota de Servicio. Bogui puede evaluar una "cuota de servicio" por viaje para apoyar la Plataforma Bogui y los servicios relacionados proporcionados por Bogui. La cantidad de la Cuota de Servicio puede variar pero Bogui deberá retenerla en su totalidad.\r\nTarifa de cancelación.\r\nDespués de solicitar un viaje, puede cancelarlo a través de la aplicación, pero tenga en cuenta que en ciertos casos puede aplicarse un cargo por cancelación. También se le puede cobrar si no se presenta después de solicitar un viaje. Consulte nuestro Centro de ayuda para obtener más información sobre la política de cancelación de Bogui, incluidos\r\nlos cargos aplicables. Las tarifas de cancelación recaudadas se pasarán en su totalidad al conductor que haya aceptado su solicitud de viaje descontando la comisión respectiva de Bogui.\r\nCuota de Daño.\r\nSi un conductor informa que usted ha dañado materialmente el vehículo del conductor, usted acuerda pagar una "cuota de daños" de hasta $ 300 dólares americanos o el equivalente a su moneda local dependiendo de la extensión del daño (según lo determine Bogui a su sola discreción) hacia la reparación o limpieza del vehículo. Bogui se reserva el derecho (pero no está obligado) a verificar o de otro modo requerir documentación de daños antes de procesar la Cuota de Daño. Los honorarios de daños acumulados se pasarán en su totalidad al conductor cuyo vehículo fue dañado.\r\nPeajes.\r\nEn algunos casos se pueden aplicar el cobro de peajes (o peajes de retorno) a su viaje. Consulte nuestro Centro de ayuda en la página de bogui.cl de su país. Para obtener más información sobre las tarifas de peaje y una lista de peajes aplicables y gastos de devolución. No garantizamos que la cantidad cobrada por Bogui coincida con el precio del peaje cargado al Conductor, en su caso. La cantidad de peajes cobrados (y los peajes de retorno) serán pagados en su totalidad al Conductor que proveyó su viaje descontando la respectiva comisión de Bogui.\r\nOtros cargos.\r\nOtras cuotas y recargos pueden aplicarse a su viaje, incluyendo: cuotas aeroportuarias reales o anticipadas, tarifas estatales o locales, cargos por eventos según lo determinado por Bogui o sus socios comerciales, y cargos de procesamiento por pagos divididos. Además, cuando lo requiera la ley, Bogui cobrará los impuestos aplicables. Estos otros cargos no se comparten con su conductor a menos que se indique expresamente lo contrario. Consulte la página de bogui.cl de su país para obtener información sobre otros cargos que pueden aplicarse a su viaje.\r\nConsejos.\r\nDespués de un viaje, usted puede elegir dar propina en efectivo a su conductor.\r\nGENERAL\r\nFACILITACION DE CARGOS.\r\nTodos los cargos se gestionan a través de un servicio de procesamiento de pagos de terceros (por ejemplo, Stripe Inc. Transbank,o Braintree una división de PayPal, Inc.). Bogui puede reemplazar sus servicios de procesamiento de pagos de terceros sin previo aviso. Los cargos se harán a través de la Plataforma Bogui.\r\nAdemás Bogui pone a disposición de los usuarios la opción de pagar un viaje en efectivo por los servicios de traslado prestados por un conductor , esto quiere decir que si un usuario decide cancelar en efectivo la tarifa por el servicio de traslado, es el conductor quien recaudará dicha tarifa en su totalidad, siendo responsabilidad del conductor cobrar dicha tarifa y proporcionando el cambio correcto en caso de ameritarlo, y es usted como conductor quien debe conservar dicha tarifa bajo su propio riesgo y disponer de ella , sin embargo usted reconoce y autoriza a Bogui Ltda que en contraprestación de los servicios de Bogui en relación a los servicios prestados por uso de la plataforma, usted adeudará a la empresa Bogui ltda la tasa de servicio respectivo, y será su total responsabilidad el pago de todos los impuestos que se generen en su País en relación con la tarifa en efectivo y la respectiva tasa por el uso de la plataforma Bogui, y de ser el caso dicha tasa se pagará libre de cualquier impuesto que le sea aplicable.\r\nTodos los montos generados de las tasas de comisión por el uso de la plataforma Bogui y cualquier otra cantidad adeudada a la empresa Bogui ltda por el uso de la plataforma, es una deuda activa y vigente que usted adquiere con la empresa, dichos montos serán compensados y deducidos por la empresa de los viajes realizados y completados con tarjeta, cuyos montos son recolectados directamente por Bogui Ltda.\r\nEn el fortuito caso que no existiese fondos suficientes recolectados por Bogui Ltda en su cuenta de conductor, para cubrir los montos adeudados por las tasas y comisiones fijadas , para los montos recolectados por el conductor en efectivo y otras deudas vigentes con la empresa, es su obligación proporcionar un medio de pago para saldar dicha deuda, en caso contrario usted mantendrá una deuda comercial con la empresa y esta se reserva el derecho de incrementar dichos montos cobrando un interés por mora hasta que haya liquidado dichos montos a la empresa. La empresa además podrá poner a disposición diversos medios de pago como por ejemplo (transferencia electrónica, puntos de cobro, web pay) u otro medio que la empresa estime idóneo para facilitar los pagos. Usted reconoce y acepta que asumirá todos los costos de cualquier comisión o tarifa que pudiesen generarse en relación al pago de la tarifa adeudada a la empresa, como por ejemplo comisiones por cambios de divisa, comisiones por transferencias bancarias, comisiones por giros internacionales además de cualquier tipo de impuestos al que pudiesen estar sometidos, agregando dichos montos a la empresa de tal forma que la empresa reciba el monto total e íntegro de la deuda. La empresa no renunciará a sus derechos de cobro respecto a las cantidades adeudadas en caso de no existir fondos suficientes recaudados en su cuenta por pagos con tarjeta de crédito, ni en caso que usted no haya enterado los montos adeudados por otros medios a la empresa, ni esperar que se genere algún monto en particular para iniciar las respectivas acciones de cobro. Bogui se reserva el derecho de desactivar aquellas cuentas que tengan alguna deuda vigente con la empresa, en caso de posteriormente regularizar dichas deudas la empresa no se cierra a la posibilidad que el usuario de la plataforma solicite una reactivación, la que será analizada y verificada, pudiendo no ser aceptada la solicitud.\r\nSin reembolsos.\r\nTodos los cargos no son reembolsables. Esta política de no reembolso se aplicará en todo momento, independientemente de su decisión de terminar el uso de la Plataforma Bogui, de cualquier interrupción de la Plataforma Bogui o de los Servicios, o cualquier otra razón.\r\nCréditos y Descuentos de Viajes.\r\nUsted puede recibir créditos ( "Créditos de Bogui") o descuentos de Viaje ( "Descuentos de Viaje") que usted puede aplicar para el pago de ciertos Cargos al completar un Viaje. Los Créditos Bogui y los Descuentos de viaje sólo son válidos para su uso en la Plataforma Bogui, y no son transferibles o canjeables por dinero en efectivo excepto como lo requiere la ley. Los Créditos Bogui y los Descuentos de Viaje no se pueden combinar, y si el costo de su viaje excede el crédito o descuento aplicable, cargaremos su saldo con el método de pago en el archivo por el costo pendiente del viaje. Los Descuentos de Viaje solo se aplican a la tarifa, no a la cuota de servicio u otros cargos. Si divide el pago de un viaje con otro Usuario, sus Créditos Bogui o Descuento de viaje solo se aplicarán a su parte de los Gastos. Las restricciones adicionales sobre los Créditos Bogui y los Descuentos de viaje pueden aplicarse según se le comuniquen en una promoción relevante o haciendo clic en el Crédito Bogui o en el Descuento de viaje correspondiente en la sección Pagos de la Aplicación Bogui.\r\nAutorización de Tarjeta de Crédito.\r\nMediante la adición de un nuevo método de pago o de cada solicitud de viaje, Bogui podrá solicitar la autorización de su método de pago seleccionado para verificar el método de pago, asegurarse de cubrir el costo del viaje y protegerlo contra conductas no autorizadas. La autorización no es un cargo, sin embargo, puede reducir su crédito disponible por el monto de la autorización hasta el ciclo de procesamiento siguiente de su banco. Si el monto de nuestra autorización\r\nsupera el total de los fondos depositados en su cuenta, es posible que el banco que emita su tarjeta de débito, crédito o cheque esté sujeto al sobregiro de los cargos de la ley de Bancos Local. No podemos ser responsables de estos cargos y no podemos ayudarle a recuperarlos de su banco emisor. Consulte nuestro Centro de ayuda para obtener más información sobre nuestro uso de retenciones previas a la autorización.\r\nPagos.\r\nSi usted es un Conductor, usted recibirá el pago por su provisión de Servicios. Todos los pagos de tarifas están sujetos a una Comisión Bogui, que se discute a continuación. Las propinas no estarán sujetas a ninguna Comisión de Bogui. Bogui procesará todos los pagos que se le deban a través de su procesador de pagos de terceros. Usted reconoce y acepta que tales cantidades no incluirán ningún interés y serán netas de cualquier cantidad que debemos retener por ley.\r\nComisión.\r\nA cambio de permitirle ofrecer sus Servicios a través de la Plataforma Bogui como un Conductor, usted acepta pagar a Bogui una tarifa basada en cada transacción en la que presta Servicios (la "Comisión"). El monto de la Comisión aplicable será del 15 % para cada una de las transacciones y para cada categoría aplica el mismo porcentaje. Bogui se reserva el derecho de cambiar la Comisión en cualquier momento a discreción de Bogui basada en factores del mercado local, y Bogui le proporcionará aviso en caso de tal cambio. El uso continuado de la Plataforma Bogui después de cualquier cambio en el cálculo de la Comisión constituirá su consentimiento para dicho cambio.\r\nPrecios.\r\nUsted autoriza expresamente a Bogui a fijar los precios en su nombre para todos los cargos que se aplican a la prestación de servicios. Bogui se reserva el derecho de cambiar el horario de la tarifa en cualquier momento a nuestra discreción en base a los factores locales del mercado, y le notificaremos en caso de cambios en la tarifa base, por kilómetro y / o por minuto que resultaría En un cambio en las tarifas aplicables. Los cargos pueden estar sujetos a los límites máximos establecidos en la página de bogui.cl de su País.\r\nAjuste de tarifas.\r\nBogui se reserva el derecho de ajustar o retener la totalidad o una parte de las tarifas si considera que ha intentado defraudar o abusar de los sistemas de pago de Bogui, para resolver una queja de un viaje (por ejemplo, no pudo finalizar correctamente una instancia particular de un Servicios en la aplicación Bogui, cuando el viaje haya terminado). La decisión de Bogui de ajustar o retener la tarifa de cualquier manera se ejercerá de manera razonable.\r\nComunicación.\r\nAl convertirse en un usuario, acepta recibir comunicaciones de nosotros, incluso por correo electrónico, mensajes de texto, llamadas y notificaciones push. Usted acepta que los textos, las llamadas o los mensajes pregrabados pueden ser generados por los sistemas de marcación telefónica automática. Las comunicaciones de Bogui, sus compañías afiliadas y / o controladores, pueden incluir, pero no se limitan a comunicaciones operacionales relativas a su cuenta de usuario o uso de la plataforma Bogui o Servicios, actualizaciones sobre características nuevas y existentes en la plataforma Bogui, Por nosotros o nuestros socios de terceros, y noticias sobre Bogui y los desarrollos de la industria. Los cargos estándar de mensajería de texto aplicados por su proveedor de telefonía celular se aplicarán a los mensajes de texto que enviemos.\r\nSI DESEA DESCONECTARSE DE CUALQUIER TIPO DE COMUNICADO VISITE NUESTRA PAGINA WEB.\r\nSU INFORMACION.\r\nSu Información es cualquier información que usted proporcione, publique o publique a través de la Plataforma de Bogui (incluyendo cualquier información de perfil que proporcione) o envíe a otros Usuarios (incluyendo comentarios en la aplicación, cualquier característica de correo electrónico, o a través de Facebook, Twitter o cualquier otra publicación de medios sociales) (su "Información"). Usted acepta que utilicemos su Información para crear una cuenta de usuario que le permita usar la Plataforma Bogui y participar en los Servicios. Nuestra recopilación y uso de información personal en relación con la Plataforma Bogui y los Servicios es tal como se provee en la Política de Privacidad de Bogui ubicada en www.bogui.cl/privacidad. Usted es el único responsable de su Información y sus interacciones con otros miembros del público, y actuamos sólo como un conducto pasivo para su publicación en línea de su Información. Usted acepta proporcionar y mantener información exacta, actual y completa y que nosotros y otros miembros del público podemos confiar en su información como exacta, actual y completa. Para permitir que Bogui utilice su Información, usted nos otorga un derecho no exclusivo, mundial, perpetuo, irrevocable, libre de regalías, transferible, sub licenciable (a través de múltiples niveles) y la licencia para ejercer los derechos de autor, publicidad y derechos de base de datos que Usted tiene en su Información, y para usar, copiar, realizar, exhibir y distribuir dicha Información para preparar trabajos derivados, o incorporar en otros trabajos, tal Información, en cualquier medio ahora conocido o no actualmente conocido. Bogui no asume ninguna propiedad sobre su Información; En su lugar, entre Bogui y usted, sujeto a los derechos que se nos otorgan en este Acuerdo, usted conserva plena propiedad de toda su Información y cualquier derecho de propiedad intelectual u otros derechos de propiedad relacionados con su Información.\r\nEs posible que pueda crear o iniciar sesión en su cuenta de usuario de Bogui a través de cuentas en línea que pueda tener con sitios de redes sociales de terceros (cada cuenta, una "cuenta SNS"). Al conectarse a Bogui a través de una cuenta SNS, usted entiende que Bogui puede acceder, almacenar y poner a disposición cualquier contenido de la cuenta SNS de acuerdo con la configuración de permisos de su cuenta SNS (por ejemplo, amigos, amigos mutuos, contactos o listas siguientes / seguidas "Contenido SNS")). Usted entiende que el Contenido SNS puede estar disponible a través de la Plataforma Bogui para otros Usuarios. A menos que se especifique lo contrario en este Acuerdo, todo el Contenido SNS, si lo hubiere, será considerado como su Información. (Social Network Service)\r\nPromociones y Programas de Referencia.\r\nBogui, a su entera discreción, puede hacer promociones disponibles con diferentes características a cualquier Usuario o posible Usuario. Estas promociones, a menos que se le hagan a usted, no tendrán ninguna influencia en su acuerdo o relación con Bogui. Bogui se reserva el derecho de retener o deducir créditos o beneficios obtenidos a través de una promoción, en el evento que Bogui determine o considere que la recaudación de la promoción o recepción del crédito o beneficio fue erróneo, fraudulento, ilegal o en violación de la promoción aplicable a los Términos del presente Acuerdo.\r\nComo parte de su cuenta de usuario, Bogui puede proporcionarle o permitirle crear un "Código Bogui", un código alfanumérico único para que usted distribuya a amigos, familiares y otras personas (cada uno un "Usuario Referido") para convertirse en un nuevo Bogui Usuario ( "Usuarios Referidos") o Conductores ( "Conductores Referidos"). Los códigos de Bogui sólo se pueden distribuir con fines promocionales y deben entregarse de forma gratuita. Usted no puede vender, comerciar, o hacer trueque con su código de Bogui. Está prohibido anunciar códigos Bogui, incluyendo pero no limitando a: Google, Facebook, Twitter, Bing, Instagram, WhatsApp, Telegram. Bogui se reserva el derecho de desactivar o invalidar cualquier código de Bogui en cualquier momento a discreción de Bogui.\r\nDe vez en cuando, Bogui puede ofrecerle incentivos para referir nuevos usuarios a la comunidad Bogui (el "Programa de Referencia"). Estos incentivos pueden venir en forma de Créditos Bogui, y Bogui puede establecer o cambiar los\r\ntipos de incentivos, cantidades, términos, restricciones y requisitos de calificación para cualquier incentivo a su sola discreción. La distribución de los códigos de Bogui y la participación en el Programa de Referencia están sujetos a este Acuerdo y a las reglas adicionales del Programa de Referencia.\r\nACTIVIDADES RESTRINGIDAS.\r\nCon respecto a su uso de la Plataforma Bogui y su participación en los Servicios, usted acepta no:\r\nSuplantar a cualquier persona o entidad;\r\nAcosar, amenazar o acosar a cualquier persona, o llevar armas;\r\nViolar cualquier ley, estatuto, regla, permiso, ordenanza o regulación;\r\nInterferir o interrumpir los Servicios o la Plataforma Bogui o los servidores o redes conectados a la Plataforma Bogui;\r\nPublicar información o interactuar en la Plataforma Bogui o Servicios de una manera que sea falsa, inexacta, engañosa (directamente o por omisión o no actualización de información), difamatoria, abusiva, obscena, profana, ofensiva, sexualmente orientada, amenazante, hostigadora, O ilegal;\r\nUtilizar la Plataforma Bogui de cualquier forma que infrinja los derechos de terceros, incluyendo pero no limitado a: derechos de propiedad intelectual, derechos de autor, patentes, marcas registradas, secretos comerciales u otros derechos de propiedad o derechos de publicidad o privacidad;\r\nEnviar, por correo electrónico o de otra forma transmitir cualquier código malicioso, archivos o programas diseñados para interrumpir, dañar, destruir o limitar la funcionalidad de cualquier software o hardware o equipo de telecomunicaciones o interceptar o expropiar cualquier sistema, datos o información personal;\r\nForjar cabeceras o de otra manera manipular identificadores para disfrazar el origen de cualquier información transmitida a través de la Plataforma Bogui;\r\n"Marco" o "espejo" de cualquier parte de la Plataforma Bogui, sin nuestra autorización previa por escrito o usar etiquetas, códigos u otros dispositivos que contengan cualquier referencia a nosotros para dirigir a cualquier persona a cualquier otro sitio web para cualquier propósito; o\r\nModificar, adaptar, traducir, realizar ingeniería inversa, descifrar, descompilar o desmontar cualquier parte de la Plataforma Bogui o cualquier software utilizado en o para la Plataforma Bogui; Alquilar, , prestar, vender, redistribuir, licenciar o sub licenciar la Plataforma Bogui o acceder a cualquier parte de la Plataforma Bogui;\r\nUtilizar cualquier robot, araña, aplicación de búsqueda / recuperación de sitio u otro dispositivo o proceso manual o automático para recuperar, indexar, raspar, "extraer datos" o de alguna manera reproducir o eludir la estructura de navegación o presentación de la Plataforma Bogui o de su contenido;\r\nEnlace directo o indirecto a cualquier otro sitio web;\r\nTransferir o vender su cuenta de usuario, contraseña y / o identificación a cualquier otra parte\r\nDiscriminar u hostigar a cualquiera en base a raza, origen nacional, religión, género, identidad de género, discapacidad física o mental, condición médica, estado civil, edad u orientación sexual, o\r\nHacer que cualquier tercero se involucre en las actividades restringidas arriba.\r\nGARANTIAS, ACUERDOS Y REPRESENTACION DE CONDUCTORES.\r\nAl proporcionar Servicios como conductor en la Plataforma Bogui, usted representa, garantiza y acepta que:\r\nUsted posee una licencia de conducir válida y está autorizado y médicamente apto para operar un vehículo de motor y tiene todas las licencias, aprobaciones y autoridad apropiadas para proveer transporte a Usuarios en todas las regiones o jurisdicciones en las cuales usted provee Servicios.\r\nUsted posee, o tiene el derecho legal de operar, el vehículo que usted usa al proveer servicios, y dicho vehículo está en buenas condiciones de operación y cumple con las normas de seguridad de la industria y todos los requisitos legales y estatales.\r\nUsted no se involucrará en conducta temeraria mientras maneja, maneja con locura, maneja un vehículo que no es seguro para conducir, se involucra en un accidente de automóvil o colisión de cualquier tipo, permite que un tercero no autorizado le acompañe en el vehículo mientras presta servicios, Proporcionar servicios como conductor mientras está bajo la influencia de alcohol o drogas, o tomar medidas que dañen o amenacen con dañar la seguridad de la comunidad de Bogui o de terceros.\r\nUsted sólo proporcionará servicios utilizando el vehículo que ha sido reportado y aprobado por Bogui, y para el cual se le ha proporcionado una fotografía a Bogui, y no transportará más pasajeros que puedan estar sentado en dicho vehículo, y no más de los autorizados en cualquier caso.\r\nNo hará ninguna declaración falsa con respecto a Bogui, la Plataforma Bogui, los Servicios o su estado como Conductor.\r\nUsted no podrá, mientras presta los Servicios, operar como un servicio público de transporte o taxi, aceptar salidas de la calle, cobrar por los viajes (excepto lo expresamente previsto en este Acuerdo), exigir que un usuario pague en efectivo excepto que dicho viaje haya sido solicitado con antelación en la aplicación de Bogui como un viaje en efectivo, caso contrario el o los cobros se harán automáticamente por Bogui , o use un lector de tarjetas de crédito, Tal como un Lector , para aceptar el pago o para emprender cualquier otra actividad de una manera que sea inconsistente con sus obligaciones bajo este acuerdo.\r\nUsted acepta que podremos obtener información sobre usted, incluyendo sus antecedentes penales y de conducción, y usted acepta proporcionar cualquier otra autorización necesaria para facilitar nuestro acceso a dichos registros durante el término del Acuerdo.\r\nUsted tiene una póliza válida de seguro de responsabilidad civil (en cantidades de cobertura consistentes con todos los requisitos legales aplicables) que lo autoriza para operar un vehículo para proporcionar servicios.\r\nUsted no intentará defraudar a Bogui o usuarios en la Plataforma de Bogui o en conexión con su provisión de Servicios. Si sospechamos que usted ha participado en una actividad fraudulenta, podemos retener las tarifas aplicables u otros pagos por el viaje(s) en cuestión.\r\nUsted pagará todos los impuestos federales, estatales y locales aplicables basados en su provisión de Servicios y cualquier pago recibido por usted.\r\nPropiedad intelectual.\r\nTodos los derechos de propiedad intelectual de la plataforma Bogui serán propiedad de Bogui de forma absoluta y total. Estos derechos incluyen y no se limitan a los derechos de base de datos, derechos de autor, derechos de diseño (ya sean registrados o no), marcas registradas (registradas o no registradas) y otros derechos similares dondequiera\r\nque exista en el mundo junto con el derecho a solicitar la protección de los mismos. Todas las demás marcas comerciales, logotipos, marcas de servicio, nombres de empresas o productos establecidos en la plataforma Bogui son propiedad de sus respectivos propietarios. Usted reconoce y acepta que cualquier pregunta, comentario, sugerencia, idea, retroalimentación u otra información proporcionada por usted a nosotros no son confidenciales y serán propiedad exclusiva de Bogui. Bogui tendrá derechos exclusivos, incluyendo todos los derechos de propiedad intelectual, y tendrá derecho al uso y difusión sin restricciones de estos Envíos para cualquier propósito, comercial o de otro tipo, sin reconocimiento ni compensación para usted.\r\nBogui y otros logotipos, diseños, gráficos, iconos, scripts y nombres de servicio de Bogui son marcas registradas, marcas registradas o marcas registradas de Bogui en Chile y / o en otros países (colectivamente, las "Marcas Bogui"). Si usted presta servicios como un Conductor, Bogui le otorga, durante la vigencia de este Contrato, y sujeto a su cumplimiento con los términos y condiciones de este Acuerdo, una licencia limitada, revocable y no exclusiva para exhibir y usar las Marcas Bogui Únicamente en relación con la prestación de los Servicios a través de la Plataforma Bogui ( "Licencia"). La Licencia es intransferible y no puede ser cedida, y no concederá a terceros ningún derecho, permiso, licencia o sub licencia con respecto a ninguno de los derechos concedidos sin el permiso previo por escrito de Bogui, que podrá retener en su exclusiva discreción. Las Marcas Bogui no pueden ser utilizadas de ninguna manera que pueda causar confusión.\r\nUsted reconoce que Bogui es el propietario y licenciante de las Marcas Bogui, incluyendo toda buena voluntad asociada con las mismas, y que su uso de las Marcas Bogui no conferirá ningún interés o propiedad adicional de las Marcas Bogui en usted, sino más bien beneficios en beneficio de Bogui. Usted acepta utilizar las marcas Bogui estrictamente de acuerdo con las Pautas de Uso de Marcas registradas de Bogui, que pueden ser proporcionadas a usted y revisadas de vez en cuando, e inmediatamente cesar cualquier uso que Bogui considere no conforme o de otra manera inaceptable.\r\nUsted acepta que no:\r\n(1) creará materiales que incorporen las Marcas Bogui o cualquier derivado de las Marcas Bogui que no sean expresamente aprobadas por escrito;\r\n(2) utilizar las Marcas Bogui de cualquier forma que tienda a menoscabar su validez como marcas registradas, marcas de servicio, nombres comerciales o marcas comerciales, o usar las Marcas Bogui, excepto de acuerdo con los términos, condiciones y restricciones de la presente;\r\n(3) tomar cualquier otra acción que pudiera poner en peligro o menoscabar los derechos de Bogui como propietario de las Marcas de Bogui o la legalidad y / o aplicabilidad de las Marcas de Bogui, incluyendo, sin limitación, el desafío u oposición de la propiedad de Bogui en las Marcas de Bogui;\r\n(4) solicitar el registro de la marca registrada o la renovación del registro de marca registrada de cualquiera de las Marcas Bogui, o cualquier derivado de las Marcas Bogui, cualquier combinación de las Marcas Bogui y cualquier otro nombre o marca, marca de servicio, nombre comercial, símbolo o palabra Que es similar a las marcas de Bogui;\r\n(5) usar las Marcas Bogui en o en conexión con cualquier producto, servicio o actividad que viole alguna ley, estatuto, regulación gubernamental o norma.\r\nLa infracción de cualquier disposición de esta Licencia puede resultar en la terminación inmediata de la Licencia, a discreción de Bogui. Si crea cualquier material que lleve las Marcas Bogui (en violación de este Contrato o de otro modo), usted acepta que Bogui, en el momento de su creación, posee exclusivamente todos los derechos, títulos e intereses sobre dichos materiales, incluyendo sin limitación cualquier modificación a las Marcas Bogui o Trabajos derivados basados en las marcas de Bogui. Asimismo, acepta asignar a Bogui cualquier interés o derecho que pueda tener sobre dichos materiales, así como proporcionar información y ejecutar cualquier documento que razonablemente solicite Bogui para que Bogui pueda formalizar dicha asignación.\r\nBogui respeta la propiedad intelectual de los demás, y espera que los usuarios hagan lo mismo. Si cree, de buena fe, que cualquier material de la Plataforma Bogui o Servicios infringe sus derechos de autor, visite la página Política de derechos de autor para obtener información sobre cómo presentar una reclamación por derechos de autor.\r\nDescargo de responsabilidad.\r\nLas siguientes renuncias se hacen en nombre de Bogui, nuestros afiliados, subsidiarias, sucesores y cesionarios, y cada uno de nuestros respectivos funcionarios, directores, empleados, agentes y accionistas.\r\nBogui no proporciona servicios de transporte, y Bogui no es un transportista. Corresponde al Controlador decidir si ofrecer o no un viaje a un usuario contactado a través de la Plataforma Bogui, y es responsabilidad del usuario decidir si acepta o no un viaje desde cualquier Conductor contactado a través de la Plataforma Bogui. No podemos asegurar que un Conductor o Usuario complete un servicio de transporte contratado. No tenemos control sobre la calidad o seguridad del transporte que se produce como resultado de los Servicios.\r\nLa Plataforma Bogui se suministra "tal como está" y sin ninguna garantía o condición, expresa, implícita o legal. No garantizamos ni prometemos ningún resultado específico del uso de la Plataforma de Bogui y / o de los Servicios, incluyendo la capacidad de proporcionar o recibir Servicios en cualquier lugar o tiempo dado. Expresamos específicamente ninguna garantía implícita de título, comerciabilidad, aptitud para un propósito particular y no infracción. Algunos estados no permiten la renuncia de garantías implícitas, por lo que la renuncia anterior puede no aplicarse a usted.\r\nNo garantizamos que el uso de la plataforma o servicios de Bogui sea exacto, completo, confiable, actual, seguro, ininterrumpido, siempre disponible o libre de errores, o satisfacer sus requisitos, que cualquier defecto en la Plataforma de Bogui será Corregido o que la Plataforma Bogui esté libre de virus u otros componentes dañinos. Renunciamos a responsabilidad por, y no se hace garantía con respecto a, la conectividad y la disponibilidad de la Plataforma Bogui o Servicios.\r\nNo podemos garantizar que cada usuario sea quien él o ella dice ser. Por favor, utilice el sentido común cuando utilice la Plataforma Bogui y los Servicios, incluyendo mirar las fotos del Conductor o Pasajero con el que ha coincidido para asegurarse de que es la misma persona que usted ve en persona. Tenga en cuenta que también hay riesgos de tratar con personas menores de edad o personas que actúen bajo falsas pretensiones y no aceptamos responsabilidad alguna por ningún contenido, comunicación u otro uso o acceso de la Plataforma Bogui por personas menores de 18 años en violación Del presente Acuerdo. Le recomendamos a comunicarse directamente con cada conductor o pasajero potencial antes de contratar un servicio de transporte organizado.\r\nBogui no es responsable de la conducta, ya sea en línea o fuera de línea, de cualquier Usuario de la Plataforma o Servicios de Bogui. Usted es el único responsable de sus interacciones con otros usuarios. Nosotros no compramos seguro ni somos responsables por las pertenencias personales dejadas en el vehículo por los Conductores o Pasajeros. Al utilizar la Plataforma Bogui y participar en los Servicios, usted acepta dichos riesgos y acepta que Bogui no es responsable de los actos u omisiones de los Usuarios en la Plataforma Bogui o de participar en los Servicios.\r\nBogui renuncia expresamente a cualquier responsabilidad derivada del uso no autorizado de su cuenta de usuario. Si usted sospecha que cualquier parte o ente no autorizado puede estar usando su cuenta de usuario o sospecha cualquier otro incumplimiento de seguridad, usted acepta notificarnos inmediatamente.\r\nEs posible que otros obtengan información sobre usted que usted proporcione, publique o publique a través de la Plataforma de Bogui (incluyendo cualquier información de perfil que proporcione), envíe a otros Usuarios, o comparta durante los Servicios, y use dicha información para hostigar O hacerle daño. No somos responsables del uso de cualquier información personal que usted revele a otros Usuarios en la Plataforma de Bogui o a través de los Servicios. Por favor, seleccione cuidadosamente el tipo de información que publica en la Plataforma de Bogui o a través de los Servicios o comunique a otros. Excluimos toda responsabilidad, independientemente de la forma de acción, por los actos u omisiones de otros Usuarios (incluidos los usuarios no autorizados, o "piratas informáticos").\r\nLas opiniones, consejos, declaraciones, ofertas, u otra información o contenido relacionado con Bogui o disponible a través de la Plataforma Bogui, pero no directamente por nosotros, son los de sus respectivos autores, y no necesariamente se debe confiar en ellos. Dichos autores son los únicos responsables por tal contenido. Bajo ninguna circunstancia seremos responsables de ninguna pérdida o daño que resulte de su dependencia de información u otro contenido publicado por terceros, ya sea en la plataforma de Bogui o de otra manera. Nos reservamos el derecho, pero no tenemos ninguna obligación, de monitorear los materiales publicados en la Plataforma Bogui y eliminar cualquier material que en nuestra opinión viole o viola la ley o este contrato o que puede ser ofensivo, ilegal, o que pueda violar los derechos, dañar o amenazar la seguridad de los Usuarios u otros.\r\nLos datos de ubicación proporcionados por la plataforma Bogui son sólo para fines de localización básica y no se pretende que se basen en situaciones en las que se necesite información de ubicación precisa o donde datos erróneos, inexactos o incompletos puedan causar muerte, lesiones personales, daños materiales o ambientales. Ni Bogui, ni ninguno de sus proveedores de contenido, garantiza la disponibilidad, exactitud, integridad, fiabilidad u oportunidad de los datos de localización seguidos o mostrados por la Plataforma Bogui. Cualquier parte de su Información, incluidos los datos geo localizados, que suba, proporcione o publique en la Plataforma Bogui puede ser accesible para Bogui y para determinados Usuarios de la Plataforma Bogui.\r\nBogui le aconseja utilizar la Plataforma Bogui con un plan de datos con límites ilimitados o muy altos de uso de datos, y Bogui no será responsable de ninguna tarifa, costo o sobrecarga asociada con cualquier plan de datos que use para acceder a la Plataforma Bogui.\r\nDIVULGACIONES ESTATALES Y LOCALES.\r\nIndemnización.\r\nUsted defenderá, indemnizará y mantendrá a Bogui, incluidos nuestros afiliados, subsidiarias, sucesores y cesionarios, y cada uno de nuestros respectivos funcionarios, directores, empleados, agentes o accionistas, de cualquier reclamación, acción, demanda, pérdida, costo o pasivo. Y gastos (incluyendo honorarios razonables de abogados) relacionados o derivados de su uso de la Plataforma Bogui y la participación en los Servicios, incluyendo:\r\n(1) su incumplimiento de este Acuerdo o los documentos que incorpora por referencia;\r\n(2) la violación de cualquier ley o los derechos de un tercero, incluyendo, sin limitación, los Conductores, Pasajeros, otros conductores y peatones, como resultado de su propia interacción con dicho tercero;\r\n(3) cualquier alegación de que cualquier material que nos envíe o transmita a través de la Plataforma Bogui o infrinja o viole de alguna manera los derechos de autor, marcas comerciales, secretos comerciales u otros derechos de propiedad intelectual u otros derechos de terceros;\r\n(4) su propiedad, uso u operación de un vehículo de motor o de pasajeros, incluyendo su provisión de Servicios como Conductor; Y / o\r\n(5) cualquier otra actividad relacionada con los Servicios. Esta indemnización será aplicable sin tener en cuenta la negligencia de cualquier parte, incluyendo cualquier persona indemnizada.\r\nLimitación de responsabilidad.\r\nEN NINGÚN CASO, BOGUI , INCLUYENDO NUESTROS AFILIADOS, SUBSIDIARIOS, SUCESORES Y ASIGNADOS Y CADA UNO DE NUESTROS RESPECTIVOS OFICIALES, DIRECTORES, EMPLEADOS, AGENTES O ACCIONISTAS (COLECTIVAMENTE "BOGUI" PARA LOS FINES DE ESTA SECCIÓN), SERÁ RESPONSABLE ANTE USTED POR CUALQUIER DAÑO INCIDENTAL, ESPECIAL, EJEMPLAR, PUNITIVO, CONSECUENCIAL O INDIRECTO (INCLUYENDO, PERO NO LIMITADO A, DAÑOS POR ELIMINACIÓN, CORRUPCIÓN, PÉRDIDA DE DATOS, PÉRDIDA DE PROGRAMAS, FALTA DE ALMACENAR CUALQUIER INFORMACIÓN U OTRO CONTENIDO MANTENIDO O TRANSMITIDO POR LA PLATAFORMA BOGUI, INTERRUPCIONES DE SERVICIO O POR EL COSTO DE LA CONTRATACIÓN DE SERVICIOS DE SUSTITUCIÓN) DERIVADOS DE O EN RELACIÓN CON LA PLATAFORMA BOGUI, LOS SERVICIOS O ESTE ACUERDO, SIN EMBARGO QUE INCLUYEN NEGLIGENCIA, INCLUSO SI NOSOTROS O NUESTROS AGENTES O REPRESENTANTES CONOCEN O HAN SIDO AVISADOS DE LA POSIBILIDAD DE TALES DAÑOS. LA PLATAFORMA BOGUI PUEDE SER UTILIZADA POR USTED PARA SOLICITAR Y CALIFICAR EL TRANSPORTE, BIENES U OTROS SERVICIOS CON PROVEEDORES DE TERCEROS, USTED ACEPTA QUE BOGUI NO TIENE RESPONSABILIDAD O RESPONSABILIDAD CON USTED RELACIONADO CON NINGUN TRANSPORTE, BIENES U OTROS SERVICIOS PROPORCIONADOS A USTED POR TERCEROS PROVEEDORES DE ESTA QUE NO SEAN EXPRESAMENTE ESTABLECIDOS EN ESTOS TÉRMINOS. CIERTAS JURISDICCIONES NO PERMITEN LA EXCLUSIÓN O LIMITACIÓN DE CIERTOS DAÑOS. SI ESTAS LEYES SE APLICAN A USTED, ALGUNAS O TODAS LAS RENUNCIAS ANTERIORES, EXCLUSIONES O LIMITACIONES PUEDEN NO APLICARSE A USTED, Y USTED PUEDE TENER DERECHOS ADICIONALES.\r\nDuración y Terminación.\r\nEste Acuerdo es efectivo a la creación de una cuenta de usuario. El presente Contrato podrá ser rescindido:\r\na) por el Usuario, sin causa, previa notificación escrita de siete (7) días a Bogui; O\r\nb) por cualquiera de las Partes inmediatamente, sin previo aviso, por el incumplimiento material de este Acuerdo por parte de la otra Parte, incluyendo pero no limitado a cualquier incumplimiento de las Secciones de este Acuerdo. Además, Bogui puede rescindir este Acuerdo o desactivar su cuenta de usuario inmediatamente en caso de que:\r\n(1) usted ya no califique para prestar servicios o para operar el vehículo aprobado bajo la ley, regla, permiso, ordenanza o regulación aplicable;\r\n(2) cae por debajo de la calificación de Bogui o el umbral de cancelación;\r\n(3) Bogui cree de buena fe que tal acción es necesaria para proteger la seguridad de la comunidad de Bogui o de terceros, siempre que en caso de una desactivación conforme a los (1) - (3) anteriores, se le dará aviso De la desactivación potencial o real y una oportunidad de intentar sub sanar el problema a la satisfacción razonable de Bogui antes de que Bogui rescindiera definitivamente el Acuerdo. Para todas las demás infracciones de este Acuerdo, se le proporcionará aviso y una oportunidad para sub sanar el incumplimiento. Si el incumplimiento se resuelve de manera oportuna y a satisfacción de Bogui, este Contrato no será terminado de manera permanente. Las secciones (con respecto a la licencia), sobrevivirán a cualquier terminación o vencimiento de este Acuerdo.\r\nRESOLUCIÓN DE DISPUTAS Y ACUERDO DE ARBITRAJE.\r\n(A) Acuerdo de Arbitraje Vinculante entre usted y Bogui.\r\nUSTED Y BOGUI MUTUAMENTE ACUERDAN RENUNCIAR A NUESTROS DERECHOS RESPECTIVOS Y A LA RESOLUCIÓN DE CONTROVERSIAS EN UN TRIBUNAL DE JUSTICIA POR UN JUEZ O JURADO Y ACEPTAN RESOLVER CUALQUIER DISPUTA POR ARBITRAJE, como se establece a continuación. Este acuerdo de arbitraje ("Acuerdo de Arbitraje") se rige por la Ley Nacional de Arbitraje número 19971 del Ministerio de Justicia y sobrevive después de que el Acuerdo termine o su relación con Bogui termine. CUALQUIER ARBITRAJE EN VIRTUD DE ESTE ACUERDO SE REALIZARÁ DE FORMA INDIVIDUAL; ARBITRACIONES DE CLASES Y ACCIONES DE CLASE NO SON PERMITIDAS. Excepto lo expresamente estipulado a continuación, este Acuerdo de Arbitraje se aplica a todas las Reclamaciones (definidas a continuación) entre usted y Bogui, incluyendo nuestras afiliadas, subsidiarias, sucesores y cesionarios y cada uno de nuestros respectivos funcionarios, directores, empleados, agentes o accionistas.\r\nSalvo lo expresamente estipulado a continuación, TODAS LOS CONFLICTOS Y RECLAMACIONES ENTRE NOSOTROS (CADA UNA "RECLAMACIÓN" Y "RECLAMACIONES") SERÁN EXCLUSIVAMENTE RESOLVIDAS POR ARBITRAJE VINCULANTE SOLAMENTE ENTRE USTED Y BOGUI. Estas Reclamaciones incluyen, pero no se limitan a, ninguna controversia, reclamo o controversia, ya sea basada en acontecimientos pasados, presentes o futuros, que surjan de o se relacionen con este Contrato y versiones anteriores del mismo (incluyendo el incumplimiento, Interpretación o validez del mismo), la Plataforma Bogui, los Servicios, y cualquier otro bien o servicio disponible a través de la Plataforma Bogui, su relación con Bogui, la suspensión, desactivación o terminación de su Cuenta de Usuario o de este Acuerdo, Las promociones u ofertas hechas por Bogui, cualquier ley de remuneraciones, secretos comerciales, competencia desleal, compensación, descansos y , reembolso de gastos, rescisión indebida , Discriminación, acoso, represalia, fraude, difamación, angustia emocional, incumplimiento de cualquier contrato o pacto expreso o implícito, reclamos que surjan bajo las leyes locales o estatales de protección al consumidor; Reclamaciones derivadas de leyes antimonopolio, reclamaciones derivadas de la Ley de Protección del Consumidor Telefónico y de la Ley de Notificación Justa de Créditos; Ley de Derechos Civiles de 1964, Ley de Discapacidades, Ley de Discriminación por Edad en el Empleo, Ley de Protección de Beneficios de Trabajadores Mayores, Ley de Licencia Médica Familiar, Ley de Normas de Trabajo Justo, Para reclamos individuales de beneficios de empleados bajo cualquier plan de beneficios patrocinado por Bogui y cubierto por la Ley de Seguridad social de Jubilados o financiado por seguro), y demás estatutos locales, si los hubiere, tratando temas iguales o similares; Locales y de derecho común. Todas las disputas relativas a la arbitrariedad de una Reclamación (incluyendo disputas sobre el alcance, aplicabilidad, ejecutabilidad, revocabilidad o validez del Acuerdo de Arbitraje) serán decididas por el árbitro, excepto como se indica expresamente a continuación.\r\nAL COMPROMISO DE ARBITRAJE, USTED ENTIENDE QUE USTED Y BOGUI ESTÁN RENUNCIANDO AL DERECHO DE SENTARSE EN EL TRIBUNAL O TENER UN JUICIO DE JURADO PARA TODAS LAS RECLAMACIONES, EXCEPTO LO EXPRESAMENTE DE OTRA FORMA DISPUESTO EN ESTE ACUERDO DE ARBITRAJE. El presente Acuerdo de Arbitraje tiene por objeto exigir el arbitraje de toda reclamación o controversia que pueda arbitrarse legalmente, con excepción de aquellas reclamaciones y controversias que por los términos del presente Acuerdo de Arbitraje estén expresamente excluidas del requisito de arbitraje.\r\n(B) Prohibición de Reclamos de Clase y Socorro No Individualizado.\r\nUSTED ENTIENDE Y ACEPTA QUE ENTRE USTED Y BOGUI PUEDEN HACER RECLAMACIONES EN ARBITRAJE CONTRA OTRO SÓLO EN UNA CAPACIDAD INDIVIDUAL Y NO EN UNA CLASE, UNA ACCIÓN COLECTIVA O UNA BASE REPRESENTATIVA ("RENUNCIA DE ACCIÓN DE CLASE"). USTED ENTIENDE Y ACEPTA QUE ENTRE USTED Y BOGUI AMBOS ESTÁN RENUNCIANDO AL DERECHO A TENER UNA DISPUTA RESUELTA COMO DEMANDANTE O CLASE EN CUALQUIER PROCESO DE CLASE, COLECTIVO O REPRESENTANTE SUSTENTADO. A PESAR DE LO ANTERIOR, ESTA SUBSECCIÓN (B) NO SE APLICARÁ A LAS RECLAMACIONES REPRESENTANTES DE LA LEY GENERAL DE ABOGADOS PRIVADOS CONTRA BOGUI, QUE SE DIRIGEN POR SEPARADO.\r\nEl árbitro no tendrá autoridad para considerar o resolver cualquier Reclamación o emitir ningún alivio sobre cualquier base que no sea individual. El árbitro no tendrá autoridad para considerar o resolver ninguna Reclamación o emitir ningún alivio en base a una clase, colectiva o representativa.\r\nNo obstante cualquier otra disposición de este Acuerdo, el Acuerdo de Arbitraje o las Reglas, las disputas referentes al alcance, aplicabilidad, ejecutabilidad, revocabilidad o validez de la Renuncia de Acción Colectiva podrán ser resueltas únicamente por un tribunal civil de jurisdicción competente y no por un árbitro. En cualquier caso en que: (1) la disputa se presente como una acción de clase, colectiva o representativa y hay una determinación judicial definitiva de que la Exención de Acción de Clase es inaplicable en cuanto a Reclamaciones, / O acción representativa en tales demandas debe ser litigada en un tribunal civil de la jurisdicción competente, pero la renuncia de acción colectiva se hará cumplir en el arbitraje sobre una base individual en cuanto a todas las otras reclamaciones en la mayor medida posible.\r\n(C) Renuncia Representativa de PAGA.\r\nNo obstante cualquier otra disposición de este Acuerdo o del Acuerdo de Arbitraje, en la mayor medida permitida por la ley: (1) usted y Bogui acuerdan no presentar una acción representativa en nombre de terceros bajo la Ley de Abogados Privados en un tribunal o en arbitraje, y (2) por cualquier reclamación presentada en base a un procurador general privado, tanto usted como Bogui acuerdan que cualquier disputa será resuelta En el arbitraje sobre una base individual (es decir, para resolver si usted ha sido personalmente agraviado o sujeto a cualquier violación de la ley), y que tal acción no puede ser utilizada para resolver las reclamaciones o derechos de otras personas en una sola o colectiva (Es decir, para resolver si otros individuos han sido agraviados o sujetos a cualquier violación de la ley) (colectivamente,). No obstante cualquier otra disposición de este Acuerdo, el Acuerdo de Arbitraje, las disputas sobre el alcance, aplicabilidad, ejecutabilidad, revocabilidad o validez de este Representante Renuncia, sólo podrán ser resueltas por un tribunal civil de jurisdicción competente y no por un árbitro.\r\nComo parte del arbitraje, tanto usted como Bogui tendrán la oportunidad de encontrar razonablemente información no privilegiada que sea relevante para la Reclamación. El árbitro podrá otorgar cualquier recurso individualizado que esté disponible en los tribunales. El árbitro puede otorgar una medida declarativa o una medida cautelar solamente en favor de la parte individual que solicita el reclamo y sólo en la medida en que sea necesario para proporcionar el reclamo garantizado por las reclamaciones individuales de esa parte. El árbitro presentará una declaración motivada por escrito de la decisión del árbitro que explicará la decisión otorgada y las conclusiones en las que se basa la decisión.\r\nEl árbitro decidirá la sustancia de todas las reclamaciones de acuerdo con la ley aplicable y honrará todas las reclamaciones de privilegio reconocidas por la ley. El árbitro no estará obligado por decisiones en arbitrajes previos que involucren a diferentes Pilotos o Conductores, pero está obligado por decisiones en arbitrajes previos que involucren al mismo Usuario o Conductor en la medida requerida por la ley aplicable. El fallo del árbitro será definitivo y vinculante y el fallo dictado por el árbitro podrá ser inscrito en cualquier tribunal que tenga jurisdicción, siempre y cuando cualquier fallo pueda ser impugnado ante un tribunal competente.\r\nConfidencialidad\r\nUsted se compromete a no utilizar ningún tipo de información técnica, financiera, estratégica y de otra índole relacionada con los negocios, operaciones y propiedades de Bogui, incluida la información del Usuario ( "Información Confidencial") revelada por Bogui para su propio uso o para cualquier otro propósito que no sea Como se contempla aquí. Usted no revelará ni permitirá la divulgación de ninguna Información Confidencial a terceros. Usted acepta tomar todas las medidas razonables para proteger el secreto y evitar la divulgación o el uso de la Información Confidencial de Bogui con el fin de evitar que caiga en el dominio público. Sin perjuicio de lo anterior, usted no tendrá ninguna\r\nresponsabilidad con Bogui con respecto a cualquier Información Confidencial que pueda probar que estaba en el dominio público en el momento en que fue revelado por Bogui o ha entrado en el dominio público sin culpa suya y que fue conocido por usted, sin restricciones, en el momento de la divulgación, como lo demuestran los archivos existentes en el momento de la divulgación; Se revela con la previa aprobación por escrito de Bogui; Se le conoce, sin restricciones, de una fuente distinta de Bogui sin incumplimiento de este Acuerdo por usted y de otra manera no viola los derechos de Bogui; O se revela de conformidad con la orden o el requisito de un tribunal, agencia administrativa u otro organismo gubernamental; Siempre que, sin embargo, provea notificación inmediata de tal orden judicial o requisito a Bogui para permitir que Bogui solicite una orden de protección o y de otra manera prevenga o restrinja dicha divulgación.\r\nRelación con Bogui.\r\nComo Conductor en la Plataforma Bogui, usted reconoce y acepta que usted y Bogui están en una relación comercial directa, y la relación entre las partes bajo este Acuerdo es únicamente la de las partes contratantes independientes. Usted y Bogui expresamente están de acuerdo en que (1) esto no es un contrato de trabajo y no crea una relación laboral entre usted y Bogui o Bogui Ltda; Y (2) ninguna relación de empresa conjunta, franquiciador-franquiciado, asociación o agencia está pensada o creada por este Acuerdo. No tiene autoridad para obligar a Bogui, y se compromete a no presentarse como empleado, agente o representante autorizado de Bogui.\r\nBogui no ordena o controla en general o en su desempeño bajo este Acuerdo específicamente, incluyendo en conexión con su provisión de Servicios, sus actos u omisiones, o su operación y mantenimiento de su vehículo. Usted conserva el derecho exclusivo de determinar cuándo, dónde y durante cuánto tiempo utilizará la Plataforma Bogui. Usted conserva la opción de aceptar, rechazar o ignorar la solicitud de servicios de Conductor por medio de la Plataforma Bogui, o cancelar una solicitud de servicios aceptada a través de la Plataforma Bogui, sujeto a las políticas de cancelación de Bogui. Con excepción de cualquier señalización requerida por la ley, las reglas o requisitos de licencia / licencia, Bogui no tendrá derecho a exigirle que: (a) muestre los nombres, logotipos o colores de Bogui en su (s) vehículo (s); O (b) usar un uniforme o cualquier otra ropa que muestre los nombres, logotipos o colores de Bogui. Usted reconoce y acepta que tiene total discreción para proveer Servicios o participar de otra manera en otras actividades comerciales o laborales.\r\nOtros servicios.\r\nAdemás de conectar Pasajeros con Conductores, Bogui puede permitir que los Usuarios proporcionen o reciban bienes o servicios de terceros. Por ejemplo, los Usuarios pueden usar la Plataforma Bogui para ordenar una entrega de mercancías, comprar un artículo digital, solicitar un viaje en una modalidad que permite que varios usuarios viajen en una misma dirección, o cuando viaje fuera del territorio Nacional, para conectarse con el transporte local a las Plataformas y solicitudes de viajes de los conductores locales (colectivamente, los "Otros Servicios"). Usted entiende que los Otros Servicios están sujetos a los términos y precios del proveedor de terceros. Si opta por comprar otros servicios a través de la plataforma Bogui, autoriza a Bogui a cargar su método de pago en el archivo de acuerdo con las condiciones de precio establecidas por el proveedor de terceros. Usted acepta que Bogui no es responsable y no puede ser considerado responsable por los Otros Servicios o las acciones u omisiones del proveedor de terceros. Tales Otros Servicios no podrán ser investigados, monitoreados o verificados por la exactitud, pertinencia o integridad por nosotros, y nosotros no somos responsables de ningún otro Servicio a través de la Plataforma Bogui.\r\nGeneral.\r\nEste Acuerdo se regirá por las leyes del Estado sin tener en cuenta los principios de elección de la ley. Si alguna de las disposiciones de este Acuerdo es o se convierte en inválida o no vinculante, las partes permanecerán vinculadas por todas las demás disposiciones del presente. En tal caso, las partes sustituirán la disposición inválida o no vinculante\r\npor disposiciones válidas y vinculantes que tengan, en la mayor medida posible, un efecto similar al de la disposición inválida o no vinculante.\r\nUsted acepta que este Acuerdo y todos los acuerdos incorporados pueden ser asignados automáticamente por Bogui, a nuestra sola discreción, proporcionándole un aviso. Salvo que se indique explícitamente lo contrario, cualquier notificación a Bogui se dará por correo certificado, franqueo pre pagado y acuse de recibo a Bogui Ltda. Cualquier notificación a usted será proporcionada A través de la Plataforma Bogui o que se le entregue a través de la dirección de correo electrónico o física que usted proporciona a Bogui durante el proceso de registro. Los encabezamientos son para propósitos de referencia solamente y de ninguna manera definen, limitan, interpretan o describen el alcance o alcance de tal sección. El hecho de que una parte no actúe con respecto a un incumplimiento por parte de la otra parte no constituye una renuncia al derecho de la parte a actuar con respecto a violaciones posteriores o similares. Este Acuerdo establece todo el entendimiento y acuerdo entre usted y Bogui con respecto al tema de este documento y reemplaza todos los acuerdos y acuerdos anteriores entre las partes, ya sean orales o escritas.\r\nSi tiene alguna pregunta relacionada con la Plataforma Bogui o los Servicios, comuníquese con nuestro Equipo de asistencia al cliente a través de nuestro Centro de ayuda.', 'BOGUI LTDA Es una Sociedad constituida en Chile con domicilio en la ciudad de Santiago, y se encuentra regulada por el estatuto que los socios han suscrito conforme a la Ley número tres mil novecientos dieciocho y sus modificaciones y demás leyes pertinentes. http://www.leychile.cl/Navegar?idNorma=24349&idParte=0\r\nLa Razón Social de la sociedad es “Consultores en Informática Bogui Limitada, y su Rol Único Tributario 76.664.261-6 con domicilio en la ciudad de Santiago.\r\nEl presente texto explica los términos y condiciones que regulan el acceso y uso que usted como persona o empresa haga del sitio web, la plataforma y de las aplicaciones móviles que Bogui Ltda pone a disposición.\r\nSolicitamos leer detenidamente los siguientes términos y condiciones antes del uso de cualquiera de nuestros servicios, si usted no está de acuerdo con los términos y condiciones, no puede acceder o usar la plataforma web y las aplicaciones Móviles.\r\nLas aplicaciones Bogui proporcionan tecnología para las personas que buscan movilizarse a ciertos destinos (usuarios), puedan ser enlazados con personas que están dispuestos a suplir aquella necesidad (conductores), estos se denominan colectivamente “usuarios”, y cada individuo deberá crear una cuenta de usuario que le permita el acceso a la plataforma de Bogui.\r\nPara efectos del presente acuerdo los servicios de conducción proporcionados de los conductores a pasajeros que se enlacen a través de la plataforma de Bogui se referirán colectivamente como los “servicios”.\r\nLa decisión de un usuario de ofrecer o aceptar servicios es una decisión de cada usuario a discreción de cada uno de ellos.\r\nEl servicio de transporte proporcionado por un conductor a un usuario constituirá un acuerdo separado entre dichas personas.\r\n*DE LAS MODIFICACIONES\r\nEn el fortuito caso que Bogui Ltda Modifique los presentes términos y condiciones de este acuerdo, solo serán vinculados cuando usted acepte el acuerdo modificado, Bogui Ltda se reserva el derecho de modificar cualquier información vinculada a este acuerdo, tales modificaciones entraran en vigor justo después de la publicación.\r\nEl uso de la plataforma y otros servicios de Bogui Ltda después de cualquier cambio en los términos y condiciones precisaran de su consentimiento para efectuar tales cambios, a menos que se hagan cambios significativos a las disposiciones de arbitraje de este documento, usted acepta que la modificación de este acuerdo no crea una oportunidad renovada de optar por no participar en el arbitraje (en caso de ser aplicable).\r\n*DE EL ACCESO Y USO\r\nLa plataforma Bogui solo podrá ser utilizada por personas mayores de 18 años y que puedan firmar y formalizar contratos legalmente vinculante bajo la Ley aplicable.\r\nLa plataforma Bogui no está autorizada para personas menores de 18 años o personas que han tenido su cuenta de usuario temporal o permanentemente desactivada.\r\nAl convertirse en un usuario activo de la plataforma web y las aplicaciones Bogui usted declara y garantiza que al menos tiene 18 años de edad y que tiene la capacidad, la autoridad y el derecho para entrar y cumplir con los términos y condiciones de este acuerdo.\r\nNo puede permitir que otras personas utilicen su cuenta de usuario y acepta que es la única persona autorizada para el uso de su cuenta.\r\n*DE LOS PAGOS\r\nUsted como persona acepta pagar las cantidades cobradas por su uso de la Plataforma y Servicios de Bogui Ltda "Cargos". Los cargos incluyen tarifas y otros cargos aplicables, peajes, recargos e impuestos según lo establecido en la página de Bogui Ltda (www.bogui.cl), además de cualquier sugerencia al conductor que elija pagar. Bogui tiene la autoridad y se reserva el derecho de determinar y modificar los precios publicando términos de precios aplicables a la página de Bogui.cl de su País. El precio puede variar en función del tipo de servicio que solicite (por ejemplo, Bogui Sedan, Bogui Suv, Bogui Van), tal como se describe en la página de Bogui.cl de su País. Usted es responsable de revisar la página correspondiente de Bogui.cl y será responsable de todos los cargos incurridos en su cuenta de usuario, independientemente de su conocimiento de dichos cargos o las cantidades de los mismos.\r\n*DE LAS TARIFAS\r\nTarifas variables.\r\nLas tarifas variables consisten en un cargo base y cargos incrementales basados en la duración y la distancia de su viaje. Para viajes particularmente cortos, pueden aplicarse tarifas mínimas. Tenga en cuenta que utilizamos los datos GPS del teléfono del conductor para calcular la distancia recorrida en su viaje. No podemos garantizar la disponibilidad o precisión de los datos GPS. Si perdemos la señal vamos a calcular el tiempo y la distancia utilizando los datos disponibles de su viaje.\r\nTarifas Fijas.\r\nEn algunos casos Bogui puede cotizar una tarifa en el momento de su solicitud. La cotización está sujeta a cambios hasta que se confirme la solicitud de viaje. Si durante su viaje cambia su destino, hace paradas múltiples o intenta abusar de la plataforma Bogui, podemos cancelar la tarifa y cobrarle una tarifa variable basada en el tiempo y la distancia de su viaje. Bogui no garantiza que el precio de la tarifa cotizada sea igual a una tarifa variable para el mismo viaje.\r\nTarifas en Hora Punta.\r\nLas tarifas pueden estar sujetas a un multiplicador en momentos de alta demanda de los Servicios ("Hora punta") según lo determine Bogui. Para todos los viajes con tarifa variable, usaremos cálculos razonables para informarle de cualquier multiplicador de Hora Punta en el momento de su solicitud. Para las tarifas cotizadas, podemos tener en cuenta el multiplicador de Hora Punta en el precio cotizado del viaje. Cualquier cargo de Hora punta se considerará parte de la tarifa.\r\nTarifas y otros cargos.\r\nCuota de Servicio. Bogui puede evaluar una "cuota de servicio" por viaje para apoyar la Plataforma Bogui y los servicios relacionados proporcionados por Bogui. La cantidad de la Cuota de Servicio puede variar pero Bogui deberá retenerla en su totalidad.\r\nTarifa de cancelación.\r\nDespués de solicitar un viaje, puede cancelarlo a través de la aplicación, pero tenga en cuenta que en ciertos casos puede aplicarse un cargo por cancelación. También se le puede cobrar si no se presenta después de solicitar un viaje. Consulte nuestro Centro de ayuda para obtener más información sobre la política de cancelación de Bogui, incluidos\r\nlos cargos aplicables. Las tarifas de cancelación recaudadas se pasarán en su totalidad al conductor que haya aceptado su solicitud de viaje descontando la comisión respectiva de Bogui.\r\nCuota de Daño.\r\nSi un conductor informa que usted ha dañado materialmente el vehículo del conductor, usted acuerda pagar una "cuota de daños" de hasta $ 300 dólares americanos o el equivalente a su moneda local dependiendo de la extensión del daño (según lo determine Bogui a su sola discreción) hacia la reparación o limpieza del vehículo. Bogui se reserva el derecho (pero no está obligado) a verificar o de otro modo requerir documentación de daños antes de procesar la Cuota de Daño. Los honorarios de daños acumulados se pasarán en su totalidad al conductor cuyo vehículo fue dañado.\r\nPeajes.\r\nEn algunos casos se pueden aplicar el cobro de peajes (o peajes de retorno) a su viaje. Consulte nuestro Centro de ayuda en la página de bogui.cl de su país. Para obtener más información sobre las tarifas de peaje y una lista de peajes aplicables y gastos de devolución. No garantizamos que la cantidad cobrada por Bogui coincida con el precio del peaje cargado al Conductor, en su caso. La cantidad de peajes cobrados (y los peajes de retorno) serán pagados en su totalidad al Conductor que proveyó su viaje descontando la respectiva comisión de Bogui.\r\nOtros cargos.\r\nOtras cuotas y recargos pueden aplicarse a su viaje, incluyendo: cuotas aeroportuarias reales o anticipadas, tarifas estatales o locales, cargos por eventos según lo determinado por Bogui o sus socios comerciales, y cargos de procesamiento por pagos divididos. Además, cuando lo requiera la ley, Bogui cobrará los impuestos aplicables. Estos otros cargos no se comparten con su conductor a menos que se indique expresamente lo contrario. Consulte la página de bogui.cl de su país para obtener información sobre otros cargos que pueden aplicarse a su viaje.\r\nConsejos.\r\nDespués de un viaje, usted puede elegir dar propina en efectivo a su conductor.\r\nGENERAL\r\nFACILITACION DE CARGOS.\r\nTodos los cargos se gestionan a través de un servicio de procesamiento de pagos de terceros (por ejemplo, Stripe Inc. Transbank,o Braintree una división de PayPal, Inc.). Bogui puede reemplazar sus servicios de procesamiento de pagos de terceros sin previo aviso. Los cargos se harán a través de la Plataforma Bogui.\r\nAdemás Bogui pone a disposición de los usuarios la opción de pagar un viaje en efectivo por los servicios de traslado prestados por un conductor , esto quiere decir que si un usuario decide cancelar en efectivo la tarifa por el servicio de traslado, es el conductor quien recaudará dicha tarifa en su totalidad, siendo responsabilidad del conductor cobrar dicha tarifa y proporcionando el cambio correcto en caso de ameritarlo, y es usted como conductor quien debe conservar dicha tarifa bajo su propio riesgo y disponer de ella , sin embargo usted reconoce y autoriza a Bogui Ltda que en contraprestación de los servicios de Bogui en relación a los servicios prestados por uso de la plataforma, usted adeudará a la empresa Bogui ltda la tasa de servicio respectivo, y será su total responsabilidad el pago de todos los impuestos que se generen en su País en relación con la tarifa en efectivo y la respectiva tasa por el uso de la plataforma Bogui, y de ser el caso dicha tasa se pagará libre de cualquier impuesto que le sea aplicable.\r\nTodos los montos generados de las tasas de comisión por el uso de la plataforma Bogui y cualquier otra cantidad adeudada a la empresa Bogui ltda por el uso de la plataforma, es una deuda activa y vigente que usted adquiere con la empresa, dichos montos serán compensados y deducidos por la empresa de los viajes realizados y completados con tarjeta, cuyos montos son recolectados directamente por Bogui Ltda.\r\nEn el fortuito caso que no existiese fondos suficientes recolectados por Bogui Ltda en su cuenta de conductor, para cubrir los montos adeudados por las tasas y comisiones fijadas , para los montos recolectados por el conductor en efectivo y otras deudas vigentes con la empresa, es su obligación proporcionar un medio de pago para saldar dicha deuda, en caso contrario usted mantendrá una deuda comercial con la empresa y esta se reserva el derecho de incrementar dichos montos cobrando un interés por mora hasta que haya liquidado dichos montos a la empresa. La empresa además podrá poner a disposición diversos medios de pago como por ejemplo (transferencia electrónica, puntos de cobro, web pay) u otro medio que la empresa estime idóneo para facilitar los pagos. Usted reconoce y acepta que asumirá todos los costos de cualquier comisión o tarifa que pudiesen generarse en relación al pago de la tarifa adeudada a la empresa, como por ejemplo comisiones por cambios de divisa, comisiones por transferencias bancarias, comisiones por giros internacionales además de cualquier tipo de impuestos al que pudiesen estar sometidos, agregando dichos montos a la empresa de tal forma que la empresa reciba el monto total e íntegro de la deuda. La empresa no renunciará a sus derechos de cobro respecto a las cantidades adeudadas en caso de no existir fondos suficientes recaudados en su cuenta por pagos con tarjeta de crédito, ni en caso que usted no haya enterado los montos adeudados por otros medios a la empresa, ni esperar que se genere algún monto en particular para iniciar las respectivas acciones de cobro. Bogui se reserva el derecho de desactivar aquellas cuentas que tengan alguna deuda vigente con la empresa, en caso de posteriormente regularizar dichas deudas la empresa no se cierra a la posibilidad que el usuario de la plataforma solicite una reactivación, la que será analizada y verificada, pudiendo no ser aceptada la solicitud.\r\nSin reembolsos.\r\nTodos los cargos no son reembolsables. Esta política de no reembolso se aplicará en todo momento, independientemente de su decisión de terminar el uso de la Plataforma Bogui, de cualquier interrupción de la Plataforma Bogui o de los Servicios, o cualquier otra razón.\r\nCréditos y Descuentos de Viajes.\r\nUsted puede recibir créditos ( "Créditos de Bogui") o descuentos de Viaje ( "Descuentos de Viaje") que usted puede aplicar para el pago de ciertos Cargos al completar un Viaje. Los Créditos Bogui y los Descuentos de viaje sólo son válidos para su uso en la Plataforma Bogui, y no son transferibles o canjeables por dinero en efectivo excepto como lo requiere la ley. Los Créditos Bogui y los Descuentos de Viaje no se pueden combinar, y si el costo de su viaje excede el crédito o descuento aplicable, cargaremos su saldo con el método de pago en el archivo por el costo pendiente del viaje. Los Descuentos de Viaje solo se aplican a la tarifa, no a la cuota de servicio u otros cargos. Si divide el pago de un viaje con otro Usuario, sus Créditos Bogui o Descuento de viaje solo se aplicarán a su parte de los Gastos. Las restricciones adicionales sobre los Créditos Bogui y los Descuentos de viaje pueden aplicarse según se le comuniquen en una promoción relevante o haciendo clic en el Crédito Bogui o en el Descuento de viaje correspondiente en la sección Pagos de la Aplicación Bogui.\r\nAutorización de Tarjeta de Crédito.\r\nMediante la adición de un nuevo método de pago o de cada solicitud de viaje, Bogui podrá solicitar la autorización de su método de pago seleccionado para verificar el método de pago, asegurarse de cubrir el costo del viaje y protegerlo contra conductas no autorizadas. La autorización no es un cargo, sin embargo, puede reducir su crédito disponible por el monto de la autorización hasta el ciclo de procesamiento siguiente de su banco. Si el monto de nuestra autorización\r\nsupera el total de los fondos depositados en su cuenta, es posible que el banco que emita su tarjeta de débito, crédito o cheque esté sujeto al sobregiro de los cargos de la ley de Bancos Local. No podemos ser responsables de estos cargos y no podemos ayudarle a recuperarlos de su banco emisor. Consulte nuestro Centro de ayuda para obtener más información sobre nuestro uso de retenciones previas a la autorización.\r\nPagos.\r\nSi usted es un Conductor, usted recibirá el pago por su provisión de Servicios. Todos los pagos de tarifas están sujetos a una Comisión Bogui, que se discute a continuación. Las propinas no estarán sujetas a ninguna Comisión de Bogui. Bogui procesará todos los pagos que se le deban a través de su procesador de pagos de terceros. Usted reconoce y acepta que tales cantidades no incluirán ningún interés y serán netas de cualquier cantidad que debemos retener por ley.\r\nComisión.\r\nA cambio de permitirle ofrecer sus Servicios a través de la Plataforma Bogui como un Conductor, usted acepta pagar a Bogui una tarifa basada en cada transacción en la que presta Servicios (la "Comisión"). El monto de la Comisión aplicable será del 15 % para cada una de las transacciones y para cada categoría aplica el mismo porcentaje. Bogui se reserva el derecho de cambiar la Comisión en cualquier momento a discreción de Bogui basada en factores del mercado local, y Bogui le proporcionará aviso en caso de tal cambio. El uso continuado de la Plataforma Bogui después de cualquier cambio en el cálculo de la Comisión constituirá su consentimiento para dicho cambio.\r\nPrecios.\r\nUsted autoriza expresamente a Bogui a fijar los precios en su nombre para todos los cargos que se aplican a la prestación de servicios. Bogui se reserva el derecho de cambiar el horario de la tarifa en cualquier momento a nuestra discreción en base a los factores locales del mercado, y le notificaremos en caso de cambios en la tarifa base, por kilómetro y / o por minuto que resultaría En un cambio en las tarifas aplicables. Los cargos pueden estar sujetos a los límites máximos establecidos en la página de bogui.cl de su País.\r\nAjuste de tarifas.\r\nBogui se reserva el derecho de ajustar o retener la totalidad o una parte de las tarifas si considera que ha intentado defraudar o abusar de los sistemas de pago de Bogui, para resolver una queja de un viaje (por ejemplo, no pudo finalizar correctamente una instancia particular de un Servicios en la aplicación Bogui, cuando el viaje haya terminado). La decisión de Bogui de ajustar o retener la tarifa de cualquier manera se ejercerá de manera razonable.\r\nComunicación.\r\nAl convertirse en un usuario, acepta recibir comunicaciones de nosotros, incluso por correo electrónico, mensajes de texto, llamadas y notificaciones push. Usted acepta que los textos, las llamadas o los mensajes pregrabados pueden ser generados por los sistemas de marcación telefónica automática. Las comunicaciones de Bogui, sus compañías afiliadas y / o controladores, pueden incluir, pero no se limitan a comunicaciones operacionales relativas a su cuenta de usuario o uso de la plataforma Bogui o Servicios, actualizaciones sobre características nuevas y existentes en la plataforma Bogui, Por nosotros o nuestros socios de terceros, y noticias sobre Bogui y los desarrollos de la industria. Los cargos estándar de mensajería de texto aplicados por su proveedor de telefonía celular se aplicarán a los mensajes de texto que enviemos.\r\nSI DESEA DESCONECTARSE DE CUALQUIER TIPO DE COMUNICADO VISITE NUESTRA PAGINA WEB.\r\nSU INFORMACION.\r\nSu Información es cualquier información que usted proporcione, publique o publique a través de la Plataforma de Bogui (incluyendo cualquier información de perfil que proporcione) o envíe a otros Usuarios (incluyendo comentarios en la aplicación, cualquier característica de correo electrónico, o a través de Facebook, Twitter o cualquier otra publicación de medios sociales) (su "Información"). Usted acepta que utilicemos su Información para crear una cuenta de usuario que le permita usar la Plataforma Bogui y participar en los Servicios. Nuestra recopilación y uso de información personal en relación con la Plataforma Bogui y los Servicios es tal como se provee en la Política de Privacidad de Bogui ubicada en www.bogui.cl/privacidad. Usted es el único responsable de su Información y sus interacciones con otros miembros del público, y actuamos sólo como un conducto pasivo para su publicación en línea de su Información. Usted acepta proporcionar y mantener información exacta, actual y completa y que nosotros y otros miembros del público podemos confiar en su información como exacta, actual y completa. Para permitir que Bogui utilice su Información, usted nos otorga un derecho no exclusivo, mundial, perpetuo, irrevocable, libre de regalías, transferible, sub licenciable (a través de múltiples niveles) y la licencia para ejercer los derechos de autor, publicidad y derechos de base de datos que Usted tiene en su Información, y para usar, copiar, realizar, exhibir y distribuir dicha Información para preparar trabajos derivados, o incorporar en otros trabajos, tal Información, en cualquier medio ahora conocido o no actualmente conocido. Bogui no asume ninguna propiedad sobre su Información; En su lugar, entre Bogui y usted, sujeto a los derechos que se nos otorgan en este Acuerdo, usted conserva plena propiedad de toda su Información y cualquier derecho de propiedad intelectual u otros derechos de propiedad relacionados con su Información.\r\nEs posible que pueda crear o iniciar sesión en su cuenta de usuario de Bogui a través de cuentas en línea que pueda tener con sitios de redes sociales de terceros (cada cuenta, una "cuenta SNS"). Al conectarse a Bogui a través de una cuenta SNS, usted entiende que Bogui puede acceder, almacenar y poner a disposición cualquier contenido de la cuenta SNS de acuerdo con la configuración de permisos de su cuenta SNS (por ejemplo, amigos, amigos mutuos, contactos o listas siguientes / seguidas "Contenido SNS")). Usted entiende que el Contenido SNS puede estar disponible a través de la Plataforma Bogui para otros Usuarios. A menos que se especifique lo contrario en este Acuerdo, todo el Contenido SNS, si lo hubiere, será considerado como su Información. (Social Network Service)\r\nPromociones y Programas de Referencia.\r\nBogui, a su entera discreción, puede hacer promociones disponibles con diferentes características a cualquier Usuario o posible Usuario. Estas promociones, a menos que se le hagan a usted, no tendrán ninguna influencia en su acuerdo o relación con Bogui. Bogui se reserva el derecho de retener o deducir créditos o beneficios obtenidos a través de una promoción, en el evento que Bogui determine o considere que la recaudación de la promoción o recepción del crédito o beneficio fue erróneo, fraudulento, ilegal o en violación de la promoción aplicable a los Términos del presente Acuerdo.\r\nComo parte de su cuenta de usuario, Bogui puede proporcionarle o permitirle crear un "Código Bogui", un código alfanumérico único para que usted distribuya a amigos, familiares y otras personas (cada uno un "Usuario Referido") para convertirse en un nuevo Bogui Usuario ( "Usuarios Referidos") o Conductores ( "Conductores Referidos"). Los códigos de Bogui sólo se pueden distribuir con fines promocionales y deben entregarse de forma gratuita. Usted no puede vender, comerciar, o hacer trueque con su código de Bogui. Está prohibido anunciar códigos Bogui, incluyendo pero no limitando a: Google, Facebook, Twitter, Bing, Instagram, WhatsApp, Telegram. Bogui se reserva el derecho de desactivar o invalidar cualquier código de Bogui en cualquier momento a discreción de Bogui.\r\nDe vez en cuando, Bogui puede ofrecerle incentivos para referir nuevos usuarios a la comunidad Bogui (el "Programa de Referencia"). Estos incentivos pueden venir en forma de Créditos Bogui, y Bogui puede establecer o cambiar los\r\ntipos de incentivos, cantidades, términos, restricciones y requisitos de calificación para cualquier incentivo a su sola discreción. La distribución de los códigos de Bogui y la participación en el Programa de Referencia están sujetos a este Acuerdo y a las reglas adicionales del Programa de Referencia.\r\nACTIVIDADES RESTRINGIDAS.\r\nCon respecto a su uso de la Plataforma Bogui y su participación en los Servicios, usted acepta no:\r\nSuplantar a cualquier persona o entidad;\r\nAcosar, amenazar o acosar a cualquier persona, o llevar armas;\r\nViolar cualquier ley, estatuto, regla, permiso, ordenanza o regulación;\r\nInterferir o interrumpir los Servicios o la Plataforma Bogui o los servidores o redes conectados a la Plataforma Bogui;\r\nPublicar información o interactuar en la Plataforma Bogui o Servicios de una manera que sea falsa, inexacta, engañosa (directamente o por omisión o no actualización de información), difamatoria, abusiva, obscena, profana, ofensiva, sexualmente orientada, amenazante, hostigadora, O ilegal;\r\nUtilizar la Plataforma Bogui de cualquier forma que infrinja los derechos de terceros, incluyendo pero no limitado a: derechos de propiedad intelectual, derechos de autor, patentes, marcas registradas, secretos comerciales u otros derechos de propiedad o derechos de publicidad o privacidad;\r\nEnviar, por correo electrónico o de otra forma transmitir cualquier código malicioso, archivos o programas diseñados para interrumpir, dañar, destruir o limitar la funcionalidad de cualquier software o hardware o equipo de telecomunicaciones o interceptar o expropiar cualquier sistema, datos o información personal;\r\nForjar cabeceras o de otra manera manipular identificadores para disfrazar el origen de cualquier información transmitida a través de la Plataforma Bogui;\r\n"Marco" o "espejo" de cualquier parte de la Plataforma Bogui, sin nuestra autorización previa por escrito o usar etiquetas, códigos u otros dispositivos que contengan cualquier referencia a nosotros para dirigir a cualquier persona a cualquier otro sitio web para cualquier propósito; o\r\nModificar, adaptar, traducir, realizar ingeniería inversa, descifrar, descompilar o desmontar cualquier parte de la Plataforma Bogui o cualquier software utilizado en o para la Plataforma Bogui; Alquilar, , prestar, vender, redistribuir, licenciar o sub licenciar la Plataforma Bogui o acceder a cualquier parte de la Plataforma Bogui;\r\nUtilizar cualquier robot, araña, aplicación de búsqueda / recuperación de sitio u otro dispositivo o proceso manual o automático para recuperar, indexar, raspar, "extraer datos" o de alguna manera reproducir o eludir la estructura de navegación o presentación de la Plataforma Bogui o de su contenido;\r\nEnlace directo o indirecto a cualquier otro sitio web;\r\nTransferir o vender su cuenta de usuario, contraseña y / o identificación a cualquier otra parte\r\nDiscriminar u hostigar a cualquiera en base a raza, origen nacional, religión, género, identidad de género, discapacidad física o mental, condición médica, estado civil, edad u orientación sexual, o\r\nHacer que cualquier tercero se involucre en las actividades restringidas arriba.\r\nGARANTIAS, ACUERDOS Y REPRESENTACION DE CONDUCTORES.\r\nAl proporcionar Servicios como conductor en la Plataforma Bogui, usted representa, garantiza y acepta que:\r\nUsted posee una licencia de conducir válida y está autorizado y médicamente apto para operar un vehículo de motor y tiene todas las licencias, aprobaciones y autoridad apropiadas para proveer transporte a Usuarios en todas las regiones o jurisdicciones en las cuales usted provee Servicios.\r\nUsted posee, o tiene el derecho legal de operar, el vehículo que usted usa al proveer servicios, y dicho vehículo está en buenas condiciones de operación y cumple con las normas de seguridad de la industria y todos los requisitos legales y estatales.\r\nUsted no se involucrará en conducta temeraria mientras maneja, maneja con locura, maneja un vehículo que no es seguro para conducir, se involucra en un accidente de automóvil o colisión de cualquier tipo, permite que un tercero no autorizado le acompañe en el vehículo mientras presta servicios, Proporcionar servicios como conductor mientras está bajo la influencia de alcohol o drogas, o tomar medidas que dañen o amenacen con dañar la seguridad de la comunidad de Bogui o de terceros.\r\nUsted sólo proporcionará servicios utilizando el vehículo que ha sido reportado y aprobado por Bogui, y para el cual se le ha proporcionado una fotografía a Bogui, y no transportará más pasajeros que puedan estar sentado en dicho vehículo, y no más de los autorizados en cualquier caso.\r\nNo hará ninguna declaración falsa con respecto a Bogui, la Plataforma Bogui, los Servicios o su estado como Conductor.\r\nUsted no podrá, mientras presta los Servicios, operar como un servicio público de transporte o taxi, aceptar salidas de la calle, cobrar por los viajes (excepto lo expresamente previsto en este Acuerdo), exigir que un usuario pague en efectivo excepto que dicho viaje haya sido solicitado con antelación en la aplicación de Bogui como un viaje en efectivo, caso contrario el o los cobros se harán automáticamente por Bogui , o use un lector de tarjetas de crédito, Tal como un Lector , para aceptar el pago o para emprender cualquier otra actividad de una manera que sea inconsistente con sus obligaciones bajo este acuerdo.\r\nUsted acepta que podremos obtener información sobre usted, incluyendo sus antecedentes penales y de conducción, y usted acepta proporcionar cualquier otra autorización necesaria para facilitar nuestro acceso a dichos registros durante el término del Acuerdo.\r\nUsted tiene una póliza válida de seguro de responsabilidad civil (en cantidades de cobertura consistentes con todos los requisitos legales aplicables) que lo autoriza para operar un vehículo para proporcionar servicios.\r\nUsted no intentará defraudar a Bogui o usuarios en la Plataforma de Bogui o en conexión con su provisión de Servicios. Si sospechamos que usted ha participado en una actividad fraudulenta, podemos retener las tarifas aplicables u otros pagos por el viaje(s) en cuestión.\r\nUsted pagará todos los impuestos federales, estatales y locales aplicables basados en su provisión de Servicios y cualquier pago recibido por usted.\r\nPropiedad intelectual.\r\nTodos los derechos de propiedad intelectual de la plataforma Bogui serán propiedad de Bogui de forma absoluta y total. Estos derechos incluyen y no se limitan a los derechos de base de datos, derechos de autor, derechos de diseño (ya sean registrados o no), marcas registradas (registradas o no registradas) y otros derechos similares dondequiera\r\nque exista en el mundo junto con el derecho a solicitar la protección de los mismos. Todas las demás marcas comerciales, logotipos, marcas de servicio, nombres de empresas o productos establecidos en la plataforma Bogui son propiedad de sus respectivos propietarios. Usted reconoce y acepta que cualquier pregunta, comentario, sugerencia, idea, retroalimentación u otra información proporcionada por usted a nosotros no son confidenciales y serán propiedad exclusiva de Bogui. Bogui tendrá derechos exclusivos, incluyendo todos los derechos de propiedad intelectual, y tendrá derecho al uso y difusión sin restricciones de estos Envíos para cualquier propósito, comercial o de otro tipo, sin reconocimiento ni compensación para usted.\r\nBogui y otros logotipos, diseños, gráficos, iconos, scripts y nombres de servicio de Bogui son marcas registradas, marcas registradas o marcas registradas de Bogui en Chile y / o en otros países (colectivamente, las "Marcas Bogui"). Si usted presta servicios como un Conductor, Bogui le otorga, durante la vigencia de este Contrato, y sujeto a su cumplimiento con los términos y condiciones de este Acuerdo, una licencia limitada, revocable y no exclusiva para exhibir y usar las Marcas Bogui Únicamente en relación con la prestación de los Servicios a través de la Plataforma Bogui ( "Licencia"). La Licencia es intransferible y no puede ser cedida, y no concederá a terceros ningún derecho, permiso, licencia o sub licencia con respecto a ninguno de los derechos concedidos sin el permiso previo por escrito de Bogui, que podrá retener en su exclusiva discreción. Las Marcas Bogui no pueden ser utilizadas de ninguna manera que pueda causar confusión.\r\nUsted reconoce que Bogui es el propietario y licenciante de las Marcas Bogui, incluyendo toda buena voluntad asociada con las mismas, y que su uso de las Marcas Bogui no conferirá ningún interés o propiedad adicional de las Marcas Bogui en usted, sino más bien beneficios en beneficio de Bogui. Usted acepta utilizar las marcas Bogui estrictamente de acuerdo con las Pautas de Uso de Marcas registradas de Bogui, que pueden ser proporcionadas a usted y revisadas de vez en cuando, e inmediatamente cesar cualquier uso que Bogui considere no conforme o de otra manera inaceptable.\r\nUsted acepta que no:\r\n(1) creará materiales que incorporen las Marcas Bogui o cualquier derivado de las Marcas Bogui que no sean expresamente aprobadas por escrito;\r\n(2) utilizar las Marcas Bogui de cualquier forma que tienda a menoscabar su validez como marcas registradas, marcas de servicio, nombres comerciales o marcas comerciales, o usar las Marcas Bogui, excepto de acuerdo con los términos, condiciones y restricciones de la presente;\r\n(3) tomar cualquier otra acción que pudiera poner en peligro o menoscabar los derechos de Bogui como propietario de las Marcas de Bogui o la legalidad y / o aplicabilidad de las Marcas de Bogui, incluyendo, sin limitación, el desafío u oposición de la propiedad de Bogui en las Marcas de Bogui;\r\n(4) solicitar el registro de la marca registrada o la renovación del registro de marca registrada de cualquiera de las Marcas Bogui, o cualquier derivado de las Marcas Bogui, cualquier combinación de las Marcas Bogui y cualquier otro nombre o marca, marca de servicio, nombre comercial, símbolo o palabra Que es similar a las marcas de Bogui;\r\n(5) usar las Marcas Bogui en o en conexión con cualquier producto, servicio o actividad que viole alguna ley, estatuto, regulación gubernamental o norma.\r\nLa infracción de cualquier disposición de esta Licencia puede resultar en la terminación inmediata de la Licencia, a discreción de Bogui. Si crea cualquier material que lleve las Marcas Bogui (en violación de este Contrato o de otro modo), usted acepta que Bogui, en el momento de su creación, posee exclusivamente todos los derechos, títulos e intereses sobre dichos materiales, incluyendo sin limitación cualquier modificación a las Marcas Bogui o Trabajos derivados basados en las marcas de Bogui. Asimismo, acepta asignar a Bogui cualquier interés o derecho que pueda tener sobre dichos materiales, así como proporcionar información y ejecutar cualquier documento que razonablemente solicite Bogui para que Bogui pueda formalizar dicha asignación.\r\nBogui respeta la propiedad intelectual de los demás, y espera que los usuarios hagan lo mismo. Si cree, de buena fe, que cualquier material de la Plataforma Bogui o Servicios infringe sus derechos de autor, visite la página Política de derechos de autor para obtener información sobre cómo presentar una reclamación por derechos de autor.\r\nDescargo de responsabilidad.\r\nLas siguientes renuncias se hacen en nombre de Bogui, nuestros afiliados, subsidiarias, sucesores y cesionarios, y cada uno de nuestros respectivos funcionarios, directores, empleados, agentes y accionistas.\r\nBogui no proporciona servicios de transporte, y Bogui no es un transportista. Corresponde al Controlador decidir si ofrecer o no un viaje a un usuario contactado a través de la Plataforma Bogui, y es responsabilidad del usuario decidir si acepta o no un viaje desde cualquier Conductor contactado a través de la Plataforma Bogui. No podemos asegurar que un Conductor o Usuario complete un servicio de transporte contratado. No tenemos control sobre la calidad o seguridad del transporte que se produce como resultado de los Servicios.\r\nLa Plataforma Bogui se suministra "tal como está" y sin ninguna garantía o condición, expresa, implícita o legal. No garantizamos ni prometemos ningún resultado específico del uso de la Plataforma de Bogui y / o de los Servicios, incluyendo la capacidad de proporcionar o recibir Servicios en cualquier lugar o tiempo dado. Expresamos específicamente ninguna garantía implícita de título, comerciabilidad, aptitud para un propósito particular y no infracción. Algunos estados no permiten la renuncia de garantías implícitas, por lo que la renuncia anterior puede no aplicarse a usted.\r\nNo garantizamos que el uso de la plataforma o servicios de Bogui sea exacto, completo, confiable, actual, seguro, ininterrumpido, siempre disponible o libre de errores, o satisfacer sus requisitos, que cualquier defecto en la Plataforma de Bogui será Corregido o que la Plataforma Bogui esté libre de virus u otros componentes dañinos. Renunciamos a responsabilidad por, y no se hace garantía con respecto a, la conectividad y la disponibilidad de la Plataforma Bogui o Servicios.\r\nNo podemos garantizar que cada usuario sea quien él o ella dice ser. Por favor, utilice el sentido común cuando utilice la Plataforma Bogui y los Servicios, incluyendo mirar las fotos del Conductor o Pasajero con el que ha coincidido para asegurarse de que es la misma persona que usted ve en persona. Tenga en cuenta que también hay riesgos de tratar con personas menores de edad o personas que actúen bajo falsas pretensiones y no aceptamos responsabilidad alguna por ningún contenido, comunicación u otro uso o acceso de la Plataforma Bogui por personas menores de 18 años en violación Del presente Acuerdo. Le recomendamos a comunicarse directamente con cada conductor o pasajero potencial antes de contratar un servicio de transporte organizado.\r\nBogui no es responsable de la conducta, ya sea en línea o fuera de línea, de cualquier Usuario de la Plataforma o Servicios de Bogui. Usted es el único responsable de sus interacciones con otros usuarios. Nosotros no compramos seguro ni somos responsables por las pertenencias personales dejadas en el vehículo por los Conductores o Pasajeros. Al utilizar la Plataforma Bogui y participar en los Servicios, usted acepta dichos riesgos y acepta que Bogui no es responsable de los actos u omisiones de los Usuarios en la Plataforma Bogui o de participar en los Servicios.\r\nBogui renuncia expresamente a cualquier responsabilidad derivada del uso no autorizado de su cuenta de usuario. Si usted sospecha que cualquier parte o ente no autorizado puede estar usando su cuenta de usuario o sospecha cualquier otro incumplimiento de seguridad, usted acepta notificarnos inmediatamente.\r\nEs posible que otros obtengan información sobre usted que usted proporcione, publique o publique a través de la Plataforma de Bogui (incluyendo cualquier información de perfil que proporcione), envíe a otros Usuarios, o comparta durante los Servicios, y use dicha información para hostigar O hacerle daño. No somos responsables del uso de cualquier información personal que usted revele a otros Usuarios en la Plataforma de Bogui o a través de los Servicios. Por favor, seleccione cuidadosamente el tipo de información que publica en la Plataforma de Bogui o a través de los Servicios o comunique a otros. Excluimos toda responsabilidad, independientemente de la forma de acción, por los actos u omisiones de otros Usuarios (incluidos los usuarios no autorizados, o "piratas informáticos").\r\nLas opiniones, consejos, declaraciones, ofertas, u otra información o contenido relacionado con Bogui o disponible a través de la Plataforma Bogui, pero no directamente por nosotros, son los de sus respectivos autores, y no necesariamente se debe confiar en ellos. Dichos autores son los únicos responsables por tal contenido. Bajo ninguna circunstancia seremos responsables de ninguna pérdida o daño que resulte de su dependencia de información u otro contenido publicado por terceros, ya sea en la plataforma de Bogui o de otra manera. Nos reservamos el derecho, pero no tenemos ninguna obligación, de monitorear los materiales publicados en la Plataforma Bogui y eliminar cualquier material que en nuestra opinión viole o viola la ley o este contrato o que puede ser ofensivo, ilegal, o que pueda violar los derechos, dañar o amenazar la seguridad de los Usuarios u otros.\r\nLos datos de ubicación proporcionados por la plataforma Bogui son sólo para fines de localización básica y no se pretende que se basen en situaciones en las que se necesite información de ubicación precisa o donde datos erróneos, inexactos o incompletos puedan causar muerte, lesiones personales, daños materiales o ambientales. Ni Bogui, ni ninguno de sus proveedores de contenido, garantiza la disponibilidad, exactitud, integridad, fiabilidad u oportunidad de los datos de localización seguidos o mostrados por la Plataforma Bogui. Cualquier parte de su Información, incluidos los datos geo localizados, que suba, proporcione o publique en la Plataforma Bogui puede ser accesible para Bogui y para determinados Usuarios de la Plataforma Bogui.\r\nBogui le aconseja utilizar la Plataforma Bogui con un plan de datos con límites ilimitados o muy altos de uso de datos, y Bogui no será responsable de ninguna tarifa, costo o sobrecarga asociada con cualquier plan de datos que use para acceder a la Plataforma Bogui.\r\nDIVULGACIONES ESTATALES Y LOCALES.\r\nIndemnización.\r\nUsted defenderá, indemnizará y mantendrá a Bogui, incluidos nuestros afiliados, subsidiarias, sucesores y cesionarios, y cada uno de nuestros respectivos funcionarios, directores, empleados, agentes o accionistas, de cualquier reclamación, acción, demanda, pérdida, costo o pasivo. Y gastos (incluyendo honorarios razonables de abogados) relacionados o derivados de su uso de la Plataforma Bogui y la participación en los Servicios, incluyendo:\r\n(1) su incumplimiento de este Acuerdo o los documentos que incorpora por referencia;\r\n(2) la violación de cualquier ley o los derechos de un tercero, incluyendo, sin limitación, los Conductores, Pasajeros, otros conductores y peatones, como resultado de su propia interacción con dicho tercero;\r\n(3) cualquier alegación de que cualquier material que nos envíe o transmita a través de la Plataforma Bogui o infrinja o viole de alguna manera los derechos de autor, marcas comerciales, secretos comerciales u otros derechos de propiedad intelectual u otros derechos de terceros;\r\n(4) su propiedad, uso u operación de un vehículo de motor o de pasajeros, incluyendo su provisión de Servicios como Conductor; Y / o\r\n(5) cualquier otra actividad relacionada con los Servicios. Esta indemnización será aplicable sin tener en cuenta la negligencia de cualquier parte, incluyendo cualquier persona indemnizada.\r\nLimitación de responsabilidad.\r\nEN NINGÚN CASO, BOGUI , INCLUYENDO NUESTROS AFILIADOS, SUBSIDIARIOS, SUCESORES Y ASIGNADOS Y CADA UNO DE NUESTROS RESPECTIVOS OFICIALES, DIRECTORES, EMPLEADOS, AGENTES O ACCIONISTAS (COLECTIVAMENTE "BOGUI" PARA LOS FINES DE ESTA SECCIÓN), SERÁ RESPONSABLE ANTE USTED POR CUALQUIER DAÑO INCIDENTAL, ESPECIAL, EJEMPLAR, PUNITIVO, CONSECUENCIAL O INDIRECTO (INCLUYENDO, PERO NO LIMITADO A, DAÑOS POR ELIMINACIÓN, CORRUPCIÓN, PÉRDIDA DE DATOS, PÉRDIDA DE PROGRAMAS, FALTA DE ALMACENAR CUALQUIER INFORMACIÓN U OTRO CONTENIDO MANTENIDO O TRANSMITIDO POR LA PLATAFORMA BOGUI, INTERRUPCIONES DE SERVICIO O POR EL COSTO DE LA CONTRATACIÓN DE SERVICIOS DE SUSTITUCIÓN) DERIVADOS DE O EN RELACIÓN CON LA PLATAFORMA BOGUI, LOS SERVICIOS O ESTE ACUERDO, SIN EMBARGO QUE INCLUYEN NEGLIGENCIA, INCLUSO SI NOSOTROS O NUESTROS AGENTES O REPRESENTANTES CONOCEN O HAN SIDO AVISADOS DE LA POSIBILIDAD DE TALES DAÑOS. LA PLATAFORMA BOGUI PUEDE SER UTILIZADA POR USTED PARA SOLICITAR Y CALIFICAR EL TRANSPORTE, BIENES U OTROS SERVICIOS CON PROVEEDORES DE TERCEROS, USTED ACEPTA QUE BOGUI NO TIENE RESPONSABILIDAD O RESPONSABILIDAD CON USTED RELACIONADO CON NINGUN TRANSPORTE, BIENES U OTROS SERVICIOS PROPORCIONADOS A USTED POR TERCEROS PROVEEDORES DE ESTA QUE NO SEAN EXPRESAMENTE ESTABLECIDOS EN ESTOS TÉRMINOS. CIERTAS JURISDICCIONES NO PERMITEN LA EXCLUSIÓN O LIMITACIÓN DE CIERTOS DAÑOS. SI ESTAS LEYES SE APLICAN A USTED, ALGUNAS O TODAS LAS RENUNCIAS ANTERIORES, EXCLUSIONES O LIMITACIONES PUEDEN NO APLICARSE A USTED, Y USTED PUEDE TENER DERECHOS ADICIONALES.\r\nDuración y Terminación.\r\nEste Acuerdo es efectivo a la creación de una cuenta de usuario. El presente Contrato podrá ser rescindido:\r\na) por el Usuario, sin causa, previa notificación escrita de siete (7) días a Bogui; O\r\nb) por cualquiera de las Partes inmediatamente, sin previo aviso, por el incumplimiento material de este Acuerdo por parte de la otra Parte, incluyendo pero no limitado a cualquier incumplimiento de las Secciones de este Acuerdo. Además, Bogui puede rescindir este Acuerdo o desactivar su cuenta de usuario inmediatamente en caso de que:\r\n(1) usted ya no califique para prestar servicios o para operar el vehículo aprobado bajo la ley, regla, permiso, ordenanza o regulación aplicable;\r\n(2) cae por debajo de la calificación de Bogui o el umbral de cancelación;\r\n(3) Bogui cree de buena fe que tal acción es necesaria para proteger la seguridad de la comunidad de Bogui o de terceros, siempre que en caso de una desactivación conforme a los (1) - (3) anteriores, se le dará aviso De la desactivación potencial o real y una oportunidad de intentar sub sanar el problema a la satisfacción razonable de Bogui antes de que Bogui rescindiera definitivamente el Acuerdo. Para todas las demás infracciones de este Acuerdo, se le proporcionará aviso y una oportunidad para sub sanar el incumplimiento. Si el incumplimiento se resuelve de manera oportuna y a satisfacción de Bogui, este Contrato no será terminado de manera permanente. Las secciones (con respecto a la licencia), sobrevivirán a cualquier terminación o vencimiento de este Acuerdo.\r\nRESOLUCIÓN DE DISPUTAS Y ACUERDO DE ARBITRAJE.\r\n(A) Acuerdo de Arbitraje Vinculante entre usted y Bogui.\r\nUSTED Y BOGUI MUTUAMENTE ACUERDAN RENUNCIAR A NUESTROS DERECHOS RESPECTIVOS Y A LA RESOLUCIÓN DE CONTROVERSIAS EN UN TRIBUNAL DE JUSTICIA POR UN JUEZ O JURADO Y ACEPTAN RESOLVER CUALQUIER DISPUTA POR ARBITRAJE, como se establece a continuación. Este acuerdo de arbitraje ("Acuerdo de Arbitraje") se rige por la Ley Nacional de Arbitraje número 19971 del Ministerio de Justicia y sobrevive después de que el Acuerdo termine o su relación con Bogui termine. CUALQUIER ARBITRAJE EN VIRTUD DE ESTE ACUERDO SE REALIZARÁ DE FORMA INDIVIDUAL; ARBITRACIONES DE CLASES Y ACCIONES DE CLASE NO SON PERMITIDAS. Excepto lo expresamente estipulado a continuación, este Acuerdo de Arbitraje se aplica a todas las Reclamaciones (definidas a continuación) entre usted y Bogui, incluyendo nuestras afiliadas, subsidiarias, sucesores y cesionarios y cada uno de nuestros respectivos funcionarios, directores, empleados, agentes o accionistas.\r\nSalvo lo expresamente estipulado a continuación, TODAS LOS CONFLICTOS Y RECLAMACIONES ENTRE NOSOTROS (CADA UNA "RECLAMACIÓN" Y "RECLAMACIONES") SERÁN EXCLUSIVAMENTE RESOLVIDAS POR ARBITRAJE VINCULANTE SOLAMENTE ENTRE USTED Y BOGUI. Estas Reclamaciones incluyen, pero no se limitan a, ninguna controversia, reclamo o controversia, ya sea basada en acontecimientos pasados, presentes o futuros, que surjan de o se relacionen con este Contrato y versiones anteriores del mismo (incluyendo el incumplimiento, Interpretación o validez del mismo), la Plataforma Bogui, los Servicios, y cualquier otro bien o servicio disponible a través de la Plataforma Bogui, su relación con Bogui, la suspensión, desactivación o terminación de su Cuenta de Usuario o de este Acuerdo, Las promociones u ofertas hechas por Bogui, cualquier ley de remuneraciones, secretos comerciales, competencia desleal, compensación, descansos y , reembolso de gastos, rescisión indebida , Discriminación, acoso, represalia, fraude, difamación, angustia emocional, incumplimiento de cualquier contrato o pacto expreso o implícito, reclamos que surjan bajo las leyes locales o estatales de protección al consumidor; Reclamaciones derivadas de leyes antimonopolio, reclamaciones derivadas de la Ley de Protección del Consumidor Telefónico y de la Ley de Notificación Justa de Créditos; Ley de Derechos Civiles de 1964, Ley de Discapacidades, Ley de Discriminación por Edad en el Empleo, Ley de Protección de Beneficios de Trabajadores Mayores, Ley de Licencia Médica Familiar, Ley de Normas de Trabajo Justo, Para reclamos individuales de beneficios de empleados bajo cualquier plan de beneficios patrocinado por Bogui y cubierto por la Ley de Seguridad social de Jubilados o financiado por seguro), y demás estatutos locales, si los hubiere, tratando temas iguales o similares; Locales y de derecho común. Todas las disputas relativas a la arbitrariedad de una Reclamación (incluyendo disputas sobre el alcance, aplicabilidad, ejecutabilidad, revocabilidad o validez del Acuerdo de Arbitraje) serán decididas por el árbitro, excepto como se indica expresamente a continuación.\r\nAL COMPROMISO DE ARBITRAJE, USTED ENTIENDE QUE USTED Y BOGUI ESTÁN RENUNCIANDO AL DERECHO DE SENTARSE EN EL TRIBUNAL O TENER UN JUICIO DE JURADO PARA TODAS LAS RECLAMACIONES, EXCEPTO LO EXPRESAMENTE DE OTRA FORMA DISPUESTO EN ESTE ACUERDO DE ARBITRAJE. El presente Acuerdo de Arbitraje tiene por objeto exigir el arbitraje de toda reclamación o controversia que pueda arbitrarse legalmente, con excepción de aquellas reclamaciones y controversias que por los términos del presente Acuerdo de Arbitraje estén expresamente excluidas del requisito de arbitraje.\r\n(B) Prohibición de Reclamos de Clase y Socorro No Individualizado.\r\nUSTED ENTIENDE Y ACEPTA QUE ENTRE USTED Y BOGUI PUEDEN HACER RECLAMACIONES EN ARBITRAJE CONTRA OTRO SÓLO EN UNA CAPACIDAD INDIVIDUAL Y NO EN UNA CLASE, UNA ACCIÓN COLECTIVA O UNA BASE REPRESENTATIVA ("RENUNCIA DE ACCIÓN DE CLASE"). USTED ENTIENDE Y ACEPTA QUE ENTRE USTED Y BOGUI AMBOS ESTÁN RENUNCIANDO AL DERECHO A TENER UNA DISPUTA RESUELTA COMO DEMANDANTE O CLASE EN CUALQUIER PROCESO DE CLASE, COLECTIVO O REPRESENTANTE SUSTENTADO. A PESAR DE LO ANTERIOR, ESTA SUBSECCIÓN (B) NO SE APLICARÁ A LAS RECLAMACIONES REPRESENTANTES DE LA LEY GENERAL DE ABOGADOS PRIVADOS CONTRA BOGUI, QUE SE DIRIGEN POR SEPARADO.\r\nEl árbitro no tendrá autoridad para considerar o resolver cualquier Reclamación o emitir ningún alivio sobre cualquier base que no sea individual. El árbitro no tendrá autoridad para considerar o resolver ninguna Reclamación o emitir ningún alivio en base a una clase, colectiva o representativa.\r\nNo obstante cualquier otra disposición de este Acuerdo, el Acuerdo de Arbitraje o las Reglas, las disputas referentes al alcance, aplicabilidad, ejecutabilidad, revocabilidad o validez de la Renuncia de Acción Colectiva podrán ser resueltas únicamente por un tribunal civil de jurisdicción competente y no por un árbitro. En cualquier caso en que: (1) la disputa se presente como una acción de clase, colectiva o representativa y hay una determinación judicial definitiva de que la Exención de Acción de Clase es inaplicable en cuanto a Reclamaciones, / O acción representativa en tales demandas debe ser litigada en un tribunal civil de la jurisdicción competente, pero la renuncia de acción colectiva se hará cumplir en el arbitraje sobre una base individual en cuanto a todas las otras reclamaciones en la mayor medida posible.\r\n(C) Renuncia Representativa de PAGA.\r\nNo obstante cualquier otra disposición de este Acuerdo o del Acuerdo de Arbitraje, en la mayor medida permitida por la ley: (1) usted y Bogui acuerdan no presentar una acción representativa en nombre de terceros bajo la Ley de Abogados Privados en un tribunal o en arbitraje, y (2) por cualquier reclamación presentada en base a un procurador general privado, tanto usted como Bogui acuerdan que cualquier disputa será resuelta En el arbitraje sobre una base individual (es decir, para resolver si usted ha sido personalmente agraviado o sujeto a cualquier violación de la ley), y que tal acción no puede ser utilizada para resolver las reclamaciones o derechos de otras personas en una sola o colectiva (Es decir, para resolver si otros individuos han sido agraviados o sujetos a cualquier violación de la ley) (colectivamente,). No obstante cualquier otra disposición de este Acuerdo, el Acuerdo de Arbitraje, las disputas sobre el alcance, aplicabilidad, ejecutabilidad, revocabilidad o validez de este Representante Renuncia, sólo podrán ser resueltas por un tribunal civil de jurisdicción competente y no por un árbitro.\r\nComo parte del arbitraje, tanto usted como Bogui tendrán la oportunidad de encontrar razonablemente información no privilegiada que sea relevante para la Reclamación. El árbitro podrá otorgar cualquier recurso individualizado que esté disponible en los tribunales. El árbitro puede otorgar una medida declarativa o una medida cautelar solamente en favor de la parte individual que solicita el reclamo y sólo en la medida en que sea necesario para proporcionar el reclamo garantizado por las reclamaciones individuales de esa parte. El árbitro presentará una declaración motivada por escrito de la decisión del árbitro que explicará la decisión otorgada y las conclusiones en las que se basa la decisión.\r\nEl árbitro decidirá la sustancia de todas las reclamaciones de acuerdo con la ley aplicable y honrará todas las reclamaciones de privilegio reconocidas por la ley. El árbitro no estará obligado por decisiones en arbitrajes previos que involucren a diferentes Pilotos o Conductores, pero está obligado por decisiones en arbitrajes previos que involucren al mismo Usuario o Conductor en la medida requerida por la ley aplicable. El fallo del árbitro será definitivo y vinculante y el fallo dictado por el árbitro podrá ser inscrito en cualquier tribunal que tenga jurisdicción, siempre y cuando cualquier fallo pueda ser impugnado ante un tribunal competente.\r\nConfidencialidad\r\nUsted se compromete a no utilizar ningún tipo de información técnica, financiera, estratégica y de otra índole relacionada con los negocios, operaciones y propiedades de Bogui, incluida la información del Usuario ( "Información Confidencial") revelada por Bogui para su propio uso o para cualquier otro propósito que no sea Como se contempla aquí. Usted no revelará ni permitirá la divulgación de ninguna Información Confidencial a terceros. Usted acepta tomar todas las medidas razonables para proteger el secreto y evitar la divulgación o el uso de la Información Confidencial de Bogui con el fin de evitar que caiga en el dominio público. Sin perjuicio de lo anterior, usted no tendrá ninguna\r\nresponsabilidad con Bogui con respecto a cualquier Información Confidencial que pueda probar que estaba en el dominio público en el momento en que fue revelado por Bogui o ha entrado en el dominio público sin culpa suya y que fue conocido por usted, sin restricciones, en el momento de la divulgación, como lo demuestran los archivos existentes en el momento de la divulgación; Se revela con la previa aprobación por escrito de Bogui; Se le conoce, sin restricciones, de una fuente distinta de Bogui sin incumplimiento de este Acuerdo por usted y de otra manera no viola los derechos de Bogui; O se revela de conformidad con la orden o el requisito de un tribunal, agencia administrativa u otro organismo gubernamental; Siempre que, sin embargo, provea notificación inmediata de tal orden judicial o requisito a Bogui para permitir que Bogui solicite una orden de protección o y de otra manera prevenga o restrinja dicha divulgación.\r\nRelación con Bogui.\r\nComo Conductor en la Plataforma Bogui, usted reconoce y acepta que usted y Bogui están en una relación comercial directa, y la relación entre las partes bajo este Acuerdo es únicamente la de las partes contratantes independientes. Usted y Bogui expresamente están de acuerdo en que (1) esto no es un contrato de trabajo y no crea una relación laboral entre usted y Bogui o Bogui Ltda; Y (2) ninguna relación de empresa conjunta, franquiciador-franquiciado, asociación o agencia está pensada o creada por este Acuerdo. No tiene autoridad para obligar a Bogui, y se compromete a no presentarse como empleado, agente o representante autorizado de Bogui.\r\nBogui no ordena o controla en general o en su desempeño bajo este Acuerdo específicamente, incluyendo en conexión con su provisión de Servicios, sus actos u omisiones, o su operación y mantenimiento de su vehículo. Usted conserva el derecho exclusivo de determinar cuándo, dónde y durante cuánto tiempo utilizará la Plataforma Bogui. Usted conserva la opción de aceptar, rechazar o ignorar la solicitud de servicios de Conductor por medio de la Plataforma Bogui, o cancelar una solicitud de servicios aceptada a través de la Plataforma Bogui, sujeto a las políticas de cancelación de Bogui. Con excepción de cualquier señalización requerida por la ley, las reglas o requisitos de licencia / licencia, Bogui no tendrá derecho a exigirle que: (a) muestre los nombres, logotipos o colores de Bogui en su (s) vehículo (s); O (b) usar un uniforme o cualquier otra ropa que muestre los nombres, logotipos o colores de Bogui. Usted reconoce y acepta que tiene total discreción para proveer Servicios o participar de otra manera en otras actividades comerciales o laborales.\r\nOtros servicios.\r\nAdemás de conectar Pasajeros con Conductores, Bogui puede permitir que los Usuarios proporcionen o reciban bienes o servicios de terceros. Por ejemplo, los Usuarios pueden usar la Plataforma Bogui para ordenar una entrega de mercancías, comprar un artículo digital, solicitar un viaje en una modalidad que permite que varios usuarios viajen en una misma dirección, o cuando viaje fuera del territorio Nacional, para conectarse con el transporte local a las Plataformas y solicitudes de viajes de los conductores locales (colectivamente, los "Otros Servicios"). Usted entiende que los Otros Servicios están sujetos a los términos y precios del proveedor de terceros. Si opta por comprar otros servicios a través de la plataforma Bogui, autoriza a Bogui a cargar su método de pago en el archivo de acuerdo con las condiciones de precio establecidas por el proveedor de terceros. Usted acepta que Bogui no es responsable y no puede ser considerado responsable por los Otros Servicios o las acciones u omisiones del proveedor de terceros. Tales Otros Servicios no podrán ser investigados, monitoreados o verificados por la exactitud, pertinencia o integridad por nosotros, y nosotros no somos responsables de ningún otro Servicio a través de la Plataforma Bogui.\r\nGeneral.\r\nEste Acuerdo se regirá por las leyes del Estado sin tener en cuenta los principios de elección de la ley. Si alguna de las disposiciones de este Acuerdo es o se convierte en inválida o no vinculante, las partes permanecerán vinculadas por todas las demás disposiciones del presente. En tal caso, las partes sustituirán la disposición inválida o no vinculante\r\npor disposiciones válidas y vinculantes que tengan, en la mayor medida posible, un efecto similar al de la disposición inválida o no vinculante.\r\nUsted acepta que este Acuerdo y todos los acuerdos incorporados pueden ser asignados automáticamente por Bogui, a nuestra sola discreción, proporcionándole un aviso. Salvo que se indique explícitamente lo contrario, cualquier notificación a Bogui se dará por correo certificado, franqueo pre pagado y acuse de recibo a Bogui Ltda. Cualquier notificación a usted será proporcionada A través de la Plataforma Bogui o que se le entregue a través de la dirección de correo electrónico o física que usted proporciona a Bogui durante el proceso de registro. Los encabezamientos son para propósitos de referencia solamente y de ninguna manera definen, limitan, interpretan o describen el alcance o alcance de tal sección. El hecho de que una parte no actúe con respecto a un incumplimiento por parte de la otra parte no constituye una renuncia al derecho de la parte a actuar con respecto a violaciones posteriores o similares. Este Acuerdo establece todo el entendimiento y acuerdo entre usted y Bogui con respecto al tema de este documento y reemplaza todos los acuerdos y acuerdos anteriores entre las partes, ya sean orales o escritas.\r\nSi tiene alguna pregunta relacionada con la Plataforma Bogui o los Servicios, comuníquese con nuestro Equipo de asistencia al cliente a través de nuestro Centro de ayuda.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` int(11) NOT NULL COMMENT '1: Administrador, 2: Conductor, 3: Pasajero',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitud` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitud` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0: Desactivado, 1: Activo',
  `status_driver` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1: Conectado, 0: Desconectado',
  `default_app` int(11) DEFAULT NULL COMMENT '1: Google Maps, 2: Waze',
  `default_payment` int(11) DEFAULT NULL COMMENT '1: Tarjeta, 2: Efectivo',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `validate` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: no verificado; 1: verificado',
  `last_status` int(11) DEFAULT NULL,
  `webpay` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alive` int(11) NOT NULL DEFAULT '1' COMMENT '1: vivo, 0: muerto. Se declara a un conductor muerto al transcurrir un periodo de tiempo sin emitir socket.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `level`, `email`, `password`, `latitud`, `longitud`, `status`, `status_driver`, `default_app`, `default_payment`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `validate`, `last_status`, `webpay`, `card`, `alive`) VALUES
(1, 1, 'admin@bogui.com', '$2y$10$Kb.5BZeEH5wDY.WAEOLJY.iB5Qku/y3DjXB9LITlUSAYbV8CM9w4G', NULL, NULL, 1, '1', NULL, NULL, 'La0b2DVVHA4IeCX5MNx7XP0KowrLzoKQL3Bp2hmj4yMo40UMENtBTgkeUCur', NULL, '2018-09-10 19:00:00', '2018-12-21 10:15:50', 1, NULL, NULL, NULL, 1),
(2746, 2, 'Joscarlenyn@gmail.com', '$2y$10$Me4v6S.QlpWp64LEXLB1BueAp8AZyqcq5TNlxW9fvNiMG3wE9JOsS', '10.2972077', '-67.6315824', 1, '1', NULL, NULL, NULL, NULL, '2019-12-28 13:44:55', '2019-12-28 16:14:12', 1, NULL, NULL, NULL, 1),
(2747, 2, 'Otro@conductor.com', '$2y$10$8vP0Y3ry11iIXeH6GxL/eOm..hWh7Df0EMgYHVwSm307PNkwRemYe', '10.2970538', '-67.6316339', 1, '1', NULL, NULL, NULL, NULL, '2019-12-28 14:24:55', '2019-12-28 16:13:31', 1, NULL, NULL, NULL, 1),
(2748, 3, 'pasajero@bogui.com', '$2y$10$yGjuJ0yccU57uvoimPTT8OAo.KyAv.X0cQ6gDEjD2APCPC08O9Yzu', NULL, NULL, 1, '1', NULL, 2, NULL, NULL, '2019-12-28 14:37:07', '2019-12-28 14:46:40', 1, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_contacts`
--

CREATE TABLE `users_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_contacts`
--

INSERT INTO `users_contacts` (`id`, `user_id`, `name`, `phoneCode`, `phone`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2748, 'joscar', '58', '4148211103', NULL, '2019-12-28 14:39:57', '2019-12-28 14:44:41'),
(2, 2748, 'analista', '58', '4124758521', NULL, '2019-12-28 14:41:35', '2019-12-28 14:41:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `validations`
--

CREATE TABLE `validations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `validations`
--

INSERT INTO `validations` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 2746, 'X0DAXQ', '2019-12-28 13:44:55', '2019-12-28 13:44:55'),
(2, 2747, 'EOQEBY', '2019-12-28 14:24:55', '2019-12-28 14:24:55'),
(3, 2748, 'BBFEGHABGG', '2019-12-28 14:37:07', '2019-12-28 14:37:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `vehicle_type_id` int(11) DEFAULT NULL,
  `permit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1: Activo, 0: Inactivo',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles`
--

INSERT INTO `vehicles` (`id`, `brand`, `model`, `plate`, `year`, `vehicle_type_id`, `permit`, `expiration`, `user_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Chevrolet', 'Optra', 'SJMB36', 2017, NULL, NULL, '2019-12-29', 2746, 1, NULL, '2019-12-28 13:44:55', '2019-12-28 14:01:28'),
(2, 'Camry', 'Model', 'GHRT22', 2016, NULL, 'drivers/files/8e8ad9d326c03057d6dde4ecdbc81f71631328.jpg', '2019-12-29', 2747, 1, NULL, '2019-12-28 14:24:55', '2019-12-28 14:27:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_photos`
--

CREATE TABLE `vehicles_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_photos`
--

INSERT INTO `vehicles_photos` (`id`, `vehicle_id`, `file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'vehicles/d13182db608f9ddf79c7b62e315b793b25348293.jpg', '2019-12-28 13:44:55', '2019-12-28 13:44:55', NULL),
(2, 2, 'vehicles/ba5343bc527e9c429ab8c7879395736d85614150.jpg', '2019-12-28 14:24:55', '2019-12-28 14:24:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicle_category`
--

CREATE TABLE `vehicle_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `vehicle_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicle_category`
--

INSERT INTO `vehicle_category` (`id`, `vehicle_id`, `vehicle_type_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-12-28 14:01:28', '2019-12-28 14:01:28'),
(2, 1, 2, '2019-12-28 14:01:28', '2019-12-28 14:01:28'),
(3, 2, 4, '2019-12-28 14:27:47', '2019-12-28 14:27:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_by` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicle_type`
--

INSERT INTO `vehicle_type` (`id`, `name`, `registered_by`, `description`, `photo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Sedán', 1, 'Económico hasta 4 pasajeros', 'vehicle_type/bogui1.png', NULL, '2018-09-13 15:00:00', '2018-09-13 15:00:00'),
(2, 'SUV', 1, 'Exclusivo para 4 pasajeros', 'vehicle_type/bogui2.png', NULL, '2018-09-13 15:00:00', '2018-09-13 15:00:00'),
(3, 'Van', 1, 'Grupal hasta 7 pesonas', 'vehicle_type/bogui3.png', '2019-10-14 03:00:00', '2018-09-13 15:00:00', '2018-09-13 15:00:00'),
(4, 'Moto', 1, 'Exclusivo para 1 pasajero', 'vehicle_type/bogui4.png', NULL, '2019-10-14 03:00:00', '2019-10-14 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `webpay`
--

CREATE TABLE `webpay` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `route_id` int(11) DEFAULT NULL,
  `buyOrder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `weekly_debt`
--

CREATE TABLE `weekly_debt` (
  `id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(11) NOT NULL,
  `debt_weekly` double(15,2) NOT NULL,
  `debt_carried` double(15,2) NOT NULL,
  `debt_total` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `accounts_type`
--
ALTER TABLE `accounts_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_notifications`
--
ALTER TABLE `admin_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_notifications_users`
--
ALTER TABLE `admin_notifications_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_notifications_users_notification_id_foreign` (`notification_id`),
  ADD KEY `admin_notifications_users_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `airports`
--
ALTER TABLE `airports`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banks_registered_by_foreign` (`registered_by`);

--
-- Indices de la tabla `banks_user`
--
ALTER TABLE `banks_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banks_user_user_id_foreign` (`user_id`),
  ADD KEY `banks_user_bank_id_foreign` (`bank_id`);

--
-- Indices de la tabla `benefits`
--
ALTER TABLE `benefits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blacklist_driver_id_foreign` (`driver_id`),
  ADD KEY `blacklist_route_id_foreign` (`route_id`);

--
-- Indices de la tabla `category_notification`
--
ALTER TABLE `category_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chats_route_id_foreign` (`route_id`),
  ADD KEY `chats_motive_id_foreign` (`motive_id`);

--
-- Indices de la tabla `chat_user`
--
ALTER TABLE `chat_user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `commissions`
--
ALTER TABLE `commissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commissions_registered_by_foreign` (`registered_by`);

--
-- Indices de la tabla `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `deleted_chats`
--
ALTER TABLE `deleted_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `destinies`
--
ALTER TABLE `destinies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destinies_route_id_foreign` (`route_id`);

--
-- Indices de la tabla `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `driver_benefits`
--
ALTER TABLE `driver_benefits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employment_conditions`
--
ALTER TABLE `employment_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `night_mode`
--
ALTER TABLE `night_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`),
  ADD KEY `notifications_driver_id_foreign` (`driver_id`);

--
-- Indices de la tabla `opentok`
--
ALTER TABLE `opentok`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opentok_route_id_foreign` (`route_id`);

--
-- Indices de la tabla `panic`
--
ALTER TABLE `panic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `panic_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `panic_details`
--
ALTER TABLE `panic_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `panic_details_user_id_foreign` (`user_id`),
  ADD KEY `panic_details_panic_id_foreign` (`panic_id`),
  ADD KEY `panic_details_response_id_foreign` (`response_id`);

--
-- Indices de la tabla `panic_options`
--
ALTER TABLE `panic_options`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `panic_radius`
--
ALTER TABLE `panic_radius`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `passenger_benefits`
--
ALTER TABLE `passenger_benefits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_app`
--
ALTER TABLE `password_app`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_route_id_foreign` (`route_id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `payment_promotion`
--
ALTER TABLE `payment_promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_promotion_payment_id_foreign` (`payment_id`),
  ADD KEY `payment_promotion_promotion_id_foreign` (`promotion_id`);

--
-- Indices de la tabla `pending_documents`
--
ALTER TABLE `pending_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pending_documents_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indices de la tabla `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `persons_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotions_registered_by_foreign` (`registered_by`);

--
-- Indices de la tabla `promotion_user`
--
ALTER TABLE `promotion_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotion_user_user_id_foreign` (`user_id`),
  ADD KEY `promotion_user_promotion_id_foreign` (`promotion_id`);

--
-- Indices de la tabla `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_reviewed_id_foreign` (`reviewed_id`),
  ADD KEY `ratings_reviewer_id_foreign` (`reviewer_id`),
  ADD KEY `ratings_route_id_foreign` (`route_id`);

--
-- Indices de la tabla `refresh_tokens`
--
ALTER TABLE `refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `routes_vehicle_type_id_foreign` (`vehicle_type_id`),
  ADD KEY `routes_tariff_id_foreign` (`tariff_id`);

--
-- Indices de la tabla `route_user`
--
ALTER TABLE `route_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `route_user_route_id_foreign` (`route_id`),
  ADD KEY `route_user_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `search_radius`
--
ALTER TABLE `search_radius`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `support_motive`
--
ALTER TABLE `support_motive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `support_motive_registered_by_foreign` (`registered_by`);

--
-- Indices de la tabla `tariffs`
--
ALTER TABLE `tariffs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tariffs_vehicle_type_id_foreign` (`vehicle_type_id`);

--
-- Indices de la tabla `tariff_type`
--
ALTER TABLE `tariff_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `types_registered_by_foreign` (`registered_by`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_contacts`
--
ALTER TABLE `users_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_contacts_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `validations`
--
ALTER TABLE `validations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicles_vehicle_type_id_foreign` (`vehicle_type_id`),
  ADD KEY `vehicles_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `vehicles_photos`
--
ALTER TABLE `vehicles_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicles_photos_vehicle_id_foreign` (`vehicle_id`);

--
-- Indices de la tabla `vehicle_category`
--
ALTER TABLE `vehicle_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_category_vehicle_id_foreign` (`vehicle_id`),
  ADD KEY `vehicle_category_vehicle_type_id_foreign` (`vehicle_type_id`);

--
-- Indices de la tabla `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_type_registered_by_foreign` (`registered_by`);

--
-- Indices de la tabla `webpay`
--
ALTER TABLE `webpay`
  ADD PRIMARY KEY (`id`),
  ADD KEY `webpay_user_id_foreign` (`user_id`),
  ADD KEY `webpay_buyorder_index` (`buyOrder`);

--
-- Indices de la tabla `weekly_debt`
--
ALTER TABLE `weekly_debt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `accounts_type`
--
ALTER TABLE `accounts_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `admin_notifications`
--
ALTER TABLE `admin_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `admin_notifications_users`
--
ALTER TABLE `admin_notifications_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `airports`
--
ALTER TABLE `airports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `banks_user`
--
ALTER TABLE `banks_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `benefits`
--
ALTER TABLE `benefits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `category_notification`
--
ALTER TABLE `category_notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `chat_user`
--
ALTER TABLE `chat_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `commissions`
--
ALTER TABLE `commissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT de la tabla `deleted_chats`
--
ALTER TABLE `deleted_chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `destinies`
--
ALTER TABLE `destinies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `driver_benefits`
--
ALTER TABLE `driver_benefits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `employment_conditions`
--
ALTER TABLE `employment_conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `information`
--
ALTER TABLE `information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT de la tabla `night_mode`
--
ALTER TABLE `night_mode`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `opentok`
--
ALTER TABLE `opentok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `panic`
--
ALTER TABLE `panic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `panic_details`
--
ALTER TABLE `panic_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `panic_options`
--
ALTER TABLE `panic_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `panic_radius`
--
ALTER TABLE `panic_radius`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `passenger_benefits`
--
ALTER TABLE `passenger_benefits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `password_app`
--
ALTER TABLE `password_app`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `payment_promotion`
--
ALTER TABLE `payment_promotion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pending_documents`
--
ALTER TABLE `pending_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `promotion_user`
--
ALTER TABLE `promotion_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `refresh_tokens`
--
ALTER TABLE `refresh_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `route_user`
--
ALTER TABLE `route_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `search_radius`
--
ALTER TABLE `search_radius`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4121;
--
-- AUTO_INCREMENT de la tabla `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `support_motive`
--
ALTER TABLE `support_motive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `tariffs`
--
ALTER TABLE `tariffs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tariff_type`
--
ALTER TABLE `tariff_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2749;
--
-- AUTO_INCREMENT de la tabla `users_contacts`
--
ALTER TABLE `users_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `validations`
--
ALTER TABLE `validations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `vehicles_photos`
--
ALTER TABLE `vehicles_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `vehicle_category`
--
ALTER TABLE `vehicle_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `webpay`
--
ALTER TABLE `webpay`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `weekly_debt`
--
ALTER TABLE `weekly_debt`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin_notifications_users`
--
ALTER TABLE `admin_notifications_users`
  ADD CONSTRAINT `admin_notifications_users_notification_id_foreign` FOREIGN KEY (`notification_id`) REFERENCES `admin_notifications` (`id`),
  ADD CONSTRAINT `admin_notifications_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `banks_user`
--
ALTER TABLE `banks_user`
  ADD CONSTRAINT `banks_user_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`),
  ADD CONSTRAINT `banks_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `blacklist`
--
ALTER TABLE `blacklist`
  ADD CONSTRAINT `blacklist_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blacklist_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `commissions`
--
ALTER TABLE `commissions`
  ADD CONSTRAINT `commissions_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `destinies`
--
ALTER TABLE `destinies`
  ADD CONSTRAINT `destinies_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`);

--
-- Filtros para la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `opentok`
--
ALTER TABLE `opentok`
  ADD CONSTRAINT `opentok_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`);

--
-- Filtros para la tabla `panic`
--
ALTER TABLE `panic`
  ADD CONSTRAINT `panic_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `panic_details`
--
ALTER TABLE `panic_details`
  ADD CONSTRAINT `panic_details_panic_id_foreign` FOREIGN KEY (`panic_id`) REFERENCES `panic` (`id`),
  ADD CONSTRAINT `panic_details_response_id_foreign` FOREIGN KEY (`response_id`) REFERENCES `panic_options` (`id`),
  ADD CONSTRAINT `panic_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `payment_promotion`
--
ALTER TABLE `payment_promotion`
  ADD CONSTRAINT `payment_promotion_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`),
  ADD CONSTRAINT `payment_promotion_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `promotion_user` (`id`);

--
-- Filtros para la tabla `pending_documents`
--
ALTER TABLE `pending_documents`
  ADD CONSTRAINT `pending_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `persons`
--
ALTER TABLE `persons`
  ADD CONSTRAINT `persons_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `promotions_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `promotion_user`
--
ALTER TABLE `promotion_user`
  ADD CONSTRAINT `promotion_user_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `promotions` (`id`),
  ADD CONSTRAINT `promotion_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_reviewed_id_foreign` FOREIGN KEY (`reviewed_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `ratings_reviewer_id_foreign` FOREIGN KEY (`reviewer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `ratings_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`);

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `routes`
--
ALTER TABLE `routes`
  ADD CONSTRAINT `routes_tariff_id_foreign` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`id`),
  ADD CONSTRAINT `routes_vehicle_type_id_foreign` FOREIGN KEY (`vehicle_type_id`) REFERENCES `vehicle_type` (`id`);

--
-- Filtros para la tabla `route_user`
--
ALTER TABLE `route_user`
  ADD CONSTRAINT `route_user_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `route_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `support_motive`
--
ALTER TABLE `support_motive`
  ADD CONSTRAINT `support_motive_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `types`
--
ALTER TABLE `types`
  ADD CONSTRAINT `types_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users_contacts`
--
ALTER TABLE `users_contacts`
  ADD CONSTRAINT `users_contacts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `vehicles_photos`
--
ALTER TABLE `vehicles_photos`
  ADD CONSTRAINT `vehicles_photos_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`);

--
-- Filtros para la tabla `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD CONSTRAINT `vehicle_type_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `webpay`
--
ALTER TABLE `webpay`
  ADD CONSTRAINT `webpay_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
