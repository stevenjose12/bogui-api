<?php

	namespace App\Libraries;

	class Money {

		public static function get($number) {
			return 'CLP '.number_format($number,2,'.',',');
		}
	}