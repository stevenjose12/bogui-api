<?php

	namespace App\Libraries;

	class TariffType {

		public static function get($num) {
			$respuesta = "";
			switch ($num) {
				case 0:
					$respuesta = "Sedán";
					break;

				case 1:
					$respuesta = "SUV";
					break;

				case 2:
					$respuesta = "Van";
					break;

				case 3:
					$respuesta = "Aeropuerto";
					break;

				case 4:
					$respuesta = "Parada Adicional";
					break;
			}
			return $respuesta;
		}
	}