<?php

	namespace App\Libraries;

	class InfoBip {

		public static function send($title,$number,$message) {
			$curl = curl_init();

			$mensaje = [
				'from' => $title,
				'to' => $number,
				'text' => $message
			];

			curl_setopt_array($curl, [
			  CURLOPT_URL => "https://1knld.api.infobip.com/sms/2/text/single",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => json_encode($mensaje),
			  CURLOPT_HTTPHEADER => [
			    "accept: application/json",
			    "authorization: Basic ".env('INFO_BIP_TOKEN'),
			    "content-type: application/json"
			  ],
			]);

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
		}
	}