<?php

	namespace App\Libraries;
	use Illuminate\Pagination\LengthAwarePaginator;

	class Paginate {

		public static function get($items,$perPage = 10) {
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
	    	$col = collect($items);
	    	$currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
	    	$entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
	    	return $entries;
		}
	}