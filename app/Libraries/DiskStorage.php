<?php

	namespace App\Libraries;

	class DiskStorage {

		public static function get($disk) {
			return \Storage::disk($disk)->getAdapter()->getPathPrefix();
		}

		public static function name($extension = 'jpg') {
			 return md5(date('YmdHis')).rand(0,10000).rand(0,10000).'.'.$extension;
		}
	}