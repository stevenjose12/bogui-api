<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\User;
use App\Permission;
use Auth;

class AuthController extends Controller
{
    public function index()
    {
        if(Auth::check()){
            return redirect('admin/users');
        }

        return view('login');
    }


    public function login(AuthRequest $request)
    {

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){

            if (Auth::user()->level != '1' && Auth::user()->level != '4') {
                return response()->json([
                    'result' => false,
                    'message' => "No tiene permisos para acceder a este modulo administrativo"
                ]);
            }

            if (Auth::user()->status != '1') {
                return response()->json([
                    'result' => false,
                    'message' => "Usuario desactivado"
                ]);
            }

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'deleted_at' => null]))
            {


                $url = url('admin/users');

                if (Auth::user()->level == '4') {
                    $permisos = Auth::user()->allPermissions();
                    if (count($permisos) > 0) {
                        $permisos = $permisos[0];
                        if ($permisos->name == 'module-passengers') {
                            $url = url('admin/passengers');
                        } else if ($permisos->name == 'module-drivers') {
                            $url = url('admin/users');
                        } else if ($permisos->name == 'module-support') {
                            $url = url('admin/chats');
                        } else if ($permisos->name == 'module-moderators') {
                            $url = url('admin/moderators');
                        } else if ($permisos->name == 'module-promotions') {
                            $url = url('admin/promotions');
                        } else if ($permisos->name == 'module-ratings') {
                            $url = url('admin/ratings-app');
                        } else if ($permisos->name == 'module-configuration') {
                            $url = url('admin/tariffs');
                        }
                    } else {
                        return response()->json([
                            'result' => false,
                            'message' => "El Administrador debe asignarle permisos para acceder a la web"
                        ]);
                    }
                }

                return response()->json([
                    'result' => true,
                    'location' => $url
                ]);
            }
            else 
            {
                return response()->json([
                    'result' => false,
                    'message' => "Correo y/o contraseña invalida"
                ]);
            }

        }
        
        return response()->json([
            'result' => false,
            'message' => "Correo y/o contraseña invalida"
        ]);
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/admin');
    }

    public function getLogin() {
        return redirect('/');
    }

    public function getRegisterPasajero() {
        return redirect('/');
    }

    public function getRegisterConductor() {
        return redirect('/');
    }

    public function getCodigo() {
        return redirect('/');
    }
}
