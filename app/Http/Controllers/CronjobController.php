<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\User;
	use App\Models\RefreshToken;
	use Carbon\Carbon;

	class CronjobController extends Controller {

	    public function get() {
	    	$users = User::where('level',2)
                ->where('status_driver',1)
                ->where('status',1)
                ->where('alive',1)
                ->get();

	    	foreach($users as $user) {
	    		if (Carbon::parse($user->updated_at)->diffInMinutes(Carbon::now()) > 1) {
	    			$user->alive = 0;
	    			$user->save();
	    		}
	    	}

	    	// LIMPIAR REGISTRO VIEJOS DE LA TABLA REFRESH_TOKENS

            $now = Carbon::yesterday();
            RefreshToken::whereDate("updated_at", "<=", $now)
                ->delete();

	    }

	}
