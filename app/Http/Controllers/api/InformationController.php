<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Information;

class InformationController extends Controller
{
    public function index ()
    {
    	return Information::where('status', 1)
    		->where('type', 2)
    		->first();
    }
}
