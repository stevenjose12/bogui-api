<?php

namespace App\Http\Controllers\api;

use App\Http\Requests\AuthRequest;
use App\Models\Payment;
use App\Models\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Hash;
use App\Models\Person;
use App\Models\Rating;
use Image;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Registra la posicion actual del usuario
     */
    public function setPosition(Request $r) {
        $user = User::find($r->id);
        $user->latitud = $r->lat;
        $user->longitud = $r->lng;
        $user->save();
        return response()->json(['success'=>true]);
    }

    public function getUserById($id) {

        $user = User::where('id',$id)->first();

        if ($user==null) {
            return response()->json([
                'success' => false,
                'message' => 'Usuario no encontrado'
            ]);
        }

        $user = User::where("users.id", $user->id)
            ->join("persons", "users.id", "=", "persons.user_id")
            ->select("users.id", "email", "latitud as lat", "longitud as lng", "validate", "name", "lastname", "phone", "photo", "document", "document_photo", "default_payment", "level", "users.deleted_at")
            ->first();

        return response()->json([
            'success' => true,
            'user' => $user
        ]);
        
    }

    public function getUser(Request $r) {

        $user = User::where("users.id", $r->id)
            ->join("persons", "users.id", "=", "persons.user_id")
            ->select("users.id", "email", "latitud as lat", "longitud as lng", "validate", "name", "lastname", "phone", "photo", "document", "document_photo", "default_payment")
            ->first();

        $rantng = number_format(Rating::where('reviewed_id',$r->id)->count() == 0 ? 0 : Rating::where('reviewed_id',$r->id)->sum('points') / Rating::where('reviewed_id',$r->id)->count(),2);

        return response()->json([
            'success' => true,
            'user' => $user,
            "ranting" => $rantng
        ]);
    }

    public function password(Request $request) {
        $reglas = [
            'password' => 'confirmed'
        ];
        $mensajes = [
            'confirmed' => 'Las contraseñas no coinciden'
        ];
        $validacion = Validator::make($request->all(),$reglas,$mensajes);
        if ($validacion->fails()) {
            return response()->json([
                'result' => false,
                'error' => $validacion->messages()->first()
            ]);
        }
        else {
            $user = User::find($request->id);
            if (Hash::check($request->old_password, $user->password)) {
                $user->password = $request->password;
                $user->save();
                return response()->json([
                    'result' => true
                ]);
            } else {
                return response()->json([
                    'result' => false,
                    'error' => 'La contraseña actual no coincide'
                ]);
            }
        }
    }

    public function editUser(Request $request) {

        $reglas = [
            'email' => 'required|email',
            'phone' => 'required'
        ];
        $mensajes = [
            'required' => 'El campo :attribute es requerido',
            'email' => 'El correo electrónico no es válido',
            'unique' => 'El correo electrónico ya se encuentra registrado'
        ];
        $atributos = [
            'email' => 'Correo Electrónico',
            'phone' => 'Teléfono'
        ];
        $validacion = Validator::make($request->all(),$reglas,$mensajes);
        $validacion->setAttributeNames($atributos);

        if ($validacion->fails()) {
            return response()->json([
                'result' => false,
                'msg' => $validacion->messages()->first()
            ]);
        } else {

            // VALIDANDO EMAIL
            $email = User::where("email", $request->email)
                ->where("id", "!=", $request->id)
                ->where('level', 3)
                ->first();
            if ($email!=null) {
                return response()->json([
                    'result' => false,
                    'msg' => "El email introducido ya está en uso"
                ]);
            }

            //$phone = Person::where("phone", $request->phone)->first();
            // VALIDANDO TELEFONO
            $phone = User::join("persons", "persons.user_id", "=", "users.id")
                ->where("users.id", "!=", $request->id)
                ->where("persons.phone", $request->phone)
                ->where("users.level", 3)
                ->first();

            if ($phone!=null) {
                return response()->json([
                    'result' => false,
                    'msg' => "El teléfono introducido ya está en uso"
                ]);
            }

            $user = User::find($request->id);
            $user->email = $request->email;
            $user->save();

            $person = Person::where('user_id',$request->id)->first();
            $person->phone = $request->phone;
            $person->save();

            $user = User::where("users.id", $request->id)
                ->join("persons", "users.id", "=", "persons.user_id")
                ->select("users.id", "email", "latitud as lat", "longitud as lng", "validate", "name", "lastname", "phone", "photo", "document", "document_photo")
                ->first();

            return response()->json([
                'result' => true,
                'user' => $user
            ]);
        }
    }

    public function upAvatar(Request $r) {

        $ruta = 'passengers/profile/'.substr(str_replace(array('/','.'), array('',''),bcrypt(strtotime(date('Y-m-d H:i:s')))),7,32).'.jpg';
        Image::make($r->file('foto'))->rotate($r->rotate * -1)->save($ruta,60);

        $person = Person::where("user_id", $r->id)->first();
        $person->photo = $ruta;
        $person->save();

        $user = User::where("users.id", $r->id)
            ->join("persons", "users.id", "=", "persons.user_id")
            ->select("users.id", "email", "latitud as lat", "longitud as lng", "validate", "name", "lastname", "phone", "photo", "document", "document_photo")
            ->first();

        return response()->json(['success'=>true, 'user'=>$user]);

    }

    public function getDriver(Request $r) {

        $driver = User::where("users.id", $r->id)
            ->with(["vehicles" => function($vehicles) {
                $vehicles->where("status", 1)->with(["photos", "vehicle_categories.vehicle_type"]);
            }, "person"])
            ->first();

        $rantng = number_format(Rating::where('reviewed_id',$r->id)->count() == 0 ? 0 : Rating::where('reviewed_id',$r->id)->sum('points') / Rating::where('reviewed_id',$r->id)->count(),2);

        $driver = [
            "lat" => $driver->latitud,
            "lng" => $driver->longitud,
            "name" => $driver->person->name,
            "lastname" => $driver->person->lastname,
            "phone" => $driver->person->phone,
            "photo" => $driver->person->photo,
            "plate" => $driver->vehicles[0]->plate,
            "model" => $driver->vehicles[0]->vehicle_categories[0]->vehicle_type->name,
            "brand" => $driver->vehicles[0]->brand,
            "file" => $driver->vehicles[0]->photos[0]->file,
            "rantng" => $rantng,
            "average" => $driver->average,
            "averageBogui" => $driver->averageBogui,
            "debt" => $driver->debt,
            "weekly_debt" => $driver->weekly_debt,
            "carried" => $driver->carried,
            "last_week_carried" => $driver->last_week_carried
        ];

        return response()->json([
            "success"=>true,
            "driver"=>$driver
        ]);

    }

    public function _getDriver(Request $r) {

        $driver = User::where("users.id", $r->id)
            ->join("persons", "users.id", "=", "persons.user_id")
            ->join("vehicles", "users.id", "=", "vehicles.user_id")
            ->join("vehicles_photos", "vehicles.id", "=", "vehicles_photos.vehicle_id")
            ->select("latitud as lat", "longitud as lng", "name", "lastname", "phone", "photo", "plate", "model" ,"brand", "vehicles_photos.file")
            ->first();

        $rantng = number_format(Rating::where('reviewed_id',$r->id)->count() == 0 ? 0 : Rating::where('reviewed_id',$r->id)->sum('points') / Rating::where('reviewed_id',$r->id)->count(),2);

        $driver = [
            "lat" => $driver->latitud,
            "lng" => $driver->longitud,
            "name" => $driver->person->name,
            "lastname" => $driver->person->lastname,
            "phone" => $driver->person->phone,
            "photo" => $driver->person->photo,
            "plate" => $driver->vehicles[0]->plate,
            "model" => $driver->vehicles[0]->vehicle_categories[0]->vehicle_type->name,
            "brand" => $driver->vehicles[0]->brand,
            "file" => $driver->vehicles[0]->photos[0]->file,
            "rantng" => $rantng,
            "average" => $driver->average,
            "averageBogui" => $driver->averageBogui,
            "debt" => $driver->debt,
            "weekly_debt" => $driver->weekly_debt,
            "carried" => $driver->carried,
            "last_week_carried" => $driver->last_week_carried
        ];

        return response()->json([
            "success"=>true,
            "driver"=>$driver
        ]);

    }

    public function defaultPayment(Request $request) {

        $user = User::find($request->id);
        $user->default_payment = $request->num;
        $user->save();

        $route = Route::where("user_id", $request->id)
            ->where("status", "1")
            ->where( "paid", "0")
            ->first();

        if ($route!=null) {
            $route->payment = $request->num;


            // return response()->json(["route.id"=>$route->id]);

            // dd(">>: route id: $route->id");
            $payment = Payment::where("route_id", $route->id)
                ->whereNull("promotion_id")
                ->first();
            $payment->method = $request->num;

            $route->save();
            $payment->save();
        }

        return response()->json([
            'result' => true,
            'user' => $user
        ]);

    }

    public function validarPasajero($id) {

        $user = User::where("id", $id)->first();

        if ($user==null) return response()->json(false);

        if ($user->level!=3) return response()->json(false);

        return response()->json(true);

    }
    
}