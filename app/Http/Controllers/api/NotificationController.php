<?php

	namespace App\Http\Controllers\api;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\Notification;

	class NotificationController extends Controller {
	    
		public function get(Request $request) {
			$notificaciones = Notification::orderBy('id','desc')->where('user_id',$request->id)->with(['route' => function($q) {
				$q->with(['payments','to_destinies','to_user' => function ($q) {
		            $q->with(['person']);
		        }]);
			}])->paginate(20);

			Notification::where('user_id',$request->id)->where('viewed','0')->update(['viewed' => '1']);

			return response()->json([
				'result' => true,
				'notificaciones' => $notificaciones
			]);
		}
	}
