<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tariff;
use App\User;
use DB;
class TariffController extends Controller {

    public function all(Request $r) {

        $data = Tariff::select('vehicle_type.photo','tariffs.*')
            ->join('vehicle_type','vehicle_type.id','=','tariffs.vehicle_type_id')
            ->where('vehicle_type_id',$r->vehicle)
            ->first();
        
        $user = User::select(DB::raw("(acos(sin(radians(".$r->latitude.")) * sin(radians(users.latitud)) + 
        cos(radians(".$r->latitude.")) * cos(radians(users.latitud)) * 
        cos(radians(".$r->longitude.") - radians(users.longitud))) * 6371) as distancia"))
            ->join('vehicles','vehicles.user_id','=','users.id')
            ->whereNotNull('users.latitud')
            ->whereNotNull('users.longitud')
            // ->where('users.validate','1')
            ->where('vehicles.vehicle_type_id',$r->vehicle)
            ->orderBy(DB::raw("(acos(sin(radians(".$r->latitude.")) * sin(radians(users.latitud)) + 
            cos(radians(".$r->latitude.")) * cos(radians(users.latitud)) * 
            cos(radians(".$r->longitude.") - radians(users.longitud))) * 6371)"),'DESC')
            ->first();

        return response()->json([
            'result'=>true,
            'tarifas'=>$data,
            'user'=>$user
        ]);
    }

    public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
        // Cálculo de la distancia en grados
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
     
        // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)
        switch($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
                break;
            case 'mi':
                $distance = $degrees * 69.05482; // 1 grado = 69.05482 millas, basándose en el diametro promedio de la Tierra (7.913,1 millas)
                break;
            case 'nmi':
                $distance =  $degrees * 59.97662; // 1 grado = 59.97662 millas naúticas, basándose en el diametro promedio de la Tierra (6,876.3 millas naúticas)
        }
        return round($distance, $decimals);
    }
}
