<?php

    namespace App\Http\Controllers\api;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    use Freshwork\Transbank\CertificationBagFactory;
    use Freshwork\Transbank\TransbankServiceFactory;
    use Freshwork\Transbank\RedirectorHelper;
    use Freshwork\Transbank\CertificationBag;
    use Freshwork\Transbank\Log\LoggerFactory;
    use Freshwork\Transbank\Log\TransbankCertificationLogger;
    use Freshwork\Transbank\Log\LogHandler;
    use Freshwork\Transbank\Log\LoggerInterface;

    use App\Models\Webpay;
    use Carbon\Carbon;
    use App\Models\Route;
    use App\Models\Payment;
    use App\User;

    class WebpayController extends Controller {

        protected $oneclick;
        protected $id = 39;
        protected $test = false;

        public function __construct() {
            $bag = CertificationBagFactory::integrationOneClick();

            // $bag = new CertificationBag(
            //     \URL('certs/597032710158.key'),
            //     \URL('certs/597032710158.crt'),
            //     null,
            //     CertificationBag::INTEGRATION
            // );

            // $bag = CertificationBagFactory::production('certs/597032710158.key','certs/597032710158.crt');

            LoggerFactory::setLogger(new TransbankCertificationLogger('./transbank'));

            $this->oneclick = TransbankServiceFactory::oneclick($bag);
        }

        public function check(Request $request) {
            $user = User::find($request->id);
            return response()->json([
                'result' => $user->hasAuthorizedCreditCard() != null
            ]);
        }

        public function get(Request $request) {
            $user = Route::find($request->id)->to_user;
            if (!$user->hasAuthorizedCreditCard()) {
                $response = $this->oneclick->initInscription($user->email, $user->email, \URL('api/webpay/continue?id='.$request->id));
                return RedirectorHelper::redirectHTML($response->urlWebpay, $response->token);
            }
            else {
                return Redirect('api/webpay/response?id='.$request->id);
            }            
        }

        public function continuar(Request $request) {
            $response = $this->oneclick->finishInscription();

            $user = Route::find($request->id)->to_user;

            LogHandler::log(['title' => 'Inscripción de usuario', 'response' => $response, 'user' => $user->id]); 

            if ($response->responseCode == 0) {
                    $user->webpay = $response->tbkUser;
                    $user->card = $response->last4CardDigits;
                $user->save();
                return Redirect('api/webpay/response?id='.$request->id);
            }

            LogHandler::log(['title' => 'Error al realizar una Inscripción', 'response' => $response, 'user' => $user->id]); 

            return View('webpay.response')->with([
                'data' => [
                    'id' => $this->test ? $this->id : $user->id,
                    'status' => 0,
                    'error' => 'Error al finalizar la inscripción'
                ]
            ]);
        }

        public function response(Request $request) {
            $ruta = Route::find($request->id);
            $user = $ruta->to_user;

            if ($ruta->paid == 1) {
                return View('webpay.response')->with([
                    'data' => [
                        'id' => $this->test ? $this->id : $user->id,
                        'status' => 0,
                        'error' => 'La ruta ya ha sido pagada'
                    ]
                ]);
            }

            try {
                $payment = Payment::where('route_id',$request->id)->where('method','1')->first();

                if ($payment == null) {
                    return View('webpay.response')->with([
                        'data' => [
                            'id' => $this->test ? $this->id : $user->id,
                            'status' => 0,
                            'error' => 'Error al buscar el Payment'
                        ]
                    ]);
                }

                $total = $payment->total;

                $transaction = new Webpay;
                    $transaction->amount = $total;
                    $transaction->user_id = $user->id;
                    $transaction->route_id = $request->id;
                $transaction->save();

                $buyOrder = $transaction->createNewBuyOrder();
                $transaction->save();

                $response = $this->oneclick->authorize($total, $buyOrder, $user->email, $user->webpay);

                if ($response->responseCode == 0) {
                    
                    $transaction->completed_at = Carbon::now();
                    $transaction->save();

                    LogHandler::log(['title' => 'Pago Realizado', 'user' => $user->id, 'order' => $buyOrder]); 

                    return View('webpay.response')->with([
                        'data' => [
                            'id' => $this->test ? $this->id : $user->id,
                            'status' => 1
                        ]
                    ]);
                }
                else {

                    LogHandler::log(['title' => 'Error al realizar el pago', 'user' => $user->id, 'order' => $buyOrder]); 

                    return View('webpay.response')->with([
                        'data' => [
                            'id' => $this->test ? $this->id : $user->id,
                            'status' => 0,
                            'error' => 'Error al pagar'
                        ]
                    ]);
                }
            } catch (\Exception $e) {

                LogHandler::log(['title' => 'Error al realizar el pago', 'user' => $user->id, 'error' => $e]); 

                return View('webpay.response')->with([
                    'data' => [
                        'id' => $this->test ? $this->id : $user->id,
                        'status' => 0,
                        'error' => 'Error al pagar (try)',
                        'e' => $e
                    ]
                ]);
            }
        }

        public function delete(Request $request) {
                $user = User::find($request->id);
                    $response = $this->oneclick->removeUser($user->webpay, $user->email);
                if ($response == 1) {
                        $user->webpay = null;
                        $user->card = null;
                    $user->save();

                    LogHandler::log(['title' => 'Eliminación de una Inscripción', 'user' => $user->id]); 

                    return response()->json([
                        'result' => true
                    ]);
                }
                else {

                    LogHandler::log(['title' => 'Inscripción de usuario', 'error' => $e, 'user' => $user->id]); 

                    return response()->json([
                        'result' => false
                    ]);     
                }
        }

        public function inscripcion(Request $request) {
            $user = User::find($request->id);
            $response = $this->oneclick->initInscription($user->email, $user->email, \URL('api/webpay/inscripcion/response?id='.$request->id));
            return RedirectorHelper::redirectHTML($response->urlWebpay, $response->token);     
        }

        public function inscripcionResponse(Request $request) {
            $response = $this->oneclick->finishInscription();

            $user = User::find($request->id);

            if ($response->responseCode == 0) {
                    $user->webpay = $response->tbkUser;
                    $user->card = $response->last4CardDigits;
                $user->save();

                LogHandler::log(['title' => 'Inscripción de usuario', 'response' => $response, 'user' => $user->id]); 

                return View('webpay.inscripcion')->with([
                    'data' => [
                        'id' => $this->test ? $this->id : $user->id,
                        'status' => 1
                    ]
                ]);
            }

            LogHandler::log(['title' => 'Error al realizar una Inscripción', 'response' => $response, 'user' => $user->id]); 

            return View('webpay.inscripcion')->with([
                'data' => [
                    'id' => $this->test ? $this->id : $user->id,
                    'status' => 0,
                    'error' => 'Error al finalizar la inscripción'
                ]
            ]);
        }
    }
