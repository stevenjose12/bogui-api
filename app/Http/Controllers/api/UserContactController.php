<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserContact;
use App\Models\PanicContact;
use App\User;
use Validator;

class UserContactController extends Controller
{
    public function getAllContact(Request $request){
        $contacts = UserContact::whereNull('deleted_at')
            ->where('user_id', $request->user_id)
            ->get();
                    
        $panic_contacts = PanicContact::all();

        $contacts = $contacts->merge($panic_contacts);
        
        return response()->json([
            'result' => true,
            'contacts' => $contacts
        ]);
    }

    public function getContact(Request $request) {
        $contacts = UserContact::whereNull('deleted_at')
            ->where('user_id', $request->user_id)
            ->get();

        return response()->json([
            'result' => true,
            'contacts' => $contacts,
        ]);
    }

    public function createContact(Request $request){
        $rules = [
            'name'=> 'required|string',
            'phone'=> 'required',
            'phoneCode'=> 'required'
		];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'phone' => 'Teléfono',
            'phoneCode' => 'Código de Área'
        ];
		$validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails())
            return response()->json([
                'msg'=>$validator->errors()->first(),
                'result' => false,
            ]);
        
        $userContact = new UserContact;
        $userContact->name = $request->name;
        $userContact->phoneCode = $request->phoneCode;
        $userContact->phone = $request->phone;
        $userContact->user_id = $request->user_id;
        $userContact->save();

        return response()->json([
            'result' => true,
            'msg' => 'El nuevo contacto se ha registrado exitosamente'
        ]);
    }

    public function editContact(Request $request){
        $rules = [
            'name'=> 'required|string',
            'phone'=> 'required',
            'phoneCode'=> 'required'
		];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'phone' => 'Teléfono',
            'phoneCode' => 'Código de Área'
        ];
		$validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails())
            return response()->json([
                'msg'=>$validator->errors()->first(),
                'result' => false,
            ]);
        $userContact = UserContact::where('id', $request->id)
                ->where('user_id', $request->user_id)
                ->whereNull('deleted_at')
                ->first();
        if(isset($userContact)){
            $userContact->name = $request->name;
            $userContact->phone = $request->phone;
            $userContact->phoneCode = $request->phoneCode;
            $userContact->save();
            return response()->json([
                'result' => true,
                'msg' => 'El contacto fue editado exitosamente'
            ]);
        }
        return response()->json([
            'result' => false,
            'msg' => 'Disculpe, ha ocurrido un error'
        ]);
    }

    public function deleteContact(Request $request){
        if(isset($request->id) && isset($request->user_id)){
            UserContact::where('id', $request->id)
                ->where('user_id', $request->user_id)
                ->delete();
            return response()->json([
                'result' => true,
                'msg' => 'El contacto fue eliminado exitosamente'
            ]);
        }
        return response()->json([
            'result' => false,
            'msg' => 'Disculpe, ha ocurrido un error'
        ]);
    }
}
