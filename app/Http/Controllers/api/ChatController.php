<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChatUser;
use App\Models\Chat;
use App\Models\Message;
use App\Models\Route;
use App\Models\DeletedChat;
use App\Libraries\Paginate;
use App\Models\Person;
use App\Models\SupportMotive;
use Intervention\Image\ImageManagerStatic as Image;
use App\User;

class ChatController extends Controller {

    public function test() {
        return response()->json(["success"=>true]);
    }

    public function supportGet(Request $request) {
        $user = User::find($request->user);
        if ($user->level == 2) {
            $filters = ['1', '3'];
        } elseif ($user->level == 3) {
            $filters = ['1', '2'];
        }
        $motivos = SupportMotive::where('type', $request->type)->where(function ($q) use ($filters) {
            $q->whereIn('type_user', $filters);
        })->get();
        return response()->json([
                'result' => true,
                'motivos' => $motivos
            ]);
    }

    public function createSupport(Request $request) {

        $moderador = User::select('users.id', \DB::raw('COUNT(chat_user.id) AS count'))
            ->leftJoin('chat_user', 'chat_user.user_id', '=', 'users.id')
            ->whereIn('level', [1,4])
            ->groupBy('users.id')
            ->orderBy('count')
            ->first();


        $chat = new Chat;
        $chat->support = "1";
        $chat->motive_id = $request->type;
        $chat->route_id = $request->route_id;
        $chat->save();

        $user = new ChatUser;
        $user->chat_id = $chat->id;
        $user->user_id = $request->id;
        $user->save();

        $user = new ChatUser;
        $user->chat_id = $chat->id;
        $user->user_id = $moderador->id;
        $user->save();

        return response()->json([
            'result' => true,
            'chat' => $chat->id
        ]);
    }

    public function asuntos(Request $request) {
        $asuntos = SupportMotive::select("id", "description")
            ->where("type", 2)
            ->whereIn("type_user", ["1", "2"])
            ->get();

        return response()->json(["asuntos"=>$asuntos]);
    }

    public function exist(Request $request) {
        $chat = Chat::where("route_id", $request->id)
            ->first();

        return response()->json(["exist"=>$chat!=null]);
    }

    public function get(Request $request) {

        //<editor-fold desc="CÓDIGO DEL CONDUCTOR">
        $deleted_chats = DeletedChat::where('user_id',$request->id)
            ->get()->pluck('chat_id');

        $chats = Chat::whereNotIn('id',$deleted_chats)->orderBy('updated_at','desc')->with(['message' => function($message) {
            $message->orderBy('created_at', 'asc');
        },'chat_user' => function($q) use ($request) {
            $q->where('user_id','!=',$request->id)->with(['user' => function($q) {
                $q->with(['person']);
            }]);
        }])->whereHas('chat_user',function($q) use ($request) {
            $q->where('user_id',$request->id);
        })->get()->take(250);

        $chats = Paginate::get($chats,20);

        return response()->json([
            'result' => true,
            'chats' => $chats
        ]);
        //</editor-fold>

        //<editor-fold desc="CÓDIGO VIEJO">
//        $chats = Chat::orderBy('updated_at', 'desc')
//            ->with([
//                'message' => function ($message) {
//                    $message->orderBy('created_at', 'asc');
//                },
//                'chat_user' => function ($q) use ($request) {
//                    $q->where('user_id', '!=', $request->id)
//                        ->with(['user' => function ($q) {
//                            $q->with(['person']);
//                        }]);
//                }
//            ])
//            ->whereHas('chat_user', function ($q) use ($request) {
//                $q->where('user_id', $request->id);
//            })
//            ->get()
//            ->take(250);
//
//        $chats = Paginate::get($chats, 20);
//
//        return response()->json([
//            'result' => true,
//            'chats' => $chats
//        ]);
//
        //</editor-fold>

        // for ($i = 0; $i < count($chats); $i++) {
        //     $last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;
        //     $q = DeletedChat::where('chat_id', $chats[$i]->id)
        //         ->where('user_id', $request->id);
        //     if ($last != null) {
        //         $q->whereDate('updated_at', '>', $last->created_at);
        //     }
        //     $deleted = $q->orderBy('id', 'desc')->first();
        //     if (count($deleted) > 0) {
        //         $chats->splice($i, 1);
        //         $i--;
        //     }
        // }


    }

    public function update(Request $request) {
        $chats = Chat::orderBy('updated_at', 'desc')
            ->with(['message' => function ($message) {
                $message->orderBy('created_at', 'asc');
            },'chat_user' => function ($q) use ($request) {
                $q->where('user_id', '!=', $request->id)->with(['user' => function ($q) {
                    $q->with(['person']);
                }]);
            }])->whereHas('chat_user', function ($q) use ($request) {
                $q->where('user_id', $request->id);
            })->whereIn('id', $request->ids)->get();

        for ($i = 0; $i < count($chats); $i++) {
            $last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

            $q = DeletedChat::where('chat_id', $chats[$i]->id)
                ->where('user_id', $request->id);

            if ($last != null) {
                $q->whereDate('updated_at', '>', $last->created_at);
            }

            $deleted = $q->orderBy('id', 'desc')->first();

            if (count($deleted) > 0) {
                $chats->splice($i, 1);
                $i--;
            }
        }

        return response()->json([
            'result' => true,
            'chats' => $chats
        ]);
    }

    public function delete(Request $request) {
        $deleted = new DeletedChat;
        $deleted->chat_id = $request->chat_id;
        $deleted->user_id = $request->user_id;
        $deleted->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function view(Request $request) {
        $person = Person::where('user_id', $request->id)->first();

        $chat = Chat::with(['route','support_type','message' => function ($message) {
            $message->orderBy('created_at', 'asc');
        },'chat_user' => function ($q) use ($request) {
            $q->where('user_id', '!=', $request->id)->with(['user' => function ($q) {
                $q->with(['person']);
            }]);
        }])->whereHas('chat_user', function ($q) use ($request) {
            $q->where('user_id', $request->id);
        })->where('id', $request->chat_id)->first();

        Message::where('user_id', '!=', $request->id)->where('chat_id', $chat->id)->update(['seen' => '1']);

        $chats = Chat::whereHas('chat_user', function ($q) use ($request) {
            $q->where('user_id', $request->id);
        })->get();

        for ($i = 0; $i < count($chats); $i++) {
            $last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

            $q = DeletedChat::where('chat_id', $chats[$i]->id)
                ->where('user_id', $request->id);

            if ($last != null) {
                $q->whereDate('updated_at', '>', $last->created_at);
            }

            $deleted = $q->orderBy('id', 'desc')->first();

            if (count($deleted) > 0) {
                $chats->splice($i, 1);
                $i--;
            }
        }

        $ids = $chats->pluck('id');

        $badge_chat = Message::whereIn('chat_id', $ids)->where('user_id', '!=', $request->id)->where('seen', '0')->count();

        return response()->json([
            'result' => true,
            'chat' => $chat,
            'person' => $person,
            'count' => $badge_chat
        ]);
    }

    public function count(Request $request) {
        $chats = Chat::whereHas('chat_user', function ($q) use ($request) {
            $q->where('user_id', $request->id);
        })->get();

        for ($i = 0; $i < count($chats); $i++) {

            $last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

            $q = DeletedChat::where('chat_id', $chats[$i]->id)
                ->where('user_id', $request->id);

            if ($last != null) {
                $q->whereDate('updated_at', '>', $last->created_at);
            }

            $deleted = $q->orderBy('id', 'desc')->first();

            if (count($deleted) > 0) {
                $chats->splice($i, 1);
                $i--;
            }

        }

        $ids = $chats->pluck('id');

        $count = Message::whereIn('chat_id', $ids)->where('user_id', '!=', $request->id)->where('seen', '0')->count();

        return response()->json([
            'result' => true,
            'count' => $count
        ]);
    }

    public function loadOne(Request $request) {
        $chat = Chat::with(['message' => function ($message) {
            $message->orderBy('created_at', 'asc');
        },'chat_user' => function ($q) use ($request) {
            $q->where('user_id', '!=', $request->id)->with(['user' => function ($q) {
                $q->with(['person']);
            }]);
        }])->whereHas('chat_user', function ($q) use ($request) {
            $q->where('user_id', $request->id);
        })->where('id', $request->chat_id)->first();

        return response()->json([
            'result' => true,
            'chat' => $chat
        ]);
    }

    public function seen(Request $request) {
        Message::where('id', $request->id)->update(['seen' => '0']);

        return response()->json([
            'result' => true
        ]);
    }

    public function create(Request $request) {
        $route = Route::find($request->id);
        
        $chat = Chat::where('route_id', $request->id)->where('support', '0')->orderBy('id', 'desc')->first();

        if ($chat != null) {
            return response()->json([
                'result' => true,
                'chat' => $chat->id
            ]);
        }

        $chat = new Chat;
        $chat->route_id = $request->id;
        $chat->support = '0';
        $chat->save();

        $user = new ChatUser;
        $user->chat_id = $chat->id;
        $user->user_id = $route->user_id;
        $user->save();

        $user = new ChatUser;
        $user->chat_id = $chat->id;
        $user->user_id = $route->driver_id;
        $user->save();

        return response()->json([
            'result' => true,
            'chat' => $chat->id
        ]);
    }

    public function image(Request $request) {
        $name = md5(date('YmdHis')).rand(0, 4000).'.png';
        $dg = 0;
        if ($request->has('dg')) {
            $dg = $request->dg;
        }
        Image::make($request->file)->rotate($dg * (-1))->save('chats/'.$name, 25);

        return response()->json([
            'result' => true,
            'name' => 'chats/'.$name
        ]);
    }


//    public function get(Request $r) {

//        /**
//         * array de id de chats donde participo.
//         */
//        $ids = Chat::select("chats.*")
//            ->join("chat_user", "chat_user.chat_id", "=", "chats.id")
//            ->where("chat_user.user_id", $r->id)
//            ->groupBy("chats.id")
//            ->pluck("chats.id");
//
//        $chats = null;
//
//        foreach ($ids as $id) {
//
//            $receiver = Chat::where("chats.id", $id)
//                ->join("chat_user", "chat_user.chat_id", "=", "chats.id")
//                ->join("users", "users.id", "=", "chat_user.user_id")
//                ->join("persons", "users.id", "=", "persons.user_id")
//                ->where("chat_user.user_id","!=" ,$r->id)
//                ->select("users.id as id", "name", "lastname", "photo", "route_id")
//                ->groupBy("chat_user.user_id")
//                ->first();
//
//            $chat = Chat::where("id", $id)
//                ->select("id", "support as isSupport", "updated_at as update")
//                ->with(["message" => function($message) {
//                    $message->select("id", "chat_id", "user_id", "text", "seen as isSeen", "created_at as created")
//                        ->orderBy("created_at", "ASC");
//                }])
//                ->first();
//
//            $chat->receiver = $receiver;
//
//            $chats[] = $chat;
//
//        }
//
//        return response()->json([
//            "success" => true,
//            "chats" => $chats,
//        ]);
//
//    }
//
//    public function getMessages(Request $r) {
//
//        $receiver = Chat::where("chats.id", $r->id)
//            ->join("chat_user", "chat_user.chat_id", "=", "chats.id")
//            ->join("users", "users.id", "=", "chat_user.user_id")
//            ->join("persons", "users.id", "=", "persons.user_id")
//            ->where("chat_user.user_id","!=" ,$r->id)
//            ->select("users.id as id", "name", "lastname", "photo", "route_id")
//            ->groupBy("chat_user.user_id")
//            ->first();
//
//        $valid = $this->isValid($receiver->route_id);
//        $receiver["valid"] = $valid;
//
//        $chat = Chat::where("id", $r->id)
//            ->select("id", "support as isSupport", "updated_at as update")
//            ->with(["message" => function($message) {
//                $message->select("id", "chat_id", "user_id", "text", "seen as isSeen", "created_at as created")
//                    ->orderBy("created_at", "ASC");
//            }])
//            ->first();
//
//        $chat->receiver = $receiver;
//
//        return response()->json(["chat"=>$chat]);
//
//    }
//
    public function createChat(Request $r) {
        $check_exist_reg = Chat::where(function ($query) use ($r) {
            return $query->where('route_id', $r->id)
                        ->whereNull('support')
                        ->whereNull('motive_id');
        })
        ->orWhere(function ($query) use ($r) {
            return $query->where('route_id', $r->id)
                        ->whereNotNull('support')
                        ->whereNotNull('motive_id');
        })
        ->count();

        if ($check_exist_reg > 1) {
            return response()->json(["success"=>false]);
        }

        $chat = new Chat();
        $chat->route_id = $r->route_id;
        $chat->support = "0";
        $chat->save();

        $chatUser = new ChatUser();
        $chatUser->chat_id = $chat->id;
        $chatUser->user_id = $r->client;
        $chatUser->save();

        $chatUser = new ChatUser();
        $chatUser->chat_id = $chat->id;
        $chatUser->user_id = $r->driver;
        $chatUser->save();

        return response()->json(["success"=>true]);
    }
//
//    private function isValid($id) {
//
//        $ruta= Route::where('user_id',$id)
//            ->where('status','1')
//            ->first();
//
//        return $ruta!=null;
//
//    }
}
