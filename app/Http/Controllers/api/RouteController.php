<?php

namespace App\Http\Controllers\api;

use App\Models\PromotionUser;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Route;
use App\Models\Destiny;
use App\Models\Rating;
use App\User;
use App\Models\Airport;
use App\Models\Tariff;
use App\Models\Payment;
use App\Models\Commission;
use App\Models\Search;
use Validator;

class RouteController extends Controller
{
    public function index()
    {
    }

    public function create()
    {
    }

    public function store(Request $request) {

        $rules = [
            'user_id'=>'required',
            'tiempo'=>'required',
            'vehicle_type'=>'required',
            'tarifa_id'=>'required',
        ];

        $msg = [
            'required'=>'el campo :attribute es requerido para el registro'
        ];

        $attr = [
            'user_id'=>'Usuario',
            'tiempo'=>'Tiempo',
            'vehicle_type'=>'Tipo de bogui',
            'tarifa_id'=>'Tipo de tarifa',
        ];

        $val = Validator::make($request->all(), $rules, $msg);
        $val->setAttributeNames($attr);

        if ($val->fails()) {
            return response()->json(['result'=>false,'msg'=>$val->errors()->first()]);
        } else {

            $haveRouteWaitingForBeActivated = Route::where('user_id', $request->user_id)
                ->where('status', '1')
                ->whereNull('driver_id')
                ->count();

            if($haveRouteWaitingForBeActivated > 0){
//            if (false) {
                return response()->json(['result'=>false,'msg'=>'Ya tienes una ruta esperando por ser aprobada, por favor espere...','d'=>$haveRouteWaitingForBeActivated]);
            } else {

                $cercano = $this->getDriverCercano($request->partida['lat'], $request->partida['lng'], $request->vehicle_type);

                $user = User::find($request->user_id);
                if ($cercano==null) return response()->json(['result'=>false, 'msg'=> 'No hay boguis cercano' ]);

                $user = User::find($request->user_id);
                $n = 0;
                $new = new Route;
                $new->user_id = $request->user_id;
                $new->latitud_origin = $request->partida['lat'];
                $new->longitud_origin = $request->partida['lng'];
                $new->description = $request->partida['descripcion'];
                $new->status = 1;
                $new->time = $request->tiempo;
                $new->payment_id = 1;
                $new->payment = $user->default_payment;
                $new->vehicle_type_id = $request->vehicle_type;
                $new->tariff_id = $request->tarifa_id;
                $new->cost = $request->monto;
                if ($request->distance!=null) {
                    $new->distance = $request->distance;
                }

                if ( $this->isAirport($request->partida['lat'], $request->partida['lng']) ) $n++;
                if ( $this->isAirport($request->llegada['lat'], $request->llegada['lng']) ) $n++;
                if ( $this->isAirport($request->parada['lat'],  $request->parada['lng']) )  $n++;

                if ($n>0) {
                    $new->special_tariff = 4;
                    $new->airport = $n;
                }

                $new->save();
                $commission = Commission::first();
                $valor_x_50 =  (floor(($request->monto * ($commission->amount / 100)) / 50) * 50);
//                $user = User::find($request->user_id);

                //<editor-fold desc="PAGO POR EL USUARIO">
                $payment = new Payment;
                $payment->method = $user->default_payment;
                $payment->total = $request->pago_pasajero;
                $payment->commission = $request->p_user;
                $payment->route_id = $new->id;
                $payment->user_id = $request->user_id;
                $payment->save();
                //</editor-fold>

                //<editor-fold desc="PAGO POR CODIGO">
                if ($request->pago_codigo>0) {
                    $payment = new Payment;
                    $payment->method = "3";
                    $payment->total = $request->pago_codigo;
                    $payment->commission = $request->p_code;
                    $payment->route_id = $new->id;
                    $payment->user_id = $request->user_id;
                    $payment->promotion_id = $request->code;
                    $payment->save();
                }
                //</editor-fold>

                if ($request->parada) {
                    $parada = new Destiny;
                    $parada->route_id = $new->id;
                    $parada->latitud = $request->parada['lat'];
                    $parada->longitud = $request->parada['lng'];
                    $parada->description = $request->parada['descripcion'];
                    $parada->type = 1;
                    $parada->save();
                }

                $parada = new Destiny;
                $parada->route_id = $new->id;
                $parada->latitud = $request->llegada['lat'];
                $parada->longitud = $request->llegada['lng'];
                $parada->description = $request->llegada['descripcion'];
                $parada->type = 2;
                $parada->save();

                $new->load(['payments', 'to_destinies','to_user.person']);
                $new->to_user->average = $new->to_user->average();

//                $users = User::select('users.id', "latitud as lat", "longitud as lng")
//                    ->join('vehicles','vehicles.user_id','=','users.id')
//                    ->join('vehicle_category','vehicles.id','=','vehicle_category.vehicle_id')
//                    ->whereNotNull('users.latitud')
//                    ->whereNotNull('users.longitud')
                ////                    ->whereNotNull('vehicles.vehicle_type_id')
//                    ->where('users.status_driver', "1")
//                    ->where('vehicle_category.vehicle_type_id',$request->vehicle_type)
//                    // ->where('users.validate','1')
//                    ->groupBy('users.id')
//                    ->orderBy(DB::raw("(acos(sin(radians(".$request->partida['lat'].")) * sin(radians(users.latitud)) +
//                    cos(radians(".$request->partida['lat'].")) * cos(radians(users.latitud)) *
//                    cos(radians(".$request->partida['lng'].") - radians(users.longitud))) * 6371)"),'ASC')
//                    ->get();
//
//                $cercano = null;
//                foreach ($users as $user) {
//                    if(!$this->isDriverOcuped($user->id)) {
//                        $cercano = $user;
//                        break;
//                    }
//                }

//                $cercano = $this->getDriverCercano($request->partida['lat'], $request->partida['lng'], $request->vehicle_type);

                return response()->json([
                    'result'=>true,
                    'msg'=>'Carrera registrada con éxito',
                    'solicitud'=>$new,
                    'cercano'=>$cercano
                ]);
            }
        }
    }

    public function reenviar(Request $r) {

        $new = Route::where('id', $r->id)
            ->with(['to_destinies','to_user.person'])
            ->first();

        return response()->json([
            "success"=>true,
            "route"=>$new
        ]);

    }

    public function CloneRoute(Request $request) {

        // SE BUSCA LA RUTA ORIGINAL QUE NINGUN CONDUCTOR ACEPTO
        $route = Route::find($request->id);

        // SE CARGAN LAS RELACIONES
        $route->load('to_destinies', 'to_payments');

        if ($route == NULL) return response()->json(['result' => FALSE, 'error' => 'Nose encontro la ruta']);

        // SE HACE UN CLON DEL REGISTRO
        $clone = $route->replicate();
        $clone->status = 1;
        $clone->push();

//        dd($clone);

        // SE RECORREN LAS RELACIONES PARA CLONAR TAMBIEN SUS REGISTROS
        foreach ($route->to_destinies as $destiny) {
            $clone->to_destinies()->create($destiny->toArray());
        }
        foreach ($route->to_payments as $payment) {
            $clone->to_payments()->create($payment->toArray());
        }
        // SE CALCULA LA DISTANCIA PARA RETORNAR EL CONDUCTOR MAS CERCANO
        $cercano = $this->getDriverCercano($clone->latitud_origin, $clone->longitud_origin, $clone->vehicle_type_id);

        // SE MARCA CON ESTATUS 4 (RECHAZADA) LA RUTA ORIGINAL
        $route->status = 4;
        $route->save();

        return response()->json([
                    'result'=>true,
                    'msg'=>'Carrera registrada con éxito',
                    'solicitud'=>$clone,
                    'cercano'=>$cercano,
                    'new' => Route::where('id', $clone->id)
                        ->with(['payments', 'to_destinies','to_user.person'])
                        ->first()
                ]);
    }

    public function boguiCercano(Request $r) {

        $commission = Commission::first();

        $commission_send = $commission->amount/100;

        $commission = Commission::first();

        $commission_send = $commission->amount/100;

//        $user = User::select('users.id', "latitud as lat", "longitud as lng")
//            ->join('vehicles','vehicles.user_id','=','users.id')
//            ->join('vehicle_category','vehicles.id','=','vehicle_category.vehicle_id')
//            ->whereNotNull('users.latitud')
//            ->whereNotNull('users.longitud')
        ////            ->whereNotNull('vehicles.vehicle_type_id')
//            ->where('vehicle_category.vehicle_type_id',$r->vehicle)
//            // ->where('users.validate','1')
//            ->groupBy('users.id')
//            ->orderBy(DB::raw("(acos(sin(radians(".$r->lat.")) * sin(radians(users.latitud)) +
//                cos(radians(".$r->lat.")) * cos(radians(users.latitud)) *
//                cos(radians(".$r->lng.") - radians(users.longitud))) * 6371)"),'ASC')
//            ->first();

        return response()->json([
            "cercano"=>$this->getDriverCercano($r->lat, $r->lng, $r->vehicle),
            "comision" => $commission_send
        ]);

    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        //
    }

    public function getRoutes(Request $request)
    {
        $routes = Route::where('id', $request->route_id)
        ->with(['to_destinies'])
        ->first();

        return $routes;
    }

    public function get(Request $request)
    {

        //<editor-fold desc="CURRENT TRAVEL">
        $ruta = Route::orderBy('id', 'desc')
            ->with(['to_destinies','driver' => function ($q) {
                $q->with(['vehicle', 'person']);
            }])
            ->where('user_id', $request->id)
            ->where('status', '1')
            ->first();
        //</editor-fold>

        if ($ruta!=null && $ruta->driver!=null) {
            $rantng = number_format(Rating::where('reviewed_id', $ruta->driver->id)
                ->count() == 0 ? 0 : Rating::where('reviewed_id', $ruta->driver->id)
                    ->sum('points') / Rating::where('reviewed_id', $ruta->driver->id)
                    ->count(), 2);

            $ruta->driver->ranting = $rantng;
        }

        $promedio = 0;
        if ($ruta!=null) {
            $promedio = User::find($ruta->user_id)->average();
        }

        $historial = Route::orderBy('id', 'desc')
            ->with(['to_destinies','driver' => function ($q) {
                $q->with(['person']);
            }])
            ->where('user_id', $request->id)
            ->where('status', '2')
            ->get();

        return response()->json([
            'result' => true,
            'ruta' => $ruta,
            'promedio' => $promedio,
            'historial' => $historial
        ]);
    }

    public function valid(Request $request) {
        $ruta= Route::where('user_id', $request->id)
            ->where('status', '1')
            //->whereNotNull('driver_id')
            ->first();

        if ($ruta!=null) {
            return response()->json([
                "result" => true,
                "id" => $ruta->id
            ]);
        }

        return response()->json([
            "result" => false,
        ]);
    }

    public function cancel(Request $r)
    {
        $route = Route::find($r->id);

        $route->canceled_by = $r->driver_id;
        $route->status = '3';
        $route->canceled_reason = "";
        $route->save();

        return response()->json([ 'result' => true ]);
    }

    public function getRouteSpecific(Request $r)
    {
        $route = Route::where('id', $r->id)
            ->with(['to_destinies', 'driver' => function ($q) {
                $q->with(['person']);
            }])
            ->first();

        if ($route->payment == '1') {
            $route->payment_type = 'Tarjeta de Credito';
        } else {
            $route->payment_type = 'Efectivo';
        }

        return response()->json([
            'success' => true,
            'route' => $route,
        ]);
    }

    public function getRoute(Request $r)
    {
        $route = Route::where('id', $r->id)
            ->with(['to_destinies', 'driver' => function ($q) {
                $q->with(['person']);
            }])
            ->where('status', '2')
            ->first();

        return response()->json([
            'success' => true,
            'route' => $route,
        ]);
    }

    public function getCurrentRoute(Request $r)
    {
        $route = Route::where('id', $r->id)
            ->with(['payments', 'to_destinies'=> function($destinies){
                $destinies->whereNull('deleted_at')->orderBy('type','ASC');
            },'tariff'])
            ->first();

        return response()->json([
            'success' => true,
            'route' => $route,
        ]);
    }

    public function getPointsRoute($id) {
        $route = Route::select("latitud_origin as lat", "longitud_origin as lng", "id")
            ->where("id", $id)
            ->with(["to_destinies" => function($destiny) {
                $destiny->select("id", "route_id", "latitud as lat", "longitud as lng");
            }])
            ->first();
        return $route;
    }

    public function getCurrentRouteEdit(Request $request) {
        $rules = [
            'route_id'=>'required',
            'lat_destiny'=>'required',
            'lng_destiny'=>'required',
            'cost'=>'required',
            'time'=>'required',
        ];

        $msg = [
            'required'=>'el campo :attribute es requerido para el registro'
        ];

        $attr = [
            'route_id'=>'Ruta',
            'lat_destiny'=>'Latitud destino',
            'lng_destiny'=>'Longitud destino',
            'cost'=>'Costo',
            'time'=>'Tiempo',
        ];

        $val = Validator::make($request->all(),$rules,$msg);
        $val->setAttributeNames($attr);

        if ($val->fails()) {
            return response()->json(['result'=>false,'msg'=>$val->errors()->first()]);
        } else {
            $route = Route::find($request->route_id);
            $route->cost = $request->cost;
            $route->time = $request->time;
            if ($request->distance!=null) {
                $route->distance = $request->distance;
            }
            $route->save();

            Destiny::where([
                ['route_id','=',$request->route_id],
                ['type','=','2']
            ])->delete();

            $newDestiny = new Destiny;
            $newDestiny->latitud = $request->lat_destiny;
            $newDestiny->longitud = $request->lng_destiny;
            $newDestiny->route_id = $request->route_id;
            $newDestiny->type = '2';
            $newDestiny->description = $request->descripcion;
            $newDestiny->save();

            $commission = Commission::first();
            $valor_x_50 =  (floor(($request->cost * ($commission->amount / 100)) / 50) * 50);

            $payment = Payment::where('route_id', $request->route_id)->whereIn('method', [1, 2])->first();

            if ($payment != NULL) {
                $payment->total = $request->cost;
                $payment->commission = $valor_x_50;
                $payment->save();
            }

            $points = $this->getPointsRoute($request->route_id);
            $n = 0;

            if ( $this->isAirport($points->lat, $points->lng) ) $n++;
            if ( $this->isAirport($points->to_destinies[0]->lat, $points->to_destinies[0]->lng) ) $n++;
            if ( $points->to_destinies->count()>1 && $this->isAirport($points->to_destinies[1]->lat, $points->to_destinies[1]->lng) ) $n++;
            if ($n>0) {
                $route = Route::find($request->route_id);
                $route->airport = $n;
                $route->special_tariff = "4";
                $route->save();
            }
            return response()->json(['result'=>true,'ruta'=>$route, "points"=> $points, "n"=>$n]);
        }
    }

    public function getCurrentRouteEdit_(Request $request) {
        $rules = [
            'route_id'=>'required',
            'lat_destiny'=>'required',
            'lng_destiny'=>'required',
            'cost'=>'required',
            'time'=>'required',
        ];

        $msg = [
            'required'=>'el campo :attribute es requerido para el registro'
        ];

        $attr = [
            'route_id'=>'Ruta',
            'lat_destiny'=>'Latitud destino',
            'lng_destiny'=>'Longitud destino',
            'cost'=>'Costo',
            'time'=>'Tiempo',
        ];

        $val = Validator::make($request->all(),$rules,$msg);
        $val->setAttributeNames($attr);

        if ($val->fails()) {
            return response()->json(['result'=>false,'msg'=>$val->errors()->first()]);
        } else {
            $route = Route::find($request->route_id);
            $route->cost = $request->cost;
            $route->time = $request->time;
            if ($request->distance!=null) {
                $route->distance = $request->distance;
            }
            $route->save();

            Destiny::where([
                ['route_id','=',$request->route_id],
                ['type','=','2']
            ])->delete();

            $newDestiny = new Destiny;
            $newDestiny->latitud = $request->lat_destiny;
            $newDestiny->longitud = $request->lng_destiny;
            $newDestiny->route_id = $request->route_id;
            $newDestiny->type = '2';
            $newDestiny->save();

            $commission = Commission::first();
            $valor_x_50 =  (floor(($request->cost * ($commission->amount / 100)) / 50) * 50);

            $payment = Payment::where('route_id', $request->route_id)->whereIn('method', [1, 2])->first();

            if ($payment != NULL) {
                $payment->total = $request->cost;
                $payment->commission = $valor_x_50;
                $payment->save();
            }

            return response()->json(['result'=>true,'ruta'=>$route]);
        }
    }

    public function isAirport($lat, $lng) {
        $airport = Airport::all()->first();
        if ($airport != null) {
            return $this->getDistance($lat, $lng, $airport->lat, $airport->lng)<2;
        }
        return response()->json(['result' => false]);
    }

    /**
     * @param $lat1 latitud de origen
     * @param $lng1 longitud de origen
     * @param $lat2 latitud destino
     * @param $lng2 longitud destinio
     * @return float|int Distancia entre dos puntos en kilometros.
     */
    private function getDistance($lat1, $lng1, $lat2, $lng2) {
        if (($lat1==$lat2) && ($lng1==$lng2)) {
            return 0;
        }
        $degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng1-$lng2)))));
        return round($degrees*111.13384, 2);
    }

    public function isAirportRoute(Request $r) {

        $n = 0;
        $cost = 0;

        if ( $this->isAirport($r->partida['lat'], $r->partida['lng']) ) $n++;
        if ( $this->isAirport($r->llegada['lat'], $r->llegada['lng']) ) $n++;
        if ( $this->isAirport($r->parada['lat'],  $r->parada['lng']) )  $n++;

        if ($n>0) {
            $rate = Tariff::where("id", "4")->first()->base_tariff;
            $cost = $n*$rate;
        }

        return response()->json([
            "success" => $cost>0,
            "cost" => $cost
        ]);

    }

    public function isAirportRoute_(Request $r) {

        $out = ($r->parada)
            ? ($this->isAirport($r->partida['lat'], $r->partida['lng']) || $this->isAirport($r->llegada['lat'], $r->llegada['lng']) || $this->isAirport($r->parada['lat'], $r->parada['lng']) )
            : ($this->isAirport($r->partida['lat'], $r->partida['lng']) || $this->isAirport($r->llegada['lat'], $r->llegada['lng']));

        $cost = null;
        if ($out) {
            $cost = Tariff::where("id", "4")
                ->select("base_tariff as cost")
                ->first();
        }

        return response()->json([
            "success" => $out,
            "cost" => $cost["cost"]
        ]);

    }

    public function isDriverOcuped($id) {
        $route = Route::where("driver_id", $id)
            ->where("status", "1")
            ->first();
        return $route!=null;
    }

    public function getDriverCercano($lat, $lng, $vehicle_type) {

        switch ($vehicle_type) {
            case '1': // SEDAN
                $siguiente = '2'; // SUV
                break;
            case '2': // SUV
                $siguiente = '3'; // VAN
                break;
            case '3': // VAN
                $siguiente = '3'; // VAN
                break;
            default:
                $siguiente = $vehicle_type;
        }

        $drivers = User::select('users.id', "latitud as lat", "longitud as lng")
            ->join('vehicles', 'vehicles.user_id','=','users.id')
            ->join('vehicle_category', 'vehicles.id','=','vehicle_category.vehicle_id')
            ->whereNotNull('users.latitud') // que tenga una lat y lng definida
            ->whereNotNull('users.longitud')
            ->where('users.alive', "1")
            ->where('users.status_driver', "1") // <- que este conectado
            ->where("users.status", "1") // <- // que este activo
            ->where("users.level", "2") // <- que sea conductor
            ->where("vehicles.status",1)
            //->where('vehicle_category.vehicle_type_id', $vehicle_type)
            ->whereIn('vehicle_category.vehicle_type_id', [ $vehicle_type, $siguiente])
            ->groupBy('users.id')
            ->orderBy(DB::raw("(acos(sin(radians(".$lat.")) * sin(radians(users.latitud)) + 
                cos(radians(".$lat.")) * cos(radians(users.latitud)) * 
                cos(radians(".$lng.") - radians(users.longitud))) * 6371)"),'ASC')
            ->get();

        // test ;
        //return response()->json(["drivers"=>$drivers, "vehicle_tpe"=> $vehicle_type]);

        $km = Search::find(1);

        foreach ($drivers as $driver) {
            $distancia = $this->getDistanceKm((double)$lat, (double)$lng, (double)$driver->lat, (double)$driver->lng);

            if (!$this->isDriverOcuped($driver->id) && $distancia <= $km->radius) {
            //if ( !$this->isDriverOcuped($driver->id) ) {
                return $driver;
            }

        }

        return null;
    }

    public function getRadius() {
        $radius = Search::select("radius")
            ->get()
            ->first();

        return response()->json(["success"=>true, "radius"=>$radius->radius]);
    }

    public function pagar(Request $r) {

        // PAGO DEL USUARIO
        $payment = Payment::where("route_id", $r->id)
            ->first();

        if ($payment==null) {
            $payment = new Payment();
            $payment->method = $r->payMethod;
            $payment->total = $r->total_user;
            $payment->commission = $r->p_user;
            $payment->route_id = $r->id;
            $payment->user_id = $r->user_id;
            $payment->save();
        } else {
            $payment->method = $r->payMethod;
            $payment->total = $r->total_user;
            $payment->commission = $r->p_user;
            $payment->route_id = $r->id;
            $payment->user_id = $r->user_id;
            $payment->save();
        }

        // PAGO CUPON
        if ($r->code!=0) {

            $pay = Payment::where("route_id", $r->id)
                ->where("method", "3")
                ->first();

            if ($pay==null) $pay = new Payment();
            $pay->method = "3";
            $pay->total = $r->total_code;
            $pay->commission = $r->p_code;
            $pay->route_id = $r->id;
            $pay->user_id = $r->user_id;
            $pay->promotion_id = $r->code;
            $pay->save();

            $promotion = PromotionUser::where("promotion_id", $r->code)
                ->where("user_id", $r->user_id)
                ->first();

            if ($promotion!=null) {
                $promotion->paid = "1";
                $promotion->save();
            }

        }

        $commission = Commission::first();
        $commission_send = $commission->amount/100;

        $route = Route::find($r->id);
        $route->payment = $r->payMethod;
        $route->paid = "1";
        $route->save();
        return response()->json(["success"=>true, 'comision' => $commission_send]);

    }

    public function getDriverRoute(Request $r) {

        $route = Route::where("id", $r->id)
            ->with("driver.person")
            ->first();

        return response()->json(["route"=>$route]);

    }

    public function calificar(Request $request) {
        $rating = new Rating;
        $rating->reviewed_id = $request->reviewed_id;
        $rating->reviewer_id = $request->reviewer_id;
        $rating->route_id = $request->route;
        $rating->points = $request->points;
        $rating->bogui = $request->bogui;
        $rating->save();
        return response()->json([
            'result' => true
        ]);
    }

    public function rutasPorPagar(Request $r) {

        $ruta = Route::where("user_id", $r->id)
            ->where("status", "2")
            ->where("paid", "0")
            ->with("driver.person", "to_destinies")
            ->whereNotNull("driver_id")
            ->first();

        return response()->json(["success"=>$ruta!=null, "ruta"=>$ruta]);

    }

    public function routeCancelReject(Request $request)
    {
        $rules = [
            'route_id'=>'required',
        ];

        $msg = [
            'required'=>'el campo :attribute es requerido'
        ];

        $attr = [
            'route_id'=>'Ruta',
        ];

        $val = Validator::make($request->all(),$rules,$msg);
        $val->setAttributeNames($attr);

        if($val->fails()){
            return response()->json(['result'=>false,'msg'=>$val->errors()->first()]);
        }else{
            $ruta = Route::findOrFail($request->route_id);
            $ruta->status = '3';
            $ruta->save();
            return response()->json(['result'=>true]);
        }
    }

    private function getDistanceKm($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {

        // Cálculo de la distancia en grados

        $degrees = rad2deg(acos(min(max((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long))),-1.0),1.0)));

        // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)

        switch($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
                break;
            case 'mi':
                $distance = $degrees * 69.05482; // 1 grado = 69.05482 millas, basándose en el diametro promedio de la Tierra (7.913,1 millas)
                break;
            case 'nmi':
                $distance =  $degrees * 59.97662; // 1 grado = 59.97662 millas naúticas, basándose en el diametro promedio de la Tierra (6,876.3 millas naúticas)

        }

        return round($distance, $decimals);

    }

    public function getCost(Request $r) {
        $route = Route::find($r->id);
        return response()->json(["cost"=>$route->cost]);
    }

    public function paymentUpdate(Request $r) {

        //<editor-fold desc="PAGO USUARIO">
        $payment = Payment::where("route_id", $r->id)
            ->whereIn("method", ["1", "2"])
            ->first();

        if ($payment==null) new Payment();

        $payment->method     = $r->payMethod;
        $payment->total      = $r->total_user;
        $payment->commission = $r->p_user;
        $payment->route_id   = $r->id;
        $payment->user_id    = $r->user_id;
        $payment->save();
        //</editor-fold>

        //<editor-fold desc="PAGO CUPON">
        if ($r->code!=0) {
            $payment = Payment::where("route_id", $r->id)
                ->where("method", "3")
                ->first();

            if ($payment==null) new Payment();

            $payment->method       = "3";
            $payment->total        = $r->total_code;
            $payment->commission   = $r->p_code;
            $payment->route_id     = $r->id;
            $payment->user_id      = $r->user_id;
            $payment->promotion_id = $r->code;
            $payment->save();
        }
        //</editor-fold>

        return response()->json(["success"=>true]);

    }

    public function isValid($id) {

        $route = Route::find($id);

        // SI EL REBOTE FUE EXITOSO PERO NO LE LLEGO LA SOCKET AL PASAJERO
        if ($route->status==4) return response()->json(true);

        // EN CASO DE QUE EL STATUS NO SEA 4
        if ($route->status==1) {
            if ($route->driver_id!=null) {
                // EN CASO DE QUE LA CARRERA TENGA UN CONDUCTOR Y SIMPLEMENTE NO SE HALLA ACTUALIZADO LA APP DE PASAJERO
                return response()->json(false);
            } else {
                // EN CASO DE QUE EL REBOTE HALLA FALLADO, SE ESTABLECE MANUALMENTE EL STATUS EN 4
                $route->status = 4;
                $route->save();
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }

    }

}