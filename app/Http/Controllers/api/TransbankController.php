<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;

class TransbankController extends Controller
{

	public function index() {
	    $certificationBag = CertificationBagFactory::integrationOneClick();

	    $oneClick = TransbankServiceFactory::oneclick($certificationBag);

	    $response = $oneClick->initInscription('prueba2', 'pruebas2@boguipruebas.com', 'http://192.168.1.114/Boggui/public/api/webpayresponse');

	    $response->formUrl = RedirectorHelper::redirectHTML($response->urlWebpay, $response->token);

	    // var_dump(RedirectorHelper::redirectHTML($response->urlWebpay, $response->token));

	    // exit;

	    return response()->json([
	        'result' => true,
	        'response' => $response
	    ]);
	}

}
