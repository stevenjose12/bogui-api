<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Vehicle;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Responseunoccupied
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return tipos de vehiculos
     */
    public function vehicleTypes() {
        $data = DB::table('vehicle_type')->whereNull('deleted_at')->get();
        return $data;
    }

    /**
     * Retorna data de los vehiculos cercanos y disponibles
     */
    public function unoccupied(Request $r) {
//        if ($r->type == '1') {
//            $siguiente = '2';
//        } elseif ($r->type == '2') {
//            $siguiente = '3';
//        } else {
//            $siguiente = '3';
//        }

        switch ($r->type) {
            case '1': // SEDAN
                $siguiente = '2'; // SUV
                break;
            case '2': // SUV
                $siguiente = '3'; // VAN
                break;
            case '3': // VAN
                $siguiente = '3'; // VAN
                break;
            default:
                $siguiente = $r->type;
        }

        $data = DB::table('users')
        ->select('users.*','vehicles.id as vehicle','vehicle_category.vehicle_type_id as categoria')
        ->join('vehicles','vehicles.user_id','=','users.id')
        ->join('vehicle_category','vehicles.id','=','vehicle_category.vehicle_id')
        ->whereNotNull('users.latitud')
        ->whereNotNull('users.longitud')
        ->where('users.level', "2")
//                ->where('vehicle_category.vehicle_type_id',$r->type)
        ->where('users.status', "1")
        ->where('users.status_driver', "1")
        ->whereIn('vehicle_category.vehicle_type_id',[$r->type, $siguiente])
        ->groupBy('users.id')
        ->get();

        return response()->json([
            // "request"=>$r->all(),
            'result'=>true,
            'boguis'=>$data
        ]);

    }

}