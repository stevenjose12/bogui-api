<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\AuthRequest;
use Image;
use App\User;
use App\Models\Person;
use App\Models\Validation;
use App\Models\PasswordApp as Password;
use Hash;
use Illuminate\Support\Facades\Mail;
use Auth;

class RegisterController extends Controller {

    public function uploadAvatar(Request $request) {
        $ruta = 'passengers/profile/'.substr(str_replace(array('/','.'), array('',''),bcrypt(strtotime(date('Y-m-d H:i:s')))),7,32).'.jpg';
        Image::make($request->file('foto'))->rotate($request->rotate * -1)->save($ruta,60);
        return response()->json(['success'=>true, 'foto'=>$ruta]);
    }

    public function uploadDocument(Request $r) {
        $ruta = 'passengers/document/'.substr(str_replace(array('/','.'), array('',''),bcrypt(strtotime(date('Y-m-d H:i:s')))),7,32).'.jpg';
        Image::make($r->file('foto'))->rotate($r->rotate * -1)->save($ruta,60);
        return response()->json(['success'=>true, 'foto'=>$ruta]);
    }

    public function register(RegisterRequest $r) {

    	// VALIDANDO EMAIL
    	$user = User::where("email", $r->email)->where('level', 3)->first();
        if ($user!=null) {
            return response()->json([
                'success'=>false,
                "message"=>"El email ya esta siendo usado."]
            );
        }

        // VALIDANDO TELEFONO
        $person = User::join("persons", "persons.user_id", "=", "users.id")
        	->where("persons.phone", $r->phoneCode.$r->telefono)
        	->where("users.level", 3)
        	->first();
        if ($person!=null) {
            return response()->json(['success'=>false, "message"=>"Número telefónico ya esta siendo usado."]);
        }

        // VALIDANDO RUT
        $person = User::join("persons", "persons.user_id", "=", "users.id")
        	->where("persons.document_type", $r->documentType)
        	->where("persons.document", $r->document)
        	->where("users.level", 3)
        	->first();
        // $person = Person::where("document", $r->document)->where("document_type", $r->documentType)->where('level', 3)->first();
        if ($person!=null) {
            return response()->json(['success'=>false, "message"=>"Documento ya esta siendo usado."]);
        }

        $user = new User();
        $user->level = 3;
        $user->email = $r->email;
        $user->password = $r->password;
        $user->status = "0";
        $user->save();

        $person = new Person();
        $person->name = $r->nombre;
        $person->lastname = $r->apellido;
        $person->document_type = $r->documentType;
        $person->document = $r->document;
        $person->photo = $r->photo;
        $person->user_id = $user->id;
        $person->phone = $r->phoneCode.$r->telefono;
        $person->document_photo = $r->documentPhoto;
        $person->expiration_document = $r->expiration_document;
        $person->save();

        // - - - - - - - - - -

        $validation = new Validation();
        $validation->user_id = $user->id;
        $validation->code = $r->hash;
        $validation->save();

        Mail::send('emails.validate', ['codigo' => $r->hash], function($m) use ($user) {
            $m->to($user->email, $user->person->name.' '.$user->person->lastname)
                ->subject('Activación de su cuenta en Bogui | Bogui pasajero');
        });

        return response()->json(['success'=>true, "id"=>$user->id]);
    }

    public function login(AuthRequest $r) {

        $user = User::where('email',$r->email)
            ->where('level',3)
            ->first();

        if ($user==null) {
            return response()->json([
                'success' => false,
                'message' => 'Correo electrónico o contraseña incorrectos'
            ]);
        }

        if ($user->status == '2') {
            return response()->json([
                'success' => false,
                'message' => 'Su cuenta fue bloqueada'
            ]);
        }

        if ($user->level != '3') {
            return response()->json([
                'success' => false,
                'message' => 'Lo sentimos, su nivel de usuario no es suficiente para ingresar a la aplicación'
            ]);
        }

        if (Hash::check($r->password, $user->password)) {

            $user = User::where("users.id", $user->id)
                ->join("persons", "users.id", "=", "persons.user_id")
                ->select("users.id", "email", "latitud as lat", "longitud as lng", "validate", "name", "lastname", "phone", "photo", "document", "document_photo", "default_payment", "level", "users.deleted_at")
                ->first();

            return response()->json([
                'success' => true,
                'user' => $user
            ]);

        } else {
            return response()->json([
                'success' => false,
                'message' => 'Correo electrónico o contraseña incorrectos'
            ]);
        }

    }

    public function recuperar(Request $request) {

        Password::where('status','1')->where('email',$request->email)->update(['status' => '0']);

        $user = User::where('email',$request->email)->where('level', 3)->first();

        if (count($user) == 0) {
            return response()->json([
                'result' => false,
                'error' => "El correo electrónico no se encuentra registrado"
            ]);
        }

        if ($user->level != '3') {
            return response()->json([
                'result' => false,
                'error' => 'Lo sentimos, su nivel de usuario debe ser pasajero'
            ]);
        }

        $password = new Password;
            $password->email = $request->email;
            $password->code = $request->codigo;
        $password->save();

        Mail::send('emails.reset-pasajero', ['codigo' => $request->codigo], function ($m) use ($user) {
            $m->to($user->email,$user->person->name.' '.$user->person->lastname)
              ->subject('Recuperación de Contraseña | Bogui');
        });

        return response()->json([
            'result' => true
        ]);
    }

    public function validar(Request $request) {
        $password = Password::where('code',$request->codigo)
            ->where('email',$request->email)
            ->where('status','1')
//            ->where('level', 3)
            ->count();
        if ($password == 0) {
            return response()->json([
                'result' => false,
                'error' => "No se ha encontrado el código"
            ]);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function reset(Request $request) {
        $password = Password::where('code',$request->codigo)
            ->where('email',$request->email)
            ->where('status','1')
            ->count();
        if ($password == 0) {
            return response()->json([
                'result' => false,
                'error' => "No se procesar su solicitud"
            ]);
        }

        $user = User::where('email',$request->email)->first();
            $user->password = $request->password;
        $user->save();

        Password::where('status','1')->where('email',$request->email)->update(['status' => '0']);

        return response()->json([
            'result' => true
        ]);
    }

    public function resendMail(Request $r) {

        $user = User::find($r->id);

        $validation = Validation::where("user_id", $r->id)->first();
        if($validation==null) {
            $validation = new Validation();
            $validation->user_id = $r->id;
        }
        $validation->code = $r->hash;
        $validation->save();

        Mail::send('emails.validate', ['codigo' => $r->hash], function($m) use ($user) {
            $m->to($user->email, $user->person->name.' '.$user->person->lastname)
                ->subject('Activación de su cuenta en Bogui | Bogui pasajero');
        });

        return response()->json(["success"=>true, "message"=>"Código enviado"]);

    }

    public function validarEmail(Request $r) {

         $validation = Validation::where("user_id", $r->id)->first();

         if($r->code == $validation->code) {
             $user = User::find($r->id);
             $user->validate = 1;
             $user->save();
             $validation->delete();
             return response()->json([
                 "success"=>true,
                 "message"=>"Validación exitosa"
             ]);
         }
         return response()->json([
             "success"=>false,
             "message"=>"Código incorrecto"
         ]);

     }

}