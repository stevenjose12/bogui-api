<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotion;
use App\Models\PromotionUser;
use App\Models\Payment;
use App\Models\Route;
use App\Models\Commission;
use Carbon\Carbon;

class PromocionController extends Controller {

    public function getPromotionRoute(Request $r) {

        $payment = Payment::where("route_id", $r->id)
            ->where("method", "3")
            ->first();

        $commission = Commission::first();
        $commission_send = $commission->amount/100;

        if ($payment!=null) {

            $promocion = Promotion::select("promotions.id as id", "code", "amount", "promotions.expire_date as expire")
//                ->join("promotions", "promotions.id", "=", "promotion_user.promotion_id")
                ->where("promotions.id", $payment->promotion_id)
                ->first();

            return response()->json(["success"=> $promocion!=null, "promocion"=>$promocion, "promotion_id"=>$payment->promotion_id, "comision" => $commission_send]);
        }
        return response()->json(["success"=> false, "comision" => $commission_send]);

    }

    public function get(Request $r) {

        $now = Carbon::now()->format('Y-m-d');
        $promociones = PromotionUser::select("promotions.id as id", "code", "amount", "promotions.expire_date as expire")
            ->join("promotions", "promotions.id", "=", "promotion_user.promotion_id")
            ->whereDate('promotions.expire_date', '>=', $now)
            ->where("user_id", $r->id)
            ->where("accepted", "1")
            ->where("paid", "0")
            ->groupBy("promotions.id")
            ->orderBy("promotions.expire_date", "ASC")
            ->get();

//        $promociones = PromotionUser::select("id", "user_id", "promotion_id", "paid")
//            ->where("user_id", $r->id)
//            ->where("accepted", "1")
//            ->where("paid", "0")
//
//            ->with(["promotion" => function($promotion) use ($now){
//               $promotion->select("id", "code", "expire_date as expire", "amount")
//               ->whereDate('expire_date', '>=', $now);
//            }])
//            ->get();

        $promocioens_v = [];

        return response()->json(["success"=>true, "promociones"=>$promociones]);

    }

    public function accepted(Request $r) {

        $now = Carbon::now();

        $promocion = Promotion::where('code', $r->code)
            ->where('status', 1)
            ->whereDate('expire_date', '>=', $now->format('Y-m-d'))
            ->withCount('promotion_users')
            ->first();

        if (is_null($promocion)) {
            return response()->json(["success"=>false, 'msg' => 'El código que ingreso no existe, o ya ha expirado']);
        }

        $exists = PromotionUser::where('user_id', $r->id)
            ->where('promotion_id', $promocion->id)
            ->exists();

//        return response()->json(["exist"=>$exists]);

        if ($exists) {
            return response()->json([
                "success" => false,
                'msg' => "El código ingresado ya ha sido canjeado anteriormente"
            ]);
        }

        if( ($promocion->promotion_users_count < $promocion->num_uses) || ($promocion->num_uses==0) ) {

            if($promocion->type == 1) { // Si es para nuevos

                $tienePromociones = PromotionUser::where('user_id', $r->id)->exists();

                if($tienePromociones) {
                    return response()->json([
                        "success" => false,
                        "msg" => 'El código promocional es para pasajeros nuevos'
                    ]);
                }

            }

            $promotionUser = PromotionUser::create([
                'user_id' => $r->id,
                'accepted' => true,
                'promotion_id' => $promocion->id
            ]);

            // validando rutas activas por el usuario.
            $route = Route::where("user_id", $r->id)
                ->where("status", "1")
                ->first();

            if ($route!=null) { // HAY UNA RUTA ACTIVA

                $payment = Payment::where("route_id", $route->id)
                    ->where("method", "3")
                    ->first();

                //return response()->json(["route"=>$route, "payment"=>$payment]);

                if ($payment==null) { // RUTA SIN CUPON

                    // REVISAR

                    // CALCULANDO NUEVO VALORES
//                    if (this.code) total_code = this.code.amount>this.ruta.cost
//                        ? this.ruta.cost
//                        : this.code.amount;

                    $total_code = $promocion->amount > $route->cost
                        ? $route->cost
                        : $promocion->amount;

                    //<editor-fold desc="COMISION POR CODIGO">

                    $commission = Commission::first();

                    $commission_send = $commission->amount/100;

                    $p_code = $total_code*$commission_send;
                    if ($p_code%50!=0) {
                        $p_code = floor($p_code/50)*50;
                    }
                    //</editor-fold>

                    //<editor-fold desc="COMISION TOTAL DE LA RUTA">
                    $p_route = $route->cost*$commission_send;
                    if ($p_route%50!=0) {
                        $p_route = floor($p_route/50)*50;
                    }
                    //</editor-fold>

                    $p_user = $p_route-$p_code;

                    $pago_usuario = ($promocion->amount > $route->cost)
                        ? 0
                        : $route->cost-$promocion->amount;

                    $payment_user = Payment::where("route_id", $route->id)->first();
                    $payment_user->total = $pago_usuario;
                    $payment_user->commission = $p_user;
                    $payment_user->save();

                    $payment_code = new Payment();
                    $payment_code->method = "3";
                    $payment_code->total = $total_code;
                    $payment_code->commission = $p_code;
                    $payment_code->promotion_id = $promocion->id;
                    $payment_code->route_id = $route->id;
                    $payment_code->user_id = $r->id;
                    $payment_code->save();

                }

            }

            return response()->json([
                "success" => true,
                "msg" => 'Código canjeado correctamente'
            ]);
        }

        return response()->json([
            "success" => false,
            "msg" => 'Código agotado'
        ]);

    }

}