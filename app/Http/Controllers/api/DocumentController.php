<?php

	namespace App\Http\Controllers\api;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\PendingDocument;
	use App\Models\Person;
	use Intervention\Image\ImageManagerStatic as Image;
	use App\Libraries\DiskStorage;
	use App\Models\Vehicle;
	use App\Models\VehiclePhoto;
	use Carbon\Carbon;
	use App\User;

	class DocumentController extends Controller	{
	    
	    public function get(Request $request) {

	    	$documentos = PendingDocument::where(function($q) use ($request) {
	    		$q->where('upload',0)->where('user_id',$request->id)->whereIn('status',[0,2]);
	    	})->orWhere(function($q) use ($request) {
	    		$q->where('upload',1)->where('user_id',$request->id)->where('status',2);
	    	})->get();

	    	$documentos_pendientes = PendingDocument::where('upload',1)->where('user_id',$request->id)->where('status',0)->get();

	    	return response()->json([
	    		'result' => true,
	    		'documentos' => $documentos,
	    		'documentos_pendientes' => $documentos_pendientes
	    	]);
	    }

	    public function upload(Request $request) {
	    	$documento = PendingDocument::find($request->id);

    		$person = Person::where('user_id',$documento->user_id)->first();
		    	$name = DiskStorage::name();
		    	$dg = 0;
		    	if ($request->has('dg')) {
		    		$dg = $request->dg;
		    	}
		    	Image::make($request->file)->rotate($dg * (-1))->save('passengers/'.($this->getName($documento->type) == 'photo' ? 'profile' : 'document').'/'.$name,25);
	    		$person[$this->getName($documento->type)] = 'passengers/'.($this->getName($documento->type) == 'photo' ? 'profile/' : 'document/').$name;
	    	$person->save(); 	

	    	$documento->upload = 1;
	    	$documento->status = 0;
	    	$documento->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    private function getName($type) {
	    	$respuesta = "";
		  	switch ($type) {
		  		case 1:
		  			$respuesta = "photo";
		  			break;

		  		case 6:
		  			$respuesta = "document_photo";
		  			break;
		  	}
		    return $respuesta;
	    }
	}
