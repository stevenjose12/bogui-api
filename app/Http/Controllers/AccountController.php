<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Models\Store;

	class AccountController extends Controller {
	    
	    public function get() {
	    	return View('page.account');
	    }
	}
