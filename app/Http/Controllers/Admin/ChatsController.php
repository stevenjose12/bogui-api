<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\ChatUser;
use App\Models\Message;
use Auth;

class ChatsController extends Controller
{
    public function index()
    {
        $chats = Chat::where('support', '1')
                    ->with('support_type')
                    ->with(['chat_user' => function($query){
                        $query->where('user_id', '!=', Auth::user()->id)
                                ->with('users.person');
                    }])
                    ->withCount(['message' => function($query){
                        $query->where('user_id', '!=', Auth::user()->id)
                                ->where('seen', '0');
                    }])
                    ->orderBy('id', 'DESC')
                    ->get();

        $histories = Chat::where('support', '1')
                    ->with('support_type')
                    ->with(['chat_user' => function($query){
                        $query->where('user_id', '!=', Auth::user()->id)
                                ->with('users.person');
                    }])
                    ->onlyTrashed()
                    ->get();

        return view('admin.chats.index')->with([
            'chats' => $chats,
            'histories' => $histories,
            'user' => Auth::user()->id
        ]);
    }

    public function getMessages($id)
    {
        $messages = Message::where('chat_id', $id)->get();

        Message::where('chat_id', $id)->where('user_id', '!=', Auth::user()->id)->where('seen', '0')->update(['seen' => '1']);

        return [
            'messages' => $messages
        ];
    }

    public function getNewChat($id){
        $chat = Chat::where('support', '1')
            ->where('id', $id)
            ->with('support_type')
            ->with(['chat_user' => function($query){
                $query->where('user_id', '!=', Auth::user()->id)
                        ->with('users.person');
            }])
            ->withCount(['message' => function($q){
                $q->where('seen', '0');
            }])
            ->orderBy('message_count', 'ASC')
            ->orderBy('id', 'DESC')
            ->first();
        
        return response()->json([
            'chat' => $chat
        ]);
    }
}
