<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Models\VehicleCategory;
use App\Models\VehiclePhoto;
use Intervention\Image\ImageManagerStatic as Image;
use App\Libraries\DiskStorage;
use Validator;
use Carbon\Carbon;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::whereHas('user.person')->with(['photos', 'vehicle_type', 'user.person'])->get();

        foreach ($vehicles as $vehicle) {
            if ($vehicle->vehicle_type == null) {
                $vehicle->vehicle_type_name = 'Sin asignar';
            } else {
                $vehicle->vehicle_type_name = $vehicle->vehicle_type->name;
            }
        }

        return view('admin.vehicles.index')->with([
            'vehicles' => $vehicles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'brand' => 'required|max:255',
            'model' => 'required|max:255',
            'plate' => 'required',
            'year' => 'required|numeric|min:2000|max:' . date("Y")
        ];
        
        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'Ya esta registrado esta :attribute',
            'min' => 'El :attribute no puede ser menor a 2000',
            'max' => 'El :attribute no puede ser mayor al actual',
            'numeric' => 'El campo :attribute debe ser numerico'
        ];

        $attrs = [
            'brand' => 'Marca',
            'model' => 'Model',
            'plate' => 'Placa',
            'year' => 'Año'
        ];
        
        $validation = Validator::make($request->all(), $rules, $msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        $check_vehicles_cant = Vehicle::where('user_id', $request->user_id)->count();

        if($check_vehicles_cant == 2) {
            return response()->json([
                'error' => 'El conductor ya posee 2 vehiculos'
            ], 422);
        }

        $vehicle = new Vehicle();

        $check_plate = Vehicle::whereNull('deleted_at')->get();
        
        foreach ($check_plate as $check) {
            if ($check->plate == $request->plate) {
                return response()->json([
                    'error' => 'Ya esta registrada esta Placa'
                ], 422);
            }
        }

        $vehicle->brand = $request->brand;
        $vehicle->model = $request->model;
        $vehicle->plate = $request->plate;
        $vehicle->year = $request->year;
        $vehicle->expiration = Carbon::parse($request->expiration);
        $vehicle->status = '0';
        $vehicle->user_id = $request->user_id;

        if( ($request->hasFile('permission_photo_send')) ) {
            
            $url = 'drivers/';
            $url .= $request->file('permission_photo_send')->store('files', 'drivers');
            $vehicle->permit = $url;
        }

        $vehicle->save();

        if( ($request->hasFile('vehicle_photo_send')) ) {
            
            $name = DiskStorage::name();
            
            Image::make($request->file('vehicle_photo_send'))->save(DiskStorage::get('vehicles').$name,25);
            
            $vp = new VehiclePhoto;
            $vp->vehicle_id = $vehicle->id;
            $vp->file = 'vehicles/'.$name;
            $vp->save();
        }

        $vehicle_categories = explode(',', $request->vehicle_categories);

        if (count($vehicle_categories) > 0) {
            foreach ($vehicle_categories as $vc) {
                $vcv = new VehicleCategory;
                    $vcv->vehicle_id = $vehicle->id;
                    $vcv->vehicle_type_id = $vc;
                $vcv->save();
            }
        }

        $saved_vehicle = Vehicle::where('id',$vehicle->id)->with(['vehicle_categories', 'photos'])->first();

        return response()->json([
            'result' => true,
            'vehicle' => $saved_vehicle
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'brand' => 'required|max:255',
            'model' => 'required|max:255',
            'plate' => 'required',
            'year' => 'required|numeric|min:2000|max:' . date("Y")
        ];

        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'Ya esta registrado esta :attribute',
            'min' => 'El :attribute no puede ser menor a 2000',
            'max' => 'El :attribute no puede ser mayor al actual',
            'numeric' => 'El campo :attribute debe ser numerico',
        ];

        $attrs = [
            'brand' => 'Marca',
            'model' => 'Model',
            'plate' => 'Placa',
            'year' => 'Año'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        $vehicle = Vehicle::where('id',$id)->first();

        if ($vehicle->plate != $request->plate) {
            $check_plate = Vehicle::where('id','!=',$id)->whereNull('deleted_at')->get();
                foreach ($check_plate as $check) {
                    if ($check->plate == $request->plate) {
                        return response()->json([
                            'error' => 'Ya esta registrada esta Placa'
                        ], 422);
                    }
                }
        }

        $vehicle->brand = $request->brand;
        $vehicle->model = $request->model;
        $vehicle->plate = $request->plate;
        $vehicle->year = $request->year;
        $vehicle->expiration =  Carbon::parse($request->expiration);

        if( ($request->hasFile('permission_photo_send')) ) {
            
            $url = 'drivers/';
            $url .= $request->file('permission_photo_send')->store('files', 'drivers');
            $vehicle->permit = $url;
        }

        $vehicle->save();

        if ($request->hasFile('vehicle_photo_send')) {
            
            VehiclePhoto::where('vehicle_id', $id)->delete();
            
            $name = DiskStorage::name();
            
            Image::make($request->file('vehicle_photo_send'))->save(DiskStorage::get('vehicles').$name,25);
            
            $vp = new VehiclePhoto;
            $vp->vehicle_id = $id;
            $vp->file = 'vehicles/'.$name;
            $vp->save();
        }

        $vehicle_categories = explode(',', $request->vehicle_categories);

        VehicleCategory::where('vehicle_id', $id)->delete();

        if (count($vehicle_categories) > 0) {
            foreach ($vehicle_categories as $vc) {
                $vcv = new VehicleCategory;
                    $vcv->vehicle_id = $id;
                    $vcv->vehicle_type_id = $vc;
                $vcv->save();
            }
        }

        $saved_vehicle = Vehicle::where('id',$id)->with(['vehicle_categories', 'photos'])->first();

        return response()->json([
            'result' => true,
            'vehicle' => $saved_vehicle
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicle::find($id);
        $count_vehicles = Vehicle::where('user_id', $vehicle->user_id)->count();

        if ( ($count_vehicles == 1) ) {
            return response()->json([
                'error' => 'El conductor debe poseer al menos un vehículo'
            ], 422);
        }

        $vehicle->delete();
        VehicleCategory::where('vehicle_id', $id)->delete();
        VehiclePhoto::where('vehicle_id', $id)->delete();

        return response()->json([
            'result' => true
        ]);
    }

    public function activate($id)
    {
        $vehicle = Vehicle::with([
            'vehicle_categories'
        ])->where('id', $id)->first();
        
        $vehicle_diff = Vehicle::with([
            'vehicle_categories'
        ])->where([ 
            ['id', '!=', $id], 
            ['user_id', $vehicle->user_id] 
        ])->first();

        if ( !is_null($vehicle_diff) ) {
            if ( (count($vehicle_diff->vehicle_categories) <= 0) ) {
                return response()->json([
                    'error' => 'Debe poseer un vehículo con categorías'
                ], 422);
            }
        }

        if ( (count($vehicle->vehicle_categories) <= 0) ) {
            return response()->json([
                'error' => 'Debe asignarle una categoria al vehículo para continuar con la activación'
            ], 422);
        }

        if ($vehicle->status == '1') {

            $vehicle->status = '0';
            $msg = 'Se ha desactivado al vehículo con exito';

        } else {

            $vehicle->status = '1';
            $msg = 'Se ha activado al vehículo con exito';
        }

        $vehicle->save();

        $update = Vehicle::where('id', '!=', $id)->where('user_id', $vehicle->user_id)->update([
            'status' => ($vehicle->status == '1') ? '0' : '1'
        ]);

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'vehicle' => $vehicle
        ]);
    }

    public function uploadFiles(Request $request, $id)
    {
        if($request->hasFile('vehicle_photo')) {
            VehiclePhoto::where('vehicle_id', $id)->delete();
            $name = DiskStorage::name();
            Image::make($request->file('vehicle_photo'))->save(DiskStorage::get('vehicles').$name,25);
            $vp = new VehiclePhoto;
                $vp->vehicle_id = $id;
                $vp->file = 'vehicles/'.$name;
            $vp->save();

            return response()->json([
                'result' => true
            ]);
        }

        return response()->json([
            'result' => false
        ]);
    }
}
