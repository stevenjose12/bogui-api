<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Person;
use Mail;
use App\Mail\NewUser;
use Validator;

class ModeratorsController extends Controller
{
    /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {

            $mods = User::where('level', '4')
            ->with('person')->get();

            foreach ($mods as $user) {
                $user->person->photo = url('/').'/moderators/profile/'.$user->person->photo;
                if ($user->person->document_type == '1') {
                    $user->person->document_type_name = 'RUT';
                } else if ($user->person->document_type == '2') {
                    $user->person->document_type_name = 'Pasaporte';
                } else if ($user->person->document_type == '3') {
                    $user->person->document_type_name = 'Otro';
                }
            }

            return view('admin.mods.index')->with([
                'mods' => $mods
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $rules = [
                'name' => 'required|max:255',
                'lastname' => 'required',
                'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL'
            ];
            $msgs = [
                'required' => 'El campo :attribute es requerido',
                'unique' => 'Ya esta registrado este :attribute',
                'email' => 'Debe ingresar un :attribute valido'
            ];
            $attrs = [
                'name' => 'Nombre',
                'lastname' => 'Apellido',
                'email' => 'Correo Electrónico'
            ];
            
            $validation = Validator::make($request->all(),$rules,$msgs);
            $validation->setAttributeNames($attrs);

            if ($validation->fails()) {
                return response()->json([
                    'error' => $validation->messages()->first()
                ], 422);
            }

            $count_email = User::where('email', $request->email)->where('status', '1')->count();

            if($count_email > 0){
                return response()->json(['error' => 'Disculpe, el Correo Electrónico esta registrado'], 422);
            }

            $modules = json_decode($request->modules);

            if(count($modules) == 0){
                return response()->json([
                    'error' => 'Debes seleccionar al menos un permiso'
                ], 422);
            }

            $base_pass = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890*/-';
            $password = substr(str_shuffle($base_pass), 0, 6);

            $user = User::create([
                'email' => $request->email,
                'password' => $password,
                'level' => '4',
                'status' => '1',
                'validate' => 1
            ]);

            foreach ($modules as $key => $module) {
               $user->attachPermission($module);
            }

            $person = Person::create([
                'name' => $request->name,
                'lastname' => $request->lastname,
                'document_type' => '1',
                'document' => 'default.png',
                'photo' => 'default.png',
                'user_id' => $user->id
            ]);        
            
            Mail::to($request->email)->send(new NewUser($user, $password));

            if($request->hasFile('photo')) {
                $url = $request->file('photo')->store('profile', 'moderators');
                $person->photo = $url;
                $person->save();
            }

            if($request->hasFile('document')) {
                $url = $request->file('document')->store('documents', 'moderators');
                $person->document = $url;
                $person->save();
            }

            return response()->json([
                'result' => true
            ]);
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $user = User::with('person', 'permission_user')->where('id', $id)->first();

            $view = "admin.mods.edit_mods";

            return view($view)->with([
                'user' => $user,
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {

            $rules = [
                'name' => 'required|max:255',
                'lastname' => 'required',
                'email' => 'required|email|unique:users,email,'.$id.',id,deleted_at,NULL'
            ];
            $msgs = [
                'required' => 'El campo :attribute es requerido',
                'unique' => 'Ya esta registrado este :attribute',
                'email' => 'Debe ingresar un :attribute valido'
            ];
            $attrs = [
                'name' => 'Nombre',
                'lastname' => 'Apellido',
                'email' => 'Correo Electrónico'
            ];
            
            $validation = Validator::make($request->all(),$rules,$msgs);
            $validation->setAttributeNames($attrs);

            if ($validation->fails()) {
                return response()->json([
                    'error' => $validation->messages()->first()
                ], 422);
            }

            $count_email = User::where('email', $request->email)->where('status', '1')->where('id', '!=', $id)->count();

            if($count_email > 0){
                return response()->json(['error' => 'Disculpe, el Correo Electrónico esta registrado'], 422);
            }

            // Se serializa los objetos $modules
            $modules = json_decode($request->modules);

            if(count($modules) == 0){
                return response()->json([
                    'error' => 'Debes seleccionar al menos un permiso'
                ], 422);
            }

            // Setea todo los campos importantes para una persona
            $person = Person::where('user_id',$id)->first();
                $person->name = $request->name;
                $person->lastname = $request->lastname;
            $person->save();
            
            // Solo se setea el email de la persona
            $user = User::find($person->user_id);
                $user->email = $request->email;
            $user->save();

            $user->permission_user()->detach();

            foreach ($modules as $key => $module) {
                $user->permissions()->attach($module);
            }

            return response()->json([
                'result' => true
            ]);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Person::where("user_id",$id)->delete();
            User::find($id)->delete();
        }

        public function activate($id)
        {
            $user = User::find($id);
            if ($user->status == '1') {
                $user->status = '0';
                $msg = 'Se ha desactivado el Moderador con exito';
            } else {
                $user->status = '1';
                $msg = 'Se ha activado el Moderador con exito';
            }
            $user->save();

            return response()->json([
                'result' => true,
                'msg' => $msg,
                'user' => $user->status
            ]);
        }
}
