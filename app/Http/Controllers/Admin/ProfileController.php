<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $user->load('person');

        return view('admin.profile.index', [
            'user' => $user
        ]);
    }

    public function store(Request $request)
    {

    	$rules = [
    		'password' => 'confirmed|required' 
    	];
    	$msgs = [
    	    'required' => 'El campo :attribute es requerido',
    	    'confirmed' => 'Debe confirmar la Contraseña'
    	];
    	$attrs = [
    	    'password' => 'Contraseña'
    	];
    	
    	$validation = Validator::make($request->all(),$rules,$msgs);
    	$validation->setAttributeNames($attrs);

    	if ($validation->fails()) {
    	    return response()->json([
    	        'error' => $validation->messages()->first()
    	    ], 422);
    	}

        $user = Auth::user();
        if($request->password) {
            $user->password = $request->password;
        }
        $user->save();

        return $user;
    }
}
