<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Person;
use App\Models\Vehicle;
use App\Models\VehicleCategory;
use App\Models\Route;
use App\Models\Commission;
use Validator;
use Image;

class PassengersController extends Controller
{
    /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {

            $passengers = User::where('level', '3')
            ->with('person')
            ->with('pending_documents')
            ->with('has_been_passenger')
            ->orderBy('id', 'desc')
            ->withTrashed()
            ->get();

            foreach ($passengers as $user) {
                if ($user->person->document_type == '1') {
                    $user->person->document_type_name = 'RUT';
                } else if ($user->person->document_type == '2') {
                    $user->person->document_type_name = 'Pasaporte';
                } else if ($user->person->document_type == '3') {
                    $user->person->document_type_name = 'Otro';
                }
            }

            return view('admin.passengers.index')->with([
                'passengers' => $passengers
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $rules = [
                'name' => 'required|max:255',
                'lastname' => 'required|max:255',
                // 'email' => 'required|email|unique:users,email,'.$id.',id,deleted_at,NULL',
                'email' => 'required|email',
                'phone' => 'required',
                // 'document_photo' => 'required',
                // 'photo' => 'required'
            ];
            $msgs = [
                'required' => 'El campo :attribute es requerido',
                'unique' => 'Ya esta registrado este :attribute',
                'email' => 'Debe ingresar un :attribute valido',
                'max' => 'El :attribute no puede exceder los :size caracteres',
                'mimes' => 'El :attribute debe ser del formato JPG,JPEG o PNG'
            ];
            $attrs = [
                'name' => 'Nombre',
                'lastname' => 'Apellido',
                'email' => 'Correo Electrónico',
                'phone' => 'Telefono',
                // 'document_photo' => 'Foto del Documento',
                // 'photo' => 'Foto de Perfil'
            ];
            
            $validation = Validator::make($request->all(),$rules,$msgs);
            $validation->setAttributeNames($attrs);

            if ($validation->fails()) {
                return response()->json([
                    'error' => $validation->messages()->first()
                ], 422);
            }

            $users = User::where('id','!=',$id)->where('level','3')->get()->pluck('id');
            $current_user = User::where('id',$id)->first();
            $person = Person::where('user_id',$id)->first();
    
            if ($current_user->email != $request->email) {
                $check_email = User::whereIn('id', $users)->whereNull('deleted_at')->get();
                foreach ($check_email as $check) {
                    if ($check->email == $request->email) {
                        return response()->json([
                            'error' => 'Ya esta registrado este Correo Electronico'
                        ], 422);
                    }
                }
            }

            if ($person->phone != $request->phone) {
                $check_phone = Person::where('user_id','!=',$id)->whereNull('deleted_at')->get();
                    foreach ($check_phone as $check) {
                        if ($check->phone == $request->phone) {
                            return response()->json([
                                'error' => 'Ya esta registrado este Telefono'
                            ], 422);
                        }
                    }
            }

            if ($person->document != $request->document) {
                $check_document = Person::whereIn('user_id',$users)->whereNull('deleted_at')->get();
                    foreach ($check_document as $check) {
                        if ($check->document == $request->document) {
                            return response()->json([
                                'error' => 'Ya esta registrado este RUT o Documento'
                            ], 422);
                        }
                    }
            }
            
                $person->name = $request->name;
                $person->lastname = $request->lastname;
                $person->phone = $request->phone;
                $person->document_type = $request->document_type;
                $person->document = $request->document;
            $person->save();
            
            $user = User::find($id);
                $user->email = $request->email;
                // $user->status = $request->status;
                // if ($request->status == '1') {
                //     $user->validate = '1';
                // } else {
                //     $user->validate = '0';
                // }
                if ($request->default_payment == '' || $request->default_payment == null) {
                    $user->default_payment = null;
                } else {
                    $user->default_payment = $request->default_payment;
                }
            $user->save();

            // if($request->hasFile('photo')) {
            //     $url = 'passengers/';
            //     $url .= $request->file('photo')->store('profile', 'passengers');
            //     $person->photo = $url;
            //     $person->save();
            // }

            // if($request->hasFile('document_photo')) {
            //     $url = 'passengers/';
            //     $url .= $request->file('document_photo')->store('document', 'passengers');
            //     $person->document_photo = $url;
            //     $person->save();
            // }

            return response()->json([
                'result' => true
            ]);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Person::where("user_id",$id)->delete();
            User::find($id)->delete();
            $user = User::where('id',$id)->withTrashed()->first();

            return response()->json([
                'result' => true,
                'user' => $user->deleted_at
            ]);
        }

        public function travels($id)
        {
            $routes = Route::where('user_id', $id)
            ->whereHas('to_destinies')
            ->with(['driver.person' => function ($query){
                $query->withTrashed();
            }])
            ->with(['to_user.person' => function ($query){
                $query->withTrashed();
            }])
            ->with(['to_destinies' => function ($q) {
                $q->orderBy('type', 'asc')
                    ->orderBy('id', 'desc');
            }])
            ->with(['tariff', 'vehicle_type'])
            ->orderBy('id', 'desc')
            ->get();

            $commission = Commission::first();
            // foreach ($routes as $route) {
            //     if ($route->status == '0') {
            //         $route->status_type = 'Suspendida';
            //     } else if ($route->status == '1') {
            //         $route->status_type = 'Activa';
            //     } else if ($route->status == '2') {
            //         $route->status_type = 'Culminada';
            //     } else if ($route->status == '3') {
            //         $route->status_type = 'Cancelada';
            //     }

            //     if ($route->status_route == '0') {
            //         $route->status_pass = 'Sin Abordar';
            //     } else {
            //         $route->status_pass = 'Abordo';
            //     }
                
            //     $route->cost = floatval($route->cost);
                
            // }

            return view('admin.passengers.passenger-routes')->with([
                'routes' => $routes,
                'id' => $id
            ]);
        }

        public function activate($id)
        {
            $user = User::find($id);

            if ($user->status == '1') {

                $user->status = '0';
                $msg = 'Se ha Desactivado al Pasajero con éxito';

            } else {

                $user->status = '1';
                $msg = 'Se ha Activado al Pasajero con éxito';
            }

            $user->save();

            return response()->json([
                'result' => true,
                'msg' => $msg,
                'user' => $user->status
            ]);
        }

        public function block($id)
        {
            $user = User::find($id);

            if ($user->status == '1') {

                $user->status = '2';
                $msg = 'Se ha bloqueado al Pasajero con exito';

            } else if ($user->status == '0') {

                $user->status = '2';
                $msg = 'Se ha bloqueado al Pasajero con exito';

            } else if ($user->status == '2') {

                $user->status = '1';
                $msg = 'Se ha desbloqueado al Pasajero con exito';

            } else if ($user->status == '3') {

                $user->status = '2';
                $msg = 'Se ha bloqueado al Pasajero con exito';
            }

            $user->save();

            return response()->json([
                'result' => true,
                'msg' => $msg,
                'user' => $user->status
            ]);
        }

        public function passengersRatings()
        {
            $users = User::where('level', '3')
            ->with('person')->get();

            foreach ($users as $user) {
                if ($user->person->document_type == '1') {
                    $user->person->document_type_name = 'RUT';
                } else if ($user->person->document_type == '2') {
                    $user->person->document_type_name = 'Pasaporte';
                } else if ($user->person->document_type == '3') {
                    $user->person->document_type_name = 'Otro';
                }
                if ($user->level == '1') {
                    $user->level_type = 'Administrador';
                } else if ($user->level == '2') {
                    $user->level_type = 'Conductor';
                } else if ($user->level == '3') {
                    $user->level_type = 'Pasajero';
                } else {
                    $user->level_type = 'Moderador';
                }
            }

            return view('admin.ratings.ratings-passengers')->with([
                'users' => $users
            ]);
        }

        private function calcularDistancia($lat1, $lng1, $lat2, $lng2) {
            if( ($lat1==$lat2) && ($lng1==$lng2) ) return 0;
            $degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng1-$lng2)))));
            return round($degrees*111.13384, 2);
        }

        public function uploadFiles(Request $request,$id)
        {

            $person = Person::where('user_id',$id)->first();
            
            if($request->hasFile('photo')) {
                $url = 'passengers/';
                $url .= $request->file('photo')->store('profile', 'passengers');
                $person->photo = $url;
                $person->save();
                return response()->json([
                    'result' => true
                ]);
            }

            if($request->hasFile('document_photo')) {
                $url = 'passengers/';
                $url .= $request->file('document_photo')->store('document', 'passengers');
                $person->document_photo = $url;
                $person->save();
                return response()->json([
                    'result' => true
                ]);
            }

            return response()->json([
                'result' => false
            ]);
        }

        public function passengerValidate($id)
        {
            $user = User::find($id);

            if ($user->validate == '1') {
                $user->validate = '0';
                $msg = 'Se ha removido la validacion al Pasajero con exito';
            } else {
                $user->validate = '1';
                $msg = 'Se ha validado al Pasajero con exito';
            }
            $user->save();

            return response()->json([
                'result' => true,
                'msg' => $msg,
                'validate' => $user->validate
            ]);
        }
}
