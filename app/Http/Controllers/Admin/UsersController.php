<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Person;
use App\Models\Vehicle;
use App\Models\VehicleCategory;
use App\Models\Route;
use App\Models\Commission;
use App\Models\PendingDocument;
use Validator;
use Image;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::where('level', '2')
        ->with(['vehicles', 'vehicles.vehicle_categories'])
        ->whereHas('vehicle')
        ->whereHas('bank_user')
        ->with(['vehicle' => function ($query) { 
            $query->with('vehicle_categories', 'photos')->withTrashed();
        }])
        ->with(['vehicles_web' => function ($query) { 
            $query->with('vehicle_categories', 'photos')->orderBy('id');
        }])
        ->with('person')
        ->with('pending_documents')
        ->with(['bank_user' => function ($query) {
            $query->with('bank', 'account');
        }])
        ->with('routes')
        ->orderBy('id', 'desc')
        ->withTrashed()
        ->get();

        foreach ($users as $user) {
            if ($user->person->document_type == '1') {
                $user->person->document_type_name = 'RUT';
            } else if ($user->person->document_type == '2') {
                $user->person->document_type_name = 'Pasaporte';
            } else if ($user->person->document_type == '3') {
                $user->person->document_type_name = 'Otro';
            }
        }

        return view('admin.users.index')->with([
            'users' => $users,
            'driver_id' => $request->driver_id
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            // 'email' => 'required|email|unique:users,email,'.$id.',id,deleted_at,NULL',
            'email' => 'required|email',
            // 'document_photo' => 'required',
            'phone' => 'required',
            // 'photo' => 'required',
            // 'permit' => 'required',
            // 'criminal_records' => 'required',
            // 'license' => 'required'
        ];
        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'Ya esta registrado este :attribute',
            'email' => 'Debe ingresar un :attribute valido',
            'max' => 'El :attribute no puede exceder los :size caracteres',
            'mimes' => 'El :attribute debe ser del formato JPG,JPEG o PNG'
        ];
        $attrs = [
            'name' => 'Nombre',
            'lastname' => 'Apellido',
            'email' => 'Correo Electrónico',
            'phone' => 'Telefono',
            'document_photo' => 'Foto del Documento',
            'photo' => 'Foto de Perfil',
            'permit' => 'Permiso',
            'criminal_records' => 'Antecedentes Criminales',
            'license' => 'Licencia de Conducir'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        $users = User::where('id','!=',$id)->where('level','2')->get()->pluck('id');
        $current_user = User::where('id',$id)->first();
        $person = Person::where('user_id',$id)->first();

        if ($current_user->email != $request->email) {
            $check_email = User::whereIn('id', $users)->whereNull('deleted_at')->get();
            foreach ($check_email as $check) {
                if ($check->email == $request->email) {
                    return response()->json([
                        'error' => 'Ya esta registrado este Correo Electronico'
                    ], 422);
                }
            }
        }

        if ($person->phone != $request->phone) {
            $check_phone = Person::whereIn('user_id',$users)->whereNull('deleted_at')->get();
            foreach ($check_phone as $check) {
                if ($check->phone == $request->phone) {
                    return response()->json([
                        'error' => 'Ya esta registrado este Telefono'
                    ], 422);
                }
            }
        }

        if ($person->document != $request->document) {
            $check_document = Person::whereIn('user_id',$users)->whereNull('deleted_at')->get();
            foreach ($check_document as $check) {
                if ($check->document == $request->document) {
                    return response()->json([
                        'error' => 'Ya esta registrado este RUT o Documento'
                    ], 422);
                }
            }
        }

        $person->name = $request->name;
        $person->lastname = $request->lastname;
        $person->phone = $request->phone;
        $person->document_type = $request->document_type;
        $person->document = $request->document;
        $person->save();
        
        $user = User::find($id);
        $user->email = $request->email;
            // $user->status = $request->status;
            // if ($request->status == '1') {
            //     $user->validate = '1';
            // } else {
            //     $user->validate = '0';
            // }
        $user->save();

        // $vehicle = Vehicle::where("user_id",$id)->first();
        //     $vehicle->status = $request->status;
        // $vehicle->save();

        // if($request->hasFile('photo')) {
        //     $url = 'drivers/';
        //     $url .= $request->file('photo')->store('profile', 'drivers');
        //     $person->photo = $url;
        //     $person->save();
        // }

        // if($request->hasFile('document_photo')) {
        //     $url = 'drivers/';
        //     $url .= $request->file('document_photo')->store('files', 'drivers');
        //     $person->document_photo = $url;
        //     $person->save();
        // }

        // if($request->hasFile('permit')) {
        //     $url = 'drivers/';
        //     $url .= $request->file('permit')->store('files', 'drivers');
        //     $person->permit = $url;
        //     $person->save();
        // }

        // if($request->hasFile('criminal_records')) {
        //     $url = 'drivers/';
        //     $url .= $request->file('criminal_records')->store('files', 'drivers');
        //     $person->criminal_records = $url;
        //     $person->save();
        // }

        // if($request->hasFile('license')) {
        //     $url = 'drivers/';
        //     $url .= $request->file('license')->store('files', 'drivers');
        //     $person->license = $url;
        //     $person->save();
        // }

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Person::where("user_id",$id)->delete();
        Vehicle::where("user_id",$id)->delete();
        User::find($id)->delete();
        
        $user = User::where('id',$id)->withTrashed()->first();

        return response()->json([
            'result' => true,
            'user' => $user->deleted_at
        ]);
    }

    public function activate($id)
    {
        $user = User::find($id);

        $vehicle = Vehicle::with([
            'vehicle_categories'
        ])->where([ 
            ['user_id', $id], 
            ['status', '1']
        ])->first();

        if ( ($user->status == '0') ) {
            if ( ($vehicle == null) ) {
                return response()->json([
                    'error' => 'Debe activar un vehículo del conductor para continuar'
                ], 422);
            }
            if (count($vehicle->vehicle_categories) <= 0) {
                return response()->json([
                    'error' => 'Debe asignarle una categoría al vehículo para continuar'
                ], 422);
            }
        }

        if ( ($user->status == '1') ) {

            $msg = 'Se ha desactivado al conductor con éxito';
            
            $user->status = '0';
            $user->status_driver = '0';

            if ( !is_null($vehicle) ) $vehicle->status = '0';


        } else {
            
            $msg = 'Se ha activado al conductor con éxito';

            $user->status = '1';
            if ( !is_null($vehicle) ) $vehicle->status = '1';
        }

        $user->save();
        if ( !is_null($vehicle) ) $vehicle->save();

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'user' => $user->status,
            'vehicle' => $vehicle
        ]);
    }

    public function block($id)
    {
        $user = User::find($id);
        $vehicle = Vehicle::where("user_id",$id);
        if ($user->status == '1') {
            $user->status = '2';
            $user->status_driver = '0';
            $vehicle->status = '2';
            $msg = 'Se ha bloqueado al Conductor con exito';
        } else if ($user->status == '0') {
            $user->status = '2';
            $user->status_driver = '0';
            $vehicle->status = '2';
            $msg = 'Se ha bloqueado al Conductor con exito';
        } else if ($user->status == '3') {
            $user->status = '2';
            $user->status_driver = '0';
            $vehicle->status = '2';
            $msg = 'Se ha bloqueado al Conductor con exito';
        } else if ($user->status == '2') {
            if ($user->validate == '1') {
                $user->status = '1';
                $vehicle->status = '1';
                $msg = 'Se ha desbloqueado al Conductor con exito';
            } else {
                $user->status = '0';
                $vehicle->status = '0';
                $msg = 'Se ha desbloqueado al Conductor con exito';
            }
        }
        $user->save();

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'user' => $user->status
        ]);
    }

    public function travels($id)
    {
        $routes = Route::where('driver_id', $id)
        ->whereHas('to_destinies')
        ->with(['driver.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_user.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_destinies' => function ($q) {
            $q->orderBy('type', 'asc')
                ->orderBy('id', 'desc');
        }])
        ->with(['tariff', 'vehicle_type'])
        ->orderBy('id', 'desc')
        ->get();

        $commission = Commission::first();
        foreach ($routes as $route) {
            if ($route->status == '0') {
                $route->status_type = 'Suspendida';
            } else if ($route->status == '1') {
                $route->status_type = 'Activa';
            } else if ($route->status == '2') {
                $route->status_type = 'Culminada';
            } else if ($route->status == '3') {
                $route->status_type = 'Cancelada';
            }

            if ($route->status_route == '0') {
                $route->status_pass = 'Sin Abordar';
            } else {
                $route->status_pass = 'Abordo';
            }
            
            $route->cost = floatval($route->cost);
            if ($route->payment == '1') {
                $route->payment_type = 'Tarjeta de Credito';
            } else if ($route->payment == '2') {
                $route->payment_type = 'Efectivo';
            }

            $route->distance = 0;
            $destinies = $route->to_destinies;
            foreach ($destinies as $destiny) {
                $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
                $route->distance += $value;
            }
        }

        return view('admin.users.user_routes')->with([
            'routes' => $routes,
            'id' => $id
        ]);
    }

    public function usersBanks()
    {
        $users = User::where('level', '2')
        ->whereHas('bank_user')
        ->with('person')
        ->with(['bank_user' => function ($query) {
            $query->with('bank', 'account');
        }])
        ->get();

        return view('admin.users.users-banks')->with([
            'users' => $users
        ]);
    }

    public function usersRatings()
    {
        $users = User::where('level', '2')
        ->whereHas('vehicle')
        ->with('person', 'vehicle.vehicle_categories', 'vehicle.photos')
        ->get();

        foreach ($users as $user) {
            if ($user->person->document_type == '1') {
                $user->person->document_type_name = 'RUT';
            } else if ($user->person->document_type == '2') {
                $user->person->document_type_name = 'Pasaporte';
            } else if ($user->person->document_type == '3') {
                $user->person->document_type_name = 'Otro';
            }
            if ($user->level == '1') {
                $user->level_type = 'Administrador';
            } else if ($user->level == '2') {
                $user->level_type = 'Conductor';
            } else if ($user->level == '3') {
                $user->level_type = 'Pasajero';
            } else {
                $user->level_type = 'Moderador';
            }
        }

        return view('admin.ratings.ratings-drivers')->with([
            'users' => $users
        ]);
    }

    public function checkDocuments(Request $request,$id)
    {
        $types = [];
        if ($request->has('checks')) {

            foreach ($request->checks as $key => $check) {
                if (array_key_exists('vehicle_id', $check)) {
                    if($check['vehicle_id'] != null) {
                        $check_vehicle = Vehicle::where('id', $check['vehicle_id'])->withTrashed()->first();

                        if($check_vehicle->deleted_at != null) {

                            $user = User::where('level', '2')
                            ->with(['vehicles', 'vehicles.vehicle_categories'])
                            ->whereHas('vehicle')
                            ->whereHas('bank_user')
                            ->with(['vehicle' => function ($query) { 
                                $query->with('vehicle_categories', 'photos')->withTrashed();
                            }])
                            ->with(['vehicles_web' => function ($query) { 
                                $query->with('vehicle_categories', 'photos')->orderBy('id');
                            }])
                            ->with('person')
                            ->with('pending_documents')
                            ->with(['bank_user' => function ($query) {
                                $query->with('bank', 'account');
                            }])
                            ->with('routes')
                            ->orderBy('id', 'desc')
                            ->withTrashed()
                            ->where('id', $check_vehicle->user_id)
                            ->first();

                            if ($user->person->document_type == '1') {
                                $user->person->document_type_name = 'RUT';
                            } else if ($user->person->document_type == '2') {
                                $user->person->document_type_name = 'Pasaporte';
                            } else if ($user->person->document_type == '3') {
                                $user->person->document_type_name = 'Otro';
                            }

                            return response()->json([
                                'result' => false,
                                'msg' => 'El vehiculo '.$check_vehicle->id.' se encuentra eliminado',
                                'user' => $user
                            ]);
                        }
                    }
                }
            }

            foreach ($request->checks as $check) {
                
                if (array_key_exists('id', $check)) {
                    $pending = PendingDocument::where('id', $check['id'])->first();
                } else {
                    if($check['type'] == '3' || $check['type'] == '4') {
                        $pending = PendingDocument::where('user_id', $id)->where('type', $check['type'])->where('vehicle_id', $check['vehicle_id'])->first();
                    } else {
                        $pending = PendingDocument::where('user_id', $id)->where('type', $check['type'])->first();
                    }
                }

                // Verificando si tiene documento pendiente para editar.
                if ( !is_null($pending) ) {

                    $pending->status = $check['status'];

                    if (array_key_exists('text', $check)) {
                        $pending->comment = $check['text'];
                    }
                    if (array_key_exists('vehicle_id', $check)) {
                        $pending->vehicle_id = $check['vehicle_id'];
                    }
                    $pending->save();

                } else {
                // Si no tiene documento, se crea uno nuevamente.
                    $new_pending = New PendingDocument;
                    $new_pending->type = $check['type'];
                    $new_pending->status = $check['status'];
                    
                    if (array_key_exists('text', $check)) {
                        $new_pending->comment = $check['text'];
                    }
                    if (array_key_exists('vehicle_id', $check)) {
                        $new_pending->vehicle_id = $check['vehicle_id'];
                    }
                    $new_pending->user_id = $id;
                    $new_pending->save();
                }

                array_push($types, $check['type']);
            }
        }
        // $approved = PendingDocument::where('user_id', $id)->whereNotIn('type', $types)->where('upload', '1')->update(['status' => '1'], ['comment' => null]);
        $count = PendingDocument::where('user_id', $id)->whereIn('status',['0','2'])->count();
        $user = User::where('id',$id)->withTrashed()->first();

        $user_type = 'Conductor';

        if ( ($user->level == '3') ) $user_type = 'Pasajero';

        $msg = '';

        if ($count > 0) {

            $msg = 'Se ha notificado al '.$user_type.' de los documentos rechazados';

        } else {

            $msg = 'Se ha notificado al '.$user_type.' de los documentos aprobados';
        }

        $docs = PendingDocument::where('user_id', $id)->where('status', '0')->get();

        $rejects = PendingDocument::where('user_id', $id)->where('status', '2')->get();

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'docs' => $docs,
            'rejects' => $rejects
        ]);
    }

    private function calcularDistancia($lat1, $lng1, $lat2, $lng2) {
        if( ($lat1==$lat2) && ($lng1==$lng2) ) return 0;
        $degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng1-$lng2)))));
        return round($degrees*111.13384, 2);
    }

    public function uploadFiles(Request $request,$id)
    {
        $person = Person::where('user_id',$id)->first();
        
        if ($request->hasFile('photo')) {
            $url = 'drivers/';
            $url .= $request->file('photo')->store('profile', 'drivers');
            $person->photo = $url;
            $person->save();
            return response()->json([
                'result' => true
            ]);
        }

        if ($request->hasFile('document_photo')) {
            $url = 'drivers/';
            $url .= $request->file('document_photo')->store('files', 'drivers');
            $person->document_photo = $url;
            $person->save();
            return response()->json([
                'result' => true
            ]);
        }

        /*if ($request->hasFile('permit')) {
            $ext = $request->file('permit')->getClientOriginalExtension();
            if ($ext == '') {
                $ext = 'jpg';
            }
            $url = 'drivers/';
            $url .= $request->file('permit')->storeAs('files', $this->quickRandom().'.'.$ext, 'drivers');
            $person->permit = $url;
            $person->save();
            return response()->json([
                'result' => true
            ]);
        }*/

        if ($request->hasFile('criminal_records')) {
            $ext = $request->file('criminal_records')->getClientOriginalExtension();
            if ($ext == '') {
                $ext = 'jpg';
            }
            $url = 'drivers/';
            $url .= $request->file('criminal_records')->storeAs('files', $this->quickRandom().'.'.$ext, 'drivers');
            $person->criminal_records = $url;
            $person->save();
            return response()->json([
                'result' => true
            ]);
        }

        if ($request->hasFile('license')) {
            $url = 'drivers/';
            $url .= $request->file('license')->store('files', 'drivers');
            $person->license = $url;
            $person->save();
            return response()->json([
                'result' => true
            ]);
        }

        return response()->json([
            'result' => false
        ]);
    }

    public static function quickRandom($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function driverValidate($id)
    {
        $user = User::find($id);

        if ($user->validate == '1') {
            $user->validate = '0';
            $msg = 'Se ha removido la validacion al Conductor con exito';
        } else {
            $user->validate = '1';
            $msg = 'Se ha validado al Conductor con exito';
        }
        $user->save();

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'validate' => $user->validate
        ]);
    }

}