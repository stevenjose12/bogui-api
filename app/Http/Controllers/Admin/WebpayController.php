<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Freshwork\Transbank\CertificationBag;
use Freshwork\Transbank\Log\LoggerFactory;
use Freshwork\Transbank\Log\TransbankCertificationLogger;
use Freshwork\Transbank\Log\LogHandler;
use Freshwork\Transbank\Log\LoggerInterface;

use App\Models\Webpay;
use Carbon\Carbon;
use App\Models\Route;
use App\Models\Payment;
use App\User;

class WebpayController extends Controller
{

    protected $oneclick;
    protected $id = 39;
    protected $test = false;

    public function __construct() {
        // $bag = CertificationBagFactory::integrationOneClick();

        // $bag = new CertificationBag(
        //     'certs/32710158.key',
        //     'certs/32710158.crt',
        //     null,
        //     env('WEBPAY_SANDBOX') ? CertificationBag::INTEGRATION : CertificationBag::PRODUCTION
        // );

        $bag = CertificationBagFactory::production('certs/597032710158.key','certs/597032710158.crt');

        LoggerFactory::setLogger(new TransbankCertificationLogger('./transbank'));

        $this->oneclick = TransbankServiceFactory::oneclick($bag);
    }

    public function index()
    {
        $webpays = Webpay::with(['user' => function ($query) {
            $query->with('person')->withTrashed();
        }])
        ->orderBy('id', 'desc')
        ->get();

        return view('admin.webpay.index')->with([
            'webpays' => $webpays
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reverse($data)
    {
        $response = $this->oneclick->codeReverseOneClick($data);

        return response()->json([
            'result' => $response->reversed,
        ]);
    }
}
