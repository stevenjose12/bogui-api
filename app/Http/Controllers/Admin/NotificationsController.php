<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PendingDocument;
use App\Models\Notification;
Use App\User;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        Notification::whereNull('user_id')->where('type', '9')->update(['viewed' => '1']);

        $drivers = User::where('level', '2')->withTrashed()->get();

        $passengers = User::where('level', '3')->withTrashed()->get();

        $notifications = [];

        foreach ($drivers as $driver) {
             $notifications_11 = Notification::where('type', '11')
                ->with(['driver' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }]);
                }])
                ->orderBy('created_at', 'desc')
                ->where('driver_id', $driver->id)
                ->first();

            if ($notifications_11 != null) {
                array_push($notifications, $notifications_11);        
            }

            $notifications_9 = Notification::whereIn('type',['9'])
                ->with(['user' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents','vehicle' => function ($query2) { 
                        $query2->with('photos')->withTrashed();
                    }]);
                }])
                ->with(['driver' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents' ,'vehicle' => function ($query2) { 
                        $query2->with('photos')->withTrashed();
                    }])
                    ->with(['vehicles_web' => function ($query2) { 
                        $query2->whereHas('photos')->with(['photos' => function ($query3) {
                            $query3->withTrashed();
                        }]);
                    }]);
                }])
                ->orderBy('created_at', 'desc')
                ->where('driver_id', $driver->id)
                ->first();

            if ($notifications_9 != null) {
                array_push($notifications, $notifications_9);
            }

            $notifications_10 = Notification::whereIn('type',['10'])
                ->with(['user' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents','vehicle' => function ($query2) { 
                        $query2->with('photos')->withTrashed();
                    }]);
                }])
                ->with(['driver' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents','vehicle' => function ($query2) { 
                        $query2->with('photos')->withTrashed();
                    }])
                    ->with(['vehicles_web' => function ($query2) { 
                        $query2->whereHas('photos')->with(['photos' => function ($query3) {
                            $query3->withTrashed();
                        }]);
                    }]);
                }])
                ->orderBy('created_at', 'desc')
                ->where('user_id', $driver->id)
                ->first();

            if ($notifications_10 != null) {
                array_push($notifications, $notifications_10);        
            }
        }

        foreach ($passengers as $passenger) {

            $notifications_9 = Notification::whereIn('type',['9'])
                ->with(['user' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents']);
                }])
                ->with(['driver' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents']);
                }])
                ->orderBy('created_at', 'desc')
                ->where('driver_id', $passenger->id)
                ->first();

            if ($notifications_9 != null) {
                array_push($notifications, $notifications_9);
            }

            $notifications_10 = Notification::whereIn('type',['10'])
                ->with(['user' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents']);
                }])
                ->with(['driver' => function ($query) {
                    $query->withTrashed()->with(['person' => function ($query2) {
                        $query2->withTrashed();
                    }, 'pending_documents']);
                }])
                ->orderBy('created_at', 'desc')
                ->where('user_id', $passenger->id)
                ->first();

            if ($notifications_10 != null) {
                array_push($notifications, $notifications_10);        
            }
        }

        $notifications = collect($notifications);
        $notifications_sorted = $notifications->sortByDesc('id');
        $values = $notifications_sorted->values();

        return view('admin.notifications.index')->with([
            'notifications' => $values
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
