<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tariff;
use App\Models\Type;
use App\Models\Commission;
use Validator;

class TariffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tariffs = Tariff::with('registered')->get();

        $types = Type::all();

        $commissions = Commission::all();

        return view('admin.tariffs.index')->with([
            'tariffs' => $tariffs,
            'types' => $types,
            'commissions' => $commissions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'base_tariff' => 'required',
            'minimum_tariff' => 'required_if:other_tariff,null',
            'medium_tariff' => 'required_if:other_tariff,null',
            'maximum_tariff' => 'required_if:other_tariff,null'
        ];
        $msgs = [
            'required' => 'El campo :attribute es requerido',
        ];
        $attrs = [
            'base_tariff' => 'Tarifa Base',
            'minimum_tariff' => 'Tarifa Mínima',
            'medium_tariff' => 'Tarifa Media',
            'maximum_tariff' => 'Tarifa Máxima'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        if($request->base_tariff <= 0){
            return response()->json(['error' => 'La Tarifa Base debe ser mayor a 0'], 422);
        }

        if($request->minimum_tariff <= 0 && $request->other_tariff == null){
            return response()->json(['error' => 'La Tarifa Mínima debe ser mayor a 0'], 422);
        }

        if($request->medium_tariff <= 0 && $request->other_tariff == null){
            return response()->json(['error' => 'La Tarifa Media debe ser mayor a 0'], 422);
        }

        if($request->maximum_tariff <= 0 && $request->other_tariff == null){
            return response()->json(['error' => 'La Tarifa Máxima debe ser mayor a 0'], 422);
        }

        if ($request->other_tariff == 1) {
            $tariff = Tariff::find($id);
                $tariff->base_tariff = $request->base_tariff;
            $tariff->save();
        } else {
            $tariff = Tariff::find($id);
                $tariff->base_tariff = $request->base_tariff;
                $tariff->minimum_tariff = $request->minimum_tariff;
                $tariff->medium_tariff = $request->medium_tariff;
                $tariff->maximum_tariff = $request->maximum_tariff;
            $tariff->save();
        }

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
