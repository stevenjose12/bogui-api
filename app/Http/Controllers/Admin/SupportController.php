<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SupportMotive;
use Validator;
use Auth;

class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supports = SupportMotive::all();

        foreach ($supports as $support) {
            if ($support->type == '1') {
                $support->type_theme = 'Soporte General';
            } else {
                $support->type_theme = 'Tema de Ayuda';
            }
        }

        return view('admin.support.index')->with([
            'supports' => $supports
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'description' => 'required|between:1,255',
            'type' => 'required'
        ];
        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'between' => 'El campo :attribute debe estar entre :min y :max'
        ];
        $attrs = [
            'description' => 'Descripcion',
            'type' => 'Tipo'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        $support_motive = new SupportMotive;
            $support_motive->description = $request->description;
            $support_motive->type = $request->type;
            $support_motive->registered_by = Auth::user()->id;
        $support_motive->save();

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $support = SupportMotive::where('id', $id)->first();

        $view = "admin.support.edit_support";

        return view($view)->with([
            'support' => $support,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'description' => 'required|between:1,255',
            'type' => 'required'
        ];
        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'between' => 'El campo :attribute debe estar entre :min y :max'
        ];
        $attrs = [
            'description' => 'Descripcion',
            'type' => 'Tipo'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        $support_motive = SupportMotive::where('id', $id)->first();
            $support_motive->description = $request->description;
            $support_motive->type = $request->type;
            $support_motive->registered_by = Auth::user()->id;
        $support_motive->save();

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SupportMotive::where('id', $id)->delete();
    }

    public function activate($id)
    {
        $support = SupportMotive::find($id);
        if ($support->status == '1') {
            $support->status = '0';
            $msg = 'Se ha desactivado el Tema de Soporte con exito';
        } else {
            $support->status = '1';
            $msg = 'Se ha activado el Tema de Soporte con exito';
        }
        $support->save();

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'support' => $support->status
        ]);
    }
}
