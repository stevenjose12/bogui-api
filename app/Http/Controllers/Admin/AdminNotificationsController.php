<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\AdminNotification;
use App\Models\AdminNotificationUser;

class AdminNotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::whereIn('level', ['2','3'])->with('person')->get();

        return view('admin.send-notifications.index')->with([
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $admin_notif = new AdminNotification;
            $admin_notif->title = $request->title;
            $admin_notif->description = $request->description;
            if($request->hasFile('file')) {
                $url = 'notifications/';
                $url .= $request->file('file')->store('images', 'notifications');
                $admin_notif->file = $url;
            }
        $admin_notif->save();

        $decoded = json_decode($request->users_ids);

        if (count($decoded) > 0) {
            $ids = $decoded;
        } else {
            if ($request->user_type == '1') {
                $ids = User::get()->pluck('id');
            } else {
                $ids = User::where('level', $request->user_type)->get()->pluck('id');
            }
        }

        foreach ($ids as $id) {
            $admin_notif_user = new AdminNotificationUser;
                $admin_notif_user->notification_id = $admin_notif->id;
                $admin_notif_user->user_id = $id;
            $admin_notif_user->save();
        }

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUsers($type)
    {
        $users = User::where('level', $type)
                    ->with('person')
                    ->get();

        return response()->json([
            'result' => true,
            'users' => $users
        ]);
    }
}
