<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Route;
use App\Models\Tariff;
use App\Models\Payment;
use App\Models\WeeklyDebt;
use Auth;
use Excel;
use Mail;
use Carbon\Carbon;

class FinanceController extends Controller
{
    public function earningsDrivers(Request $request)
    {

    	$from = Carbon::now()->startOfWeek()->startOfDay();
    	$to = Carbon::now()->endOfWeek()->endOfDay();

    	$drivers = User::where('level', '2')
						->whereHas('routes')
						->with('person', 'payments')
						->with(['routes' => function ($query) use ($from,$to) {
							$query->whereIn('status', ['2'])
							->whereBetween('created_at', [$from,$to])
							->with('to_payments')
							->with(['to_destinies' => function ($q){
								$q->orderBy('type', 'asc')
								    ->orderBy('id', 'desc');
							}]);
						}])
						->withTrashed()
						->get();

		foreach ($drivers as $driver) {
			$driver->total = 0;
			$driver->commission = 0;
			$driver->balance = 0;
			$routes = $driver->routes;
			$owned_to_bog = 0;
			$owned_to_driver = 0;
			// $driver_amount_payment = $driver->payments->sum('total');
			foreach ($routes as $route) {

				$destinies = $route->to_destinies;
				foreach ($destinies as $destiny) {
				    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
				    $route->distance += $value;
				}

				$route->cost = floor($route->cost / 50) * 50;
				$payments_drivers = $route->to_payments;
				foreach ($payments_drivers as $payment) {
					if ($payment->method == '2') {
						$payment->commission = (floor($payment->commission / 50) * 50);
						$route->commission += $payment->commission;
						$owned_to_bog += $payment->commission;
						$route->payment_type = 'Efectivo';
					} else if ($payment->method == '3') {
						// $payment->commission = (floor($payment->commission / 50) * 50);
						// $route->commission += $payment->commission;
						// $owned_to_bog += $payment->commission;
						$payment->commission = (floor($payment->commission / 50) * 50);
						$route->commission += $payment->commission;
						$route->balance = $payment->total - $payment->commission;
						$route->balance = floor($route->balance / 50) * 50;
						$owned_to_driver += $route->balance;
					} else if ($payment->method == '1') {
						$payment->commission = (floor($payment->commission / 50) * 50);
						$route->commission += $payment->commission;
						$route->balance = $payment->total - $payment->commission;
						$route->balance = floor($route->balance / 50) * 50;
						$owned_to_driver += $route->balance;
						$route->payment_type = 'Tarjeta de Credito';
					}
				}

				$driver->total += $route->cost;
				$driver->total = floatval($driver->total);
				$driver->commission += $route->commission;
			}
				$driver->balance = $owned_to_driver - $owned_to_bog;
				$driver->balance += $driver->carried;
				// $driver->balance -= $driver_amount_payment;
		}

        return view('admin.finance.earnings-drivers')->with([
        	'drivers' => $drivers
        ]);
    }

    public function earningsPassengers(Request $request)
    {

    	$from = Carbon::now()->startOfWeek()->startOfDay();
    	$to = Carbon::now()->endOfWeek()->endOfDay();

    	$passengers = User::where('level', '3')
    					->whereHas('has_been_passenger')
    					->with('person')
    					->with(['has_been_passenger' => function ($query) use ($from,$to) {
    						$query->whereBetween('created_at', [$from,$to])
    								->whereIn('status', ['2'])
    								->with('to_payments');
    					}])
    					->withTrashed()
    					->get();

    	foreach ($passengers as $passenger) {
    			$passenger->total = 0;
    		foreach ($passenger->has_been_passenger as $pshp) {
    			$passenger->total += $pshp->to_payments->sum('total');
    		}
    	}

        return view('admin.finance.earnings-passengers')->with([
        	'passengers' => $passengers
        ]);
    }

    public function paymentsDrivers(Request $request)
    {
    	return view('admin.finance.payments-drivers');
    }

    public function getPayments(Request $request)
    {
    	$payments = Payment::where('method', '4')->with('user.person', 'paid_by.person')->get();

    	foreach ($payments as $pay) {
    		$pay->total = floatval($pay->total);
    		$pay->debt = floatval($pay->debt);
    	}

    	return response()->json([
    	    'payments' => $payments
    	]);
    }

    public function details($id)
    {

    	$from = Carbon::now()->startOfWeek()->startOfDay();
    	$to = Carbon::now()->endOfWeek()->endOfDay();
    	
    	$driver = User::where('id', $id)
					->whereHas('routes')
					->with('person')
					->with(['payments' => function ($query) use ($from,$to){
						$query->whereBetween('created_at', [$from,$to]);
					}])
					->with(['routes' => function ($query) use ($from,$to) {
						$query->whereIn('status', ['2'])
							->whereBetween('created_at', [$from,$to])
							->with('to_payments')
							->orderBy('id', 'desc')
							->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}]);
					}])
					->withTrashed()
					->first();

		// dd($driver);

		// $driver_amount_payment = $driver->payments->sum('total');

		$driver->balance = 0;
		// $driver_amount_payment = 0;
		$routes = $driver->routes;
		foreach ($routes as $route) {
			$destinies = $route->to_destinies;
			foreach ($destinies as $destiny) {
			    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
			    $route->distance += $value;
			}

			$route->cost = floor($route->cost / 50) * 50;
			$route->promo = 0;
			$route->difference = 0;
			$payments = $route->to_payments;
			foreach ($payments as $payment) {
				if ($payment->method == '2' && $payment->total > 0) {
					$payment->commission = (floor($payment->commission / 50) * 50);
					$route->commission += $payment->commission;
					$payment->balance = $payment->total - $payment->commission;
					$route->balance += floor($payment->balance / 50) * 50;
					$route->payment_type = 'Efectivo';
				} else if ($payment->method == '3') {
					// $payment->commission = (floor($payment->commission / 50) * 50);
					// $route->commission += $payment->commission;
					// $payment->balance = $payment->total - $payment->commission;
					// $route->balance += floor($payment->balance / 50) * 50;
					$payment->commission = (floor($payment->commission / 50) * 50);
					$route->commission += $payment->commission;
					$payment->balance = $payment->total - $payment->commission;
					$route->balance += floor($payment->balance / 50) * 50;
					$route->promo = floor($payment->total / 50) * 50;
					$route->difference = $route->cost - $route->promo;
				} else if ($payment->method == '1') {
					$payment->commission = (floor($payment->commission / 50) * 50);
					$route->commission += $payment->commission;
					$payment->balance = $payment->total - $payment->commission;
					$route->balance += floor($payment->balance / 50) * 50;
					$route->payment_type = 'Tarjeta de Credito';
				}
			}
			$driver->balance += $route->balance;
			$driver->balance += $driver->carried;
		}

		// $driver->balance -= $driver_amount_payment;

		return view('admin.finance.details_driver')->with([
		    'driver' => $driver,
		]);

    }

    public function getWeeklyReport(Request $request)
    {

    	$driver_debt = User::where('id', $request->id)->withTrashed()->first();
			if ($driver_debt->carried > 0) {
    		$driver_debt = 0;
    	} else {
    		$driver_debt = $driver_debt->carried;
    	}

    	$routes = Route::where('driver_id', $request->id)
    					->whereIn('status', ['2'])
    					->whereBetween('created_at', [$request->from,$request->to])
    					->with(['to_payments' => function ($query) use ($request){
    						$query->whereBetween('created_at', [$request->from,$request->to]);
    					}])
    					->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}])
						->orderBy('id', 'desc')
    					->get();

    	$total_earning = 0;
    	$bogui_earning = 0;
    	$driver_earning = 0;
    	$cash_taken = 0;
    	$promo_taken = 0;
    	$cash_driver_earned = 0;
    	$bogui_collected = 0;
    	$owned_to_driver = 0;
    	$owned_to_bog = 0;
    	$total_owned_bog = 0;
    	$balance = 0;
    	foreach ($routes as $route) {

    		$destinies = $route->to_destinies;
    		foreach ($destinies as $destiny) {
    		    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
    		    $route->distance += $value;
    		}

			$route->cost = floor($route->cost / 50) * 50;
			$payments = $route->to_payments;
			foreach ($payments as $payment) {
				if ($payment->method == '2') {
					$cash_taken += $payment->total;
					$payment->commission = (floor($payment->commission / 50) * 50);
					$owned_to_bog += $payment->commission;
					$total_owned_bog += $payment->commission;
				} else if ($payment->method == '3') {
					$promo_taken += $payment->total;
					// $payment->commission = (floor($payment->commission / 50) * 50);
					// $owned_to_bog += $payment->commission;
					// $total_owned_bog += $payment->commission;
					$payment->commission = (floor($payment->commission / 50) * 50);
					$bogui_collected += $payment->total;
					$owned_to_driver_trip = $payment->total - $payment->commission;
					$owned_to_driver += $owned_to_driver_trip;
				} else if ($payment->method == '1') {
					$payment->commission = (floor($payment->commission / 50) * 50);
					$bogui_collected += $payment->total;
					$owned_to_driver_trip = $payment->total - $payment->commission;
					$owned_to_driver += $owned_to_driver_trip;
				}

				$cash_driver_earned += $payment->total - $payment->commission;
	    		$total_earning += $payment->total;
	    		$bogui_earning += $payment->commission;
			}
			
    	}

    	// $owned_to_driver -= $driver_amount_payment;
    	$driver_earning = $cash_driver_earned;
		$balance = $owned_to_driver - $total_owned_bog;
		if ($driver_debt < 0) {
			$balance += $driver_debt;
		} else {
			$balance += $driver_debt;
		}

    	$weekly = array("total_earning" => $total_earning, "bogui_earning" => $bogui_earning, "driver_earning" => $driver_earning, "cash_taken" => $cash_taken, "bogui_collected" => $bogui_collected, "owned_to_driver" => $owned_to_driver, "total_owned_bog" => $total_owned_bog, "promo_taken" => $promo_taken, "balance" => $balance);

    	return response()->json([
    	    'result' => true,
    	    'weekly' => $weekly
    	]);
    }

    public function payDriver(Request $request)
    {
    	$payment = new Payment;
    		$payment->method = '4';
    		$payment->total = $request->amount;
    		$payment->commission = 0;
    		$payment->user_id = $request->id;
    		$payment->debt = $request->balance;
    		$payment->paid_by = Auth::user()->id;
    	$payment->save();

    	$msg = 'Pago exitoso!';

    	return response()->json([
    	    'result' => true,
    	    'msg' => $msg
    	]);

    }

    public function detailsPassenger($id)
    {

    	$from = Carbon::now()->startOfWeek()->startOfDay();
    	$to = Carbon::now()->endOfWeek()->endOfDay();

    	$passenger = User::where('id', $id)
					->whereHas('has_been_passenger')
					->with('person')
					->with(['payments' => function ($query) use ($from,$to){
						$query->whereBetween('created_at', [$from,$to]);
					}])
					->with(['has_been_passenger' => function ($query) use ($from,$to) {
						$query->whereIn('status', ['2'])
								->whereBetween('created_at', [$from,$to])
								->orderBy('id', 'desc')
								->with(['to_destinies' => function ($q) {
									$q->orderBy('type', 'asc')
									    ->orderBy('id', 'desc');
								}]);
					}])
					->withTrashed()
					->first();

		$passenger->total = $passenger->payments->sum('total');

		$routes = $passenger->has_been_passenger;
		foreach ($routes as $route) {
			$destinies = $route->to_destinies;
			foreach ($destinies as $destiny) {
			    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
			    $route->distance += $value;
			}

			$route->cost = floor($route->cost / 50) * 50;
			$route->promo = 0;
			$route->difference = 0;
			$payments = $route->to_payments;
			foreach ($payments as $payment) {
				if ($payment->method == '2' && $payment->total > 0) {
					$payment->commission = (floor($payment->commission / 50) * 50);
					$route->commission += $payment->commission;
					$payment->balance = $payment->total - $payment->commission;
					$route->balance += floor($payment->balance / 50) * 50;
					$route->payment_type = 'Efectivo';
				} else if ($payment->method == '3') {
					// $payment->commission = (floor($payment->commission / 50) * 50);
					// $route->commission += $payment->commission;
					// $payment->balance = $payment->total - $payment->commission;
					// $route->balance += floor($payment->balance / 50) * 50;
					$payment->commission = (floor($payment->commission / 50) * 50);
					$route->commission += $payment->commission;
					$payment->balance = $payment->total - $payment->commission;
					$route->balance += floor($payment->balance / 50) * 50;
					$route->promo = floor($payment->total / 50) * 50;
					$route->difference = $route->cost - $route->promo;
				} else if ($payment->method == '1') {
					$payment->commission = (floor($payment->commission / 50) * 50);
					$route->commission += $payment->commission;
					$payment->balance = $payment->total - $payment->commission;
					$route->balance += floor($payment->balance / 50) * 50;
					$route->payment_type = 'Tarjeta de Credito';
				}
			}
		}

		return view('admin.finance.details_passenger')->with([
		    'passenger' => $passenger,
		]);
    }

    public function getWeeklyReportPassenger(Request $request)
    {

    	$routes = Route::where('user_id', $request->id)
    					->whereIn('status', ['2'])
    					->whereBetween('created_at', [$request->from,$request->to])
    					->with('to_payments')
    					->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}])
    					->get();

    	$total_earning = 0;
    	$bogui_earning = 0;
    	$driver_earning = 0;
    	$cash_taken = 0;
    	$promo_taken = 0;
    	$cc_taken = 0;
    	$distance_traveled = 0;
    	foreach ($routes as $route) {

    		$destinies = $route->to_destinies;
    		foreach ($destinies as $destiny) {
    		    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
    		    $distance_traveled += $value;
    		}

			$payments = $route->to_payments;
			foreach ($payments as $payment) {
				$payment->total = floor($payment->total / 50) * 50;
				$payment->commission = (floor($payment->commission / 50) * 50);
				if ($payment->method == '2') {
					$cash_taken += floatval($payment->total);
				} if ($payment->method == '3') {
					$promo_taken += floatval($payment->total);
				} else if ($payment->method == '1') {
					$cc_taken += floatval($payment->total);
				}

	    		$total_earning += floatval($payment->total);
	    		$bogui_earning += floatval($payment->commission);
	    		$driver_earning += floatval((floor(($payment->total - $payment->commission) / 50) * 50));
			}
			
    	}

    	$weekly = array("total_earning" => $total_earning, "bogui_earning" => $bogui_earning, 'driver_earning' => $driver_earning, "cash_taken" => $cash_taken, "promo_taken" => $promo_taken, "cc_taken" => $cc_taken, "distance_traveled" => $distance_traveled);

    	return response()->json([
    	    'result' => true,
    	    'weekly' => $weekly
    	]);
    }

    public function getWeeklyReportBogui(Request $request)
    {

    	$routes = Route::whereIn('status', ['2'])
    					->whereBetween('created_at', [$request->from,$request->to])
    					->with('to_payments')
    					->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}])
						->orderBy('id', 'desc')
    					->get();

    	$bogui_earning = 0;
    	$distance_traveled = 0;
    	foreach ($routes as $route) {

    		$destinies = $route->to_destinies;
    		foreach ($destinies as $destiny) {
    		    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
    		    $distance_traveled += $value;
    		}

			$payments = $route->to_payments;
			foreach ($payments as $payment) {

				if ($payment->method == '1') {
					$route->payment_type = 'Tarjeta de Credito';
				} else if ($payment->method == '2') {
					$route->payment_type = 'Efectivo';
				}

				$payment->commission = (floor($payment->commission / 50) * 50);
				$route->cost = floor($route->cost / 50) * 50;
				$route->commission += $payment->commission;
				$route->balance = $payment->total - $payment->commission;
				$route->balance = floor($route->balance / 50) * 50;
	    		$bogui_earning += floatval($payment->commission);
			}
			
    	}

    	$weekly = array("bogui_earning" => $bogui_earning, "distance_traveled" => $distance_traveled);

    	return response()->json([
    	    'result' => true,
    	    'weekly' => $weekly,
    	    'routes' => $routes
    	]);
    }

    public function getWeeklyReportBoguiExcel()
    {

    	$from = Carbon::now()->startOfWeek()->startOfDay();
    	$to = Carbon::now()->endOfWeek()->endOfDay();

    	$drivers = User::where('level', '2')
    	->whereHas('routes')
    	->whereHas('bank_user')
    	->with('person')
    	->with(['bank_user' => function ($query) {
            $query->with('bank', 'account');
        }])
        ->withTrashed()
        ->get();

        $total = 0;

        foreach ($drivers as $driver) {

        	$driver->balance = 0;
        	$owned_to_bog = 0;
        	$owned_to_driver = 0;
        	// $driver_amount_payment = $driver->payments->sum('total');

        	$routes = Route::where('driver_id', $driver->id)->whereBetween('created_at', [$from,$to])->whereIn('status',['2'])->get()->pluck('id');
        	$payments = Payment::whereIn('route_id', $routes)->get();

        	foreach ($payments as $payment) {

        		$payment->total = floor($payment->total / 50) * 50;
        		$payment->commission = (floor($payment->commission / 50) * 50);

        		if ($payment->method == '1') {
        			$payment->balance = $payment->total - $payment->commission;
        			$payment->balance = floor($payment->balance / 50) * 50;
        			$owned_to_driver += $payment->balance;
        		} else if ($payment->method == '2') {
        			$owned_to_bog += $payment->commission;
        		} else if ($payment->method == '3') {
							// $owned_to_bog += $payment->commission;
							$payment->balance = $payment->total - $payment->commission;
        			$payment->balance = floor($payment->balance / 50) * 50;
        			$owned_to_driver += $payment->balance;
        		}
        	}
        	$driver->balance = $owned_to_driver - $owned_to_bog;
        	// $driver->balance -= $driver_amount_payment;
        	$total += $driver->balance;
        	$driver->total_balance = $driver->balance + $driver->carried;

        	$debt = 0;

        	if ($driver->balance < 0) {
        		$debt = $driver->balance;
        	}

        	$weekly_debt = new WeeklyDebt;
        		$weekly_debt->driver_id = $driver->id;
        		$weekly_debt->debt_weekly = $driver->balance;
        		$weekly_debt->debt_carried = $debt;
        		$weekly_debt->debt_total = $driver->total_balance;
        	$weekly_debt->save();

        }

		$routes = Route::whereIn('status', ['2'])
						->whereBetween('created_at', [$from,$to])
						->with('to_payments')
						->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}])
						->orderBy('id', 'desc')
						->get();

    	$bogui_earning = 0;
    	$distance_traveled = 0;
    	foreach ($routes as $route) {

    		$destinies = $route->to_destinies;
    		foreach ($destinies as $destiny) {
    		    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
    		    $distance_traveled += $value;
    		}
    		$route->promo = 0;
			$payments = $route->to_payments;
			foreach ($payments as $payment) {

				if ($payment->method == '1') {
					$route->payment_type = 'Tarjeta de Credito';
				} else if ($payment->method == '2') {
					$route->payment_type = 'Efectivo';
				} else if ($payment->method == '3') {
					$route->payment_type = 'Promocion';
					$route->promo += floor($payment->total / 50) * 50;
				}

				$payment->commission = (floor($payment->commission / 50) * 50);
				$route->cost = floor($route->cost / 50) * 50;
				$route->commission += $payment->commission;
				$payment->balance = $payment->total - $payment->commission;
				$route->balance += floor($payment->balance / 50) * 50;
				$route->difference = $route->cost - $route->promo;
	    	$bogui_earning += floatval($payment->commission);
			}
			
    	}

    	$from = $from->format('d-m-Y');
    	$to = $to->format('d-m-Y');

    	$file = 'Reporte de Ganancias desde '.$from.' hasta '.$to.' de  Bogui';

    	$weekly = array("bogui_earning" => $bogui_earning, "distance_traveled" => floor($distance_traveled));
    	\Excel::create($file, function($excel) use ($from, $to, $weekly, $routes, $drivers, $total) {
    	  $excel->sheet('Resumen Semanal', function($sheet) use ($from,$to,$weekly){
    	    $sheet->loadView('admin.reports.bogui-earnings-weekly')->with('weekly', $weekly)->with('from', $from)->with('to', $to);
    	    $sheet->setWidth('A', 30);
    	    $sheet->setWidth('B', 40);
    	    $sheet->setColumnFormat(array(
    	      'A' => '"CLP "#,##0.00_-',
    	    ));
    	  });
    	  $excel->sheet('Resumen de Viajes', function ($sheet) use ($from, $to, $routes){
    	  	$sheet->loadView('admin.reports.bogui-earnings-routes')->with('routes', $routes)->with('from', $from)->with('to', $to);
    	  });
    	  $excel->sheet('Datos para Depositos', function ($sheet) use ($from, $to, $drivers, $total){
    	  	$sheet->loadView('admin.reports.bogui-earnings-payments')->with('drivers', $drivers)->with('from', $from)->with('to', $to)->with('total', $total);
    	  });
    	})->store('xlsx', 'files');

    	$dates = array('from' => $from, 'to' => $to);

    	Mail::send('emails.weekly-report', $dates, function ($m) use ($from,$to,$weekly,$file) {
    	    $m->to(env('MAIL_CONTACT'), 'Bogui')
    	        ->subject($file)
    	        ->attach(public_path('files').'/'.$file.'.xlsx', array(
    	        	'as' => $file.'.xlsx',
    	        	'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    	        	));
    	});

    }

    public function getWeeklyReportBoguiExcelGenerate()
    {

    	$from = Carbon::now()->startOfWeek()->startOfDay();
    	$to = Carbon::now()->endOfWeek()->endOfDay();

    	$drivers = User::where('level', '2')
    	->whereHas('routes')
    	->whereHas('bank_user')
    	->with('person')
    	->with(['bank_user' => function ($query) {
            $query->with('bank', 'account');
        }])
        ->withTrashed()
        ->get();

        $total = 0;

        foreach ($drivers as $driver) {

        	$driver->balance = 0;
        	$owned_to_bog = 0;
        	$owned_to_driver = 0;
        	// $driver_amount_payment = $driver->payments->sum('total');

        	$routes = Route::where('driver_id', $driver->id)->whereBetween('created_at', [$from,$to])->whereIn('status',['2'])->get()->pluck('id');
        	$payments = Payment::whereIn('route_id', $routes)->get();

        	foreach ($payments as $payment) {

        		$payment->total = floor($payment->total / 50) * 50;
        		$payment->commission = (floor($payment->commission / 50) * 50);

        		if ($payment->method == '1') {
        			$payment->balance = $payment->total - $payment->commission;
        			$payment->balance = floor($payment->balance / 50) * 50;
        			$owned_to_driver += $payment->balance;
        		} else if ($payment->method == '2') {
        			$owned_to_bog += $payment->commission;
        		} else if ($payment->method == '3') {
							// $owned_to_bog += $payment->commission;
							$payment->balance = $payment->total - $payment->commission;
        			$payment->balance = floor($payment->balance / 50) * 50;
        			$owned_to_driver += $payment->balance;
        		}
        	}
        	$driver->balance = $owned_to_driver - $owned_to_bog;
        	// $driver->balance -= $driver_amount_payment;
        	$total += $driver->balance;
        	$driver->total_balance = $driver->balance + $driver->carried;

        	$debt = 0;

        	if ($driver->balance < 0) {
        		$debt = $driver->balance;
        	}

        }

		$routes = Route::whereIn('status', ['2'])
						->whereBetween('created_at', [$from,$to])
						->with('to_payments')
						->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}])
						->orderBy('id', 'desc')
						->get();

    	$bogui_earning = 0;
    	$distance_traveled = 0;
    	foreach ($routes as $route) {

    		$destinies = $route->to_destinies;
    		foreach ($destinies as $destiny) {
    		    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
    		    $distance_traveled += $value;
    		}

			$payments = $route->to_payments;
			$route->promo = 0;
			foreach ($payments as $payment) {

				if ($payment->method == '1') {
					$route->payment_type = 'Tarjeta de Credito';
				} else if ($payment->method == '2') {
					$route->payment_type = 'Efectivo';
				} else if ($payment->method == '3') {
					$route->payment_type = 'Promocion';
					$route->promo += floor($payment->total / 50) * 50;
				}

				$payment->commission = (floor($payment->commission / 50) * 50);
				$route->cost = floor($route->cost / 50) * 50;
				$route->commission += $payment->commission;
				$payment->balance = $payment->total - $payment->commission;
				$route->balance += floor($payment->balance / 50) * 50;
				$route->difference = $route->cost - $route->promo;
				$bogui_earning += floatval($payment->commission);
				
			}
			
    	}

    	$from = $from->format('d-m-Y');
    	$to = $to->format('d-m-Y');

    	$file = 'Reporte de Ganancias desde '.$from.' hasta '.$to.' de  Bogui';

    	$weekly = array("bogui_earning" => $bogui_earning, "distance_traveled" => floor($distance_traveled));
    	\Excel::create($file, function($excel) use ($from, $to, $weekly, $routes, $drivers, $total) {
    	  $excel->sheet('Resumen Semanal', function($sheet) use ($from,$to,$weekly){
    	    $sheet->loadView('admin.reports.bogui-earnings-weekly')->with('weekly', $weekly)->with('from', $from)->with('to', $to);
    	    $sheet->setWidth('A', 30);
    	    $sheet->setWidth('B', 40);
    	    $sheet->setColumnFormat(array(
    	      'A' => '"CLP "#,##0.00_-',
    	    ));
    	  });
    	  $excel->sheet('Resumen de Viajes', function ($sheet) use ($from, $to, $routes){
    	  	$sheet->loadView('admin.reports.bogui-earnings-routes')->with('routes', $routes)->with('from', $from)->with('to', $to);
    	  });
    	  $excel->sheet('Datos para Depositos', function ($sheet) use ($from, $to, $drivers, $total){
    	  	$sheet->loadView('admin.reports.bogui-earnings-payments')->with('drivers', $drivers)->with('from', $from)->with('to', $to)->with('total', $total);
    	  });
    	})->download('xlsx');

    }

    public function getWeeklyReportBoguiExcelLastWeek()
    {

    	$from = Carbon::now()->subDays(7)->startOfWeek()->startOfDay();
    	$to = Carbon::now()->subDays(7)->endOfWeek()->endOfDay();

    	$drivers = User::where('level', '2')
    	->whereHas('routes')
    	->whereHas('bank_user')
    	->with('person')
    	->with(['bank_user' => function ($query) {
            $query->with('bank', 'account');
        }])
        ->withTrashed()
        ->get();

        $total = 0;

        foreach ($drivers as $driver) {

        	$driver->balance = 0;
        	$owned_to_bog = 0;
        	$owned_to_driver = 0;
        	// $driver_amount_payment = $driver->payments->sum('total');

        	$routes = Route::where('driver_id', $driver->id)->whereBetween('created_at', [$from,$to])->whereIn('status',['2'])->get()->pluck('id');
        	$payments = Payment::whereIn('route_id', $routes)->get();

        	foreach ($payments as $payment) {

        		$payment->total = floor($payment->total / 50) * 50;
        		$payment->commission = (floor($payment->commission / 50) * 50);

        		if ($payment->method == '1') {
        			$payment->balance = $payment->total - $payment->commission;
        			$payment->balance = floor($payment->balance / 50) * 50;
        			$owned_to_driver += $payment->balance;
        		} else if ($payment->method == '2') {
        			$owned_to_bog += $payment->commission;
        		} else if ($payment->method == '3') {
							// $owned_to_bog += $payment->commission;
							$payment->balance = $payment->total - $payment->commission;
        			$payment->balance = floor($payment->balance / 50) * 50;
        			$owned_to_driver += $payment->balance;
        		}
        	}
        	$driver->balance = $owned_to_driver - $owned_to_bog;
        	// $driver->balance -= $driver_amount_payment;
        	$total += $driver->balance;
        	$driver->total_balance = $driver->balance + $driver->carried;
        	$debt = 0;

        	if ($driver->balance < 0) {
        		$debt = $driver->balance;
        	}

        	$weekly_debt = new WeeklyDebt;
        		$weekly_debt->driver_id = $driver->id;
        		$weekly_debt->debt_weekly = $driver->balance;
        		$weekly_debt->debt_carried = $debt;
        		$weekly_debt->debt_total = $driver->total_balance;
        		$weekly_debt->created_at = Carbon::now()->subDays(7)->endOfWeek()->endOfDay();
        		$weekly_debt->updated_at = Carbon::now()->subDays(7)->endOfWeek()->endOfDay();
        	$weekly_debt->save();

        }

		$routes = Route::whereIn('status', ['2'])
						->whereBetween('created_at', [$from,$to])
						->with('to_payments')
						->with(['to_destinies' => function ($q) {
							$q->orderBy('type', 'asc')
							    ->orderBy('id', 'desc');
						}])
						->orderBy('id', 'desc')
						->get();

    	$bogui_earning = 0;
    	$distance_traveled = 0;
    	foreach ($routes as $route) {

    		$destinies = $route->to_destinies;
    		foreach ($destinies as $destiny) {
    		    $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
    		    $distance_traveled += $value;
    		}

    		$route->promo = 0;
			$payments = $route->to_payments;
			foreach ($payments as $payment) {

				if ($payment->method == '1') {
					$route->payment_type = 'Tarjeta de Credito';
				} else if ($payment->method == '2') {
					$route->payment_type = 'Efectivo';
				} else if ($payment->method == '3') {
					$route->payment_type = 'Promocion';
					$route->promo += floor($payment->total / 50) * 50;
				}

				$payment->commission = (floor($payment->commission / 50) * 50);
				$route->cost = floor($route->cost / 50) * 50;
				$route->commission += $payment->commission;
				$payment->balance = $payment->total - $payment->commission;
				$route->balance += floor($payment->balance / 50) * 50;
				$route->difference = $route->cost - $route->promo;
	    		$bogui_earning += floatval($payment->commission);
			}
			
    	}

    	$from = $from->format('d-m-Y');
    	$to = $to->format('d-m-Y');

    	$file = 'Reporte de Ganancias desde '.$from.' hasta '.$to.' de  Bogui';

    	$weekly = array("bogui_earning" => $bogui_earning, "distance_traveled" => floor($distance_traveled));
    	\Excel::create($file, function($excel) use ($from, $to, $weekly, $routes, $drivers, $total) {
    	  $excel->sheet('Resumen Semanal', function($sheet) use ($from,$to,$weekly){
    	    $sheet->loadView('admin.reports.bogui-earnings-weekly')->with('weekly', $weekly)->with('from', $from)->with('to', $to);
    	    $sheet->setWidth('A', 30);
    	    $sheet->setWidth('B', 40);
    	    $sheet->setColumnFormat(array(
    	      'A' => '"CLP "#,##0.00_-',
    	    ));
    	  });
    	  $excel->sheet('Resumen de Viajes', function ($sheet) use ($from, $to, $routes){
    	  	$sheet->loadView('admin.reports.bogui-earnings-routes')->with('routes', $routes)->with('from', $from)->with('to', $to);
    	  });
    	  $excel->sheet('Datos para Depositos', function ($sheet) use ($from, $to, $drivers, $total){
    	  	$sheet->loadView('admin.reports.bogui-earnings-payments')->with('drivers', $drivers)->with('from', $from)->with('to', $to)->with('total', $total);
    	  });
    	})->store('xlsx', 'files');

    	$dates = array('from' => $from, 'to' => $to);

    	Mail::send('emails.weekly-report', $dates, function ($m) use ($from,$to,$weekly,$file) {
    	    $m->to(env('MAIL_CONTACT'), 'Bogui')
    	        ->subject($file)
    	        ->attach(public_path('files').'/'.$file.'.xlsx', array(
    	        	'as' => $file.'.xlsx',
    	        	'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    	        	));
    	});

    }

    private function calcularDistancia($lat1, $lng1, $lat2, $lng2) {
        if( ($lat1==$lat2) && ($lng1==$lng2) ) return 0;
        $degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng1-$lng2)))));
        return round($degrees*111.13384, 2);
    }
}
