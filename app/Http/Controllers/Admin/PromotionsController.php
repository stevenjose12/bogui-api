<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotion;
use Validator;
use Auth;
use Carbon\Carbon;

class PromotionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::all();

        return view('admin.promotions.index')->with([
            'promotions' => $promotions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'expire_date' => 'required',
            'amount' => 'required|numeric',
            'type' => 'required',
            'description' => 'required|between:1,255',
            'num_uses' => 'required|numeric'
        ];
        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'Ya esta registrado este :attribute',
            'numeric' => 'Debe ingresar un :attribute valido',
            'max' => 'El campo :attribute no puede sobrepasar los :size caracteres',
            'between' => 'El campo :attribute debe estar entre :min y :max'
        ];
        $attrs = [
            'expire_date' => 'Fecha de vencimiento',
            'amount' => 'Valor del descuento',
            'type' => 'Tipo de promocion',
            'description' => 'Descripcion',
            'num_uses' => 'Numero de usos'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        if($request->amount <= 0){
            return response()->json(['error' => 'El Valor del descuento debe ser mayor a 0'], 422);
        }

        if($request->num_uses < 0){
            return response()->json(['error' => 'La cantidad de usos debe ser mayor o igual a 0'], 422);
        }

        $date=new Carbon($request->expire_date);

        $promotion = new Promotion;
            $promotion->code = rand(1000000,9999999);
            $promotion->num_uses = $request->num_uses;
            $promotion->expire_date = $date->format('Y-m-d');
            $promotion->amount = $request->amount;
            $promotion->status = '1';
            $promotion->type = $request->type;
            $promotion->description = $request->description;
            $promotion->registered_by = Auth::user()->id;
        $promotion->save();

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotions = Promotion::where('id', $id)->first();

        $view = "admin.promotions.edit_promos";

        return view($view)->with([
            'promotions' => $promotions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'expire_date' => 'required',
            'amount' => 'required|numeric',
            'type' => 'required',
            'description' => 'required|max:255',
            'num_uses' => 'required|numeric'
        ];
        $msgs = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'Ya esta registrado este :attribute',
            'numeric' => 'Debe ingresar un :attribute valido',
            'max' => 'El campo :attribute no puede sobrepasar los 255 caracteres'
        ];
        $attrs = [
            'expire_date' => 'Fecha de vencimiento',
            'amount' => 'Valor del descuento',
            'type' => 'Tipo de promocion',
            'description' => 'Descripcion',
            'num_uses' => 'Numero de usos'
        ];
        
        $validation = Validator::make($request->all(),$rules,$msgs);
        $validation->setAttributeNames($attrs);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->messages()->first()
            ], 422);
        }

        if($request->amount <= 0){
            return response()->json(['error' => 'El Valor del descuento debe ser mayor a 0'], 422);
        }

        if($request->num_uses < 0){
            return response()->json(['error' => 'La cantidad de usos debe ser mayor o igual a 0'], 422);
        }

        $date=new Carbon($request->expire_date);

        $promotion = Promotion::where('id', $id)->first();
            $promotion->num_uses = $request->num_uses;
            $promotion->expire_date = $date->format('Y-m-d');
            $promotion->amount = $request->amount;
            $promotion->type = $request->type;
            $promotion->description = $request->description;
            $promotion->registered_by = Auth::user()->id;
        $promotion->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Promotion::where('id', $id)->delete();
    }

    public function activate($id)
    {
        $promotion = Promotion::find($id);
        if ($promotion->status == '1') {
            $promotion->status = '0';
            $msg = 'Se ha desactivado la Promoción con exito';
        } else {
            $promotion->status = '1';
            $msg = 'Se ha activado la Promoción con exito';
        }
        $promotion->save();

        return response()->json([
            'result' => true,
            'msg' => $msg,
            'promotion' => $promotion->status
        ]);
    }
}
