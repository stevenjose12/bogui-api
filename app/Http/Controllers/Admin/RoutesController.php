<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Route;
use App\Models\Commission;
use App\User;

class RoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Route::whereHas('to_destinies')
        ->with(['driver.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_user.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_destinies' => function ($query){
            $query->orderBy('type', 'asc')
                ->orderBy('id', 'desc');
        }])
        ->with(['tariff', 'vehicle_type'])
        ->orderBy('id', 'desc')
        ->get();

        $commission = Commission::first();

        foreach ($routes as $route) {
            if ($route->status == '0') {
                $route->status_type = 'Suspendida';
            } else if ($route->status == '1') {
                $route->status_type = 'Activa';
            } else if ($route->status == '2') {
                $route->status_type = 'Culminada';
            } else if ($route->status == '3') {
                $route->status_type = 'Cancelada';
            }

            if ($route->status_route == '0') {
                $route->status_pass = 'Sin Abordar';
            } else {
                $route->status_pass = 'Abordo';
            }

            // $destinies = $route->to_destinies;
            // foreach ($destinies as $destiny) {
            //     $value = $this->calcularDistancia($route->latitud_origin,$route->longitud_origin,$destiny->latitud,$destiny->longitud);
            //     $route->distance += $value;
            // }

            $route->cost = floatval($route->cost);
            $route->commission = $route->cost * ($commission->amount / 100);
            $route->commission = floor($route->commission / 50) * 50;
        }
        return view('admin.routes.index')->with([
            'routes' => $routes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $route = Route::where('id', $id)
        ->whereHas('to_destinies')
        ->with(['driver.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_user.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_destinies' => function ($query){
            $query->orderBy('type', 'asc')
                ->orderBy('id', 'desc');
        }])
        ->with(['tariff', 'vehicle_type', 'payments'])
        ->first();

        $commission = Commission::first();
        if ($route->status == '0') {
            $route->status_type = 'Suspendida';
        } else if ($route->status == '1') {
            $route->status_type = 'Activa';
        } else if ($route->status == '2') {
            $route->status_type = 'Culminada';
        } else if ($route->status == '3') {
            $route->status_type = 'Cancelada';
        }

        if ($route->status_route == '0') {
            $route->status_pass = 'Sin Abordar';
        } else {
            $route->status_pass = 'Abordo';
        }

        $route->cost = floatval($route->cost);
        $route->commission = $route->cost * ($commission->amount / 100);
        $route->commission = floor($route->commission / 50) * 50;
        if ($route->payment == '1') {
            $route->payment_type = 'Tarjeta de Credito';
        } else if ($route->payment == '2') {
            $route->payment_type = 'Efectivo';
        } else if ($route->payment == '3') {
            $route->payment_type = 'Promocion';
        }

        $origin = array('latitud' => $route->latitud_origin, 'longitud' => $route->longitud_origin, 'route_type' => 'Origen', 'type' => 0);

        foreach ($route->to_destinies as $dst) {
            if ($dst->type == '1') {
                $dst->route_type = 'Parada Adicional';
            } else if ($dst->type == '2') {
                $dst->route_type = 'Destino';
            }
        }

        $route->driver_earning = $route->cost - $route->commission;
        $route->driver_earning = floor($route->driver_earning / 50) * 50;
        $route->to_destinies->prepend($origin);

        return view('admin.routes.edit_routes')->with([
            'route' => $route,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function routesUsers()
    {
        $users_ids = User::where('level', '2')->withTrashed()->get()->pluck('id');

        $routes = Route::whereHas('to_destinies')
        ->whereIn('driver_id', $users_ids)
        ->with(['driver.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_destinies' => function ($query){
            $query->orderBy('type', 'asc')
                ->orderBy('id', 'desc');
        }])
        ->with(['tariff', 'vehicle_type'])
        ->orderBy('id', 'desc')
        ->get();

        return view('admin.routes.users-all-routes')->with([
            'routes' => $routes
        ]);
    }

    public function routesPassengers()
    {
        $users_ids = User::where('level', '3')->withTrashed()->get()->pluck('id');

        $routes = Route::whereHas('to_destinies')
        ->whereIn('user_id', $users_ids)
        ->with(['to_user.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_destinies' => function ($query){
            $query->orderBy('type', 'asc')
                ->orderBy('id', 'desc');
        }])
        ->with(['tariff', 'vehicle_type'])
        ->orderBy('id', 'desc')
        ->get();

        return view('admin.routes.passengers-all-routes')->with([
            'routes' => $routes
        ]);
    }

    private function calcularDistancia($lat1, $lng1, $lat2, $lng2) {
        if( ($lat1==$lat2) && ($lng1==$lng2) ) return 0;
        $degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng1-$lng2)))));
        return round($degrees*111.13384, 2);
    }
}
