<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\User;

class RatingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers = User::where('level', '2')
        ->whereHas('vehicle')
        ->with('person', 'vehicle.vehicle_categories', 'vehicle.photos')
        ->get();

        $clients = User::where('level', '3')
            ->with('person')->get();

        $users = $drivers->merge($clients);

        foreach ($users as $user) {
            if ($user->person->document_type == '1') {
                $user->person->document_type_name = 'RUT';
            } else if ($user->person->document_type == '2') {
                $user->person->document_type_name = 'Pasaporte';
            } else if ($user->person->document_type == '3') {
                $user->person->document_type_name = 'Otro';
            }
            if ($user->level == '1') {
                $user->level_type = 'Administrador';
            } else if ($user->level == '2') {
                $user->level_type = 'Conductor';
            } else if ($user->level == '3') {
                $user->level_type = 'Pasajero';
            } else {
                $user->level_type = 'Moderador';
            }
        }

        $bogui = Rating::where('bogui', '1')
                        ->with(['reviewer' => function ($query){
                            $query->with('person');
                        }])
                        ->groupBy('reviewer_id')
                        ->get();

        foreach ($bogui as $bg) {
            if ($bg->reviewer->level == '1') {
                $bg->reviewer->level_type = 'Administrador';
            } else if ($bg->reviewer->level == '2') {
                $bg->reviewer->level_type = 'Conductor';
            } else if ($bg->reviewer->level == '3') {
                $bg->reviewer->level_type = 'Pasajero';
            } else {
                $bg->reviewer->level_type = 'Moderador';
            }
        }

        return view('admin.ratings.index')->with([
            'users' => $users,
            'bogui' => $bogui
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reviewed = Rating::where('reviewed_id', $id)
                        ->with('reviewer')
                        ->get();

        $reviewer = Rating::where('reviewer_id', $id)
                        ->with('reviewed')
                        ->get();

        $users = $revieweds->merge($reviewer);

        foreach ($users as $user) {
            if ($user->person->document_type == '1') {
                $user->person->document_type_name = 'RUT';
            } else if ($user->person->document_type == '2') {
                $user->person->document_type_name = 'Pasaporte';
            } else if ($user->person->document_type == '3') {
                $user->person->document_type_name = 'Otro';
            }
            if ($user->level == '1') {
                $user->level_type = 'Administrador';
            } else if ($user->level == '2') {
                $user->level_type = 'Conductor';
            } else if ($user->level == '3') {
                $user->level_type = 'Pasajero';
            } else {
                $user->level_type = 'Moderador';
            }
        }

        return view('admin.ratings.show')->with([
            'users' => $users
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUsers(Request $request)
    {
        $reviewed = Rating::where('reviewed_id', $request->id)
                        ->with('reviewer.person')
                        ->get();

        $reviewer = Rating::where('reviewer_id', $request->id)
                        ->with('reviewed.person')
                        ->get();

        return response()->json([
            'result' => true,
            'reviewed' => $reviewed,
            'reviewer' => $reviewer
        ]);
    }

    public function ratingsApp()
    {
        $bogui = Rating::where('bogui', '1')
                        ->with(['reviewer' => function ($query){
                            $query->with('person');
                        }])
                        ->groupBy('reviewer_id')
                        ->get();

        foreach ($bogui as $bg) {
            if ($bg->reviewer->level == '1') {
                $bg->reviewer->level_type = 'Administrador';
            } else if ($bg->reviewer->level == '2') {
                $bg->reviewer->level_type = 'Conductor';
            } else if ($bg->reviewer->level == '3') {
                $bg->reviewer->level_type = 'Pasajero';
            } else {
                $bg->reviewer->level_type = 'Moderador';
            }
        }

        return view('admin.ratings.ratings-app')->with([
            'bogui' => $bogui
        ]);
    }
}
