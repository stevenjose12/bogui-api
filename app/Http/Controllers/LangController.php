<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class LangController extends Controller {
        
        public function change($lang,Request $request) {
        	\Cookie::queue("BoguiLang",$lang, 2628000);
        	return Back();
        }
	}