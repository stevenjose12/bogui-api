<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Route;
use App\Models\Commission;
use App\User;

class RouteController extends Controller
{
    public function index(Request $request, $id)
    {
        $route = Route::where('id', $id)
        ->whereHas('to_destinies')
        ->with(['driver.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_user.person' => function ($query){
            $query->withTrashed();
        }])
        ->with(['to_destinies' => function ($query){
            $query->orderBy('type', 'asc')
                ->orderBy('id', 'desc');
        }])
        ->with(['tariff', 'vehicle_type'])
        ->first();

        if ($route == null || $route->status != '1') {
            return View('page.no-disponible');
        }

        $commission = Commission::first();
        if ($route->status == '0') {
            $route->status_type = 'Suspendida';
        } else if ($route->status == '1') {
            $route->status_type = 'Activa';
        } else if ($route->status == '2') {
            $route->status_type = 'Culminada';
        } else if ($route->status == '3') {
            $route->status_type = 'Cancelada';
        }

        if ($route->status_route == '0') {
            $route->status_pass = 'Sin Abordar';
        } else {
            $route->status_pass = 'Abordo';
        }

        $route->cost = floatval($route->cost);
        $route->commission = $route->cost * ($commission->amount / 100);
        $route->commission = floor($route->commission / 50) * 50;
        if ($route->payment == '1') {
            $route->payment_type = 'Tarjeta de Credito';
        } else if ($route->payment == '2') {
            $route->payment_type = 'Efectivo';
        }

        $origin = array('latitud' => $route->latitud_origin, 'longitud' => $route->longitud_origin, 'route_type' => 'Partida', 'type' => 0);

        foreach ($route->to_destinies as $dst) {
            if ($dst->type == '1') {
                $dst->route_type = 'Adicional';
            } else if ($dst->type == '2') {
                $dst->route_type = 'Destino';
            }
        }

        $route->driver_earning = $route->cost - $route->commission;
        $route->driver_earning = floor($route->driver_earning / 50) * 50;
        $route->to_destinies->prepend($origin);

        $driver = User::where('id', $route->driver_id)->with(['vehicle.vehicle_categories', 'person'])->first();
        // dd($driver);
        return view('page.route', [
            'route' => $route,
            'driver' => $driver,
            'id' => $request->get('id')
        ]);
    }
}
