<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Panic;

class FollowPassengerController extends Controller
{
    public function index(Request $request){
        $panic = Panic::where('id', $request->id)
            ->where('status', 1)
            ->where('type', 2)
            // ->whereBetween('created_at',[
            //     Carbon::now()->format('Y-m-d 00:00:00'),
            //     Carbon::now()->format('Y-m-d 23:59:59')
            // ])
            ->with('user.person')
            ->first();
        if(isset($panic)){
            $panic->hour = Carbon::parse($panic->created_at)->format('d-m-Y H:m');
            return view('page.followPassenger', [
                'panic' => $panic,
            ]);
        }else
            return View('page.no-disponible');
    }
    
    public function create(Request $request){
        if($request->has('user_id')){
            $panic = new Panic;
            $panic->user_id = $request->user_id;
            $panic->type = 2;
            $panic->status = 1;
            if(isset($request->latitude))
                $panic->latitude = $request->latitude;
            if(isset($request->longitude))
                $panic->longitude = $request->longitude;
            $panic->save();
            
            return response()->json([
                'result' => true,
                'url' => \URL::to('/followPassenger/'.$panic->id),
                'date' => $panic->created_at
            ]);
        }else{
            return response()->json([
                'result' => false
            ], 500);
        }
    }

}
