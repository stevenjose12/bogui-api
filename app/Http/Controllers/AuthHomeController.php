<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PasswordApp AS Password;
use App\User;
use Hash;
use Auth;
use App\Models\Country;
use Validator;
use App\Models\Person;
use App\Models\Bank;
use App\Models\AccountType;
use App\Models\Vehicle;
use App\Models\VehiclePhoto;
use App\Models\BankUser;
use Intervention\Image\ImageManagerStatic as Image;
use App\Libraries\DiskStorage;
use App\Models\Validation;
use App\Libraries\GeneratePassword;
use App\Libraries\InfoBip;
use Mail;
use Carbon\Carbon;

class AuthHomeController extends Controller {
	
	public function getLogin() {
		return View('page.login');
	}

	public function postLogin(Request $request) {
		
		$user = User::where('email',$request->email)->where('level', $request->type)->first();

		if ( (is_null($user)) ) {
			return response()->json([
				'result' => false,
				'error' => 'Correo electrónico o contraseña incorrectos'
			]);
		}
		
		if ( ($user->level != '2' && $user->level != '3') ) {
			return response()->json([
				'result' => false,
				'error' => 'Lo sentimos, su nivel de usuario requiere entrar a la zona administrativa'
			]);
		}
		
		if ( Auth::attempt(['email' => $request->email, 'password' => $request->password, 'level' => $request->type]) ) {
			return response()->json([
				'result' => true,
				'url' => \URL('account')
			]);
		}
		
		else {
			return response()->json([
				'result' => false,
				'error' => 'Correo electrónico o contraseña incorrectos'
			]);
		}
	}

	public function getRegister() {
		$bancos = Bank::orderBy('name','asc')->get()->pluck('name','id');
		$cuentas = AccountType::all()->pluck('name','id');
		$paises = Country::select('*',\DB::raw('CONCAT("(+",phonecode,") ",name) AS fullname'))->orderBy('name','asc')->get()->pluck('fullname','phonecode');
		return View('page.register')->with([
			'paises' => $paises,
			'bancos' => $bancos,
			'cuentas' => $cuentas
		]);
	}

	public function postRegister(Request $request) {
		$reglas = [
			'nombre' => 'required',
			'apellido' => 'required',
			// 'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
			'email' => 'required|email',
			'code' => 'required',
			'telefono' => 'required|numeric',
			'document_type' => 'required',
			'document' => 'required',
			'password' => 'required|string|min:6|confirmed',
			'photo' => 'required|mimes:jpg,png,gif,jpeg',
			'expiration_document' => 'required'
			// 'document_photo' => 'required|mimes:jpg,png,gif,jpeg'
		];

		$atributos = [
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'email' => 'Correo Electrónico',
			'code' => 'Código',
			'telefono' => 'Teléfono',
			'document_type' => 'Tipo de Documento',
			'document' => 'RUT o Pasaporte',
			'password' => 'Contraseña',
			'photo' => 'Foto',
			'document_photo' => 'Foto del Documento',
			'expiration_document' => 'Fecha expiración'
		];

		$validacion = Validator::make($request->all(),$reglas);
		$validacion->setAttributeNames($atributos);

		if ($validacion->fails()) {
			return response()->json([
				'result' => false,
				'error' => $validacion->messages()->first()
			]);
		}
		else {

			$_user = User::where('email',$request->email)->where('level',3)->count();

			if ($_user > 0) {
				return response()->json([
					'result' => false,
					'error' => 'El correo electrónico ya se encuentra registrado'
				]);
			}

			$person = Person::where('document',$request->document)->whereHas('person',function($q) {
				$q->where('level',3);
			})->count();

			if ($person > 0) {
				return response()->json([
					'result' => false,
					'error' => "El RUT ya se encuentra registrado"
				]);
			}

			// $person = Person::where(function($q) use ($request) {
			// 	$q->where('phone','+'.$request->code.' '.$request->telefono)
			// 		->orWhere('phone','+'.$request->code.$request->telefono);
			// })->count();

			$person = Person::where(function($q) use ($request) {
				$q->where('phone',$request->code.$request->telefono)
					->orWhere('phone',$request->telefono);
			})->whereHas('person',function($q) {
				$q->where('level',3);
			})->count();

			if ($request->code == '56') {
				if (strlen($request->telefono) != 9) {
					return response()->json([
						'result' => false,
						'error' => "Para Chile, el número de teléfono debe poseer 9 dígitos. Por favor verifique el número que ha ingresado."
					]);
				}
			}

			if ($person > 0) {
				return response()->json([
					'result' => false,
					'error' => "El número de teléfono ya se encuentra registrado"
				]);
			}

			$user = new User;
			$user->level = '3';
			$user->email = $request->email;
			$user->password = $request->password;
			$user->status = '0';
			$user->save();

			$person = new Person;
			$person->name = $request->nombre;
			$person->lastname = $request->apellido;
			$person->document_type = $request->document_type;
			$person->document = $request->document;
			$person->user_id = $user->id;
			$person->phone = $request->code.$request->telefono;
			$person->expiration_document = Carbon::parse($request->expiration_document);
			$person->save();

			$this->uploadAvatar($person->id,$request->photo);
			//$this->uploadDocument($person->id,$request->document_photo);

			$codigo = GeneratePassword::get();
			$validation = new Validation;
			$validation->user_id = $user->id;
			$validation->code = strtoupper($codigo);
			$validation->save();

			InfoBip::send("Bogui",'+'.$request->code.$request->telefono,"Bienvenido a Bogui, su codigo de verificacion es: ".strtoupper($codigo));

			try {
				\Mail::send('emails.validate', ['codigo' => strtoupper($codigo)], function($m) use ($user) {
					$m->to($user->email, $user->person->name.' '.$user->person->lastname)
						->subject('Activación de su cuenta en Bogui | Bogui pasajero');
				});	
			}   
			catch(\Exception $e) {

			}

			return response()->json([
				'result' => true,
				'url' => \URL('login'),
				'user_id' => $user->id
			]);
		}
	}

	private function uploadAvatar($id,$file) {
		$person = Person::find($id);
			$ruta = 'passengers/profile/'.substr(str_replace(array('/','.'), array('',''),bcrypt(strtotime(date('Y-m-d H:i:s')))),7,32).'.jpg';
			Image::make($file)->save($ruta,60);
			$person['photo'] = $ruta;
		$person->save();
	}

	private function uploadDocument($id,$file) {
		$person = Person::find($id);
			$ruta = 'passengers/document/'.substr(str_replace(array('/','.'), array('',''),bcrypt(strtotime(date('Y-m-d H:i:s')))),7,32).'.jpg';
			Image::make($file)->save($ruta,60);
			$person['document_photo'] = $ruta;
		$person->save();
	}

	public function postRegisterConductor(Request $request) {
		$reglas = [
			'nombre' => 'required',
			'apellido' => 'required',
			// 'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
			'email' => 'required|email',
			'code' => 'required',
			'telefono' => 'required|numeric',
			'document_type' => 'required',
			'document' => 'required',
			'password' => 'required|string|min:6|confirmed',
			'photo' => 'required|mimes:jpg,png,gif,jpeg',
			'document_photo' => 'required|mimes:jpg,png,gif,jpeg',
			'certificado' => 'required|mimes:jpg,png,gif,jpeg',
			'photo_vehicle' => 'required|mimes:jpg,png,gif,jpeg',
			'marca' => 'required',
			'modelo' => 'required',
			'year' => 'required|numeric|min:2000|max:' . date("Y"),
			'patente' => 'required',
			'licencia' => 'required|mimes:jpg,png,gif,jpeg',
			'permiso' => 'required|mimes:jpg,png,gif,jpeg',
			'banco' => 'required',
			'type_account' => 'required',
			'rut_banco' => 'required',
			'account' => 'required|numeric',
			// 'expiration_license' => 'required|date_format:d-m-Y',
			'expiration_permit' => 'required|date_format:d-m-Y',
			// 'expiration_document' => 'required|date_format:d-m-Y',
			// 'expiration_criminal_records' => 'required|date_format:d-m-Y'
		];
		
		$atributos = [
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'email' => 'Correo Electrónico',
			'code' => 'Código',
			'telefono' => 'Teléfono',
			'document_type' => 'Tipo de Documento',
			'document' => 'RUT o Pasaporte',
			'password' => 'Contraseña',
			'photo' => 'Foto',
			'document_photo' => 'Foto del Documento',
			'certificado' => 'Certificado',
			'marca' => 'Marca de Vehículo',
			'modelo' => 'Modelo de Vehículo',
			'year' => 'año',
			'patente' => 'Patente',
			'licencia' => 'Licencia de Conducir',
			'permiso' => 'Permiso de Circulación',
			'banco' => 'Banco',
			'type_account' => 'Tipo de Cuenta',
			'rut_banco' => 'RUT de la Cuenta Bancaria',
			'account' => 'Número de Cuenta',
			'expiration_license' => 'Expiración de Licencia de Conducir',
			'expiration_permit' => 'Expiración del Permiso de Circulación',
			'expiration_document' => 'Expiración del RUT o Pasaporte',
			'expiration_criminal_records' => 'Expiración del Cerificado de Antecedentes'
		];
		
		$msgs = [
			'min' => 'El :attribute del modelo no puede ser menor al año 2000',
			'max' => 'El :attribute del modelo no puede ser mayor al año' . date("Y")
        ];
	
		$validacion = Validator::make($request->all(), $reglas, $msgs);
		$validacion->setAttributeNames($atributos);

		if ($validacion->fails()) {
			return response()->json([
				'result' => false,
				'error' => $validacion->messages()->first()
			]);
		}
		else {
		
			$_user = User::where('email',$request->email)->where('level',2)->count();

			if ($_user > 0) {
				return response()->json([
					'result' => false,
					'error' => 'El correo electrónico ya se encuentra registrado'
				]);
			}
			
			$person = Person::where('document',$request->document)->whereHas('person',function($q) {
				$q->where('level',2);
			})->count();

			if ($person > 0) {
				return response()->json([
					'result' => false,
					'error' => "El RUT ya se encuentra registrado"
				]);
			}

			$person = Person::where(function($q) use ($request) {
				$q->where('phone',$request->code.$request->telefono)
					->orWhere('phone',$request->telefono);
			})->whereHas('person',function($q) {
				$q->where('level',2);
			})->count();

			// $person = Person::where(function($q) use ($request) {
			// 	$q->where('phone','+'.$request->code.' '.$request->telefono)
			// 		->orWhere('phone','+'.$request->code.$request->telefono)
			// 		->orWhere('phone',$request->code.$request->telefono);
			// })->count();

			if ($request->code == '56') {
				if (strlen($request->telefono) != 9) {
					return response()->json([
						'result' => false,
						'error' => "Para Chile, el número de teléfono debe poseer 9 dígitos. Por favor verifique el número que ha ingresado."
					]);
				}
			}

			if ($person > 0) {
				return response()->json([
					'result' => false,
					'error' => "El número de teléfono ya se encuentra registrado"
				]);
			}

			$user = new User; 
			$user->level = '2';
			$user->email = $request->email;
			$user->password = $request->password;
			$user->status = '0';
			$user->save();

			$person = new Person;
			$person->name = $request->nombre;
			$person->lastname = $request->apellido;
			$person->phone = $request->code.$request->telefono;
			$person->document = $request->document;
			$person->document_type = $request->document_type;
			$person->user_id = $user->id;
			$person->expiration_permit = Carbon::parse($request->expiration_permit);
			// $person->expiration_criminal_records = Carbon::parse($request->expiration_criminal_records);
			// $person->expiration_license = Carbon::parse($request->expiration_license);
			// $person->expiration_document = Carbon::parse($request->expiration_document);
			$person->save();

			$vehicle = new Vehicle;
			$vehicle->brand = $request->marca;
			$vehicle->model = $request->modelo;
			$vehicle->plate = $request->patente;
			$vehicle->year = $request->year;
			$vehicle->expiration = Carbon::parse($request->expiration_permit);
			$vehicle->user_id = $user->id;
			$vehicle->save();

			$vehicle_photo = new VehiclePhoto;
			$vehicle_photo->vehicle_id = $vehicle->id;
			$name = DiskStorage::name();
			$dg = 0;
			
			if ($request->has('dg')) {
				$dg = $request->dg;
			}
			
			Image::make($request->photo_vehicle)->rotate($dg * (-1))->save(DiskStorage::get('vehicles').$name,25);
			$vehicle_photo->file = 'vehicles/'.$name;
			$vehicle_photo->save();

			$bank = new BankUser;
			$bank->user_id = $user->id;
			$bank->bank_id = $request->banco;
			$bank->type = $request->type_account;
			$bank->number = $request->account;
			$bank->identification = $request->rut_banco;
			$bank->save();

			$codigo = GeneratePassword::get();

			$validation = new Validation;
			$validation->user_id = $user->id;
			$validation->code = $codigo;
			$validation->save();

			InfoBip::send("Bogui Conductor",'+'.$request->code.$request->telefono,"Bienvenido a Bogui, su codigo de verificacion es: ".$codigo);

			try {
				\Mail::send('emails.validate-driver', ['codigo' => $codigo], function ($m) use ($user) {
					$m->to($user->email,$user->person->name.' '.$user->person->lastname)
						->subject('Verificación de Cuenta | Bogui Conductor');
				});	
			}   
			catch(\Exception $e) {

			}	
				
			$this->upload($person->id,$request->document_photo,'document_photo');
			$this->upload($person->id,$request->photo,'photo');
			$this->upload($person->id,$request->licencia,'license');
			$this->uploadFile($vehicle->id, 'permit', $request, 'permiso');
			$this->uploadFile($person->id,'criminal_records', $request, 'certificado');          

			return response()->json([
				'result' => true,
				'url' => \URL('login'),
				'user_id' => $user->id
			]);
		}
	}

	private function upload($id,$file,$_name) {
		$person = Person::find($id);
		$name = DiskStorage::name();
		Image::make($file)->save(DiskStorage::get('drivers').($_name == 'photo' ? 'profile' : 'files').'/'.$name,25);
		$person[$_name] = 'drivers/'.($_name == 'photo' ? 'profile/' : 'files/').$name;
		$person->save();
	}

	private function uploadFile($id, $_name, $request, $request_name) {

		// Verifico si la imagen que esta llegando contiene la extensión
		if ( ($request->file($request_name)->getClientOriginalExtension() == null) ) {
			$ext = $this->extensionImg($request->file($request_name));
		} else {
			$ext = $request->file($request_name)->getClientOriginalExtension();
		}

		// Guardo la imagen en sus respectivas tablas
		if ($_name == 'permit') {

			$vehicle = Vehicle::with([
				'user'
			])->find($id);
			
			$name = DiskStorage::name($ext);
			$request->file($request_name)->move(DiskStorage::get('drivers').'files', $name);
			$vehicle[$_name] = 'drivers/files/'.$name;
			$vehicle->save();

		} else {

			$person = Person::with([
				'person'
			])->find($id);

			$name = DiskStorage::name($ext);
			$request->file($request_name)->move(DiskStorage::get('drivers').'files',$name);
			$person[$_name] = 'drivers/files/'.$name;
			$person->save();
		}
	}

	public function getCodigo() {
		return View('page.codigo');
	}

	public function postCodigo(Request $request) {
		$reglas = [
			'codigo' => 'required'
		];
		$atributos = [
			'codigo' => 'Código'
		];
		$validacion = Validator::make($request->all(),$reglas);
		$validacion->setAttributeNames($atributos);
		if ($validacion->fails()) {
			return response()->json([
				'result' => false,
				'error' => $validacion->messages()->first()
			]);
		}
		else {

			$validation = Validation::orderBy('id','desc')->where('user_id',Auth::id())->where('code',$request->codigo)->first();
			if ($validation == null) {
				return response()->json([
					'result' => false,
					'error' => 'No se pudo encontrar el código'
				]);
			}

			$user = User::find(Auth::id());
				$user->validate = 1;
			$user->save();

			return response()->json([
				'result' => true,
				'url' => \URL('account')
			]);
		}	
	}

	public function postReenviar(Request $request) {
		$codigo = GeneratePassword::get();

		InfoBip::send("Bogui",str_replace(' ','',Auth::user()->person->phone),"Bienvenido a Bogui, su codigo de verificacion es: ".$codigo);

		$user = Auth::user();

		if ($user->level == 2) {
			try {
				\Mail::send('emails.validate-driver', ['codigo' => $codigo], function ($m) use ($user) {
					$m->to($user->email,$user->person->name.' '.$user->person->lastname)
						->subject('Verificación de Cuenta | Bogui Conductor');
				});	
			}   
			catch(\Exception $e) {

			}	
		}
		else if ($user->level == 3) {
			try {
				\Mail::send('emails.validate', ['codigo' => $codigo], function($m) use ($user) {
					$m->to($user->email, $user->person->name.' '.$user->person->lastname)
						->subject('Activación de su cuenta en Bogui | Bogui pasajero');
				});	
			}   
			catch(\Exception $e) {

			}
		}

		$validation = new Validation;
			$validation->user_id = Auth::id();
			$validation->code = $codigo;
		$validation->save();

		return response()->json([
			'result' => true
		]);
	}

	public function getReset() {
		return View('page.reset');
	}

	public function sendCode(Request $request) {
		Password::where('status','1')->where('email',$request->email)->where('level',$request->type)->update(['status' => '0']);

		$user = User::where('email',$request->email)->where('level',$request->type)->first();

		if (count($user) == 0) {
			return response()->json([
				'result' => false,
				'error' => "El correo electrónico no se encuentra registrado"
			]);
		}

		$codigo = GeneratePassword::get();

		$password = new Password;
			$password->email = $request->email;
			$password->code = $codigo;
			$password->level = $request->type;
		$password->save();

		$validation = new Validation;
			$validation->user_id = $user->id;
			$validation->code = $codigo;
		$validation->save();

		if ($request->type == 2) {
			Mail::send('emails.reset', ['codigo' => $codigo], function ($m) use ($user) {
				$m->to($user->email,$user->person->name.' '.$user->person->lastname)
					->subject('Recuperación de Contraseña | Bogui Conductor');
			});
		}
		else if ($request->type == 3) {
			Mail::send('emails.reset-pasajero', ['codigo' => $codigo], function ($m) use ($user) {
				$m->to($user->email,$user->person->name.' '.$user->person->lastname)
					->subject('Recuperación de Contraseña | Bogui');
			});
		}	    	

		return response()->json([
			'result' => true
		]);
	}

	public function checkCode(Request $request) {
		$password = Password::where('code',$request->codigo)
			->where('email',$request->email)
			->where('status','1')
			->where('level',$request->type)
			->count();
		if ($password == 0) {
			return response()->json([
				'result' => false,
				'error' => "No se ha encontrado el código"
			]);
		}

		return response()->json([
			'result' => true
		]);
	}

	public function postReset(Request $request) {
		$password = Password::where('code',$request->codigo)
			->where('email',$request->email)
			->where('status','1')
			->where('level',$request->type)
			->count();
		if ($password == 0) {
			return response()->json([
				'result' => false,
				'error' => "No se procesar su solicitud"
			]);
		}

		$user = User::where('email',$request->email)->where('level',$request->type)->first();
			$user->password = $request->password;
		$user->save();

		Password::where('status','1')->where('email',$request->email)->update(['status' => '0']);

		return response()->json([
			'result' => true,
			'url' => URL('login')
		]);
	}
	
	public function extensionImg ($file)
	{
		if ( ($file->getClientMimeType() == 'image/jpeg') ) {
			return 'jpg';
		} else if ( ($file->getClientMimeType() == 'image/png') ) {
			return 'png';
		} else if ( ($file->getClientMimeType() == 'image/gif') ) {
			return 'gif';
		} else if ( ($file->getClientMimeType() == 'image/jpeg') ) {
			return 'jpeg';
		}
	}

	public function logout() {
		Auth::logout();
		return Redirect('/');
	}
}
