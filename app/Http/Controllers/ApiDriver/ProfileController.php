<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Intervention\Image\ImageManagerStatic as Image;
	use App\User;
	use Hash;
	use Validator;
	use App\Libraries\DiskStorage;
	use App\Models\Person;
	use File;
	use App\Models\Vehicle;
	use App\Models\VehiclePhoto;
	use App\Models\Notification;
	use Carbon\Carbon;

	class ProfileController extends Controller {

		public function get(Request $request) {
			$user = User::with(['person','vehicle' => function($q) {
				$q->with(['photos']);
			}])->where('id',$request->id)->first();
			return response()->json([
				'result' => true,
				'user' => $user
			]);
		}

		public function defaultApp(Request $request) {
			$user = User::find($request->id);
				$user->default_app = $request->app;
			$user->save();
			return response()->json([
				'result' => true,
				'user' => $user
			]);
		}

		public function updateStatus(Request $request) {
			$user = User::find($request->id);
				$user->status_driver = $request->status;
			$user->save();
			return response()->json([
				'result' => true
			]);
		}
	    
	    public function password(Request $request) {
	    	$reglas = [
	    		'password' => 'confirmed|required',
	    		'old_password' => 'required',
	    		'password_confirmation' => 'required'
	    	];
	    	$mensajes = [
	    		'confirmed' => 'Las contraseñas no coinciden',
	    		'required' => 'El campo :attribute es requerido',
	    	];
	    	$atributos = [
	    		'password' => 'Contraseña Nueva',
	    		'old_password' => 'Contraseña Actual',
	    		'password_confirmation' => 'Repetir Contraseña Nueva'
	    	];
	    	$validacion = Validator::make($request->all(),$reglas,$mensajes);
	    	$validacion->setAttributeNames($atributos);
	    	if ($validacion->fails()) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => $validacion->messages()->first()
	    		]);
	    	}
	    	else {
	    		$user = User::find($request->id);
	    		if (Hash::check($request->old_password,$user->password)) {
		    			$user->password = $request->password;
		    		$user->save();
		    		return response()->json([
		    			'result' => true
		    		]);
	    		}
	    		else {
					return response()->json([
		    			'result' => false,
		    			'error' => 'La contraseña actual no coincide'
		    		]);
	    		}	    		
	    	}
	    }

	    public function upload(Request $request) {
	    	$person = Person::where('user_id',$request->id)->first();
		    	$name = DiskStorage::name();
		    	$dg = 0;
		    	if ($request->has('dg')) {
		    		$dg = $request->dg;
		    	}
		    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('drivers').($request->name == 'photo' ? 'profile' : 'files').'/'.$name,25);
	    		File::delete(public_path($person[$request->name]));
	    		$person[$request->name] = 'drivers/'.($request->name == 'photo' ? 'profile/' : 'files/').$name;
	    	$person->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function post(Request $request) {
	    	$reglas = [
	    		// 'email' => 'required|email|unique:users,email,'.$request->id.',id,deleted_at,NULL',
	    		'email' => 'required|email',
	    		'telefono' => 'required'
	    	];
	    	$mensajes = [
	    		'required' => 'El campo :attribute es requerido',
	    		'email' => 'El correo electrónico no es válido',
	    		'unique' => 'El correo electrónico ya se encuentra registrado' 
	    	];
	    	$atributos = [
	    		'email' => 'Correo Electrónico',
	    		'telefono' => 'Teléfono'
	    	];
	    	$validacion = Validator::make($request->all(),$reglas,$mensajes);
	    	$validacion->setAttributeNames($atributos);
	    	if ($validacion->fails()) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => $validacion->messages()->first()
	    		]);
	    	}
	    	else {

	    		$request->telefono = str_replace(' ', '', $request->telefono);
	    		
	    		$_persons = Person::where('phone',$request->telefono)->whereHas('person',function($q) {
	    			$q->where('level',2);
	    		})->where('user_id','!=',$request->id)->count();

	    		$_users = User::where('email',$request->email)->where('level',2)->where('id','!=',$request->id)->count();
	    		
	    		if (substr($request->telefono, 0, 2) == '56'){
	    			$check_phone = substr($request->telefono, 2);
	    			if (strlen($check_phone) != 9) {
		    			return response()->json([
			    			'result' => false,
			    			'error' => "Para Chile, el número de teléfono debe poseer 9 dígitos. Por favor verifique el número que ha ingresado."
			    		]);
	    			}
	    		}

	    		if ($_persons != 0) {
	    			return response()->json([
		    			'result' => false,
		    			'error' => "El número de teléfono ya esta registrado"
		    		]);
	    		}

	    		if ($_users != 0) {
	    			return response()->json([
		    			'result' => false,
		    			'error' => "El correo electrónico ya esta registrado"
		    		]);
	    		}

	    		$user = User::find($request->id);
	    			$user->email = $request->email;
	    		$user->save();

	    		$person = Person::where('user_id',$request->id)->first();
	    			$person->phone = $request->telefono;
	    		$person->save();

	    		$user = User::with(['vehicle.vehicle_categories'])
                    ->where('id',$user->id)
                    ->first();

		    	return response()->json([
	    			'result' => true,
	    			'user' => $user
	    		]);
	    	}
	    }

	    public function postVehicle(Request $request) {
	    	// $vehicle = Vehicle::find($request->id);
	    	// 	$vehicle->brand = $request->brand;
	    	// 	$vehicle->model = $request->model;
	    	// 	$vehicle->year = $request->year;
	    	// 	$vehicle->plate = $request->plate;
	    	// $vehicle->save();

    		return response()->json([
    			'result' => true
    		]);
	    }

	    public function vehiclePhoto(Request $request) {
	   //  	if ($request->type == "1") {
	   //  		$user = User::where('id',$request->id)->with(['vehicle' => function($q) {
	   //  			$q->with(['photos']);
	   //  		}])->first();
	   //  		$vehicle_photo = $user->vehicle->photos[0];
	   //  			$name = DiskStorage::name();
	   //  			$vehicle_photo->file = $name;
	   //  			$dg = 0;
			 //    	if ($request->has('dg')) {
			 //    		$dg = $request->dg;
			 //    	}
			 //    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('vehicles').$name,25);
			 //    	File::delete(public_path($vehicle_photo->file));
			 //    	$vehicle_photo->file = 'vehicles/'.$name;
	   //  		$vehicle_photo->save();
	   //  	}
	   //  	else {
				// $person = Person::where('user_id',$request->id)->first();
			 //    	$name = DiskStorage::name();
			 //    	$dg = 0;
			 //    	if ($request->has('dg')) {
			 //    		$dg = $request->dg;
			 //    	}
			 //    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('drivers').($request->name == 'photo' ? 'profile' : 'files').'/'.$name,25);
		  //   		File::delete(public_path($person[$request->name]));
		  //   		$person[$request->name] = 'drivers/'.($request->name == 'photo' ? 'profile/' : 'files/').$name;
	   //  		$person->save();
	   //  	}	    	

			return response()->json([
    			'result' => true
    		]);
	    }

	    public function updateAlive(Request $request) {
            try {
                $user = User::find($request->id);
                $user->latitud = $request->latitude;
                $user->longitud = $request->longitude;
                $user->alive = 1;
                $user->save();
                return response()->json(true);
            } catch (\Exception $e) {
                return response()->json(false);
            }
        }

	    public function updateLocation(Request $request) {
	    	$user = User::find($request->id);
	    		$user->latitud = $request->latitud;
	    		$user->longitud = $request->longitud;
	    		$user->alive = 1;
	    	$user->save();
            return response()->json(['result' => true, "status"=>$user->status_driver, "alive"=>1 ]);
	    }

	    public function addVehicle(Request $request) 
	    {
	    	$reglas = [
	    		'brand' => 'required',
	    		'model' => 'required',
	    		'year' => 'required',
	    		'user_id' => 'required',
	    		'plate' => 'required|unique:vehicles',
	    		'expiration' => 'required',
	    		'permit_url' => 'required',
	    	];

	    	$mensajes = [
	    		'required' => 'El campo :attribute es requerido',
	    	];

	    	$atributos = [
	    		'brand' => 'marca',
	    		'model' => 'modelo',
	    		'year' => 'año',
	    		'user_id' => 'id de usuario',
	    		'plate' => 'patente',
	    		'expiration' => 'fecha de expiración',
	    		'permit_url' => 'permiso de circulación',
	    	];

	    	$validacion = Validator::make($request->all(),$reglas,$mensajes);
	    	$validacion->setAttributeNames($atributos);
	    	
	    	if ($validacion->fails()) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => $validacion->messages()->first()
	    		]);
	    	}

	    	$vehicle = new Vehicle;
	    	$vehicle->brand = $request->brand;
	    	$vehicle->model = $request->model;
	    	$vehicle->year = $request->year;
	    	$vehicle->status = 0;
	    	$vehicle->plate = $request->plate;
	    	$date = Carbon::createFromFormat('d/m/Y', $request->expiration);
	    	$vehicle->expiration = $date->format('Y-m-d');
	    	$vehicle->permit = 'drivers/files/' . $request->permit_url;
	    	$vehicle->user_id = $request->user_id;
	    	$vehicle->save();

	    	$photo = new VehiclePhoto;
			$photo->vehicle_id = $vehicle->id;
			
			$name = DiskStorage::name();
			Image::make($request->file)
	    		->rotate($request->dg * (-1))
	    		->save(DiskStorage::get('vehicles') . $name, 25);

			$photo->file = 'vehicles/' . $name;
			$photo->save();

			$notification = Notification::create([
				'type' => 11,
				'description' => 'El conductor :name ha registrado un nuevo vehículo',
				'driver_id' => $request->user_id
			]);

	    	return response()->json([
    			'result' => true,
				'vehicle' => $vehicle->load('vehicle_categories', 'photos'),
				'notification' => $notification->load('driver.person')
    		]);

	    }

	    public function toggleVehicle (Request $request) 
	    {
	    	$vehicle = Vehicle::find($request->vehicle_id);

	    	$vehicle_two = Vehicle::where('id', '!=', $request->vehicle_id)
	    		->where('user_id', $vehicle->user_id)
	    		->whereHas('vehicle_categories')
	    		->first();

	    	if ($vehicle->status && is_null($vehicle_two)) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => 'No es posible desactivar el vehículo, debido a que no tiene otro vehículo con categoría asignada'
	    		]);
	    	}
	    	
	    	$vehicle->status = !$vehicle->status;
	    	$vehicle->save();
	    	
	    	if (!is_null($vehicle_two)) {
	    		$vehicle_two->status = !$vehicle_two->status;	
	    		$vehicle_two->save();
	    	}

	    	return response()->json([
    			'result' => true
    		]);
	    }
	}
