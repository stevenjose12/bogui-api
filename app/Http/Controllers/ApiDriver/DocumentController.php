<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\PendingDocument;
	use App\Models\Person;
	use Intervention\Image\ImageManagerStatic as Image;
	use App\Libraries\DiskStorage;
	use App\Models\Vehicle;
	use App\Models\VehiclePhoto;
	use Carbon\Carbon;
	use App\User;

	class DocumentController extends Controller	{
	    
	    public function get(Request $request) {

	    	$documentos = PendingDocument::with(['vehicle'])->where(function($q) use ($request) {
	    		$q->where('upload',0)->where('user_id',$request->id)->whereIn('status',[0,2]);
	    	})->orWhere(function($q) use ($request) {
	    		$q->where('upload',1)->where('user_id',$request->id)->where('status',2);
	    	})->get();

	    	$documentos_pendientes = PendingDocument::where('upload',1)->where('user_id',$request->id)->where('status',0)->get();

	    	return response()->json([
	    		'result' => true,
	    		'documentos' => $documentos,
	    		'documentos_pendientes' => $documentos_pendientes
	    	]);
	    }

	    public function check(Request $request) {
	    	$user = User::find($request->id);

	    	$documentos = PendingDocument::where(function($q) use ($request) {
	    		$q->where('upload',0)->where('user_id',$request->id)->whereIn('status',[0,2]);
	    	})->orWhere(function($q) use ($request) {
	    		$q->where('upload',1)->where('user_id',$request->id)->where('status',2);
	    	})->get();

	    	$documentos_pendientes = PendingDocument::where('upload',1)->where('user_id',$request->id)->where('status',0)->get();
	    	
	    	if (count($documentos_pendientes) > 0 || count($documentos) > 0) {
	    		$user->status_driver = '0';
	    		$user->last_status = 0;
	    		$user->save();
	    	}

	    	return response()->json([
	    		'result' => true,
	    		'status' => $user->status_driver,
	    		'mostrar' => count($documentos_pendientes) == 0 && count($documentos) == 0
	    	]);
	    }

	    public function upload(Request $request) {

//            return response()->json($request->file('foto'));

	    	$documento = PendingDocument::find($request->id);

	    	if ($documento->type == 3) {
	    		if ($documento->vehicle_id)
	    			$vehicle = Vehicle::find($documento->vehicle_id);
	    		else
    				$vehicle = Vehicle::where('user_id',$documento->user_id)->orderBy('id','desc')->first();
    			$vehicle_photo = VehiclePhoto::where('vehicle_id',$vehicle->id)->orderBy('id','desc')->first();
    				$name = DiskStorage::name();
	    			$dg = 0;
			    	if ($request->has('dg')) {
			    		$dg = $request->dg;
			    	}
			    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('vehicles').$name,25);
			    	$vehicle_photo->file = 'vehicles/'.$name;
    			$vehicle_photo->save();
    		}
    		else if ($documento->type == 4) {
    			if ($documento->vehicle_id)
	    			$vehicle = Vehicle::find($documento->vehicle_id);
	    		else
    				$vehicle = Vehicle::where('user_id',$documento->user_id)->orderBy('id','desc')->first();
    			$name = DiskStorage::name();
                $dg = 0;
                if ($request->has('dg')) {
                    $dg = $request->dg;
                }

                try {
                    Image::make($request->file)
                        ->rotate($dg * (-1))
                        ->save(DiskStorage::get('drivers') . ($this->getName($documento->type) == 'photo' ? 'profile' : 'files') . '/' . $name, 25);
                } catch (\Exception $e) {
                    return response()->json($e);
                }

                $vehicle->permit = 'drivers/'.($this->getName($documento->type) == 'photo' ? 'profile/' : 'files/').$name;
                if ($request->has('date') && $request->date != null) {
               		$vehicle->expiration = Carbon::parse($request->date);
               	}

                $vehicle->save();
	    	} else {
	    		$person = Person::where('user_id',$documento->user_id)->first();
                $name = DiskStorage::name();
                $dg = 0;
                if ($request->has('dg')) {
                    $dg = $request->dg;
                }

                try {
                    Image::make($request->file)
                        ->rotate($dg * (-1))
                        ->save(DiskStorage::get('drivers') . ($this->getName($documento->type) == 'photo' ? 'profile' : 'files') . '/' . $name, 25);
                } catch (\Exception $e) {
                    return response()->json($e);
                }

                $person[$this->getName($documento->type)] = 'drivers/'.($this->getName($documento->type) == 'photo' ? 'profile/' : 'files/').$name;
                if ($request->has('date') && $request->date != null && $this->getNameDate($documento->type) != '') {
                    $person[$this->getNameDate($documento->type)] = Carbon::parse($request->date);
                }
		    	$person->save(); 
	    	}	    	

	    	$documento->upload = 1;
	    	$documento->status = 0;
	    	$documento->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function uploadFile(Request $request) {
	    	$documento = PendingDocument::find($request->id);

	    	$person = Person::where('user_id',$documento->user_id)->first();
	    		$name = DiskStorage::name($request->extension);
	    		$request->file('file')->move(DiskStorage::get('drivers').'files',$name);
	    		$person[$this->getName($documento->type)] = 'drivers/files/'.$name;
	    		if ($request->has('date') && $request->date != null && $this->getNameDate($documento->type) != '') {
	    			$person[$this->getNameDate($documento->type)] = Carbon::parse($request->date);
	    		}
	    	$person->save();

	    	$documento->upload = 1;
	    	$documento->status = 0;
	    	$documento->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    private function getName($type) {
	    	$respuesta = "";
		  	switch ($type) {
		  		case 1:
		  			$respuesta = "photo";
		  			break;

		  		case 2:
		  			$respuesta = "license";
		  			break;

		  		case 4:
		  			$respuesta = "permit";
		  			break;

		  		case 5:
		  			$respuesta = "criminal_records";
		  			break;

		  		case 6:
		  			$respuesta = "document_photo";
		  			break;
		  	}
		    return $respuesta;
	    }

	    private function getNameDate($type) {
	    	$respuesta = "";
		  	switch ($type) {
		  		case 2:
		  			$respuesta = "expiration_license";
		  			break;

		  		case 4:
		  			$respuesta = "expiration_permit";
		  			break;

		  		case 5:
		  			$respuesta = "expiration_criminal_records";
		  			break;

		  		case 6:
		  			$respuesta = "expiration_document";
		  			break;
		  	}
		    return $respuesta;
	    }
	}
