<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\Store;

	class StoreController extends Controller {
	 	
	 	public function get(Request $request) {
            $store = Store::orderBy('id','desc')->where('type',$request->nivel)->first();
	 		return response()->json([
	 			'result' => true,
	 			'store' => $store
	 		]);
	 	}   
	}
