<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\NightMode;

	class NightModeController extends Controller {
	    
	    public function get() {
	    	$night = NightMode::orderBy('id','desc')->first();
	    	return response()->json([
	    		'result' => true,
	    		'night' => $night
	    	]);	
	    }
	}
