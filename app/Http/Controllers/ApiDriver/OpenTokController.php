<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\OpenTok;
	use OpenTok\OpenTok as OpenTokSDK;
	use OpenTok\MediaMode;
	use App\User;
	use App\Models\Route;

	class OpenTokController extends Controller {

		private $OT;
	    
	    public function __construct() {
	    	$this->OT = new OpenTokSDK(env('OPENTOK_API_KEY'),env('OPENTOK_SECRET_KEY'));
	    }

	    public function get(Request $request) {
	    	if ($request->mode == 1) {
	    		$ruta = Route::where('user_id',$request->id)->where('status',1)->orderBy('id','desc')->first();
	    	}
	    	else {
	    		$ruta = Route::where('driver_id',$request->id)->where('status',1)->orderBy('id','desc')->first();
	    	}

	    	$opentok = OpenTok::where('route_id',$ruta->id)->first();

	    	if (count($opentok) == 0) {
	    		$session = $this->OT->createSession(['mediaMode' => MediaMode::ROUTED]);
		    	$sessionId = $session->getSessionId();
		    	$token = $this->OT->generateToken($sessionId);

		    	$opentok = new OpenTok;
		    		$opentok->route_id = $ruta->id;
		    		$opentok->sessionId = $sessionId;
		    		$opentok->token = $token;
		    	$opentok->save();
	    	}	    	

	    	$cliente = User::where('id',$ruta->user_id)->with('person')->first();
	    	$conductor = User::where('id',$ruta->driver_id)->with('person')->first();

	    	return response()->json([
	    		'result' => true,
	    		'sessionId' => $opentok->sessionId,
	    		'token' => $opentok->token,
	    		'cliente' => $cliente,
	    		'conductor' => $conductor
	    	]);
	    }
	}
