<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\Chat;
	use App\Models\DeletedChat;
	use Carbon\Carbon;
	use App\Models\Person;
	use App\Models\Message;
	use App\Libraries\Paginate;
	use App\Models\Route;
	use App\Models\ChatUser;
	use Intervention\Image\ImageManagerStatic as Image;

	class ChatController extends Controller {
	    
	    public function get(Request $request) {

	    	$deleted_chats = DeletedChat::where('user_id',$request->id)
	        				->get()->pluck('chat_id');

	        $chats = Chat::whereNotIn('id',$deleted_chats)->orderBy('updated_at','desc')->with(['message' => function($message) {
	                    $message->orderBy('created_at', 'asc');
	                },'chat_user' => function($q) use ($request) {
	                	$q->where('user_id','!=',$request->id)->with(['user' => function($q) {
	                		$q->with(['person']);
	                	}]);
	                }])->whereHas('chat_user',function($q) use ($request) {
	                	$q->where('user_id',$request->id);
	                })->get()->take(250);

	        // for ($i = 0; $i < count($chats); $i++) {

	        // 	$last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

	        // 	$q = DeletedChat::where('chat_id',$chats[$i]->id)
	        // 				->where('user_id',$request->id);

	        // 	if ($last != null) {
	        // 		$q->whereDate('updated_at','>',$last->created_at);
	        // 	}

	        // 	$deleted = $q->orderBy('id','desc')->first();

	        // 	if (count($deleted) > 0) {
	        // 		$chats->splice($i,1);
        	// 		$i--;
	        // 	}
	        // }

	        $chats = Paginate::get($chats,20);

	    	return response()->json([
	    		'result' => true,
	    		'chats' => $chats
	    	]);
	    }

	    public function update(Request $request) {

	    	$deleted_chats = DeletedChat::where('user_id',$request->id)
	        				->get()->pluck('chat_id');

	        $chats = Chat::whereNotIn('id',$deleted_chats)->orderBy('updated_at','desc')->with(['message' => function($message) {
	                    $message->orderBy('created_at', 'asc');
	                },'chat_user' => function($q) use ($request) {
	                	$q->where('user_id','!=',$request->id)->with(['user' => function($q) {
	                		$q->with(['person']);
	                	}]);
	                }])->whereHas('chat_user',function($q) use ($request) {
	                	$q->where('user_id',$request->id);
	                })->whereIn('id',$request->ids)->get();

	        // for ($i = 0; $i < count($chats); $i++) {

	        // 	$last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

	        // 	$q = DeletedChat::where('chat_id',$chats[$i]->id)
	        // 				->where('user_id',$request->id);

	        // 	if ($last != null) {
	        // 		$q->whereDate('updated_at','>',$last->created_at);
	        // 	}

	        // 	$deleted = $q->orderBy('id','desc')->first();

	        // 	if (count($deleted) > 0) {
	        // 		$chats->splice($i,1);
        	// 		$i--;
	        // 	}
	        // }

	    	return response()->json([
	    		'result' => true,
	    		'chats' => $chats
	    	]);
	    }

	    public function delete(Request $request) {
	    	$deleted = new DeletedChat;
	    		$deleted->chat_id = $request->chat_id;
	    		$deleted->user_id = $request->user_id;
	    	$deleted->save();

	    	return response()->json([
	    		'result' => true
	    	]);
	    }

	    public function view(Request $request) {

	    	$person = Person::where('user_id',$request->id)->first();

	    	$deleted_chats = DeletedChat::where('user_id',$request->id)
	        				->get()->pluck('chat_id');

	    	$chat = Chat::with(['route','support_type','message' => function($message) {
	                    $message->orderBy('created_at', 'asc');
	                },'chat_user' => function($q) use ($request) {
	                	$q->where('user_id','!=',$request->id)->with(['user' => function($q) {
	                		$q->with(['person']);
	                	}]);
	                }])->whereHas('chat_user',function($q) use ($request) {
	                	$q->where('user_id',$request->id);
	                })->where('id',$request->chat_id)->first();

	        Message::where('user_id','!=',$request->id)->where('chat_id',$chat->id)->update(['seen' => '1']);

	        $chats = Chat::whereNotIn('id',$deleted_chats)->whereHas('chat_user',function($q) use ($request) {
                        $q->where('user_id',$request->id);
                    })->get();

	        // for ($i = 0; $i < count($chats); $i++) {

	        // 	$last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

	        // 	$q = DeletedChat::where('chat_id',$chats[$i]->id)
	        // 				->where('user_id',$request->id);

	        // 	if ($last != null) {
	        // 		$q->whereDate('updated_at','>',$last->created_at);
	        // 	}

	        // 	$deleted = $q->orderBy('id','desc')->first();

	        // 	if (count($deleted) > 0) {
	        // 		$chats->splice($i,1);
        	// 		$i--;
	        // 	}
	        // }

	        $ids = $chats->pluck('id');

            $badge_chat = Message::whereIn('chat_id',$ids)->where('user_id','!=',$request->id)->where('seen','0')->count();

	    	return response()->json([
	    		'result' => true,
	    		'chat' => $chat,
	    		'person' => $person,
	    		'count' => $badge_chat
	    	]);
	    }

	    public function count(Request $request) {
	    	$deleted_chats = DeletedChat::where('user_id',$request->id)
	        				->get()->pluck('chat_id');

	    	$chats = Chat::whereNotIn('id',$deleted_chats)->whereHas('chat_user',function($q) use ($request) {
	                	$q->where('user_id',$request->id);
	                })->get();

	    	// for ($i = 0; $i < count($chats); $i++) {

	     //    	$last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

	     //    	$q = DeletedChat::where('chat_id',$chats[$i]->id)
	     //    				->where('user_id',$request->id);

	     //    	if ($last != null) {
	     //    		$q->whereDate('updated_at','>',$last->created_at);
	     //    	}

	     //    	$deleted = $q->orderBy('id','desc')->first();

	     //    	if (count($deleted) > 0) {
	     //    		$chats->splice($i,1);
      //   			$i--;
	     //    	}
	     //    }

	        $ids = $chats->pluck('id');

	    	$count = Message::whereIn('chat_id',$ids)->where('user_id','!=',$request->id)->where('seen','0')->count();

	    	return response()->json([
	    		'result' => true,
	    		'count' => $count
	    	]);
	    }

	    public function loadOne(Request $request) {

	    	$chat = Chat::with(['message' => function($message) {
	                    $message->orderBy('created_at', 'asc');
	                },'chat_user' => function($q) use ($request) {
	                	$q->where('user_id','!=',$request->id)->with(['user' => function($q) {
	                		$q->with(['person']);
	                	}]);
	                }])->whereHas('chat_user',function($q) use ($request) {
	                	$q->where('user_id',$request->id);
	                })->where('id',$request->chat_id)->first();
	                
	    	return response()->json([
	    		'result' => true,
	    		'chat' => $chat
	    	]);
	    }

	    public function seen(Request $request) {
	    	
	    	Message::where('id', $request->id)->update(['seen' => '0']);

	    	return response()->json([
	    	    'id'=> $request->id,
	    		'result' => true
	    	]);

	    }

	    public function create(Request $request) {

	    	$route = Route::find($request->id);
	    	
	    	$chat = Chat::where('route_id',$request->id)->where('support','0')->orderBy('id','desc')->first();

	        if ($chat != null) {
	        	return response()->json([
		    		'result' => true,
		    		'chat' => $chat->id
		    	]);
	        }

	        $chat = new Chat;
		        $chat->route_id = $request->id;
		        $chat->support = '0';
	        $chat->save();

	        $user = new ChatUser;
		        $user->chat_id = $chat->id;
		        $user->user_id = $route->user_id;
	        $user->save();

	        $user = new ChatUser;
		        $user->chat_id = $chat->id;
		        $user->user_id = $route->driver_id;
	        $user->save();

        	return response()->json([
	    		'result' => true,
	    		'chat' => $chat->id
	    	]);
	    }

	    public function image(Request $request) {

	    	$name = md5(date('YmdHis')).rand(0,4000).'.png';
			$dg = 0;
	    	if ($request->has('dg')) {
	    		$dg = $request->dg;
	    	}
	    	Image::make($request->file)->rotate($dg * (-1))->save('chats/'.$name,25);

			return response()->json([
	    		'result' => true,
	    		'name' => 'chats/'.$name
	    	]);
	    }
	}
