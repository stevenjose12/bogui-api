<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\Route;
	use Carbon\Carbon;
	use App\Models\WeeklyDebt;
	use App\Models\Discount;

	class GananciaController extends Controller {
	    
	    public function get(Request $request) {
	    	if ($request->has('desde')) {
	    		$desde = Carbon::parse($request->desde)->format('Y-m-d H:i:s');
	    	}
	    	else {
	    		$desde = Carbon::now()->startOfWeek();
	    	}
	    	if ($request->has('hasta')) {
	    		$hasta = Carbon::parse($request->hasta)->format('Y-m-d H:i:s');
	    	}
	    	else {
	    		$hasta = Carbon::now()->endOfWeek();
	    	}
	    	$ganancias = Route::with(['payments'])
	    					->where('created_at','>=',$desde)
	    					->where('created_at','<=',$hasta)
	    					->where('driver_id',$request->id)
	    					->where('status','2')
	    					->get();
	    	$deuda = WeeklyDebt::where('driver_id',$request->id)
	    					->where('created_at','>=',Carbon::parse($desde)->subDays(7)->startOfWeek()->startOfDay())
	    					->where('created_at','<=',Carbon::parse($hasta)->subDays(7)->endOfWeek()->endOfDay())
	    					->orderBy('id','desc')->first();
	    	// METODO PREVIO PARA OBTENER LA DEUDA PENDIENTE
	    	// if ($deuda) {
	    	// 	$deuda->debt_carried = $deuda->debt_total;
	    	// }	    
	    	if ($deuda) {
	    		if ($deuda->debt_total > 0) {
	    			$deuda->debt_carried = 0;
	    		} else {
	    			$deuda->debt_carried = $deuda->debt_total;
	    		}
	    	}	    	
	    	return response()->json([
	    		'result' => true,
	    		'ganancias' => $ganancias,
	    		'deuda' => $deuda,
	    		'descuento' => Discount::orderBy('id','desc')->first()
	    	]);
	    }
	}
