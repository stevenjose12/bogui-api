<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\BankUser;
	use App\Models\AccountType;

	class BankController extends Controller {
	    
	    public function get(Request $request) {
	    	$cuentas = AccountType::all();
	    	$datos = BankUser::where('user_id',$request->id)->with(['bank','account'])->first();
	    	return response()->json([
	    		'result' => true,
	    		'datos' => $datos,
	    		'cuentas' => $cuentas
	    	]);
	    }

	    public function post(Request $request) {
	    	$datos = BankUser::where('user_id',$request->id)->first();
	    		$datos->bank_id = $request->bank;
	    		$datos->type = $request->type;
	    		$datos->number = $request->number;
	    		$datos->identification = $request->rut;
	    	$datos->save();
	    	return response()->json([
	    		'result' => true
	    	]);
	    }

	    public function types() {
	    	$cuentas = AccountType::all();
	    	return response()->json([
	    		'result' => true,
	    		'cuentas' => $cuentas
	    	]);
	    }
	}
