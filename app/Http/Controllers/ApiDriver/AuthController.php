<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\PasswordApp as Password;
	use App\User;
	use Hash;
	use Validator;
	use Mail;
	use App\Models\Country;
	use App\Models\Bank;
	use App\Models\Person;
	use App\Models\Vehicle;
	use App\Models\VehiclePhoto;
	use App\Models\BankUser;
	use Intervention\Image\ImageManagerStatic as Image;
	use App\Libraries\DiskStorage;
	use App\Models\Validation;
	use App\Models\AccountType;
	use Carbon\Carbon;

	class AuthController extends Controller {
	    
	    public function login(Request $request) {
	    	$user = User::where('email',$request->email)->where('level',2)->first();
	    	if (count($user) == 0) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => 'Correo electrónico o contraseña incorrectos'
	    		]);
	    	}
	    	if ($user->level != '2') {
	    		return response()->json([
	    			'result' => false,
	    			'error' => 'Lo sentimos, su nivel de usuario no es suficiente para ingresar a la aplicación'
	    		]);
	    	}
	    	if (Hash::check($request->password,$user->password)) {
	    		$user = User::with([
    				'vehicle.vehicle_categories.vehicle_type',
    				'vehicles.vehicle_categories.vehicle_type',
	    		  	'person', 
	    		  	'vehicle.photos', 
	    		  	'vehicles.photos' 
    		  	])
                    ->where('id',$user->id)
                    ->first();
	    		return response()->json([
	    			'result' => true,
	    			'user' => $user
	    		]);
	    	}
	    	else {
	    		return response()->json([
	    			'result' => false,
	    			'error' => 'Correo electrónico o contraseña incorrectos'
	    		]);
	    	}
	    }

	    public function countries() {
	    	return response()->json([
	    		'result' => true,
	    		'paises' => Country::orderBy('name','asc')->get()
	    	]);
	    }

	    public function banks() {
	    	return response()->json([
	    		'result' => true,
	    		'banks' => Bank::orderBy('name','asc')->get()
	    	]);
	    }

	    public function validarRegister(Request $request) {
	    	$reglas = [
	    		'nombre' => 'required',
	    		'apellido' => 'required',
	    		// 'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
	    		'email' => 'required|email',
	    		'password' => 'required|confirmed|min:5',
	    		'tipo' => 'required',
	    		'code' => 'required',
	    		'rut' =>'required',
	    		'telefono' => 'required|numeric'
	    	];
	    	$mensajes = [
	    		'required' => 'El campo :attribute es requerido',
	    		'email' => 'El correo eletrónico no es válido',
	    		'confirmed' => 'Las contraseñas no coinciden',
	    		'numeric' => 'El campo :attribute es requerido',
	    		'email.unique' => 'El correo electrónico ya se encuentra registrado',
	    		'telefono.unique' => 'El número de teléfono ya se encuentra registrado',
	    		'rut.unique' => 'El RUT ya se encuentra registrado'
	    	];
	    	$atributos = [
	    		'nombre' => 'Nombre',
	    		'apellido' => 'Apellido',
	    		'email' => 'Correo Electrónico',
	    		'password' => 'Contraseña',
	    		'tipo' => 'Tipo de Documento',
	    		'code' => 'Código',
	    		'rut' => 'RUT',
	    		'telefono' => 'Teléfono'
	    	];
	    	$validacion = Validator::make($request->all(),$reglas,$mensajes);
	    	if ($validacion->fails()) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => $validacion->messages()->first()
	    		]);
	    	}
	    	else {

	    		$_user = User::where('email',$request->email)->where('level',2)->count();

	    		if ($_user > 0) {
	    			return response()->json([
	    				'result' => false,
	    				'error' => 'El correo electrónico ya se encuentra registrado'
	    			]);
	    		}

	    		$person = Person::where('document',$request->rut)->whereHas('person',function($q) {
	    			$q->where('level',2);
	    		})->count();

	    		if ($person > 0) {
	    			return response()->json([
		    			'result' => false,
		    			'error' => "El RUT ya se encuentra registrado"
		    		]);
	    		}

	    		if ($request->code == '56') {
	    			if (strlen($request->telefono) != 9) {
		    			return response()->json([
			    			'result' => false,
			    			'error' => "Para Chile, el número de teléfono debe poseer 9 dígitos. Por favor verifique el número que ha ingresado."
			    		]);
	    			}
	    		}

	    		// $person = Person::where(function($q) use ($request) {
	    		// 	$q->where('phone','+'.$request->code.' '.$request->telefono)
	    		// 		->orWhere('phone','+'.$request->code.$request->telefono);
	    		// })->count();

	    		// if ($person > 0) {
	    		// 	return response()->json([
		    	// 		'result' => false,
		    	// 		'error' => "El número de teléfono ya se encuentra registrado"
		    	// 	]);
	    		// }

	    		$person = Person::where(function($q) use ($request) {
	    			$q->where('phone',$request->code.$request->telefono)
	    				->orWhere('phone',$request->telefono);
	    		})->whereHas('person',function($q) {
	    			$q->where('level',2);
	    		})->count();

	    		if ($person > 0) {
	    			return response()->json([
		    			'result' => false,
		    			'error' => "El número de teléfono ya se encuentra registrado"
		    		]);
	    		}

	    		$cuentas = AccountType::all();

	    		return response()->json([
	    			'result' => true,
	    			'cuentas' => $cuentas
	    		]);
	    	}
	    }

	    public function register(Request $request) {
	    	// $reglas = [
	    	// 	'email' => 'unique:users,email,NULL,id,deleted_at,NULL'
	    	// ];
	    	// $mensajes = [
	    	// 	'unique' => 'El correo electrónico ya se encuentra registrado',
	    	// ];
	    	// $atributos = [
	    	// 	'email' => 'Correo Electrónico'
	    	// ];
	    	// $validacion = Validator::make($request->all(),$reglas,$mensajes);
	    	// if ($validacion->fails()) {
	    	// 	return response()->json([
	    	// 		'result' => false,
	    	// 		'error' => $validacion->messages()->first()
	    	// 	]);
	    	// }
	    	// else {

	    		$_user = User::where('email',$request->email)->where('level',2)->count();

	    		if ($_user > 0) {
	    			return response()->json([
	    				'result' => false,
	    				'error' => 'El correo electrónico ya se encuentra registrado'
	    			]);
	    		}

	    		$request->phone = str_replace(' ', '', $request->phone);
	    		$request->phone = substr($request->phone, 1);

	    		// $person = Person::where(function($q) use ($request) {
	    		// 	$q->where('phone','+'.$request->code.' '.$request->telefono)
	    		// 		->orWhere('phone','+'.$request->code.$request->telefono);
	    		// })->count();

	    		$person = Person::where(function($q) use ($request) {
	    			$q->where('phone',$request->phone)
	    				->orWhere('phone',$request->telefono);
	    		})->whereHas('person',function($q) {
	    			$q->where('level',2);
	    		})->count();

	    		// if ($request->code == '56') {
	    		// 	if (strlen($request->telefono) != 9) {
		    	// 		return response()->json([
			    // 			'result' => false,
			    // 			'error' => "Para Chile, el número de teléfono debe poseer 9 dígitos. Por favor verifique el número que ha ingresado."
			    // 		]);
	    		// 	}
	    		// }
	    		
	    		if ($person > 0) {
	    			return response()->json([
		    			'result' => false,
		    			'error' => "El número de teléfono ya se encuentra registrado"
		    		]);
	    		}

	    		$user = new User;
	    			$user->level = '2';
	    			$user->email = $request->email;
	    			$user->password = $request->password;
	    			$user->latitud = $request->latitud;
	    			$user->longitud = $request->longitud;
	    			$user->status = '0';
	    		$user->save();

	    		$person = new Person;
	    			$person->name = $request->name;
	    			$person->lastname = $request->lastname;
	    			$person->phone = $request->phone;
	    			// $person->phone = $request->code.$request->telefono;
	    			$person->document = $request->document;
	    			$person->document_type = $request->document_type;
	    			$person->user_id = $user->id;
	    			// $person->expiration_document = Carbon::parse($request->expiration_document);
	    			// $person->expiration_criminal_records = Carbon::parse($request->expiration_criminal_records);
	    			$person->expiration_permit = Carbon::parse($request->expiration_permit);
	    			// $person->expiration_license = Carbon::parse($request->expiration_license);
	    		$person->save();

	    		$vehicle = new Vehicle;
	    			$_vehicle = json_decode($request->vehicle);
	    			$vehicle->brand = $_vehicle->brand;
	    			$vehicle->model = $_vehicle->model;
	    			$vehicle->plate = $_vehicle->plate;
	    			$vehicle->expiration = Carbon::parse($request->expiration_permit);
	    			$vehicle->year = $_vehicle->year;
	    			$vehicle->user_id = $user->id;
	    		$vehicle->save();

	    		$vehicle_photo = new VehiclePhoto;
	    			$vehicle_photo->vehicle_id = $vehicle->id;
	    			$name = DiskStorage::name();
	    			$dg = 0;
			    	if ($request->has('dg')) {
			    		$dg = $request->dg;
			    	}
			    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('vehicles').$name,25);
			    	$vehicle_photo->file = 'vehicles/'.$name;
	    		$vehicle_photo->save();

	    		$bank = new BankUser;
	    			$_bank = json_decode($request->bank);
	    			$bank->user_id = $user->id;
	    			$bank->bank_id = $_bank->bank_id;
	    			$bank->type = $_bank->type;
	    			$bank->number = $_bank->number;
	    			$bank->identification = $_bank->identification;
	    		$bank->save();

	    		$validation = new Validation;
		    		$validation->user_id = $user->id;
		    		$validation->code = $request->codigo;
		    	$validation->save();

	    		$codigo = [
	    			'codigo' => $request->codigo
	    		];

	    		if ($request->has('names')) {
		    		foreach(json_decode($request->names) as $item) {
		    			$person[$item->file] = 'drivers/'.($item->file == 'photo' ? 'profile/' : 'files/').$item->name;
		    		}

		    		$person->save();	
	    		} 

	    		$vehicle->permit = $person->permit;
	    		$vehicle->save();

	    		try {
	    			\Mail::send('emails.validate-driver', ['codigo' => $request->codigo], function ($m) use ($user) {
			            $m->to($user->email,$user->person->name.' '.$user->person->lastname)
			              ->subject('Verificación de Cuenta | Bogui Conductor');
			        });	
	    		}   
	    		catch(\Exception $e) {

	    		}		

	    		return response()->json([
	    			'result' => true,
	    			'id' => $person->id,
	    			'user_id' => $user->id,
	    			'vehicle_id' => $vehicle->id
	    		]);
	    	// }
	    }

	    public function uploadPhotoOnly(Request $request) {
		$name = DiskStorage::name();
	    	$dg = 0;
	    	if ($request->has('dg')) {
	    		$dg = $request->dg;
	    	}
	    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('drivers').($request->name == 'photo' ? 'profile' : 'files').'/'.$name,25);

	    	return response()->json([
    			'result' => true,
    			'name' => $name
    		]);
	    }

	    public function upload(Request $request) {
	    	$person = Person::find($request->id);
		    	$name = DiskStorage::name();
		    	$dg = 0;
		    	if ($request->has('dg')) {
		    		$dg = $request->dg;
		    	}
		    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('drivers').($request->name == 'photo' ? 'profile' : 'files').'/'.$name,25);
	    		$person[$request->name] = 'drivers/'.($request->name == 'photo' ? 'profile/' : 'files/').$name;
	    	$person->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function uploadPermit(Request $request) {
	    	$vehicle = Vehicle::find($request->vehicle_id);
	    	$name = DiskStorage::name();
	    	$dg = 0;
	    	if ($request->has('dg')) {
	    		$dg = $request->dg;
	    	}
	    	Image::make($request->file)->rotate($dg * (-1))->save(DiskStorage::get('drivers'). 'files/'.$name,25);
    		$vehicle->permit = 'drivers/files/' . $name;
	    	$vehicle->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function uploadFile(Request $request) {
	    	$person = Person::find($request->id);
	    		$name = DiskStorage::name($request->extension);
	    		$request->file('file')->move(DiskStorage::get('drivers').'files',$name);
	    		$person[$request->name] = 'drivers/files/'.$name;
	    	$person->save();

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function recuperar(Request $request) {
	    	Password::where('status','1')->where('email',$request->email)->where('level',2)->update(['status' => '0']);

	    	$user = User::where('email',$request->email)->where('level',2)->first();

	    	if (count($user) == 0) {
	    		return response()->json([
		    		'result' => false,
		    		'error' => "El correo electrónico no se encuentra registrado"
		    	]);
	    	}

	    	if ($user->level != '2') {
	    		return response()->json([
	    			'result' => false,
	    			'error' => 'Lo sentimos, su nivel de usuario debe ser de conductor'
	    		]);
	    	}

	    	$password = new Password;
	    		$password->email = $request->email;
	    		$password->code = $request->codigo;
	    		$password->level = 2;
	    	$password->save();

	    	$validation = new Validation;
	    		$validation->user_id = $user->id;
	    		$validation->code = $request->codigo;
	    	$validation->save();

	    	Mail::send('emails.reset', ['codigo' => $request->codigo], function ($m) use ($user) {
	            $m->to($user->email,$user->person->name.' '.$user->person->lastname)
	              ->subject('Recuperación de Contraseña | Bogui Conductor');
	        });

	    	return response()->json([
	    		'result' => true
	    	]);
	    }

	    public function validar(Request $request) {
	    	$password = Password::where('code',$request->codigo)
	    		->where('email',$request->email)
	    		->where('status','1')
	    		->where('level',2)
	    		->count();
	    	if ($password == 0) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => "No se ha encontrado el código"
	    		]);
	    	}

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function reset(Request $request) {
	    	$password = Password::where('code',$request->codigo)
	    		->where('email',$request->email)
	    		->where('status','1')
	    		->where('level',2)
	    		->count();
	    	if ($password == 0) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => "No se procesar su solicitud"
	    		]);
	    	}

	    	$user = User::where('email',$request->email)->where('level',2)->first();
	    		$user->password = $request->password;
	    	$user->save();

	    	Password::where('status','1')->where('email',$request->email)->update(['status' => '0']);

	    	return response()->json([
    			'result' => true
    		]);
	    }

	    public function checkUser(Request $request) {
//	    	$user = User::with(['vehicle.vehicle_categories'])
//                ->find($request->id);

            $user = User::with([
            	// 'vehicle.vehicle_categories.vehicle_type',
            	'vehicles.vehicle_categories.vehicle_type',
            	 'person',
            	 // 'vehicle.photos',
            	 'vehicles.photos'
            	])
                ->where('id',$request->id)
                ->first();

            $vehicle = Vehicle::where('user_id',$user->id)->with(['vehicle_categories.vehicle_type','photos'])->where('status',1)->first();

            if (!$vehicle) {
            	$vehicle = Vehicle::where('user_id',$user->id)->with(['vehicle_categories.vehicle_type','photos'])->first();
            }

            $user->vehicle = $vehicle;
            
            if ($request->has('status')) {
            	$user->status_driver = $request->status;
            	$user->save();
            }
            // else if ($user->last_status != null && $user->last_status != $user->status_driver) {
            // 	$user->status_driver = $user->last_status;
            // 	$user->save();
            // }

	    	return response()->json([
    			'result' => true,
    			'user' => $user,
    			'vehicle' => $vehicle
    		]);
	    }

	    public function reenviar(Request $request) {
                // Eliminar los códigos anteriores.
                Validation::where('user_id', $request->id)->delete();

	    	$user = User::where('id',$request->id)->with(['person'])->orderBy('id','desc')->first();

	    	$validation = new Validation;
	    		$validation->user_id = $request->id;
	    		$validation->code = $request->codigo;
	    	$validation->save();

	    	$data = [
	    		'codigo' => $request->codigo
	    	];

	    	try {
    			\Mail::send('emails.validate-driver', ['codigo' => $request->codigo], function ($m) use ($user) {
		            $m->to($user->email,$user->person->name.' '.$user->person->lastname)
		              ->subject('Verificación de Cuenta | Bogui Conductor');
		        });	
    		}   
    		catch(\Exception $e) {

    		}		

	    	// Mail::send('emails.validate-driver', ['codigo' => $request->codigo], function ($m) use ($user) {
	     //        $m->to($user->email,$user->person->name.' '.$user->person->lastname)
	     //          ->subject('Verificación de Cuenta | Bogui Conductor');
	     //    });

	        return response()->json([
    			'result' => true,
    			'user' => $user
    		]);
	    }

	    public function validarCodigo(Request $request) {

	    	$validation = Validation::orderBy('id','desc')->where('user_id',$request->id)->where('code',$request->codigo)->first();
	    	if ($validation == null) {
	    		return response()->json([
	    			'result' => false,
	    			'error' => 'No se pudo encontrar el código'
	    		]);
	    	}

	    	$user = User::find($request->id);
	    	$user->validate = 1;
	    	$user->save();

	    	return response()->json([
    			'result' => true,
    			'user' => $user
    		]);
	    }
	}

