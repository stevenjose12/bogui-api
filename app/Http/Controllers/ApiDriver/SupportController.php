<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\SupportMotive;
	use App\Models\ChatUser;
	use App\Models\Chat;
	use App\User;

	class SupportController extends Controller {
	    
	    public function get(Request $request) {
	    	$motivos = SupportMotive::where('type',$request->type)->where(function($q) {
	    		$q->where('type_user','1')
	    			->orWhere('type_user','3');
	    	})->get();
	    	return response()->json([
	    		'result' => true,
	    		'motivos' => $motivos
	    	]);
	    }

	    public function create(Request $request) {
	    	$moderador = User::select('users.id',\DB::raw('COUNT(chat_user.id) AS count'))
	    				  ->leftJoin('chat_user','chat_user.user_id','=','users.id')
	    				  ->whereIn('level',[1,4])
	    				  ->groupBy('users.id')
	    				  ->orderBy('count')
	    				  ->first();


	    	$chat = new Chat;
		        $chat->support = "1";
		        $chat->motive_id = $request->type;
		        $chat->route_id = $request->route_id;
	        $chat->save();

	        $user = new ChatUser;
		        $user->chat_id = $chat->id;
		        $user->user_id = $request->id;
	        $user->save();

	        $user = new ChatUser;
		        $user->chat_id = $chat->id;
		        $user->user_id = $moderador->id;
	        $user->save();	        

	        return response()->json([
	    		'result' => true,
	    		'chat' => $chat->id
	    	]);
	    }
	}
