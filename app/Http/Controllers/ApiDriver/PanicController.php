<?php

	namespace App\Http\Controllers\ApiDriver;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PanicOption;
use App\Models\Panic;
use App\Models\PanicRadius;
use App\Models\PanicDetail;
use DB;
use App\User;

class PanicController extends Controller {

    public function getDrivers(Request $request) {
        $panic = Panic::where('user_id', $request->id)->where('status', 1)->first();
        $new_panic = false;
        if (!$panic) {
            $search = PanicRadius::orderBy('id', 'desc')->first();

            if (!$search) {
                $search = new PanicRadius;
                $search->distance = 10; // Km
                $search->save();
            }

            $drivers = User::having('distance', '<', $search->distance)
                ->where('level', 2)
                ->where('status', 1)
                ->where('id', '!=', $request->id)
                ->select(DB::raw("*,
                    (3959 * ACOS(COS(RADIANS($request->latitude))
                    * COS(RADIANS(latitud))
                    * COS(RADIANS($request->longitude) - RADIANS(longitud))
                    + SIN(RADIANS($request->latitude))
                    * SIN(RADIANS(latitud)))) AS distance"))
                ->orderBy('distance', 'asc')
                ->get();

            $new_panic = true;

            $panic = new Panic;
            $panic->user_id = $request->id;
            $panic->latitude = $request->latitude;
            $panic->longitude = $request->longitude;
            $panic->save();
        }

        return response()->json([
            'result' => true,
            'drivers' => $new_panic ? $drivers->pluck('id') : [],
            'panic_id' => $panic->id,
            'new_panic' => $new_panic
        ]);
    }

    public function getOptions(Request $request) {
        $options = PanicOption::all();

        return response()->json([
            'result' => true,
            'options' => $options
        ]);
    }

    public function driverResponse(Request $request) {
        $detail = new PanicDetail;
        $detail->panic_id = $request->panic_id;
        $detail->user_id = $request->id;
        $detail->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function setOption(Request $request) {
        $panic = PanicDetail::where('user_id',$request->user_id)
            ->where('panic_id', $request->panic_id)
            ->first();

        $panic->response_id = $request->response_id;
        $panic->comment = $request->comment;
        $panic->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function finish(Request $request) {

        Panic::where('id',$request->id)->where('status',1)->update([ 'status' => 0 ]);

        return response()->json([
            'result' => true
        ]);
    }

    public function get(Request $request) {

        $panic = Panic::where('user_id',$request->id)->where('status',1)->first();

        if ($panic) {
            return response()->json([
                'result' => true
            ]);
        }

        return response()->json([
            'result' => false
        ]);
    }

}
