<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\Notification;
	use App\Models\PendingDocument;

	class NotificationController extends Controller {
	    
	    public function get(Request $request) {
	    	$notificaciones = Notification::orderBy('id','desc')->where('user_id',$request->id)->with(['route' => function($q) {
	    		$q->with(['payments','to_destinies','to_user' => function ($q) {
	                $q->with(['person']);
	            }]);
	    	}])->paginate(20);

	    	Notification::where('user_id',$request->id)->where('viewed','0')->update(['viewed' => '1']);

	        $documentos = PendingDocument::where(function($q) use ($request) {
                $q->where('upload',0)->where('user_id',$request->id)->whereIn('status',[0,2]);
            })->orWhere(function($q) use ($request) {
                $q->where('upload',1)->where('user_id',$request->id)->where('status',2);
            })->get();

            $documentos_pendientes = PendingDocument::where('upload',1)->where('user_id',$request->id)->where('status',0)->get();

	    	return response()->json([
	    		'result' => true,
	    		'notificaciones' => $notificaciones,
	    		'documentos' => $documentos,
	    		'documentos_pendientes' => $documentos_pendientes
	    	]);
	    }
	}
