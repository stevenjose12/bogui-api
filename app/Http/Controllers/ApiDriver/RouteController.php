<?php

    namespace App\Http\Controllers\ApiDriver;

    use App\Models\Promotion;
    use App\Models\PromotionUser;
    use App\Models\VehicleType;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Models\Route;
    use App\User;
    use App\Models\Rating;
    use Carbon\Carbon;
    use DB;
    use App\Models\Blacklist;
    use App\Models\Chat;
    use App\Models\Message;
    use App\Models\Notification;
    use App\Models\DeletedChat;
    use App\Models\Destiny;
    use App\Models\Payment;
    use App\Models\Search;
    use App\Models\Tariff;
    use App\Models\Airport;
    use App\Models\PendingDocument;
    use App\Models\Commission;
    use App\Models\VehicleCategory;

    class RouteController extends Controller
    {
        public function get(Request $request)
        {
            $ruta = Route::orderBy('id', 'desc')->with(['payments','to_destinies','to_user' => function ($q) {
                $q->with(['person']);
            }])->where('driver_id', $request->driver_id)->where('status', '1')->first();

            $promedio = 0;
            if (count($ruta) > 0) {
                $promedio = User::find($ruta->user_id)->average();
            }

            $historial = Route::orderBy('id', 'desc')->with(['payments','to_destinies','to_user' => function ($q) {
                $q->with(['person']);
            }])->where('driver_id', $request->driver_id)->where('status', '!=', '1')->paginate(10);

            $badge_notificaciones = Notification::where('user_id',$request->driver_id)->where('viewed','0')->count();

            $deleted_chats = DeletedChat::where('user_id',$request->id)
                            ->get()->pluck('chat_id');

            $chats = Chat::whereNotIn('id',$deleted_chats)->whereHas('chat_user',function($q) use ($request) {
                        $q->where('user_id',$request->driver_id);
                    })->get();

            // for ($i = 0; $i < count($chats); $i++) {

            //     $last = count($chats[$i]->message) > 0 ? $chats[$i]->message[count($chats[$i]->message) - 1] : null;

            //     $q = DeletedChat::where('chat_id',$chats[$i]->id)
            //                 ->where('user_id',$request->driver_id);

            //     if ($last != null) {
            //         $q->whereDate('updated_at','>',$last->created_at);
            //     }

            //     $deleted = $q->orderBy('id','desc')->first();

            //     if (count($deleted) > 0) {
            //         $chats->splice($i,1);
            //         $i--;
            //     }
            // }

            $ids = $chats->pluck('id');

            $badge_chat = Message::whereIn('chat_id',$ids)->where('user_id','!=',$request->driver_id)->where('seen','0')->count();

            $documentos = PendingDocument::where(function($q) use ($request) {
                $q->where('upload',0)->where('user_id',$request->driver_id)->whereIn('status',[0,2]);
            })->orWhere(function($q) use ($request) {
                $q->where('upload',1)->where('user_id',$request->driver_id)->where('status',2);
            })->get();

            $documentos_pendientes = PendingDocument::where('upload',1)->where('user_id',$request->driver_id)->where('status',0)->get();

            $user = User::find($request->driver_id);

            if (count($documentos_pendientes) > 0 || count($documentos) > 0) {
                $user->status_driver = '0';
                $user->last_status = 0;
                $user->save();
            }

            return response()->json([
                'result' => true,
                'ruta' => $ruta,
                'promedio' => $promedio,
                'historial' => $historial,
                'fecha_actual' => Carbon::now()->format('Y-m-d H:i:s'),
                'badge_chat' => $badge_chat,
                'badge_notificaciones' => $badge_notificaciones,
                'documentos' => $documentos,
                'documentos_pendientes' => $documentos_pendientes,
                'status' => $user->status_driver,
                'mostrar' => count($documentos_pendientes) == 0 && count($documentos) == 0
            ]);
        }

        public function getFactura(Request $request)
        {
            $ruta = Route::orderBy('id', 'desc')->with(['payments','to_destinies','to_user' => function ($q) {
                $q->with(['person']);
            }])->where('id', $request->id)->first();

            return response()->json([
                'result' => true,
                'ruta' => $ruta
            ]);
        }

        public function aceptar(Request $request)
        {
            $route = Route::find($request->id);
            if ($route->driver_id != null) {
                return response()->json([
                        'result' => false,
                        'error' => 'No se pudo procesar la solicitud'
                    ]);
            }
            $route->driver_id = $request->driver_id;
            // $route->date_driver = Carbon::now()->format('YYYY-MM-DD HH:mm');
            $route->save();
            return response()->json([
                'result' => true
            ]);
        }

        public function cancelar(Request $request)
        {
            $route = Route::find($request->id);
            if ($route->paid == 1) {
                return response()->json([
                    'result' => true,
                    'error' => 'Lo sentimos, el pasajero ya ha pagado el viaje'
                ]);
            }
            $route->canceled_by = $request->driver_id;
            $route->status = '3';
            $route->canceled_reason = $request->motivo;
            $route->save();
            return response()->json([
                'result' => true
            ]);
        }

        public function testEmailFinish(Request $r) {

            $route = Route::find($r->id);
            $route->status = '2';
            $route->save();

            $route->metodo = $route->payments->filter(function($q) { return $q->method == 1 || $q->method == 2; })->first()->method;
            $route->has_parada = $route->to_destinies->filter(function($q) {
                    return $q->type == 1;
                })->count() > 0;
            if ($route->has_parada) {
                $route->parada_descripcion = $route->to_destinies->filter(function($q) {
                    return $q->type == 1;
                })->first()->description;
            }

            $route->destino_descripcion = $route->to_destinies->filter(function($q) {
                return $q->type == 2;
            })->first()->description;

//            $route->to_user =

//            return response()->json([
//                'success' => true,
//                'route' => $route->to_user->email
//            ]);

            try {
                \Mail::send('emails.viaje-finalizado', ['route' => $route], function ($m) use ($route) {
                    $m->to($route->to_user->email, $route->to_user->person->name.' '.$route->to_user->person->lastname)
                        ->subject('Viaje Finalizado | Bogui Conductor');
                });
                return response()->json([
                    'success' => true,
                    'msg' => 'correo enviado'
                ]);
            } catch(\Exception $e) {
                return response()->json([
                    'success' => false,
                    "error" => $e
                ]);
            }

        }

        public function finalizar(Request $request) {

            $route = Route::find($request->id);
            $route->status = '2';
            $route->save();

            $route->metodo = $route->payments->filter(function($q) { return $q->method == 1 || $q->method == 2; })->first()->method;
            $route->has_parada = $route->to_destinies->filter(function($q) {
                return $q->type == 1;
            })->count() > 0;
            if ($route->has_parada) {
                $route->parada_descripcion = $route->to_destinies->filter(function($q) {
                    return $q->type == 1;
                })->first()->description; 
            }            
            $route->destino_descripcion = $route->to_destinies->filter( function($q) {
                return $q->type == 2;
            })->first()->description;

            $vehicleActive = $route->driver->vehicles->filter( function($q) {
                return $q->status == 1;
            } )->first();

            $vehicleType = VehicleCategory::where('vehicle_id', $vehicleActive->id)->first();

            $route->vehicleCategoryName = $vehicleType->vehicle_type->name;

//            return response()->json([
//                'route' => $route
//            ]);

            try {
                \Mail::send('emails.viaje-finalizado', ['route' => $route], function ($m) use ($route) {
                    $m->to($route->to_user->email, $route->to_user->person->name.' '.$route->to_user->person->lastname)
                      ->subject('Viaje Finalizado | Bogui Conductor');
                });

                return response()->json([
                    'result' => true,
                    "msg" => 'correo enviado'
                ]);

            } catch(\Exception $e) {
                return response()->json([
                    'result' => false,
                    "error" => $e
                ]);
            }
            
//            return response()->json([
//                'result' => true
//            ]);
        }

        public function finalizarTest(Request $request) {

            $route = Route::find($request->id);
            $route->status = '2';
            $route->save();

            $route->metodo = $route->payments->filter(function($q) { return $q->method == 1 || $q->method == 2; })->first()->method;
            $route->has_parada = $route->to_destinies->filter( function($q) {
                    return $q->type == 1;
                })->count() > 0;

            if ($route->has_parada) {
                $route->parada_descripcion = $route->to_destinies->filter(function($q) {
                    return $q->type == 1;
                })->first()->description;
            }

            $route->destino_descripcion = $route->to_destinies->filter(function($q) {
                return $q->type == 2;
            })->first()->description;

            try {
                \Mail::send('emails.viaje-finalizado', ['route' => $route], function ($m) use ($route) {
                    $m->to($route->to_user->email,$route->to_user->person->name.' '.$route->to_user->person->lastname)
                        ->subject('Viaje Finalizado | Bogui Conductor');
                });
            } catch(\Exception $e) {
                return response()->json([
                    'result' => true,
                    "error" => $e
                ]);
            }

            return response()->json(['result' => true, 'route' => $route]);

        }

        public function status(Request $request)
        {
            $route = Route::find($request->id);
            $route->status_route = $request->status_route;
            $route->save();

            $ruta = Route::with(['to_destinies','to_user' => function ($q) {
                $q->with(['person']);
            }])->where('id', $route->id)->first();

            $promedio = 0;
            if (count($ruta) > 0) {
                $promedio = User::find($ruta->user_id)->average();
            }

            return response()->json([
                'result' => true,
                'ruta' => $ruta
            ]);
        }

        public function calificar(Request $request)
        {
            $rating = new Rating;
            $rating->reviewed_id = $request->reviewed_id;
            $rating->reviewer_id = $request->reviewer_id;
            $rating->route_id = $request->route;
            $rating->points = $request->points;
            $rating->save();
            return response()->json([
                'result' => true
            ]);
        }

        public function calificarBogui(Request $request)
        {
            $rating = new Rating;
            $rating->bogui = 1;
            $rating->reviewer_id = $request->reviewer_id;
            $rating->route_id = $request->route;
            $rating->points = $request->points;
            $rating->save();
            return response()->json([
                'result' => true
            ]);
        }

        public function tiempoAdicional(Request $request)
        {
            $ruta = Route::find($request->id);
            $ruta->time_stop = $request->time;
            $ruta->save();

            $tarifa = Tariff::find(5);

            if ($tarifa) {
                $tarifa = $tarifa->base_tariff;
            } else {
                $tarifa = 0;
            }

            $tiempo = intval($request->time / 60);
            $costo_stop = $tiempo * $tarifa;

            $ruta->cost = $ruta->cost+$costo_stop;
            $ruta->save();

            $payment = Payment::where('route_id',$request->id)->whereIn('method',[1,2])->first();
            $descuento = Payment::where('route_id',$request->id)->where('method',3)->first();
            $promocion = null;
            if ($descuento!=null) {
                $promocion = Promotion::where("id",$descuento->promotion_id)->first();
            }

             $costo = $ruta->cost;
//             return response()->json(["costo"=>$costo]);

//            if ($descuento != null) {
//                $costo = $costo - $descuento->total;
//            }
//
//            if ($costo < 0) {
//                $costo = 0;
//            }

            //<editor-fold desc="Description">

            //<editor-fold desc="PAGAR CON CODIGO">
            $total_code = 0;
            if ($promocion!=null) {
                $total_code = ($promocion->amount > $costo)
                    ? $costo
                    : $promocion->amount;
            }
//            return response()->json([
//                "costo"=>$costo,
//                "total_code"=>$total_code,
//                "descuento_total"=>$descuento->total,
//                "promotion_amount"=>$promocion->amount
//            ]);
            //</editor-fold>

            $commission = Commission::first();

            $commission_send = $commission->amount/100;

            //<editor-fold desc="COMICION POR CODIGO">
            $p_code = $total_code*$commission_send;
            if ($p_code%50!=0) {
                $p_code = floor($p_code/50)*50;
            }
            //</editor-fold>

            //<editor-fold desc="COMICION TOTAL DE LA RUTA">
            $p_route = $costo*$commission_send;
            if ($p_route%50!=0) {
                $p_route = floor($p_route/50)*50;
            }
            //</editor-fold>

            $p_user = $p_route-$p_code;

            $pago_usuario = ($total_code > $costo)
                ? 0
                : $costo-$total_code;

            $payment->total = $pago_usuario;
            $payment->commission = $p_user;
            $payment->stop = $costo_stop;
            $payment->save();

            if ($descuento!=null) {
                $descuento->total = $total_code;
                $descuento->commission = $p_code;
                $descuento->save();
            }
            //</editor-fold>

//            $payment->total = $costo;
//            $payment->commission = intval((intval(($costo * 0.15)) / 50)) * 50;
//            $payment->stop = $costo_stop;
//            $payment->save();

            return response()->json([
                'result' => true,
                'ruta' => $ruta
            ]);

        }

        public function getDriverCercanoBlackList($lat, $lng, $vehicle_type, $route_id)
        {
            $blacklist = Blacklist::where('route_id', $route_id)->get()->pluck('driver_id');

            //
            switch ($vehicle_type) {
                case '1': // SEDAN
                    $siguiente = '2'; // SEDAN
                    break;
                case '2': // SUV
                    $siguiente = '3'; // SEDAN
                    break;
                case '3': // VAN
                    $siguiente = '3'; // SUV
                    break;
                default:
                    $siguiente = $vehicle_type;
            }
//            if($vehicle_type == '1') {
//                $siguiente = '1';
//            }elseif ($vehicle_type == '2') {
//                $siguiente = '1';
//            }else {
//                $siguiente = '2';
//            }

            $drivers = User::select('users.id', "latitud as lat", "longitud as lng")
                ->join('vehicles','vehicles.user_id','=','users.id')
                ->join('vehicle_category','vehicles.id','=','vehicle_category.vehicle_id')
                ->whereNotNull('users.latitud')
                ->whereNotNull('users.longitud')
                ->whereNotIn('users.id',$blacklist)
                ->where('users.status_driver', "1")
                ->where("users.status", "1")
                ->where("users.validate", "1")
                ->where('vehicles.status',1)
                ->where('users.alive', "1")
                ->whereIn('vehicle_category.vehicle_type_id', [$vehicle_type, $siguiente])
                ->groupBy('users.id')
                ->orderBy(DB::raw("(acos(sin(radians(".$lat.")) * sin(radians(users.latitud)) + 
                cos(radians(".$lat.")) * cos(radians(users.latitud)) * 
                cos(radians(".$lng.") - radians(users.longitud))) * 6371)"),'ASC')
                ->get();

            $km = Search::find(1);

            foreach ($drivers as $driver) {
               $distancia = $this->getDistance($lat, $lng, $driver->lat, $driver->lng);
               if (!$this->isDriverOcuped($driver->id) && $distancia <= $km->radius) {
                   return $driver->id;
               }
            }

            return null;
        }

        private function getDistance($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
            // Cálculo de la distancia en grados
            $degrees = rad2deg(acos(min(max((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long))),-1.0),1.0)));
            // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)
            switch($unit) {
                case 'km':
                    $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
                    break;
                case 'mi':
                    $distance = $degrees * 69.05482; // 1 grado = 69.05482 millas, basándose en el diametro promedio de la Tierra (7.913,1 millas)
                    break;
                case 'nmi':
                    $distance =  $degrees * 59.97662; // 1 grado = 59.97662 millas naúticas, basándose en el diametro promedio de la Tierra (6,876.3 millas naúticas)
            }
            return round($distance, $decimals);
        }

        public function isDriverOcuped($id)
        {
            $route = Route::where("driver_id", $id)
                ->where("status", "1")
                ->first();
            return $route!=null;
        }

        public function getNewDriver(Request $request)
        {
            $blacklist = new Blacklist;
            $blacklist->driver_id = $request->driver_id;
            $blacklist->route_id = $request->route_id;
            $blacklist->save();

            return response()->json([
                'result' => true,
                'cantidad' => Blacklist::where('route_id', $request->route_id)->count(),
                'driver' => $this->getDriverCercanoBlackList($request->latitud, $request->longitud, $request->vehicle_type, $request->route_id)
            ]);
        }

        public function dateDriver(Request $request) {
            $route = Route::find($request->id);
                $route->date_driver = Carbon::now()->format('Y-m-d H:i:s');
            $route->save();
            return response()->json([
                'result' => true,
                'date' => $route->date_driver
            ]);
        }

        public function savePhoto(Request $request) {
            $name = date('YmdHis').rand(0,1000).'.jpg';
            $img = public_path('routes/'.$name);
            file_put_contents($img, file_get_contents($request->url));
            $ruta = Route::find($request->id);
                $ruta->route_photo = $name;
            $ruta->save();
            return response()->json([
                'result' => true
            ]);
        }

        public function getMontos(Request $request) {
            $ruta = Route::with(['tariff'])->where('id',$request->id)->first();
            if ($ruta->paid == '1') {
                return response()->json([
                    'result' => false,
                    'error' => 'El pago de la ruta ya se ha realizado. No es posible finalizar antes de tiempo'
                ]);
            }
            return response()->json([
                'result' => true,
                'tarifa' => $ruta->tariff
            ]);
        }

        public function recalcular(Request $request) {

            $costo = intval((intval($request->cost) / 50)) * 50;

            $route = Route::find($request->id);
            $payment = Payment::where('route_id',$request->id)->whereIn('method',[1,2])->first();
            $descuento = Payment::where('route_id',$request->id)->where('method',3)->first();

            $_parada = Destiny::where('route_id',$request->id)->where('type','1')->first();

            $_airport = 0;
            $_cant = 0;

            if ($this->isAirport($route->latitud_origin,$route->longitud_origin)) {
                $_airport += Tariff::find(4)->base_tariff;
                $_cant++;
            }

            if ($this->isAirport($request->latitud,$request->longitud)) {
                $_airport += Tariff::find(4)->base_tariff;
                $_cant++;
            }
            
            if ($_parada != null && $this->isAirport($_parada->latitud,$_parada->longitud)) {
                $_airport += Tariff::find(4)->base_tariff;
                $_cant++;
            }

            $costo = $costo + $_airport;

            if ($payment->stop) {
                $costo = $costo + $payment->stop;
            }
            
            $route->status = '2';
            $route->distance = round($request->km,2);
            $route->cost = $costo;
            $route->airport = $_cant;
            $route->save();

            $destino = Destiny::where('route_id',$request->id)->where('type','2')->first();
            $destino->latitud = $request->latitud;
            $destino->longitud = $request->longitud;
            $destino->save();

            $promocion = null;
            if ($descuento!=null) {
                $promocion = Promotion::where("id",$descuento->promotion_id)->first();
            }

//            if ($descuento != null) {
//                $costo = $costo - $descuento->total;
//            }
//
//            if ($costo < 0) {
//                $costo = 0;
//            }

            //<editor-fold desc="Description">

            //<editor-fold desc="PAGAR CON CODIGO">
            $total_code = 0;
            if ($promocion!=null) {
                $total_code = ($promocion->amount > $costo)
                    ? $costo
                    : $promocion->amount;

//                $promotion = PromotionUser::where("promotion_id", $promocion->code)
//                    ->where("user_id", $route->user_id)
//                    ->first();
//
//                if ($promotion!=null) {
//                    $promotion->paid = "1";
//                    $promotion->save();
//                }

            }
            //</editor-fold>

            //<editor-fold desc="COMICION POR CODIGO">

            $commission = Commission::first();

            $commission_send = $commission->amount/100;

            $p_code = $total_code*$commission_send;
            if ($p_code%50!=0) {
                $p_code = floor($p_code/50)*50;
            }
            //</editor-fold>

            //<editor-fold desc="COMICION TOTAL DE LA RUTA">
            $p_route = $costo*$commission_send;
            if ($p_route%50!=0) {
                $p_route = floor($p_route/50)*50;
            }
            //</editor-fold>

            $p_user = $p_route-$p_code;

            $pago_usuario = ($total_code > $costo)
                ? 0
                : $costo-$total_code;

            $payment->total = $pago_usuario;
            $payment->commission = $p_user;
            $payment->save();

            if ($descuento!=null) {
                $descuento->total = $total_code;
                $descuento->commission = $p_code;
                $descuento->save();
            }
            //</editor-fold>

//            $payment->total = $costo;
//            $payment->commission = intval((intval(($costo * 0.15)) / 50)) * 50;
//            $payment->save();

            $route->metodo = $route->payments->filter(function($q) { return $q->method == 1 || $q->method == 2; })->first()->method;
            $route->has_parada = $route->to_destinies->filter(function($q) {
                return $q->type == 1;
            })->count() > 0;
            if ($route->has_parada) {
                $route->parada_descripcion = $route->to_destinies->filter(function($q) {
                    return $q->type == 1;
                })->first()->description; 
            }            
            $route->destino_descripcion = $route->to_destinies->filter(function($q) {
                return $q->type == 2;
            })->first()->description;

            try {
                \Mail::send('emails.viaje-finalizado', ['route' => $route], function ($m) use ($route) {
                    $m->to($route->to_user->email,$route->to_user->person->name.' '.$route->to_user->person->lastname)
                      ->subject('Viaje Finalizado | Bogui Conductor');
                }); 
            }   
            catch(\Exception $e) {

            }

            return response()->json([
                'result' => true,
                'descuento'=>$descuento
            ]);

        }

        public function isAirport($lat, $lng)
        {
            $airport = Airport::all()->first();
            if ($airport != null) {
                return $this->getDistance($lat, $lng, $airport->lat, $airport->lng)<2;
            }
            return false;
        }
    }
