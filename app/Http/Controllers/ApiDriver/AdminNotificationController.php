<?php

	namespace App\Http\Controllers\ApiDriver;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\AdminNotification;
	use App\Models\AdminNotificationUser;
	use Carbon\Carbon;

	class AdminNotificationController extends Controller {
	    
	    public function get(Request $request) {
	    	$notificaciones = AdminNotification::whereDate('created_at','>=',Carbon::now()->subDays(3))
	    						->whereDate('created_at','<=',Carbon::now())
						    	->whereHas('users',function($q) use ($request) {
					                $q->where('users.id',$request->id);
					            })->orderBy('id','desc')->get();

            foreach($notificaciones as $notification) {
            	$query = AdminNotificationUser::where('notification_id',$notification->id)->where('user_id',$request->id)->first();
            		$query->viewed = '1';
            	$query->save();
            }

            return response()->json([
            	'result' => true,
            	'notificaciones' => $notificaciones
            ]);
	    }

	    public function check(Request $request) {
	    	$notificaciones = AdminNotification::whereDate('created_at','>=',Carbon::now()->subDays(3))
                                                ->whereDate('created_at','<=',Carbon::now())
                                                ->whereHas('admin_notifications_users',function($q) use ($request) {
                                                    $q->where('user_id',$request->id)->where('viewed','0');
                                                })->count();

            return response()->json([
            	'result' => true,
            	'notificaciones' => $notificaciones
            ]);
	    }

	}
