<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;


class CountryController extends Controller {

    public function getPhoneCode() {

        $countries = Country::select("name", "phonecode")->get();

        return response()->json($countries);
    }

//    public function getPhoneCode() {
//        return response()->json([
//            'result' => true,
//            'paises' => Country::orderBy('name','asc')->get()
//        ]);
//    }

}