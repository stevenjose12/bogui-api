<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\Terms;
	use App\Models\Privacy;
	use App\Models\Country;
	use App\Models\About;
	use App\Models\Benefit;
	use App\Models\EmploymentConditions;
	use App\Models\DriverBenefits;
	use App\Models\PassengerBenefits;
	use App\Models\Tariff;

	class HomeController extends Controller {
	    
	    public function get() {
	    	$tarifas = Tariff::all();
	    	$paises = Country::select('*',\DB::raw('CONCAT("(+",phonecode,") ",name) AS fullname'))->orderBy('name','asc')->get()->pluck('fullname','phonecode');
	    	return View('page.home')->with([
	    		'paises' => $paises,
	    		'tarifas' => $tarifas,
	    		'distance_min' => floatval($tarifas[0]->value_km_less),
	    		'distance_max' => floatval($tarifas[0]->value_km_greater)
	    	]);
	    }

	    public function nosotros() {
	    	$nosotros = About::orderBy('id','desc')->first();
	    	$beneficios = Benefit::all();
	    	return View('page.nosotros')->with([
	    		'nosotros' => $nosotros,
	    		'beneficios' => $beneficios
	    	]);
	    }

	    public function informacion() {
	    	$terminos = Terms::orderBy('id','desc')->first();
	    	$privacidad = Privacy::orderBy('id','desc')->first();
	    	return View('page.informacion')->with([
	    		'terminos' => $terminos,
	    		'privacidad' => $privacidad
	    	]);
	    }

	    public function solicitar() {
	    	return View('page.solicitar');
	    }

	    public function beneficiosPasajero() {
	    	$beneficios = PassengerBenefits::all();
	    	return View('page.beneficios-pasajero')->with([
	    		'beneficios' => $beneficios
	    	]);
	    }

	    public function beneficiosConductor() {
	    	$beneficios = DriverBenefits::all();
	    	return View('page.beneficios-conductor')->with([
	    		'beneficios' => $beneficios
	    	]);
	    }

	    public function consigueEmpleo() {
	    	$condiciones = EmploymentConditions::all();
	    	return View('page.consigue-empleo')->with([
	    		'condiciones' => $condiciones
	    	]);
	    }
	}
