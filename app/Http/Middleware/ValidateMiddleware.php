<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ValidateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->validate == '0') {
            return Redirect('codigo');
        }
        return $next($request);
    }
}
