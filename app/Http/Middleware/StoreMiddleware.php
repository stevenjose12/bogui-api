<?php

namespace App\Http\Middleware;

use Closure;
use View;
use App\Models\Store;
use App\Models\Contact;

class StoreMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $conductor = Store::where('type','2')->orderBy('id','desc')->first();
        $pasajero = Store::where('type','1')->orderBy('id','desc')->first();
        $contacto = Contact::orderBy('id','desc')->first();

        View::share('_conductor',$conductor);
        View::share('_pasajero',$pasajero);
        View::share('_contact',$contacto);
        return $next($request);
    }
}
