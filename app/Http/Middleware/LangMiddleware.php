<?php

    namespace App\Http\Middleware;

    use Closure;
    use App;
    use Carbon\Carbon;

    class LangMiddleware {

        public function handle($request, Closure $next) {
            $lang = "es";
            // if ($request->cookie('BoguiLang') != null) {
            //     $lang = $request->cookie('BoguiLang');
            // }
            App::setLocale($lang);
            Carbon::setLocale($lang);
            return $next($request);
        }
    }
