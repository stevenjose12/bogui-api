<?php

namespace App\Http\Middleware;

use Closure;
use View;

class PublicMiddleware {

    public function handle($request, Closure $next) {
        View::share('_prefix','');
        return $next($request);
    }
}
