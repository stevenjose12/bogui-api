<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class NoValidateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->validate != '0') {
            return Redirect('/');
        }
        return $next($request);
    }
}
