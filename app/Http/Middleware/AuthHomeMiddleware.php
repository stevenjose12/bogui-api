<?php

namespace App\Http\Middleware;

use Closure;

class AuthHomeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && (\Auth::user()->level == '3' || \Auth::user()->level == '2')) {
            return $next($request);
        }
        else if (\Auth::check() && (\Auth::user()->level == '1' || \Auth::user()->level == '4')) {
            return redirect("admin");
        }
        else {
            return redirect("/");
        }
        
    }
}
