<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection as Collection;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required",
            "password" => "required"
        ];
    }

    public function messages()
    {
        return [
            "required" => "El campo :attribute no puede estar vacío"
        ];
    }

    public function attributes()
    {
        return [
            "email" => "usuario",
            "password" => "contraseña" 
        ];
    }

    public function response(array $errors){
        if ($this->expectsJson()) {
            return new JsonResponse(Collection::make($errors)->first()[0], 422);
        }
    }
}
