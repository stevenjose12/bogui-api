<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'apellido'=>'required',
            'document'=>'required',
            'documentPhoto'=>"",
            'email' => 'required|email',
            'nombre'=>'required',
            'password'=>'required|confirmed',
            'telefono'=>'nullable|numeric|digits_between:7,11',
            'photo'=>'required'
        ];
    }

    public function messages() {
        return [
            'required'=>'Este campo es requerido',
            'numeric'=>'Este campo debe ser numérico',
            'confirmed'=>'Las contraseñas deben coincidir',
            'email'=>'Debe ingresar un email válido',
            'unique'=>'Este correo ya esta siendo usado',
            'digits_between'=>'Este campo debe tener entre 7 a 11 digitos'
        ];
    }

}