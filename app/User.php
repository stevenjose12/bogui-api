<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Person;
use App\Models\Rating;
use App\Models\Route;
use App\Models\Payment;
use App\Models\WeeklyDebt;
use App\Models\PendingDocument;
use Hash;
use DB;
use Carbon\Carbon;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'level', 'status', 'validate', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['average', 'averageBogui', 'debt', 'weekly_debt', 'carried', 'last_week_carried'];

     public function setPasswordAttribute($pass)
    {
        return $this->attributes['password'] = Hash::make($pass);
    }

    public function person()
    {
        return $this->hasOne('App\Models\Person', 'user_id')->withTrashed();
    }

    public function permission_user()
    {
        return $this->belongsToMany(Permission::class, 'permission_user', 'user_id', 'permission_id');
    }

    public function vehicles () {
        return $this->hasMany('App\Models\Vehicle', 'user_id', 'id');
    }

    public function main_vehicle () {
        return $this->hasOne('App\Models\Vehicle', 'user_id', 'id')->where("status", 1);
    }
    
    public function vehicle() {
        return $this->hasOne('App\Models\Vehicle','user_id')->where('status',1);
    }

    public function vehicles_web() {
        return $this->hasMany('App\Models\Vehicle','user_id', 'id');
    }

    public function getAverageAttribute($value) {
        return $this->average();
    }

    public function getAverageBoguiAttribute($value) {
        return $this->averageBogui();
    }

    public function getDebtAttribute($value) {
        return $this->debt();
    }

    public function getWeeklyDebtAttribute($value) {
        return $this->weekly_debt();
    }

    public function getCarriedAttribute($value) {
        return $this->carried();
    }

    public function getLastWeekCarriedAttribute($value) {
        return $this->last_week_carried();
    }

    public function routes() {
        return $this->hasMany('App\Models\Route', 'driver_id');
    }

    public function has_been_passenger() {
        return $this->hasMany('App\Models\Route', 'user_id');
    }

    public function average() {
        return number_format(Rating::where('reviewed_id',$this->id)->count() == 0 ? 0 : Rating::where('reviewed_id',$this->id)->sum('points') / Rating::where('reviewed_id',$this->id)->count(),2);
    }

    public function averageBogui() {
        return number_format(Rating::where('reviewer_id',$this->id)->where('bogui', '1')->count() == 0 ? 0 : Rating::where('reviewer_id',$this->id)->where('bogui', '1')->sum('points') / Rating::where('reviewer_id',$this->id)->where('bogui', '1')->count(),2);
    }

    public function payments() {
        return $this->hasMany('App\Models\Payment', 'user_id');
    }

    public function bank_user() {
        return $this->hasOne('App\Models\BankUser','user_id')->withTrashed();
    }

    public function weekly_debt() {
        $from = Carbon::now()->subDays(7)->startOfWeek()->startOfDay();
        $to = Carbon::now()->subDays(7)->endOfWeek()->endOfDay();
        return WeeklyDebt::where('driver_id', $this->id)->whereBetween('created_at', [$from,$to])->sum('debt_weekly');
    }

    public function debt() {
        return WeeklyDebt::where('driver_id', $this->id)->sum('debt_total');
    }

    public function carried() {
        $from = Carbon::now()->subDays(7)->startOfWeek()->startOfDay();
        $to = Carbon::now()->subDays(7)->endOfWeek()->endOfDay();
        return WeeklyDebt::where('driver_id', $this->id)->whereBetween('created_at', [$from,$to])->sum('debt_total');
    }

    public function hasAuthorizedCreditCard()
    {
        return $this->webpay;
    }

    public function pending_documents()
    {
        return $this->hasMany('App\Models\PendingDocument', 'user_id')->whereIn('status', ['0','2'])->orderBy('type', 'asc');
    }

    public function notifications() {
        return $this->belongsToMany('App\Models\AdminNotification', 'admin_notifications_users', 'user_id', 'notification_id');
    }

    public function last_week_carried() {
        $from = Carbon::now()->subDays(14)->startOfWeek()->startOfDay();
        $to = Carbon::now()->subDays(14)->endOfWeek()->endOfDay();
        return WeeklyDebt::where('driver_id', $this->id)->whereBetween('created_at', [$from,$to])->sum('debt_total');
    }
    
}