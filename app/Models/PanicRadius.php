<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class PanicRadius extends Model {
	    protected $table = "panic_radius";

	    use SoftDeletes;
	}
