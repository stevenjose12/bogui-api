<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class PanicDetail extends Model {
        protected $table = "panic_details";

        use SoftDeletes;

        public function panic() {
            return $this->belongsTo('App\Models\Panic','panic_id');
        }

        public function user() {
            return $this->belongsTo('App\User','user_id');
        }

        public function response() {
            return $this->belongsTo('App\Models\PanicOption','response_id');
        }
        
    }
