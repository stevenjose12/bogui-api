<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TariffType extends Model
{
    protected $table = "tariff_type";
}
