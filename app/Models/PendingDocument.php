<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendingDocument extends Model {
	
	protected $table="pending_documents";

	public function user() {
		return $this->belongsTo('App\User','user_id');
	}

	public function vehicle() {
		return $this->belongsTo('App\Models\Vehicle','vehicle_id')->withTrashed();
	}
}
