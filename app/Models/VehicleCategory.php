<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleCategory extends Model
{
    protected $table="vehicle_category";

    public function vehicle() {
    	return $this->belongsTo('App\Models\Vehicle','vehicle_id');
    }

    public function vehicle_type() {
    	return $this->belongsTo('App\Models\VehicleType','vehicle_type_id');
    }
}
