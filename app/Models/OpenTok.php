<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class OpenTok extends Model {
	    protected $table="opentok";

	    public function route() {
	    	return $this->belongsTo('App\Models\OpenTok');
	    }
	}
