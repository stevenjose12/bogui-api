<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Vehicle extends Model {

		use SoftDeletes;

	    protected $table="vehicles";

	    public function photos() {
	    	return $this->hasMany('App\Models\VehiclePhoto','vehicle_id');
	    }

	    public function vehicle_type()
	    {
	    	return $this->belongsTo('App\Models\VehicleType', 'vehicle_type_id');
	    }

	    public function user()
	    {
	    	return $this->belongsTo('App\User', 'user_id')->withTrashed();
	    }

	    public function vehicle_categories() {
	    	return $this->hasMany('App\Models\VehicleCategory','vehicle_id')->where('vehicle_type_id','!=',0);
	    }
	}
