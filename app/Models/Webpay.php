<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\Builder;
	use Carbon\Carbon;

	class Webpay extends Model {
	    protected $table="webpay";

	    public function user() {
	    	return $this->belongsTo('App\User','user_id');
	    }

	    public function route() {
	    	return $this->belongsTo('App\Models\Route','route_id');
	    }

	    public function scopeToday(Builder $query) {
	        return $query->whereDate('created_at', '=', Carbon::today(config('app.timezone'))->toDateString());
	    }

	    public function getTodayTransactionsCount() {
	        $query = $this->newQuery();
	        return $query->today()->count();
	    }

	    public function createNewBuyOrder() {
	        $count = $this->getTodayTransactionsCount() + 1;
	        return $this->buyOrder = Carbon::now(config('app.timezone'))->format('YmdHis') . str_pad($count, 3, '0', STR_PAD_LEFT);
	    }
	}
