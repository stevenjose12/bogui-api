<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destiny extends Model
{
    protected $table = 'destinies';

    public function toRoute()
    {
        return $this->belongsTo('App\Model\Route','route_id')->withTrashed();
    }

    protected $fillable = [
        'route_id', 'latitud', 'longitud', 'type', 'type_special'
    ];
}
