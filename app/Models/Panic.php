<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Panic extends Model {

	    protected $table = "panic";

	    use SoftDeletes;

	    public function user() {
	    	return $this->belongsTo('App\User','user_id');
	    }

	    public function details() {
	    	return $this->hasMany('App\Models\PanicDetail','panic_id');
	    }

	}