<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryNotification extends Model
{
    protected $table="category_notification";
}
