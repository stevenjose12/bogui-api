<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupportMotive extends Model
{
	use SoftDeletes;
    protected $table = 'support_motive';
}
