<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model {

	use SoftDeletes;
    protected $table = "persons";


    protected $fillable = [
        'name', 'lastname', 'document_type', 'document', 'photo', 'permit', 'criminal_records', 'user_id'
    ];
    
    public function person() {
    	return $this->belongsTo('App\User', 'user_id');
    }

}