<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class Promotion extends Model
{
	use SoftDeletes;
    protected $table="promotions";
    

    public function promotion_users () 
    {
        return $this->hasMany(PromotionUser::class, 'promotion_id', 'id', 'promotion_user');
    }
}

