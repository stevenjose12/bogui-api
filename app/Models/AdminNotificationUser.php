<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class AdminNotificationUser extends Model {
	    protected $table="admin_notifications_users";

	    use SoftDeletes;

	    public function user() {
	    	return $this->belongsTo('App\User','user_id');
	    }

	    public function notification() {
	    	return $this->belongsTo('App\Models\AdminNotification','notification_id');
	    }
	}
