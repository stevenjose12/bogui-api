<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleType extends Model
{
	use SoftDeletes;
    protected $table="vehicle_type";

    public function vehicles()
    {
    	return $this->hasMany('App\Models\Vehicle', 'vehicle_type_id', 'id');
    }
}
