<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Information extends Model
{
    use SoftDeletes;

    protected $table = "information";

    protected $fillable = [
        'title', 'description', 'logo', 'type', 'status', 'start_date', 'end_date'
    ];
}
