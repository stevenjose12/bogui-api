<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'payments';

    public function to_route()
    {
        return $this->belongsTo(Route::class, 'route_id')->withTrashed();
    }

    protected $fillable = [
        'method', 'total', 'commission', 'route_id', 'user_id'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id')->withTrashed();
    }

    public function paid_by()
    {
        return $this->belongsTo('App\User','paid_by','id')->withTrashed();
    }

    public function route() 
    {
        return $this->belongsTo('App\Models\route','route_id','id')->withTrashed();
    }
}