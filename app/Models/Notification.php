<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table="notifications";
    protected $guarded = [];

    public function route() {
    	return $this->belongsTo('App\Models\Route','route_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    public function driver()
    {
    	return $this->belongsTo('App\User','driver_id');
    }

}