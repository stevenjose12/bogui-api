<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class Rating extends Model {
	    protected $table="ratings";

	    public function reviewed() {
	    	return $this->belongsTo('App\User','reviewed_id')->withTrashed();
	    }

	    public function reviewer() {
	    	return $this->belongsTo('App\User','reviewer_id')->withTrashed();
	    }
	}
