<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'routes';
    protected $appends = ['status_pass', 'status_type', 'payment_type'];

    public function to_destinies()
    {
        return $this->hasMany('App\Models\Destiny', 'route_id');
    }

    public function to_user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }

    public function driver()
    {
        return $this->belongsTo('App\User', 'driver_id', 'id')->withTrashed();
    }

    public function tariff()
    {
        return $this->belongsTo('App\Models\Tariff', 'tariff_id', 'id')->withTrashed();
    }

    public function vehicle_type()
    {
        return $this->belongsTo('App\Models\VehicleType', 'vehicle_type_id', 'id');
    }

    public function payments() {
        return $this->hasMany('App\Models\Payment','route_id');
    }

    public function to_payments()
    {
        return $this->hasMany('App\Models\Payment', 'route_id');
    }

    public function getPaymentTypeAttribute()
    {
        $payment_type = 'Efectivo';

        if ($this->payment == '1') {
            $payment_type = 'Tarjeta de Credito';
        } 

        return $payment_type;
    }

    public function getStatusPassAttribute()
    {
        $status_pass = 'Sin Abordar';

        if($this->status_route == '1'){
            $this->status_pass = 'Abordo';
        }

        return $status_pass;
    }

    public function getStatusTypeAttribute()
    {
        $status_type = '';

        if ($this->status == '0') {
            $status_type = 'Suspendida';
        } else if ($this->status == '1') {
            $status_type = 'Activa';
        } else if ($this->status == '2') {
            $status_type = 'Culminada';
        } else if ($this->status == '3') {
            $status_type = 'Cancelada';
        }

        return $status_type;
    }
}
