<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class PanicOption extends Model {
	    protected $table = 'panic_options';

	    use SoftDeletes;
	}
