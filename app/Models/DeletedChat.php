<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class DeletedChat extends Model {
	    protected $table="deleted_chats";

	    public function user() {
	    	return $this->belongsTo('App\User','user_id');
	    }

	    public function chat() {
	    	return $this->belongsTo('App\Models\Chat','chat_id');
	    }
	}
