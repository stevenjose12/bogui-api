<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class BankUser extends Model {
		use SoftDeletes;
	    protected $table="banks_user";

	    public function bank() {
	    	return $this->belongsTo('App\Models\Bank','bank_id');
	    }

	    public function account() {
	    	return $this->belongsTo('App\Models\AccountType','type','id');
	    }
	}
