<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeeklyDebt extends Model
{
    protected $table="weekly_debt";

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }
}
