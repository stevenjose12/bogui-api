<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class NightMode extends Model {
	    protected $table="night_mode";
	}
