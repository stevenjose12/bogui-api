<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class VehiclePhoto extends Model {

		use SoftDeletes;

	    protected $table="vehicles_photos";

	    public function vehicle() {
	    	return $this->belongsTo('App\Models\Vehicle','vehicle_id');
	    }
	}
