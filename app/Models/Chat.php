<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model {

	use SoftDeletes;
    protected $table = "chats";

    public function chat_user() {
        return $this->hasMany('App\Models\ChatUser', 'chat_id', 'id');
    }

    public function message() {
        return $this->hasMany('App\Models\Message', 'chat_id', 'id');
    }

    public function support_type() {
    	return $this->belongsTo('App\Models\SupportMotive', 'motive_id', 'id');
    }

    public function route() {
        return $this->belongsTo('App\Models\Route','route_id');
    }

}