<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionUser extends Model {

    protected $table = "promotion_user";
    protected $fillable = ['user_id', 'promotion_id', 'paid', 'accepted'];

    public function promotion() {
        return $this->hasOne('App\Models\Promotion', "id", "promotion_id");
    }

}