<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tariff extends Model
{
    use SoftDeletes;
    protected $table = "tariffs";

    public function getVehicleTypeIdAttribute($vehicle_type_id)
    {
        if ($this->original_vehicle_type_id) {
            return $vehicle_type_id;
        }
        else {
            $vehicle_type_ids = array(0 => 'Otros' ,1 => 'Sedán', 2 => 'SUV', 3 => 'Van', 4 => 'Moto');
            return $vehicle_type_ids[$vehicle_type_id]; 
        }
    }

    public function registered()
    {
        return $this->hasOne('App\User', 'id', 'registered_by')->withTrashed();
    }
}
