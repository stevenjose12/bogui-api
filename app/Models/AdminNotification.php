<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class AdminNotification extends Model {
	    protected $table="admin_notifications";

	    use SoftDeletes;

	    public function admin_notifications_users() {
	    	return $this->hasMany('App\Models\AdminNotificationUser','notification_id');
	    }

	    public function users() {
	        return $this->belongsToMany('App\User', 'admin_notifications_users', 'notification_id', 'user_id');
	    }
	}
