<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatUser extends Model {

    protected $table = "chat_user";

    public function users(){
    	return $this->hasMany('App\User', 'id', 'user_id')->withTrashed();
    }

    public function user() {
    	return $this->belongsTo('App\User','user_id')->withTrashed();
    }

}