<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table = "countries";
    //protected $dates = false;

    public $fillable = ["id", "sortname", "name", "phonecode"];

}