const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const Models = require('./Models/Main')

io.on('connection',socket => {
	socket.on('nueva-carrera',carrera => {
		io.emit('nueva-carrera',carrera)
	})
})

app.get('/',(req,res) => {
	res.status(403).send("403 Acceso Denegado")
})

server.listen(11018)