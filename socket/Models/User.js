const sequelize = require('../conexion')
const Seq = require('sequelize')

const User = sequelize.define('users',{
	id: {
		type: Seq.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	level: Seq.INTEGER,
	email: Seq.STRING,
	password: Seq.STRING,
	latitud: Seq.STRING,
	longitud: Seq.STRING,
	status: Seq.INTEGER,
	remember_token: Seq.STRING,
	// validate: Seq.TINYINT
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	deletedAt: 'deleted_at'
})

module.exports = User